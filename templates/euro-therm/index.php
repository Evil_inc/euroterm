﻿<?php
  defined('_JEXEC') or die;
  if ($_GET['Itemid']!=101) {
    $suff = "_qq";
  }

error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/script.js" type="text/javascript"></script>
    <jdoc:include type="head" />
  <meta name="description" content="">
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/style.css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!--[if IE 8]><link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie8.css" /><![endif]-->
  <!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7.css" /><![endif]-->
  <!--[if IE 6]><link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie6.css" /><![endif]-->
  <!--[if IE 6]>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/DD_belatedPNG.js"></script>
    <script>
      DD_belatedPNG.fix('#wrapper, #logo img, #top-phoneNumber, #top-search, #top-nav, #promo-images, #catalog, #slider, #diler-net a, .nspPrev, .nspNext, #footer-nav ul ul li a'); 
    </script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript">
        $(function() {
          if ($.browser.msie && $.browser.version.substr(0,1)<7)
          {
                $('li').has('ul').mouseover(function(){
                        $(this).children('ul').show();
                        }).mouseout(function(){
                        $(this).children('ul').hide();
                        })
      }
    });
    </script>
  <![endif]-->
  <script>
    startList = function() {
    if (document.all&&document.getElementById) {
            navRoot = document.getElementById("catalog ul");
            for (i=0; i<navRoot.childNodes.length; i++) {
                  node = navRoot.childNodes[i];
                  if (node.nodeName=="LI") {
                        node.onmouseover=function() {
                              this.className+=" over";
                        }
                        node.onmouseout=function() {
                              this.className=this.className.replace »
                              (" over", "");
                        }
                  }
        }  
      }
    }
    window.onload=startList;
  </script>
  <?php
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (stripos($user_agent, 'MSIE 6.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 7.0') === false) {
       if (!isset($HTTP_COOKIE_VARS["ie"])) {setcookie("ie", "yes", time()+60*60*24*360);header ("Location:ie6.html");}
    }
  ?>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6557468-68']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="wrapper">
  <div id="header">
  <div id="logo">
    <a href="<?php echo $this->baseurl ?>">
      <jdoc:include type="modules" name="logo" style="none"/>
    </a>
  </div>
  <div id="top-phoneNumber">
    <jdoc:include type="modules" name="top-phoneNumer" style="none"/>
  </div>
  <div id="top-search">
    <jdoc:include type="modules" name="top-search" style="none"/>
  </div>
  <jdoc:include type="modules" name="top-right-icons" style="none"/>
  <div id="top-nav">
    <jdoc:include type="modules" name="top-nav" style="none"/>
  </div>
  <?php if ($this->countModules('promo-images')): ?>
    <div id="promo-images">
      <jdoc:include type="modules" name="promo-images" style="none"/>
    </div>                                                     
  <?php endif; ?>             
<!-- ******************************** END HEADER ******************************************** -->
  <div id="container">

    <?php if ($_GET['Itemid']==101) : ?>
        <div id="catalog">
          <span class="catalog-title">Каталог</span>
          <jdoc:include type="modules" name="catalog" style="none"/>
        </div>
        <?php if ($this->countModules('mainPage-slider')): ?>
          <div id="slider">
            <span class="slider-title">Стальные радиаторы</span>
            <jdoc:include type="modules" name="mainPage-slider" style="none"/>
          </div>                                                     
        <?php endif; ?>
        <div id="diler-action-news-col<?php echo $suff; ?>">
          <jdoc:include type="modules" name="diller-net" style="none"/>
          <jdoc:include type="modules" name="special-offers" style="none"/>
          <jdoc:include type="modules" name="actions" style="none"/>
          <div id="news">
            <p class="news-title">Новости</p>
            <jdoc:include type="modules" name="news" style="xhtml"/>
            <a href="novosti" class="all-news">Все новости</a>
          </div>
        </div>
    <?php endif; ?>
    <?php if ($_GET['Itemid']!=101) : ?>
      <div id='leftcol'>
        <div id="catalog">
          <span class="catalog-title">Каталог</span>
          <jdoc:include type="modules" name="catalog" style="none"/>
        </div>

        <div id="diler-action-news-col<?php echo $suff; ?>">
          <jdoc:include type="modules" name="diller-net" style="none"/>
          <jdoc:include type="modules" name="special-offers" style="none"/>
          <jdoc:include type="modules" name="actions" style="none"/>
          <div id="news">
            <p class="news-title">Новости</p>
            <jdoc:include type="modules" name="news" style="xhtml"/>
            <a href="novosti" class="all-news">Все новости</a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div id="content<?php echo $suff; ?>">
        <div id="breadcrumbs_top">
            <jdoc:include type="modules" name="breadcrambs" />
        </div>
      <?php if ($this->countModules('navigator')): ?>
<!--          <jdoc:include type="modules" name="navigator" style="none"/>-->
      <?php endif; ?>
      <jdoc:include type="message" />
      <jdoc:include type="component" />
      <?php if ($this->countModules('brands')): ?>
        <div id="brands<?php echo $suff; ?>">
          <p class="brands-title<?php echo $suff; ?>">Бренды</p>
          <jdoc:include type="modules" name="brands" style="none"/>
        </div>
      <?php endif; ?>
    </div>

<!--?php if ($this->countModules('')): ?>
  <div class="">
        <jdoc:include type="modules" name="*****" style="none"/>
  </div-->                                                      
<!--?php endif; ?-->

</div>    
<!-- ******************************************* FOOTER ********************************************************************** -->
        <div id="footer">
      <div id="footer-holder">
        <?php if ($this->countModules('footer-nav')): ?>
          <div id="footer-nav">
            <jdoc:include type="modules" name="footer-nav" style="none"/>
            <div class="clear"></div>
          </div>                                                      
        <?php endif; ?>      
      </div>
      <div id="footer-copyright-block">          
        <div id="footer-copyright-holder">
          <jdoc:include type="modules" name="footer-copyrights" style="none"/>    
          <div class="clear"></div>
        </div>
      </div>
    </div>
    
</div>
</div>
  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <!--script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

        <jdoc:include type="modules" name="debug" />

</body>
</html>
