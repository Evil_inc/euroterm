-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2014 at 10:38 AM
-- Server version: 5.5.35-0ubuntu0.12.04.2
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `euroterm`
--

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_assets`
--

CREATE TABLE IF NOT EXISTS `o1hap_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `o1hap_assets`
--

INSERT INTO `o1hap_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 1, 167, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 108, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 109, 110, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 111, 112, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 113, 114, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 115, 116, 1, 'com_login', 'com_login', '{}'),
(13, 1, 117, 118, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 119, 120, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 121, 122, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 123, 124, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 125, 126, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 127, 128, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 129, 132, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 133, 134, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 135, 136, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 137, 138, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 139, 140, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 141, 144, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(25, 1, 145, 148, 1, 'com_weblinks', 'com_weblinks', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(26, 1, 149, 150, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 53, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 130, 131, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(31, 25, 146, 147, 2, 'com_weblinks.category.6', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 142, 143, 1, 'com_users.notes.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 151, 152, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 153, 154, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(37, 27, 21, 22, 3, 'com_content.article.3', 'О компании', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(36, 27, 19, 20, 3, 'com_content.article.2', 'Товары', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(38, 27, 43, 44, 3, 'com_content.article.4', 'Бренды', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(39, 27, 23, 24, 3, 'com_content.article.5', 'Дистрибьюторам', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(40, 27, 25, 26, 3, 'com_content.article.6', 'Контакты', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 27, 27, 28, 3, 'com_content.article.7', 'Главная', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 8, 54, 73, 2, 'com_content.category.8', 'Каталог', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(43, 42, 55, 56, 3, 'com_content.article.8', 'Стальные радиаторы подпункт 1', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 42, 57, 58, 3, 'com_content.article.9', 'Стальные радиаторы подпункт 2', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 42, 59, 60, 3, 'com_content.article.10', 'Стальные радиаторы подпункт 3', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 42, 61, 62, 3, 'com_content.article.11', 'Алюмниевые радиаторы подпункт 1', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 42, 63, 64, 3, 'com_content.article.12', 'Алюминиевые радиаторы подпункт 2', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 42, 65, 66, 3, 'com_content.article.13', 'Алюминиевые радиаторы подпункт 3', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 42, 67, 68, 3, 'com_content.article.14', 'Биметаллические радиаторы подпункт 1', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 42, 69, 70, 3, 'com_content.article.15', 'Биметаллические радиаторы подпункт 2', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(51, 42, 71, 72, 3, 'com_content.article.16', 'Биметаллические радиаторы подпункт 3', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(52, 8, 74, 83, 2, 'com_content.category.9', 'Новости', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(53, 52, 75, 76, 3, 'com_content.article.17', 'Профессиональный партнер GRUNDFOS 1', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(54, 52, 77, 78, 3, 'com_content.article.18', 'Международные события', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(55, 8, 84, 103, 2, 'com_content.category.10', 'Бренды', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(69, 55, 97, 98, 3, 'com_content.article.25', 'Бренд SANNYHETER', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(68, 55, 95, 96, 3, 'com_content.article.24', 'Бренд SOLAR', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(67, 55, 93, 94, 3, 'com_content.article.23', 'Бренд NOVA FLORIDA', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(66, 55, 91, 92, 3, 'com_content.article.22', 'Бренд GLOBAL', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(65, 55, 89, 90, 3, 'com_content.article.21', 'Бренд FONDIAL', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(64, 55, 87, 88, 3, 'com_content.article.20', 'Бренд EUROTHERM', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(63, 55, 85, 86, 3, 'com_content.article.19', 'Бренд EUROTHERM(RED)', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(70, 52, 79, 80, 3, 'com_content.article.26', '«Аква-Терм Киев»  15 - 18 мая  Киев', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(71, 27, 29, 30, 3, 'com_content.article.27', 'Акции и скидки', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(72, 27, 31, 32, 3, 'com_content.article.28', 'Диллерская сеть', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(73, 1, 155, 156, 1, 'com_virtuemart', 'virtuemart', '{}'),
(74, 1, 157, 158, 1, 'com_virtuemart_allinone', 'virtuemart_allinone', '{}'),
(75, 1, 159, 160, 1, 'com_xmap', 'com_xmap', '{}'),
(76, 27, 33, 34, 3, 'com_content.article.29', 'Специальные предложения', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(77, 1, 161, 162, 1, 'com_chronoforms', 'chronoforms', '{}'),
(78, 27, 35, 36, 3, 'com_content.article.30', 'Алюминиевые радиаторы Италия', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(79, 27, 37, 38, 3, 'com_content.article.31', 'Биметаллические радиаторы Китай', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(80, 27, 39, 40, 3, 'com_content.article.32', 'Алюминиевые радиаторы Китай', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(81, 27, 41, 42, 3, 'com_content.article.33', 'Бренды', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(82, 55, 99, 100, 3, 'com_content.article.34', 'Бренд ALLTHERMO', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(83, 55, 101, 102, 3, 'com_content.article.35', 'Бренд Classic+', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(84, 8, 104, 107, 2, 'com_content.category.18', 'Слайдер на главной', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(85, 1, 163, 164, 1, 'com_djimageslider', 'com_djimageslider', '{}'),
(86, 27, 45, 46, 3, 'com_content.article.36', 'Карта проезда', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(87, 84, 105, 106, 3, 'com_content.article.37', 'Eurotherm', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(88, 52, 81, 82, 3, 'com_content.article.38', 'новоcть 3', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(89, 27, 47, 48, 3, 'com_content.article.39', 'Металлопластиковые трубы', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(90, 27, 49, 50, 3, 'com_content.article.40', 'Краны шаровые', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(91, 27, 51, 52, 3, 'com_content.article.41', 'Стальные радиаторы EUROTHERM', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(92, 1, 165, 166, 1, 'com_zoo', 'com_zoo', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_associations`
--

CREATE TABLE IF NOT EXISTS `o1hap_associations` (
  `id` varchar(50) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_banners`
--

CREATE TABLE IF NOT EXISTS `o1hap_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_banner_clients`
--

CREATE TABLE IF NOT EXISTS `o1hap_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `o1hap_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `o1hap_categories`
--

INSERT INTO `o1hap_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES
(1, 0, 0, 0, 21, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2009-10-18 16:07:09', 0, '0000-00-00 00:00:00', 0, '*'),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:26:37', 0, '0000-00-00 00:00:00', 0, '*'),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":"","foobar":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:27:35', 0, '0000-00-00 00:00:00', 0, '*'),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:27:57', 0, '0000-00-00 00:00:00', 0, '*'),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:15', 0, '0000-00-00 00:00:00', 0, '*'),
(6, 31, 1, 9, 10, 1, 'uncategorised', 'com_weblinks', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*'),
(7, 32, 1, 11, 12, 1, 'uncategorised', 'com_users.notes', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*'),
(8, 42, 1, 13, 14, 1, 'katalog', 'com_content', 'Каталог', 'katalog', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2012-04-16 18:53:08', 0, '0000-00-00 00:00:00', 0, '*'),
(9, 52, 1, 15, 16, 1, 'novosti', 'com_content', 'Новости', 'novosti', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2012-04-16 20:07:07', 0, '0000-00-00 00:00:00', 0, '*'),
(10, 55, 1, 17, 18, 1, 'produktsiya', 'com_content', 'Бренды', 'produktsiya', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2012-04-17 08:13:46', 42, '2012-04-23 12:46:12', 0, '*'),
(18, 84, 1, 19, 20, 1, 'euro', 'com_content', 'Слайдер на главной', 'euro', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2012-04-30 16:59:38', 42, '2012-05-03 19:34:56', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_chronoforms`
--

CREATE TABLE IF NOT EXISTS `o1hap_chronoforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `form_type` tinyint(1) NOT NULL,
  `content` longtext NOT NULL,
  `wizardcode` longtext,
  `events_actions_map` longtext,
  `params` longtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `app` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `o1hap_chronoforms`
--

INSERT INTO `o1hap_chronoforms` (`id`, `name`, `form_type`, `content`, `wizardcode`, `events_actions_map`, `params`, `published`, `app`) VALUES
(2, 'Form', 1, '<div class="ccms_form_element cfdiv_text" id="autoID-5f2de65447a4c33da3ad29dbf3ee91fe_container_div"><label>Ф.И.О</label><input maxlength="150" size="30" class=" validate[''required'',''nodigit'']" title="" type="text" value="" name="input_text_0" />\n<div class="clear"></div><div id="error-message-input_text_0"></div></div><div class="ccms_form_element cfdiv_text" id="autoID-bd44c7df7997592563c9befb4e54f9fb_container_div"><label>Ваш Телефон</label><input maxlength="150" size="30" class=" validate[''phone'']" title="" type="text" value="" name="input_text_1" />\n<div class="clear"></div><div id="error-message-input_text_1"></div></div><div class="ccms_form_element cfdiv_text" id="autoID-7f1ca20790cccd0d059aac12daa6a9d7_container_div"><label>Ваш E-Mail</label><input maxlength="150" size="30" class=" validate[''required'',''email'']" title="" type="text" value="" name="input_text_2" />\n<div class="clear"></div><div id="error-message-input_text_2"></div></div><div class="ccms_form_element cfdiv_textarea" id="autoID-375d0fd91eb5fe6cb2db1feeb253bb1c_container_div"><label>Сообщение</label><textarea cols="45" rows="12" class="" title="" name="input_textarea_3"></textarea>\n<div class="clear"></div><div id="error-message-input_textarea_3"></div></div><div class="ccms_form_element cfdiv_text" id="autoID-af2f62150f55e7acb7fa65eb86963e6e_container_div"><label>Введите код</label><input maxlength="5" size="5" class="chrono_captcha_input" title="" type="text" value="" name="chrono_verification" />\n{chronocaptcha_img}<div class="clear"></div><div id="error-message-chrono_verification"></div></div><div class="ccms_form_element cfdiv_submit" id="autoID-11e9dc9272aaf7651f9db6996c4dccfd_container_div"><input name="input_submit_5" class="" value="Отправить" type="submit" />\n<div class="clear"></div><div id="error-message-input_submit_5"></div></div><div class="ccms_form_element cfdiv_custom" id="input_id_6_container_div"><span class="valid-mes">Поля отмеченные звездочкой обязательны для заполнения</span><div class="clear"></div><div id="error-message-input_custom_6"></div></div>', 'array (\n  ''field_0'' => \n  array (\n    ''input_text_0_input_id'' => '''',\n    ''input_text_0_label_text'' => ''Ф.И.О'',\n    ''input_text_0_input_name'' => ''input_text_0'',\n    ''input_text_0_input_value'' => '''',\n    ''input_text_0_input_maxlength'' => ''150'',\n    ''input_text_0_input_size'' => ''30'',\n    ''input_text_0_input_class'' => '''',\n    ''input_text_0_input_title'' => '''',\n    ''input_text_0_label_over'' => ''0'',\n    ''input_text_0_hide_label'' => ''0'',\n    ''input_text_0_multiline_start'' => ''0'',\n    ''input_text_0_multiline_add'' => ''0'',\n    ''input_text_0_validations'' => ''required,nodigit'',\n    ''input_text_0_instructions'' => '''',\n    ''input_text_0_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''text'',\n  ),\n  ''field_1'' => \n  array (\n    ''input_text_1_input_id'' => '''',\n    ''input_text_1_label_text'' => ''Ваш Телефон'',\n    ''input_text_1_input_name'' => ''input_text_1'',\n    ''input_text_1_input_value'' => '''',\n    ''input_text_1_input_maxlength'' => ''150'',\n    ''input_text_1_input_size'' => ''30'',\n    ''input_text_1_input_class'' => '''',\n    ''input_text_1_input_title'' => '''',\n    ''input_text_1_label_over'' => ''0'',\n    ''input_text_1_hide_label'' => ''0'',\n    ''input_text_1_multiline_start'' => ''0'',\n    ''input_text_1_multiline_add'' => ''0'',\n    ''input_text_1_validations'' => ''phone'',\n    ''input_text_1_instructions'' => '''',\n    ''input_text_1_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''text'',\n  ),\n  ''field_2'' => \n  array (\n    ''input_text_2_input_id'' => '''',\n    ''input_text_2_label_text'' => ''Ваш E-Mail'',\n    ''input_text_2_input_name'' => ''input_text_2'',\n    ''input_text_2_input_value'' => '''',\n    ''input_text_2_input_maxlength'' => ''150'',\n    ''input_text_2_input_size'' => ''30'',\n    ''input_text_2_input_class'' => '''',\n    ''input_text_2_input_title'' => '''',\n    ''input_text_2_label_over'' => ''0'',\n    ''input_text_2_hide_label'' => ''0'',\n    ''input_text_2_multiline_start'' => ''0'',\n    ''input_text_2_multiline_add'' => ''0'',\n    ''input_text_2_validations'' => ''required,email'',\n    ''input_text_2_instructions'' => '''',\n    ''input_text_2_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''text'',\n  ),\n  ''field_3'' => \n  array (\n    ''input_textarea_3_input_id'' => '''',\n    ''input_textarea_3_label_text'' => ''Сообщение'',\n    ''input_textarea_3_input_name'' => ''input_textarea_3'',\n    ''input_textarea_3_input_value'' => '''',\n    ''input_textarea_3_input_class'' => '''',\n    ''input_textarea_3_input_title'' => '''',\n    ''input_textarea_3_label_over'' => ''0'',\n    ''input_textarea_3_hide_label'' => ''0'',\n    ''input_textarea_3_input_cols'' => ''45'',\n    ''input_textarea_3_input_rows'' => ''12'',\n    ''input_textarea_3_wysiwyg_editor'' => ''0'',\n    ''input_textarea_3_editor_buttons'' => ''1'',\n    ''input_textarea_3_editor_width'' => ''400'',\n    ''input_textarea_3_editor_height'' => ''200'',\n    ''input_textarea_3_multiline_start'' => ''0'',\n    ''input_textarea_3_multiline_add'' => ''0'',\n    ''input_textarea_3_validations'' => '''',\n    ''input_textarea_3_instructions'' => '''',\n    ''input_textarea_3_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''textarea'',\n  ),\n  ''field_4'' => \n  array (\n    ''input_captcha_4_input_id'' => '''',\n    ''input_captcha_4_label_text'' => ''Введите код'',\n    ''input_captcha_4_input_name'' => ''chrono_verification'',\n    ''input_captcha_4_input_value'' => '''',\n    ''input_captcha_4_input_maxlength'' => ''5'',\n    ''input_captcha_4_input_size'' => ''5'',\n    ''input_captcha_4_input_class'' => ''chrono_captcha_input'',\n    ''input_captcha_4_input_title'' => '''',\n    ''input_captcha_4_label_over'' => ''0'',\n    ''input_captcha_4_hide_label'' => ''0'',\n    ''input_captcha_4_validations'' => '''',\n    ''input_captcha_4_instructions'' => '''',\n    ''input_captcha_4_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''captcha'',\n    ''real_type'' => ''text'',\n    ''after'' => ''{chronocaptcha_img}'',\n  ),\n  ''field_5'' => \n  array (\n    ''input_submit_5_input_id'' => '''',\n    ''input_submit_5_input_name'' => ''input_submit_5'',\n    ''input_submit_5_input_value'' => ''Отправить'',\n    ''input_submit_5_input_class'' => '''',\n    ''input_submit_5_button_type'' => ''submit'',\n    ''input_submit_5_back_button'' => ''0'',\n    ''input_submit_5_reset_button'' => ''0'',\n    ''input_submit_5_back_button_value'' => ''Back'',\n    ''input_submit_5_reset_button_value'' => ''Reset'',\n    ''input_submit_5_multiline_start'' => ''0'',\n    ''input_submit_5_multiline_add'' => ''0'',\n    ''tag'' => ''input'',\n    ''type'' => ''submit'',\n  ),\n  ''field_6'' => \n  array (\n    ''input_custom_6_label_text'' => '''',\n    ''input_custom_6_input_name'' => ''input_custom_6'',\n    ''input_custom_6_input_id'' => ''input_id_6'',\n    ''input_custom_6_clean'' => ''0'',\n    ''input_custom_6_code'' => ''<span class="valid-mes">Поля отмеченные звездочкой обязательны для заполнения</span>'',\n    ''input_custom_6_instructions'' => '''',\n    ''input_custom_6_tooltip'' => '''',\n    ''tag'' => ''input'',\n    ''type'' => ''custom'',\n  ),\n)', 'YToxOntzOjY6ImV2ZW50cyI7YToyOntzOjQ6ImxvYWQiO2E6MTp7czo3OiJhY3Rpb25zIjthOjQ6e3M6MTg6ImNmYWN0aW9uX2xvYWRfanNfMCI7czowOiIiO3M6MTk6ImNmYWN0aW9uX2xvYWRfY3NzXzEiO3M6MDoiIjtzOjIzOiJjZmFjdGlvbl9sb2FkX2NhcHRjaGFfMiI7czowOiIiO3M6MjA6ImNmYWN0aW9uX3Nob3dfaHRtbF8zIjtzOjA6IiI7fX1zOjY6InN1Ym1pdCI7YToxOntzOjc6ImFjdGlvbnMiO2E6MTA6e3M6MjQ6ImNmYWN0aW9uX2NoZWNrX2NhcHRjaGFfNCI7YToxOntzOjY6ImV2ZW50cyI7YToyOntzOjM3OiJjZmFjdGlvbmV2ZW50X2NoZWNrX2NhcHRjaGFfNF9zdWNjZXNzIjtzOjA6IiI7czozNDoiY2ZhY3Rpb25ldmVudF9jaGVja19jYXB0Y2hhXzRfZmFpbCI7YToxOntzOjc6ImFjdGlvbnMiO2E6MTp7czoyMToiY2ZhY3Rpb25fZXZlbnRfbG9vcF81IjtzOjA6IiI7fX19fXM6MjM6ImNmYWN0aW9uX3VwbG9hZF9maWxlc182IjthOjE6e3M6NjoiZXZlbnRzIjthOjI6e3M6MzY6ImNmYWN0aW9uZXZlbnRfdXBsb2FkX2ZpbGVzXzZfc3VjY2VzcyI7czowOiIiO3M6MzM6ImNmYWN0aW9uZXZlbnRfdXBsb2FkX2ZpbGVzXzZfZmFpbCI7YToxOntzOjc6ImFjdGlvbnMiO2E6MTp7czoyMToiY2ZhY3Rpb25fZXZlbnRfbG9vcF83IjtzOjA6IiI7fX19fXM6MjI6ImNmYWN0aW9uX2N1c3RvbV9jb2RlXzgiO3M6MDoiIjtzOjI1OiJjZmFjdGlvbl9oYW5kbGVfYXJyYXlzXzE2IjtzOjA6IiI7czoxODoiY2ZhY3Rpb25fZGJfc2F2ZV85IjtzOjA6IiI7czoxNzoiY2ZhY3Rpb25fZW1haWxfMTAiO3M6MDoiIjtzOjE3OiJjZmFjdGlvbl9lbWFpbF8xMSI7czowOiIiO3M6MTc6ImNmYWN0aW9uX2VtYWlsXzEyIjtzOjA6IiI7czoyMzoiY2ZhY3Rpb25fY3VzdG9tX2NvZGVfMTMiO3M6MDoiIjtzOjMxOiJjZmFjdGlvbl9zaG93X3RoYW5rc19tZXNzYWdlXzE0IjtzOjA6IiI7fX19fQ==', '{"form_mode":"easy","form_method":"post","auto_detect_settings":"1","load_files":"1","tight_layout":"0","action_url":"","form_tag_attach":"","add_form_tags":"1","relative_url":"1","dynamic_files":"0","enable_plugins":"0","show_top_errors":"1","datepicker_config":"","datepicker_type":"0","datepicker_moo_style":"datepicker_dashboard","enable_jsvalidation":"1","jsvalidation_errors":"1","jsvalidation_theme":"red","jsvalidation_lang":"ru","jsvalidation_showErrors":"0","jsvalidation_errorsLocation":"1","adminview_actions":"","dataview_actions":""}', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_chronoform_actions`
--

CREATE TABLE IF NOT EXISTS `o1hap_chronoform_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chronoform_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `params` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `content1` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=705 ;

--
-- Dumping data for table `o1hap_chronoform_actions`
--

INSERT INTO `o1hap_chronoform_actions` (`id`, `chronoform_id`, `type`, `enabled`, `params`, `order`, `content1`) VALUES
(702, 2, 'email', 0, '{"to":"","cc":"","bcc":"","subject":"","fromname":"","fromemail":"","replytoname":"","replytoemail":"","dto":"","dcc":"","dbcc":"","dsubject":"","dfromname":"","dfromemail":"","dreplytoname":"","dreplytoemail":"","recordip":"1","attachments":"","sendas":"html","action_label":"#3","encrypt_enabled":"0","gpg_sec_key":"","replace_nulls":"0"}', 12, 'You may customize this message under the "Template" tab in the Email settings box, there is an option there to auto generate the template in 2 seconds.'),
(703, 2, 'custom_code', 1, '{"mode":"controller","action_label":"After Email(s)"}', 13, ''),
(704, 2, 'show_thanks_message', 1, '{}', 14, '<span class="thanks-message">Ваш запрос отправлен. В ближайшее время с Вами свяжется наш представитель. Приятной работы.</span> <a class="back-to-contacts" href="http://euro-therm.com.ua/kontakty">Вернуться к странице контактов.</a>'),
(701, 2, 'email', 0, '{"to":"","cc":"","bcc":"","subject":"","fromname":"","fromemail":"","replytoname":"","replytoemail":"","dto":"","dcc":"","dbcc":"","dsubject":"","dfromname":"","dfromemail":"","dreplytoname":"","dreplytoemail":"","recordip":"1","attachments":"","sendas":"html","action_label":"#2","encrypt_enabled":"0","gpg_sec_key":"","replace_nulls":"0"}', 11, 'You may customize this message under the "Template" tab in the Email settings box, there is an option there to auto generate the template in 2 seconds.'),
(695, 2, 'event_loop', 1, '{}', 7, ''),
(696, 2, 'upload_files', 0, '{"files":"","upload_path":"","max_size":"100","min_size":"0","max_error":"Sorry, Your uploaded file size exceeds the allowed limit.","min_error":"Sorry, Your uploaded file size is less than the minimum limit.","type_error":"Sorry, Your uploaded file type is not allowed.","safe_file_name":"1"}', 6, ''),
(697, 2, 'custom_code', 1, '{"mode":"view","action_label":"Before Email(s)"}', 8, ''),
(698, 2, 'handle_arrays', 1, '{}', 16, ''),
(699, 2, 'db_save', 0, '{"table_name":"","model_id":"chronoform_data","save_under_modelid":"0","params_fields":"","ndb_enable":"0","ndb_driver":"mysql","ndb_host":"localhost","ndb_user":"","ndb_password":"","ndb_database":"","ndb_table_name":"","ndb_prefix":"jos_"}', 9, ''),
(700, 2, 'email', 1, '{"to":"euro_therm@mail.ru","cc":"","bcc":"","subject":"Euro-therm client form","fromname":"Euro-therm","fromemail":"info@euro-therm.com.ua","replytoname":"","replytoemail":"","dto":"","dcc":"","dbcc":"","dsubject":"","dfromname":"","dfromemail":"","dreplytoname":"","dreplytoemail":"","recordip":"1","attachments":"","sendas":"html","action_label":"#1","encrypt_enabled":"0","gpg_sec_key":"","replace_nulls":"0"}', 10, '<table cellpadding="0" cellspacing="0" border="0">\r\n	<tr>\r\n		<td>\r\n			Ф.И.О\r\n		</td>\r\n		<td>\r\n			{input_text_0}\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			Ваш Телефон\r\n		</td>\r\n		<td>\r\n			{input_text_1}\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			Ваш E-Mail\r\n		</td>\r\n		<td>\r\n			{input_text_2}\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			Сообщение\r\n		</td>\r\n		<td>\r\n			{input_textarea_3}\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td>\r\n			Введите код\r\n		</td>\r\n		<td>\r\n			{chrono_verification}\r\n		</td>\r\n	</tr>\r\n</table>'),
(694, 2, 'check_captcha', 1, '{"error":"\\u0412\\u044b \\u0432\\u0432\\u0435\\u043b\\u0438 \\u043d\\u0435\\u0432\\u0435\\u0440\\u043d\\u044b\\u0439 \\u043a\\u043e\\u0434."}', 4, ''),
(693, 2, 'event_loop', 1, '{}', 5, ''),
(692, 2, 'show_html', 1, '{}', 3, ''),
(691, 2, 'load_captcha', 1, '{"fonts":"0","encoded_image":"1"}', 2, ''),
(689, 2, 'load_js', 1, '{"dynamic_file":"0"}', 0, ''),
(690, 2, 'load_css', 1, '{}', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_contact_details`
--

CREATE TABLE IF NOT EXISTS `o1hap_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_content`
--

CREATE TABLE IF NOT EXISTS `o1hap_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `title_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Deprecated in Joomla! 3.0',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(10) unsigned NOT NULL DEFAULT '0',
  `mask` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `o1hap_content`
--

INSERT INTO `o1hap_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(3, 37, 'О компании', 'o-kompanii', '', '<p><img src="images/12.png" border="0" width="350" height="172" style="float: right; margin-left: 7px; margin-right: 7px;" /></p>\r\n<p style="text-align: justify;">Компания <strong>EUROTHERM</strong> начала свою деятельность в 2004г.  На сегодняшний день компания <strong>EUROTHERM</strong> является одним из лидирующих импортеров на рынок Украины товаров сантехнического назначения.</p>\r\n<p style="text-align: justify;">Приоритетным направлением деятельности компании является поставка радиаторов отопления производства Турции торговой марки <strong>EUROTHERM</strong>, а также алюминиевых и биметаллических радиаторов производства Турции, Италии и Китай.</p>\r\n<p style="text-align: justify;">Преимущества работы с нами – это индивидуальный подход к каждому клиенту, экономия финансовых и временных ресурсов наших клиентов за счет гибкой ценовой политики и эффективной работы компании, организация стабильных поставок.</p>\r\n<p>Залог успеха и процветания вашего бизнеса – это партнерские отношения с нами!</p>', '', 1, 0, 0, 2, '2012-04-16 17:54:41', 42, '', '2012-04-28 03:57:57', 42, 0, '0000-00-00 00:00:00', '2012-04-16 17:54:41', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 13, '', '', 1, 1600, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(2, 36, 'Товары', 'tovary', '', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec congue sagittis felis, ut egestas felis faucibus ut. Sed nec justo et velit rutrum fermentum non eu tellus. Integer porta mauris egestas massa commodo tincidunt. Curabitur laoreet condimentum iaculis. Integer leo velit, ultricies vitae bibendum ac, euismod sed lacus. Quisque ultrices commodo nisl ac malesuada. Phasellus a semper nibh. Donec porta lectus eu sem imperdiet porttitor. Curabitur consectetur molestie elit imperdiet lobortis.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Proin mi enim, pharetra vitae lobortis non, consectetur sed quam. Nullam lorem nibh, consequat ut blandit sit amet, blandit eget leo. Aliquam metus quam, lacinia vel fringilla ac, iaculis in arcu. Sed porttitor, urna ut laoreet vestibulum, leo dolor porttitor lorem, eget vehicula diam est pretium nisi. Vestibulum non felis in dui imperdiet imperdiet ut sit amet orci. Fusce non dolor lectus. Nam leo enim, aliquet eget imperdiet rutrum, cursus ut nisi. Curabitur ultricies lobortis arcu eu tincidunt. Pellentesque sodales ante lacus. Proin porta ante ac odio luctus a consectetur libero pharetra. Phasellus ac quam nec nunc mattis consectetur. Ut ac orci tortor. Vivamus ut erat scelerisque nunc mollis pharetra. Donec congue luctus eleifend. Nam vitae ligula ac dolor eleifend elementum. Integer et purus dolor, ac tempus mauris.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Proin gravida tortor non tellus hendrerit suscipit auctor justo semper. Maecenas ac quam nisl, sed mattis nibh. Phasellus lobortis, est et tristique dictum, urna orci aliquam velit, sed scelerisque nunc eros nec lectus. Donec turpis augue, ullamcorper ac porta vitae, posuere a lectus. In et tortor magna. Quisque fringilla imperdiet elementum. Nam dapibus lectus id nibh fermentum porta. Duis hendrerit gravida augue eget aliquam.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Curabitur sed felis tellus, in sodales enim. Phasellus iaculis eleifend lectus, lacinia eleifend leo porta at. Nulla urna odio, faucibus at gravida at, egestas vitae turpis. Nulla non eros lacus. Vestibulum ipsum purus, consequat sit amet iaculis quis, fringilla sit amet enim. Aliquam pulvinar, nisi sit amet pellentesque facilisis, purus arcu dapibus odio, non gravida urna nunc fringilla ligula. Cras aliquam ligula at enim ultrices eu tincidunt felis tempus. Cras semper, tellus id semper ornare, elit sem sagittis felis, quis aliquam ligula massa quis sapien.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Aliquam erat volutpat. Morbi pharetra aliquet magna, eget molestie lacus euismod in. Pellentesque non bibendum ante. Nam in eros tortor. Fusce quis ultricies turpis. Etiam et purus vel dui vehicula molestie vitae ac orci. Praesent et pretium ipsum. Donec ultricies nulla nec quam eleifend convallis eget vel orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>', '', -2, 0, 0, 2, '2012-04-16 17:53:20', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 17:53:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 13, '', '', 1, 119, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 38, 'Бренды', 'brendy', '', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td><img src="images/brands/1.png" border="0" width="130" height="56" /></td>\r\n<td><strong> </strong></td>\r\n<td><strong>EUROTHERM</strong><br />Стальные панельные радиаторы. Для производства радиаторов EUROTHERM используется листовая холоднокатаная сталь высокого качества <br /><span style="color: #808080;">Страна: <strong>Турция<br /></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/2.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>EUROTHERM</strong><br />Биметаллические радиаторы — это сочетание высокой теплоотдачи алюминия и отличной надежности меди<br /><span style="color: #808080;">Страна: <strong>Китай</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/3.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>Компания Fondital</strong><br />созданная в 1970 году Президентом Сильвестро Ниболи, проектирует и производит системы для отопления.<br /><span style="color: #808080;">Страна: <strong>Италия</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/4.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>Global</strong><br />Основанная в 1971 году братьями Фарделли, фирма GLOBAL является одним из предприятий, стоявших у истоков создания алюминиевого радиатора<br /><span style="color: #808080;">Страна: <strong>Италия</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/5.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>Радиаторы Nova Florida</strong> - это одни из наиболее популярных моделей алюминиевых радиаторов, обладают высоким качеством, высокой теплоотдачей, современным внешним видом, прочностью покрытия<br /><span style="color: #808080;">Страна: <strong>Италия</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/6.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td>Модель литого под давлением радиатора от компании <strong>Fondital</strong>, изготовленного из алюминиевого сплава для достижения высокой теплоотдачи и экономии энергоресурсов, благодаря низкой термической инерции.<br /><span style="color: #808080;">Страна: <strong>Италия</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/7.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>Sannyheter <br /></strong>Алюминиевый литой секционный радиатор SANNYHETER обладает малым весом, хорошей теплоотдачей и современным видом.<strong><span style="color: #808080;"><br /></span></strong><span style="color: #808080;">Страна:</span><strong><span style="color: #808080;"> Китай</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/8.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td>Радиатор <strong>ALLTERMO</strong>, изготовлен методом литья под давлением с учётом жёстких требований, предъявляемых к радиаторам при эксплуатации их на территории Украины<br /><span style="color: #808080;">Страна: <strong>Китай</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/brands/9.png" border="0" width="130" height="56" /></td>\r\n<td> </td>\r\n<td><strong>Classic+ <br /></strong>Биметаллические радиаторы Classicplus используют для отопления промышленных, жилых и общественных помещений<strong><span style="color: #808080;"><br /></span></strong><span style="color: #808080;">Страна:</span><strong><span style="color: #808080;"> Китай</span></strong></td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 1, 0, 0, 2, '2012-04-16 17:55:07', 42, '', '2012-05-08 13:22:41', 42, 0, '0000-00-00 00:00:00', '2012-04-16 17:55:07', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"1","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 25, 0, 12, '', '', 1, 3206, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(5, 39, 'Дистрибьюторам', 'distribyutoram', '', '<p style="text-align: justify;">Компания <strong>EUROTHERM</strong> на протяжении многих лет успешно осуществляет продажи товаров по всей территории Украины и России. Приглашает Вас присоединиться к числу наших партнеров, рассмотрим предложения по вопросам поставок товаров в Ваши торговые точки. Компания <strong>EUROTHERM</strong> имеет хорошую деловую репутация благодаря высокому уровню сервиса, оперативности выполнения заказов и внимательному отношению к потребностям заказчика .</p>\r\n<p style="text-align: justify;"><img src="images/diller_map.png" border="0" width="228" height="156" style="float: right; margin-left: 5px; margin-right: 5px;" />Компания <strong>EUROTHERM</strong> приглашает к взаимовыгодному сотрудничеству: региональные представительства и дистрибьюторов, торговые сети, магазины и рынки, строительные компании, монтажные бригады и организации, юридические и физические лица.</p>\r\n<p>Компания<strong> EUROTHERM</strong> – это всегда гарантия качества, доступные цены и индивидуальный подход!<br /><br /><strong>Работаем за наличный и безналичный расчет</strong></p>\r\n<p><strong><br /></strong></p>', '', 1, 0, 0, 2, '2012-04-16 17:55:35', 42, '', '2012-06-13 10:28:56', 42, 42, '2012-06-13 10:28:56', '2012-04-16 17:55:35', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 9, 0, 11, '', '', 1, 1756, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(31, 79, 'Биметаллические радиаторы Китай', 'биметаллические-радиаторы-китай', '', '<h3>All Termo (Китай)</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/8.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/d.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">7,30</span> <span style="font-size: large;">у.е. <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 59,86</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/brands/8.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" />Китайские <strong>биметаллические радиаторы Alltermo Bimetall</strong> — это новый вид радиаторов, предназначенных для отопления общественных зданий. <img src="images/all.jpg" border="0" width="197" height="190" style="float: right; margin-left: 7px; margin-right: 7px; border: 0;" />Они могут применяться для обогрева высотных зданий с централизованным отоплением и для малоэтажных застроек с автономной системой отопления.<br />При разработке <strong>радиаторов Alltermo Bimetall</strong> были учтены как европейские стандарты, так и особенности украинских систем отопления. Эти приборы для обогрева зданий достойно смотрятся рядом с лучшими радиаторами мира.<br />Биметаллические радиаторы ALLTERMO — это отличное сочетание качества и цены. <br /><br /><strong>Технические характеристики</strong><br />Мощность э биметаллических радиаторов — 100 Вт.<br />Стандартное давление — 16 бар (атмосфер).<br />Параметры: 400 х 80 х 77 мм, объем — 0,25 л.<br />Вес — 1,15 кг.<br />Диаметр соединений — G1.<br />Межосевое расстояние — 350 мм.</p>\r\n<p>{/tabs}</p>\r\n<h3><br />Радиаторы Classic+</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/9.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/d.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">7,40</span> <span style="font-size: large;">у.е. <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 60,68</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><strong><img src="images/brands/9.png" border="0" width="130" height="56" style="margin-left: 5px; margin-right: 5px; float: left;" />Биметаллические радиаторы Classicplus</strong> используют для отопления промышленных, жилых и общественных помещений. Коттеджи и гаражи, индивидуальные дома и садовые домики — применение этих отопительных приборов будет эффективным.<br /><br /><strong>Технические характеристики</strong><br />Значение кислотности (PH) теплоносителя должно быть от 6,5 до 8,5;<br />Температура теплоносителя не должна превышать 110?С;<br />Рабочее давление должно быть до 30 бар;<br />Испытания на прочность проходят под давлением 45 бар.</p>\r\n<p>{/tabs}</p>\r\n<h3> </h3>\r\n<h3>EUROTHERM</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/2.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/d.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,90</span> <span style="font-size: large;">y.e. <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 56,58</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/brands/2.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n<p style="text-align: justify;">Биметаллические радиаторы отопления <strong>EUROTHERM</strong> — это агрегаты, состоящие из двух видов металла (преимущественно из стали и алюминия). Циркуляция теплоносителя осуществляется по стальным трубкам, заключенным в алюминиевый кожух. Сочетание этих металлов и обеспечивает биметаллическим радиаторам <strong>EUROTHERM</strong> улучшенные эксплуатационные характеристики, а, следовательно, и первое место по востребованности.</p>\r\n<p style="text-align: justify;">Биметаллические радиаторы <strong>EUROTHERM</strong> способны создать в доме тепловой комфорт с наименьшими денежными затратами — этому способствуют повышенная теплоотдача, небольшой вес и низкие показатели тепловой инерции, позволяющие радиатору <strong>EUROTHERM</strong> оперативно реагировать на изменение температуры. Элегантный дизайн позволит им вписаться в любой интерьер, и даже стать неким украшением комнаты, вам не понадобится приобретать декоративные щиты, чтобы спрятать за ними биметаллические радиаторы <strong>EUROTHERM</strong>.</p>\r\n<p style="text-align: justify;">Радиатор биметалл <strong>EUROTHERM</strong> 30 bar 500/80</p>\r\n<p style="text-align: justify;">Биметаллические радиаторы отопления <strong>EUROTHERM</strong> удачно сочетают лучшие свойства секционных алюминиевых радиаторов и трубчатых стальных радиаторов: прочность (выдерживают высокое давление), долговечность (срок службы - до 20 лет) и высокий уровень теплоотдачи в сочетании с современным дизайном биметаллического радиатора.</p>\r\n<ul>\r\n<li>Высота — 570 мм,</li>\r\n<li>Глубина — 80 мм,</li>\r\n<li>Ширина — 80 мм,</li>\r\n<li>Межосевое расстояние — 500 мм,</li>\r\n<li>Масса - 1,4 кг</li>\r\n<li>Максимальная t теплоносителя – 110 С</li>\r\n<li>Рабочее давление — 24 Атм,</li>\r\n<li>Материал — биметалл (алюминий+сталь)</li>\r\n</ul>\r\n<p style="text-align: justify;">В биметаллическом радиаторе <strong>EUROTHERM</strong> применяются два металла - сталь и алюминий. Стальной сердечник усиливает конструкцию биметаллических радиаторов.<br />Стальная начинка биметаллических радиаторов «спокойнее» других реагирует на щелочность воды (ph-фактор).<br />Поверхностный слой - алюминий обладает высокой теплопроводностью, что существенно улучшает теплоотдачу биметаллического радиатора и уменьшает его инертность.<br />Технология производства биметаллических радиаторов отопления <strong>EUROTHERM</strong> - литьё под давлением.</p>\r\n<p>{/tabs}</p>', '', 1, 0, 0, 2, '2012-04-26 16:20:49', 42, '', '2012-08-31 09:44:16', 42, 42, '2014-05-27 13:07:08', '2012-04-26 16:20:49', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 33, 0, 5, '', '', 1, 5074, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(6, 40, 'Контакты', 'kontakty', '', '<table border="0" align="left">\r\n<tbody>\r\n<tr>\r\n<td><img src="images/tel.png" border="0" width="35" height="35" /></td>\r\n<td><span style="font-size: small;"> </span></td>\r\n<td><span style="font-size: medium;">+38 (050)</span><span style="font-size: medium;"> 194 89 99<br />+38 (067)<span> 423 33 86</span></span></td>\r\n<td style="width: 15px;"> </td>\r\n<td><img src="images/mail.png" border="0" width="35" height="35" /></td>\r\n<td><span style="font-size: medium;"> </span></td>\r\n<td><span style="font-size: medium;">euro_therm@mail.ru</span></td>\r\n<td style="width: 15px;"> </td>\r\n<td><a href="mailto:euro_therm@mail.ru"><img src="images/s.png" border="0" width="35" height="35" /></a></td>\r\n<td><span style="font-size: medium;"><a href="skype:euro_therm">euro_therm</a></span></td>\r\n<td style="width: 15px;"> </td>\r\n<td><span style="font-size: medium;"><img src="images/map.png" border="0" width="35" height="35" /></span></td>\r\n<td><a href="map"><span style="font-size: medium;">карта проезда</span></a></td>\r\n</tr>\r\n<tr>\r\n<td colspan="13"> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<p><br /><br /></p>\r\n<p>{chronoforms}Form{/chronoforms}</p>', '', 1, 0, 0, 2, '2012-04-16 17:55:58', 42, '', '2012-06-11 12:17:12', 42, 42, '2012-06-11 12:17:12', '2012-04-16 17:55:58', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 33, 0, 10, '', '', 1, 2972, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(7, 41, 'Главная', 'главная', '', '<p style="text-align: justify;">Компания <strong>EUROTHERM</strong> начала свою деятельность в 2004г.  На сегодняшний день компания <strong>EUROTHERM</strong> является одним из лидирующих импортеров на рынок Украины товаров сантехнического назначения.<br /><br />Приоритетным направлением деятельности компании является поставка радиаторов отопления производства Турции торговой марки <strong>EUROTHERM</strong>, а также алюминиевых и биметаллических радиаторов производства Турции, Италии и Китай.<br /><br />Преимущества работы с нами – это индивидуальный подход к каждому клиенту, экономия финансовых и временных ресурсов наших клиентов за счет гибкой ценовой политики и эффективной работы компании, организация стабильных поставок.<br /><br />Залог успеха и процветания вашего бизнеса – это партнерские отношения с нами!</p>\r\n<p style="text-align: justify;"><strong>Работаем за наличный и безналичный расчет</strong></p>\r\n<p style="text-align: justify;"> </p>', '', 1, 0, 0, 2, '2012-04-16 17:57:15', 42, '', '2012-06-13 10:28:21', 42, 0, '0000-00-00 00:00:00', '2012-04-16 17:57:15', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"0","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 9, '', '', 1, 12174, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(30, 78, 'Алюминиевые радиаторы Италия', 'алюминиевые-радиаторы-италия', '', '<h3>Радиаторы Global</h3>\r\n<p><br />{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/4.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/e.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,75</span> <span style="font-size: large;">евро  <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 72,56</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/glob_1.jpg" border="0" width="307" height="190" style="border: 0; float: right; margin-left: 7px; margin-right: 7px;" /></p>\r\n<p style="text-align: justify;"><strong><img src="images/brands/4.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" />Итальянская компания GLOBAL</strong> <br />di Fardelli Ottorino e C. S.r.l. предлагает высококачественные алюминиевые радиаторы. <br /><strong>Условия эксплуатации</strong>: алюминиевые радиаторы Global проходят обязательную проверку работоспособности при проверочном давлении 24 bar, при этом стандартное рабочее давление должно соответствовать16 bar. <br />Допустимый нагрев теплоносителя — до 110 ° C, а диапазон PH должен быть в пределах 6,5 - 8,5.<br /><br /><strong>Покрытие</strong>: каждый алюминиевый радиатор Global от итальянского производителя отличается высоким качеством покрытия. Как правило, это — многослойное покрытие, окрашенное в белый цвет (RAL 9010) и отличающееся высокой прочностью.</p>\r\n<p>{/tabs}</p>\r\n<h3>Алюминиевые радиаторы FONDITAL</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/3.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/e.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,60</span> <span style="font-size: large;">евро <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 70,95</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> {tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/fon_1.jpg" border="0" style="border: 0; float: right; margin-left: 7px; margin-right: 7px;" /></p>\r\n<p style="text-align: justify;"><strong><img src="images/brands/3.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" />Продукция концерна FONDITAL</strong> — алюминиевые радиаторы, по праву занимающие лидирующие позиции среди алюминиевых производимых в мире радиаторов.<br />Появившись в Украине и на просторах СНГ в числе первых, эти алюминиевые радиаторы десятилетиями подтверждают свое высокое качество. С каждым годом их модели становятся более совершенными. На сегодняшний день <strong>радиаторы Fondital</strong> адаптированы под свойства нашего теплоносителя, а именно — его кислотность. <br /><br /><strong>Сертификация</strong>: радиаторы FONDITAL сертифицированы. Им присвоен сертификат UNI EN ISO 9001/2000, признанный во всем мире. Кроме того, качество алюминиевых радиаторов FONDITAL подтверждено украинским сертификатом, также на них оформлены все необходимые документы. <br /><strong>ВАЖНО: на радиаторы FONDITAL нет подделок.</strong><br /><br /><strong>Технические характеристики</strong>: оптимальное давление для работы радиаторов FONDITAL равняется 16 бар. Давление в 60 бар — приводит к разрыву. До поступления в продажу каждый алюминиевый радиатор FONDITAL подвергается проверке под давлением 24 бар.<br />Внешний вид радиаторов этого концерна безупречен, они надежно защищены от коррозии. Такое качество возможно благодаря двухслойному покрытию (используется метод порошкового напыления и анафореза).</p>\r\n<p>{/tabs}</p>\r\n<h3>Радиаторы Nova Florida</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/5.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/e.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,50</span> <span style="font-size: large;">евро <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 69,87</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/nov_1.jpg" border="0" width="307" height="190" style="float: right; margin-left: 7px; margin-right: 7px;" /><img src="images/brands/5.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /><strong>Алюминиевые радиаторы Nova Florida</strong> пользуются большим спросом. Они характеризуются высокой теплоотдачей, отличным качеством исполнения, прочностью покрытия и красивым внешним видом.<br /><strong>Сертификация</strong>: все радиаторы Nova Florida проходят обязательный послеконвейерный контроль качества. Эти радиаторы прошли сертификацию в Украине. Их можно использовать в автономных системах и системах центрального отопления многоэтажных и частных домов.<br /><br /><strong>Технические характеристики</strong>: 120 °С — максимальная допустимая температура теплоносителя. <br />Каждый экземпляр проходит обязательную проверку на прочность и подвергается испытаниям под давлением 24 бар (что равняется 24 атмосферам). Оптимальное рабочее давление — 16 бар.<br />Приведенная тепловая мощность алюминиевых радиаторов актуальна при соблюдении условий: <br />В среднем температура теплоносителя и воздуха должна быть — 70°C.<br />Схема подвода теплоносителя к радиатору — "сверху-вниз", в качестве теплоносителя используется вода.<br />Алюминиевые радиаторы Nova Florida — это набор секций, причем один прибор может содержать различное число секций.</p>\r\n<p>{/tabs}</p>', '', 1, 0, 0, 2, '2012-04-26 16:04:33', 42, '', '2012-05-07 13:26:35', 42, 42, '2012-08-31 09:39:41', '2012-04-26 16:04:33', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 50, 0, 6, '', '', 1, 3208, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(8, 43, 'Стальные радиаторы подпункт 1', 'stalnye-radiatory-podpunkt-1', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:54:33', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:54:33', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 8, '', '', 1, 3, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `o1hap_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(9, 44, 'Стальные радиаторы подпункт 2', 'stalnye-radiatory-podpunkt-2', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:55:03', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:55:03', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 7, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(10, 45, 'Стальные радиаторы подпункт 3', 'stalnye-radiatory-podpunkt-3', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:55:18', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:55:18', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 6, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(11, 46, 'Алюмниевые радиаторы подпункт 1', 'alyumnievye-radiatory-podpunkt-1', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:56:01', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:56:01', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 5, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(12, 47, 'Алюминиевые радиаторы подпункт 2', 'alyuminievye-radiatory-podpunkt-2', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:56:26', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:56:26', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 4, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(13, 48, 'Алюминиевые радиаторы подпункт 3', 'alyuminievye-radiatory-podpunkt-3', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:56:49', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:56:49', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 3, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(14, 49, 'Биметаллические радиаторы подпункт 1', 'bimetallicheskie-radiatory-podpunkt-1', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:57:16', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:57:16', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 2, '', '', 1, 6, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(15, 50, 'Биметаллические радиаторы подпункт 2', 'bimetallicheskie-radiatory-podpunkt-2', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:57:38', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:57:38', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(16, 51, 'Биметаллические радиаторы подпункт 3', 'bimetallicheskie-radiatory-podpunkt-3', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 8, '2012-04-16 18:57:54', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-16 18:57:54', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(17, 53, 'Профессиональный партнер GRUNDFOS 1', 'professionalnyj-partner-grundfos-1', '', '<p><strong style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"><br class="Apple-interchange-newline" /><img src="images/news/news_img.png" border="0" alt="" align="left" />Lorem Ipsum</strong><span style="color: #000000; font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', '', -2, 0, 0, 9, '2012-04-16 20:18:55', 42, '', '2012-04-16 20:30:41', 42, 0, '0000-00-00 00:00:00', '2012-04-16 20:18:55', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 3, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(18, 54, 'Международные события', 'vtoraya-novost', '', '<p> Международные выставки отопительного оборудования 2012 года<img src="images/news/n2.jpg" border="0" style="float: left;" /></p>\r\n', '\r\n<div class="textcaldaia">\r\n<table border="0">\r\n<tbody>\r\n<tr>\r\n<td colspan="2">Международные выставки отопительного оборудования 2012 года</td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/news/n2.jpg" border="0" style="float: left;" /></td>\r\n<td><strong>СТРОЙСИБ</strong><br />Россия     Новосибирск<br />С 14- ПО -17 ФЕВРАЛЯ 2012</td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/news/n2_2.jpg" border="0" /></td>\r\n<td><strong>MCE 2012 - МИЛАН</strong><br />С 27-ПО-30 МАРТА 2012<br />Павильон 1 / СТЕНД L20N29</td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/news/n2_3.jpg" border="0" /></td>\r\n<td valign="top">\r\n<div class="textcaldaia"><strong>AQUATHERM - МОСКВА</strong><br />С 7-по-12 ФЕВРАЛЯ 2012<br />Павильон 2 ХОЛЛ 7 / СТЕНД C303</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n</div>', 1, 0, 0, 9, '2012-04-16 21:02:19', 42, '', '2012-04-30 18:09:16', 42, 0, '0000-00-00 00:00:00', '2012-04-16 21:02:19', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 14, 0, 2, '', '', 1, 647, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(29, 76, 'Специальные предложения', 'специальные-предложения', '', '<p><img src="images/dost.png" border="0" width="354" height="340" style="border: 0; float: right; margin-left: 5px; margin-right: 5px;" />Для крупных клиентов и постоянных заказчиков</p>\r\n<p>компания ОРГАНИЗОВЫВАЕТ ДОСТАВКУ ЗА СВОЙ СЧЕТ</p>', '', 1, 0, 0, 2, '2012-04-19 08:49:31', 42, '', '2012-04-28 03:25:18', 42, 0, '0000-00-00 00:00:00', '2012-04-19 08:49:31', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 7, '', '', 1, 871, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(19, 63, 'Бренд EUROTHERM(RED)', 'brend1', '', '<p><img src="images/brands/1.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:20:24', 42, '', '2012-04-30 16:20:12', 42, 0, '0000-00-00 00:00:00', '2012-04-17 08:20:24', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 7, '', '', 1, 105, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(20, 64, 'Бренд EUROTHERM', 'brend2', '', '<p><img src="images/brands/2.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:20:58', 42, '', '2012-04-30 16:19:56', 42, 42, '2012-07-11 10:38:29', '2012-04-17 08:20:58', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 0, 8, '', '', 1, 85, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(21, 65, 'Бренд FONDIAL', 'brend3', '', '<p><img src="images/brands/3.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:22:16', 42, '', '2012-04-30 16:20:22', 42, 0, '0000-00-00 00:00:00', '2012-04-17 08:22:16', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 6, '', '', 1, 97, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(22, 66, 'Бренд GLOBAL', 'brend4', '', '<p><img src="images/brands/4.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:22:40', 42, '', '2012-04-30 16:19:34', 42, 0, '0000-00-00 00:00:00', '2012-04-17 08:22:40', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 5, '', '', 1, 86, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(23, 67, 'Бренд NOVA FLORIDA', 'brend5', '', '<p><img src="images/brands/5.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:23:03', 42, '', '2012-04-26 16:56:43', 42, 0, '0000-00-00 00:00:00', '2012-04-17 08:23:03', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 4, '', '', 1, 95, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(24, 68, 'Бренд SOLAR', 'brend6', '', '<p><a href="http://vk.com"><img src="images/brands/6.png" border="0" alt="" /></a></p>', '', 1, 0, 0, 10, '2012-04-17 08:23:38', 42, '', '2012-04-30 17:08:24', 42, 42, '2012-04-30 17:08:24', '2012-04-17 08:23:38', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 3, '', '', 1, 101, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(25, 69, 'Бренд SANNYHETER', 'brend7', '', '<p><img src="images/brands/7.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-17 08:24:03', 42, '', '2012-04-30 16:21:01', 42, 0, '0000-00-00 00:00:00', '2012-04-17 08:24:03', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 2, '', '', 1, 82, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(34, 82, 'Бренд ALLTHERMO', 'бренд-allthermo', '', '<p><img src="images/brands/8.png" border="0" alt="" /></p>', '', 1, 0, 0, 10, '2012-04-30 16:21:48', 42, '', '0000-00-00 00:00:00', 0, 42, '2012-10-10 07:04:02', '2012-04-30 16:21:48', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(33, 81, 'Бренды', 'бренды', '', '<p>к</p>', '', -2, 0, 0, 2, '2012-04-28 03:35:17', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-28 03:35:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(32, 80, 'Алюминиевые радиаторы Китай', 'алюминиевые-радиаторы-китай', '', '<h3 style="text-align: justify;">All Termo (Китай)</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/8.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/d.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,35</span> <span style="font-size: large;">y.е. <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;"> 52,07</span> грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/brands/8.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /><strong>Алюминиевый литой секционный радиатор</strong> , собой экономичный отопительный прибор, отвечающий европейским и украинским стандартам . Алюминиевый радиатор предназначен для использования в любых отопительных системах , устойчив к коррозии ,имеет небольшой вес , низкую термическую инерционность и повышенную поверхность теплообмена . Дизайн алюминиевых радиаторов , примирил два фактора , находящиеся в конфликте :</p>\r\n<p style="text-align: justify;"><img src="images/all_1.png" border="0" width="197" height="190" style="float: right; margin-left: 7px; margin-right: 7px;" /></p>\r\n<p style="text-align: justify;">необходимость выпускать конструкцию , позволяющую эффективно организовать конвективный теплообмен за счет большого количества ребер и технологических просветов , но в то же время наличие сплошной поверхности для придания дополнительной жесткости посекционной конструкции. Алюминиевые радиаторы , изготовленные литьем под давлением , позволили решить эту техническую задачу . Вопрос надежности решается испытанием по повышенным давлением. При необходимости мы можем помочь Вам перепаковать литые алюминиевые секции на стальных ниппелях, идущих в комплекте к алюминиевым радиаторам под ваши требования .Поверхность алюминиевого радиатора от воздействия окружающей среды защищена в два этапа грунтовка методом анафореза ,позволяющая положить грунт в самых труднодоступных местах. , а затем покрытие эмалью при помощи эпоксидных порошков, которое производится электростатическим методом .</p>\r\n<p> {/tabs}</p>\r\n<h3 style="text-align: justify;"> </h3>\r\n<h3 style="text-align: justify;">SANNYHETER</h3>\r\n<p>{tab=Цена}</p>\r\n<table border="0">\r\n<tbody>\r\n<tr valign="middle">\r\n<td style="height: 58px;" rowspan="2" valign="top">\r\n<p style="text-align: justify;"><img src="images/brands/7.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="middle"> </td>\r\n<td valign="middle"> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr valign="middle">\r\n<td valign="middle"><span style="font-size: x-large;"><img src="images/d.png" border="0" width="30" height="30" style="margin-left: 5px; margin-right: 5px;" /></span></td>\r\n<td valign="middle"><span style="font-size: x-large;">6,00</span> <span style="font-size: large;">y.e. <br /></span></td>\r\n<td style="width: 25px;"> </td>\r\n<td><img src="images/u.png" border="0" style="margin-left: 5px; margin-right: 5px;" /></td>\r\n<td><span style="font-size: large;"><span style="font-size: x-large;">  49,20 </span>грн</span></td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{tab=Описание}</p>\r\n<p style="text-align: justify;"><img src="images/brands/7.png" border="0" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n<p style="text-align: justify;"><img src="images/suni.jpg" border="0" width="197" height="190" style="float: right; margin-left: 7px; margin-right: 7px;" /></p>\r\n<p style="text-align: justify;"><strong>Алюминиевый радиатор SANNYHETER</strong> — это секционный радиатор. Он очень экономичный, соответствует стандартам Украины и Европы.<br /><strong>Общие характеристики</strong>: алюминиевый литой секционный радиатор может использоваться в каждой отопительной системе. У него повышенная поверхность теплообмена, он устойчив к коррозии. Кроме того, у литого секционного радиатора SANNYHETER повышенная поверхность теплообмена и низкая термическая инерционность.<br /><br /><strong>Особенности технологии изготовления</strong><br />В дизайне литого секционного радиатора объединилось два непримиримых фактора: эффективная конструкция, обеспечивающая конвективный теплообмен (благодаря наличию большого количества технологических просветов и ребер) в сочетании со сплошной поверхностью, придающей посекционной конструкции дополнительную жесткость и прочность.<br />Совмещение несовместимого стало возможным благодаря технологии изготовления радиаторов, применяющей метод литья под давлением, надежность данной конструкции проверялась под повышенным давлением. <br /><br />Если есть необходимость, то в конструкции этих радиаторов предусматривается перепаковка под ваши требования литых алюминиевых секций на стальных ниппелях, которые идут в комплекте.<br />От пагубного влияния внешних факторов литые секционные радиаторы надежно защищены: сначала грунтовка методом анафореза в два этапа не оставляет необработанных мест (даже труднодоступные места обязательно подвергаются грунтовке), после чего радиатор покрывается эмалью электростатическим методом при помощи эпоксидных порошков.<br /><br /><strong>Технические характеристики</strong><br />Алюминиевый литой <strong>секционный радиатор SANNYHETER</strong> обладает малым весом, хорошей теплоотдачей и современным видом.<br />Стандартное рабочее давления для этого прибора — 16 бар.</p>\r\n<p>{/tabs}</p>', '', 1, 0, 0, 2, '2012-04-26 16:30:27', 42, '', '2012-08-31 09:48:33', 42, 0, '0000-00-00 00:00:00', '2012-04-26 16:30:27', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 25, 0, 4, '', '', 1, 1949, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(36, 86, 'Карта проезда', 'карта-проезда', '', '<p>{source}<span style="font-family: courier new, courier, monospace;"><br /><span>&lt;</span>iframe width="750" height="380" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.ua/maps/ms?msa=0&amp;amp;msid=207395731615601762285.0004bf1e3947beccc8f31&amp;amp;hl=ru&amp;amp;ie=UTF8&amp;amp;t=h&amp;amp;ll=50.003312,36.295846&amp;amp;spn=0.005241,0.016093&amp;amp;z=16&amp;amp;output=embed"<span>&gt;</span><span>&lt;</span>/iframe<span>&gt;</span><span>&lt;</span>br /<span>&gt;</span><span>&lt;</span>small<span>&gt;</span>Просмотреть <span>&lt;</span>a href="http://maps.google.com.ua/maps/ms?msa=0&amp;amp;msid=207395731615601762285.0004bf1e3947beccc8f31&amp;amp;hl=ru&amp;amp;ie=UTF8&amp;amp;t=h&amp;amp;ll=50.003312,36.295846&amp;amp;spn=0.005241,0.016093&amp;amp;z=16&amp;amp;source=embed" style="color:#0000FF;text-align:left"<span>&gt;</span>Мои карты<span>&lt;</span>/a<span>&gt;</span> на карте большего размера<span>&lt;</span>/small<span>&gt;</span><br /></span>{/source}</p>\r\n<table border="0" align="left">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p> </p>\r\n</td>\r\n<td> </td>\r\n<td> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n<td style="width: 15px;"> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td><img src="images/tel.png" border="0" width="35" height="35" /></td>\r\n<td> </td>\r\n<td><span style="font-size: medium;">+38 (050)</span><span style="font-size: medium;"> 194 89 99</span></td>\r\n<td style="width: 15px;"> </td>\r\n<td><img src="images/mail.png" border="0" width="35" height="35" /></td>\r\n<td><span style="font-size: medium;">euro_therm@mail.ru</span></td>\r\n<td> </td>\r\n<td style="width: 15px;"> </td>\r\n<td><img src="images/s.png" border="0" width="35" height="35" /></td>\r\n<td><span style="font-size: medium;"><a href="skype:euro_therm">euro_therm</a></span></td>\r\n<td style="width: 15px;"> </td>\r\n<td><span style="font-size: medium;"><img src="images/map.png" border="0" width="35" height="35" /></span></td>\r\n<td><a href="map"><span style="font-size: medium;">карта проезда</span></a></td>\r\n</tr>\r\n<tr>\r\n<td colspan="13"> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<p><br /><br /></p>', '', 1, 0, 0, 2, '2012-04-30 17:45:45', 42, '', '2012-05-03 18:13:17', 42, 0, '0000-00-00 00:00:00', '2012-04-30 17:45:45', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 20, 0, 3, '', '', 1, 738, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `o1hap_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(37, 87, 'Eurotherm', 'eurotherm', '', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<p><img src="images/eurol_1.png" border="0" width="197" height="190" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="top">\r\n<p>Выполнен из листовой холоднокатаной стали высочайшего качества, толщина которой составляет 1,10 мм. Они имееют высокую тепловую производительность. Такие радиаторы отличаются надежностью. Все они проходят тестирование давлением 13 атмr454t54t</p>\r\n<p><span style="font-size: medium;"><a href="stalnye-radiatory/stalnye-radiatory-podpunkt-1">цены</a>   </span> <a href="stalnye-radiatory/stalnye-radiatory-podpunkt-1"><span style="font-size: medium;">описание</span></a>1    <a href="google.com">1111</a></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', -2, 0, 0, 18, '2012-04-17 11:48:17', 42, '', '2012-10-10 07:04:41', 42, 0, '0000-00-00 00:00:00', '2012-04-17 11:48:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 9, 0, 0, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(38, 88, 'новоcть 3', 'novost-5', '', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<p><img src="images/news/n1.jpg" border="0" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="top">новоcть 3</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n', '\r\n<p><img src="images/news/n1.jpg" border="0" style="float: left; margin-left: 5px; margin-right: 5px;" /> Самая крупная выставка отопительного оборудования <br />Украина город Киев <br />Дата проведения: 15 - 18 мая 2012</p>\r\n<p> </p>\r\n<p>Место проведения: <strong>Киев</strong><br />Международный выставочный центр<br />станция метро «<strong>Левобережная</strong>»<br />пр. Броварской, 15.<br /><br /><br /><strong>Время работы</strong><br />15 мая - 10:00-18:00<br />16 мая - 10:00-18:00<br />17 мая - 10:00-18:00<br />18 мая - 10:00-16:00</p>\r\n<p style="text-align: left;"> </p>\r\n<p style="text-align: left;"><strong>Телефон организаторов выставки:</strong><br />+(38 044) 496 86 45 <br />+(38 063) 879-18-53</p>', -2, 0, 0, 9, '2012-04-17 11:48:17', 42, '', '2012-10-10 07:02:47', 42, 0, '0000-00-00 00:00:00', '2012-04-17 11:48:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 0, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(26, 70, '«Аква-Терм Киев»  15 - 18 мая  Киев', 'novost-3', '', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<p><img src="images/news/n1.jpg" border="0" style="float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n</td>\r\n<td valign="top">Самая крупная выставка отопительного оборудования <br />Украина город Киев <br />Дата проведения: 15 - 18 мая 2012</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n', '\r\n<p><img src="images/news/n1.jpg" border="0" style="float: left; margin-left: 5px; margin-right: 5px;" /> Самая крупная выставка отопительного оборудования <br />Украина город Киев <br />Дата проведения: 15 - 18 мая 2012</p>\r\n<p> </p>\r\n<p>Место проведения: <strong>Киев</strong><br />Международный выставочный центр<br />станция метро «<strong>Левобережная</strong>»<br />пр. Броварской, 15.<br /><br /><br /><strong>Время работы</strong><br />15 мая - 10:00-18:00<br />16 мая - 10:00-18:00<br />17 мая - 10:00-18:00<br />18 мая - 10:00-16:00</p>\r\n<p style="text-align: left;"> </p>\r\n<p style="text-align: left;"><strong>Телефон организаторов выставки:</strong><br />+(38 044) 496 86 45 <br />+(38 063) 879-18-53</p>', 1, 0, 0, 9, '2012-04-17 11:48:17', 42, '', '2012-04-30 18:15:32', 42, 0, '0000-00-00 00:00:00', '2012-04-17 11:48:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 15, 0, 1, '', '', 1, 788, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(27, 71, 'Акции и скидки', 'акции-и-скидки', '', '<p>При заказе 100 и более радиаторов в ассортименте действует дополнительная скидка от текущих цен<br /><strong></strong></p>', '', 1, 0, 0, 2, '2012-04-17 12:39:33', 42, '', '2012-04-28 13:23:15', 42, 0, '0000-00-00 00:00:00', '2012-04-17 12:39:33', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 8, '', '', 1, 1790, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(35, 83, 'Бренд Classic+', 'classic', '', '<p><img src="images/brands/9.png" border="0" width="130" height="56" /></p>', '', 1, 0, 0, 10, '2012-04-30 16:21:48', 42, '', '2012-04-30 16:56:24', 42, 0, '0000-00-00 00:00:00', '2012-04-30 16:21:48', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 0, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(28, 72, 'Диллерская сеть', 'диллерская-сеть', '', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id sem eget risus vehicula aliquam eget et lacus. Fusce faucibus eros et mi volutpat sagittis. Vivamus in tristique nunc. Fusce posuere commodo mi vel placerat. Ut quam diam, eleifend a condimentum sed, interdum eu erat. Sed tincidunt imperdiet nisi sit amet imperdiet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec scelerisque consequat condimentum. Aliquam a tellus mi. In hac habitasse platea dictumst. Sed ut est nisl. Curabitur tempor, neque a suscipit malesuada, metus turpis suscipit ante, et ultrices lectus est et felis. Vivamus adipiscing massa vel erat iaculis tempor laoreet in lorem. Nunc erat velit, scelerisque ac imperdiet et, condimentum sit amet nunc. Pellentesque lorem lacus, ullamcorper eu vestibulum bibendum, posuere a lectus.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Etiam tincidunt mi et quam venenatis tempus. Nunc convallis pretium ligula, ut molestie est porttitor eget. Cras pulvinar sollicitudin luctus. Maecenas sit amet vulputate lectus. Sed interdum urna a lacus pellentesque non ultricies urna tempor. Phasellus consectetur pulvinar felis, tincidunt consequat lectus congue vel. Nulla ut lorem ligula, et porttitor tellus. Cras vel arcu neque, in elementum eros.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Quisque blandit, tellus euismod fringilla pretium, ante velit aliquam mi, et bibendum turpis augue varius mi. Praesent molestie posuere tortor sed posuere. Etiam eleifend, lectus at ultrices convallis, magna quam imperdiet erat, sit amet tristique nisl nisi eget odio. Etiam sed magna massa. Phasellus accumsan lectus ut quam adipiscing dictum. Phasellus sollicitudin elementum risus, eget cursus massa condimentum ut. Suspendisse potenti. Morbi adipiscing euismod arcu vitae interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id urna pretium elit egestas tincidunt nec nec libero.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Nullam non neque commodo nibh vulputate gravida ac eget magna. Nam vel velit ultricies erat dignissim suscipit. Suspendisse potenti. Vivamus posuere, velit non ultricies cursus, mi orci condimentum enim, vel lobortis arcu erat pharetra erat. Maecenas sagittis ante ac dolor mattis non rhoncus sem tempus. Fusce ac dui bibendum urna auctor sollicitudin ut ac turpis. Sed porttitor interdum urna, ac lobortis enim imperdiet pellentesque. Donec lacinia laoreet erat quis pharetra. Maecenas massa velit, suscipit sed sollicitudin et, elementum a elit.</p>\r\n<p style="text-align: justify; font-size: 11px; line-height: 14px; margin-top: 0px; margin-right: 0px; margin-bottom: 14px; margin-left: 0px; color: #000000; font-family: Arial, Helvetica, sans; padding: 0px;">Sed in erat turpis, venenatis suscipit mauris. Cras eros metus, dapibus sed hendrerit ut, eleifend quis nunc. Aliquam sollicitudin, sapien sed semper consequat, sem nunc iaculis magna, non placerat nunc nisi ut sem. Praesent id nisi eu nibh rhoncus interdum eu quis velit. Suspendisse dapibus tincidunt nibh, at dapibus justo semper sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin iaculis cursus fermentum. Mauris scelerisque, metus sed sollicitudin ullamcorper, enim massa rutrum tortor, eu vehicula nulla orci at nisi. Nunc vestibulum iaculis ipsum, vel laoreet felis facilisis nec. Donec non consectetur sem. Fusce diam odio, ultricies id consequat at, cursus eu elit. Nam sed molestie purus. Nulla ac lacus purus. Cras augue ante, semper ultrices tempor ut, consequat eu turpis.</p>', '', -2, 0, 0, 2, '2012-04-17 12:40:20', 42, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-04-17 12:40:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 5, '', '', 1, 16, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(39, 89, 'Металлопластиковые трубы', 'металлопластиковые-трубы', '', '<p><img src="images/metalloplastikovye-truby.jpg" border="0" alt="Металлопластиковые трубы" width="250" style="float: left; margin: 5px 10px;" /></p>\r\n<p>Металлопластиковые трубы — это комбинация пяти слоев сделанных на основе алюминиевой ленты, которая с помощью ультразвукового метода сваривается внахлест. На эту ленту с внутренней и наружной стороны наносятся слои клея и полиэтилена, отличающегося высокой плотностью.</p>\r\n<h4>Особенности металлопластиковых труб</h4>\r\n<p>Лучшие свойства пластика и металла гармонично слились в многослойных металлопластиковых трубах.</p>\r\n<p>Достоинства одного материала усиливаются преимуществами другого, восполняя недочеты слоя-компаньона.</p>\r\n<p><strong>Например, всем известно, что металлическим трубам при контакте с водой свойственны:</strong></p>\r\n<ul>\r\n<li>токсичность;</li>\r\n<li>коррозия;</li>\r\n<li>образование накипи;</li>\r\n<li>потеря напора;</li>\r\n<li>жесткость.</li>\r\n</ul>\r\n<p>Использование трубы, созданной из полиэтилена PE-X, компенсируют эти негативные качества. Ведь пластик защищает металл от агрессивного воздействия воды.</p>\r\n<p><strong>У пластика также есть свои недостатки:</strong></p>\r\n<ul>\r\n<li>пропускает ультрафиолетовые лучи и кислород;</li>\r\n<li>отличается неустойчивостью и высокой степенью теплового расширения.</li>\r\n</ul>\r\n<p>Алюминиевая труба компенсирует слабые стороны пластика.</p>\r\n<h4>Использование металлопластиковых труб</h4>\r\n<p>Системы водоснабжения (как горячего, так и холодного) и системы для подогрева пола, а также водяное отопление зданий с помощью радиаторов трудно представить без использования металлопластиковых труб, потому что они обладают практическими достоинствами и высокой технологичностью. Итак, →</p>\r\n<h4>Преимущества металлопластиковых труб</h4>\r\n<ul>\r\n<li>срок использования — более 50 лет (при условии соблюдения правил эксплуатации);</li>\r\n<li>устойчивость к коррозии;</li>\r\n<li>устойчивость к перепадам температуры и давления;</li>\r\n<li>отсутствие токсичности;</li>\r\n<li>легкий, простой монтаж;</li>\r\n<li>небольшой вес (легкость);</li>\r\n<li>прочность;</li>\r\n<li>гибкость;</li>\r\n<li>устойчивая форма;</li>\r\n<li>термостойкость и химическая стойкость;</li>\r\n<li>низкая теплопроводность и тепловое расширение;</li>\r\n<li>электробезопасность;</li>\r\n<li>кислородная непроницаемость;</li>\r\n<li>малые потери давления;</li>\r\n<li>компактная упаковка. </li>\r\n</ul>\r\n<h4>Установка</h4>\r\n<p>Каким бы идеальным не был исходный материал, неумелое обращение может свести на «нет» все его достоинства.</p>\r\n<p><strong>Компания EUROTHERM</strong> <strong>— признанный лидер среди компаний, специализирующихся на отоплении и водоснабжении. </strong></p>\r\n<p>Тщательное изучение потребностей клиента, а именно — изучение особенностей здания и систем отопления и водоснабжения, гарантируют качество выполненных работ. Ведь залог успеха — комплексный подход к изготовлению труб с необходимым диаметром, а также фитингов к ним.</p>\r\n<p>Позвоните на номера</p>\r\n<ul>\r\n<li><strong>+38 (050) 194 89 99</strong></li>\r\n<li><strong>+38 (067) 423 33 86</strong></li>\r\n</ul>\r\n<p>заказывайте установку отопительных или водопроводных систем, и прослужат вам многие годы.</p>', '', 1, 0, 0, 2, '2012-08-27 07:48:54', 42, '', '2012-08-28 13:35:56', 42, 42, '2012-08-28 13:35:56', '2012-08-27 07:48:54', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 2, '', '', 1, 1056, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(40, 90, 'Краны шаровые', 'краны-шаровые', '', '<h2>Кран шаровый СТБ 607</h2>\r\n<table>\r\n<tbody>\r\n<tr valign="top">\r\n<td>\r\n<p> </p>\r\n</td>\r\n<td style="padding-left: 17px;">\r\n<p>Кран шаровый латунный со стальным шаром, внутренней и наружной резьбами и металлической бабочкой.</p>\r\n<p style="text-align: center;"><img src="images/stories/kran_7.JPG" border="0" alt="" /> <img src="images/stories/kran_8.JPG" border="0" alt="" /></p>\r\n<p><strong>Таблица размеров</strong></p>\r\n<table class="pad" border="1" cellspacing="3" cellpadding="3">\r\n<tbody>\r\n<tr>\r\n<td style="text-align: center;"><strong>Артикул</strong></td>\r\n<td style="text-align: center;"><strong>Размер</strong></td>\r\n<td style="text-align: center;"><strong>Количество в ящике, шт.</strong></td>\r\n<td style="text-align: center;"><strong>Вес ящика, кг</strong></td>\r\n<td style="text-align: center;"><strong>Цена в у.е.</strong></td>\r\n<td style="text-align: center;"><strong>Цена в грн.</strong></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6070012</td>\r\n<td style="text-align: center;">1/2</td>\r\n<td style="text-align: center;">250</td>\r\n<td style="text-align: center;">33</td>\r\n<td style="text-align: center;">1.2</td>\r\n<td style="text-align: center;"> 9.84 </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6070034</td>\r\n<td style="text-align: center;">3/4</td>\r\n<td style="text-align: center;">160</td>\r\n<td style="text-align: center;">32</td>\r\n<td style="text-align: center;">1.75</td>\r\n<td style="text-align: center;">14.35</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6070010</td>\r\n<td style="text-align: center;">1</td>\r\n<td style="text-align: center;">100</td>\r\n<td style="text-align: center;">27</td>\r\n<td style="text-align: center;">2.45</td>\r\n<td style="text-align: center;">20.09</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<div class="leading-1">\r\n<h2>Кран шаровый СТБ 605</h2>\r\n<table>\r\n<tbody>\r\n<tr valign="top">\r\n<td>\r\n<p> </p>\r\n</td>\r\n<td style="padding-left: 17px;">\r\n<p>Кран шаровый латунный со стальным шаром, внутренней и наружной резьбами и металлической ручкой.</p>\r\n<p style="text-align: center;"><img src="images/stories/kran_5.JPG" border="0" alt="" /> <img src="images/stories/kran_6.JPG" border="0" alt="" /></p>\r\n<p><strong>Таблица размеров</strong></p>\r\n<table class="pad" border="1" cellspacing="3" cellpadding="3">\r\n<tbody>\r\n<tr>\r\n<td style="text-align: center;"><strong>Артикул</strong></td>\r\n<td style="text-align: center;"><strong>Размер</strong></td>\r\n<td style="text-align: center;"><strong>Количество в ящике, шт.</strong></td>\r\n<td style="text-align: center;"><strong>Вес ящика, кг</strong></td>\r\n<td style="text-align: center;"><strong>Цена в у.е.</strong></td>\r\n<td style="text-align: center;"><strong>Цена в грн.</strong></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6050012</td>\r\n<td style="text-align: center;">1/2</td>\r\n<td style="text-align: center;">200</td>\r\n<td style="text-align: center;">28</td>\r\n<td style="text-align: center;">1.2</td>\r\n<td style="text-align: center;">9.84</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6050034</td>\r\n<td style="text-align: center;">3/4</td>\r\n<td style="text-align: center;">140</td>\r\n<td style="text-align: center;">26</td>\r\n<td style="text-align: center;">1.75</td>\r\n<td style="text-align: center;">14.35</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6050010</td>\r\n<td style="text-align: center;">1</td>\r\n<td style="text-align: center;">100</td>\r\n<td style="text-align: center;">30</td>\r\n<td style="text-align: center;">2.45</td>\r\n<td style="text-align: center;">20.09</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6050114</td>\r\n<td style="text-align: center;">1 1/4</td>\r\n<td style="text-align: center;">40</td>\r\n<td style="text-align: center;">20</td>\r\n<td style="text-align: center;">4.1</td>\r\n<td style="text-align: center;">33.62</td>\r\n</tr>\r\n<tr>\r\n<td>6050112</td>\r\n<td style="text-align: center;">1 1/2</td>\r\n<td style="text-align: center;">40</td>\r\n<td style="text-align: center;">26</td>\r\n<td style="text-align: center;">6</td>\r\n<td style="text-align: center;">49.2</td>\r\n</tr>\r\n<tr>\r\n<td>6050002</td>\r\n<td style="text-align: center;">2</td>\r\n<td style="text-align: center;">20</td>\r\n<td style="text-align: center;">22</td>\r\n<td style="text-align: center;"> 9.2    </td>\r\n<td style="text-align: center;">75.44  </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class="leading-2">\r\n<h2>Кран шаровый СТБ 602</h2>\r\n<table>\r\n<tbody>\r\n<tr valign="top">\r\n<td>\r\n<p> </p>\r\n</td>\r\n<td style="padding-left: 17px;">\r\n<p>Кран шаровый латунный со стальным шаром, внутренними резьбами и металлической бабочкой.</p>\r\n<p style="text-align: center;"><img src="images/stories/kran_3.JPG" border="0" alt="" /> <img src="images/stories/kran_4.JPG" border="0" alt="" /></p>\r\n<p><strong>Таблица размеров</strong></p>\r\n<table class="pad" border="1" cellspacing="3" cellpadding="3">\r\n<tbody>\r\n<tr>\r\n<td><strong>Артикул</strong></td>\r\n<td><strong>Размер</strong></td>\r\n<td><strong>Количество в ящике, шт.</strong></td>\r\n<td><strong>Вес ящика, кг</strong></td>\r\n<td><strong>Цена в у.е.</strong></td>\r\n<td><strong>Цена в грн.</strong></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6020012</td>\r\n<td style="text-align: center;">1/2</td>\r\n<td style="text-align: center;">300</td>\r\n<td style="text-align: center;">38</td>\r\n<td style="text-align: center;">1.14   </td>\r\n<td style="text-align: center;">9.35   </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6020034</td>\r\n<td style="text-align: center;">3/4</td>\r\n<td style="text-align: center;">200</td>\r\n<td style="text-align: center;">38</td>\r\n<td style="text-align: center;">1.75</td>\r\n<td style="text-align: center;">14.35</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6020010</td>\r\n<td style="text-align: center;">1</td>\r\n<td style="text-align: center;">100</td>\r\n<td style="text-align: center;">27</td>\r\n<td style="text-align: center;">2.45</td>\r\n<td style="text-align: center;">20.09</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<div class="leading-3">\r\n<h2>Кран шаровый СТБ 600</h2>\r\n<table>\r\n<tbody>\r\n<tr valign="top">\r\n<td>\r\n<p> </p>\r\n</td>\r\n<td style="padding-left: 17px;">\r\n<p>Кран шаровый латунный со стальным шаром, внутренними резьбами и металлической ручкой.</p>\r\n<p style="text-align: center;"><img src="images/stories/kran_1.JPG" border="0" alt="" /> <img src="images/stories/kran_2.JPG" border="0" alt="" /></p>\r\n<p><strong>Таблица размеров</strong></p>\r\n<table class="pad" border="1" cellspacing="3" cellpadding="3">\r\n<tbody>\r\n<tr>\r\n<td><strong>Артикул</strong></td>\r\n<td><strong>Размер</strong></td>\r\n<td><strong>Количество в ящике, шт.</strong></td>\r\n<td><strong>Вес ящика, кг</strong></td>\r\n<td><strong><strong>Цена в у.е.</strong></strong></td>\r\n<td><strong>Цена в грн.</strong></td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000012</td>\r\n<td style="text-align: center;">1/2</td>\r\n<td style="text-align: center;">200</td>\r\n<td style="text-align: center;">27</td>\r\n<td style="text-align: center;">1.14  </td>\r\n<td style="text-align: center;">  </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000034</td>\r\n<td style="text-align: center;">3/4</td>\r\n<td style="text-align: center;">140</td>\r\n<td style="text-align: center;">25</td>\r\n<td style="text-align: center;">1.75</td>\r\n<td style="text-align: center;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000010</td>\r\n<td style="text-align: center;">1</td>\r\n<td style="text-align: center;">100</td>\r\n<td style="text-align: center;">30</td>\r\n<td style="text-align: center;">2.45</td>\r\n<td style="text-align: center;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000114</td>\r\n<td style="text-align: center;">1 1/4</td>\r\n<td style="text-align: center;">40</td>\r\n<td style="text-align: center;">20</td>\r\n<td style="text-align: center;"> </td>\r\n<td style="text-align: center;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000112</td>\r\n<td style="text-align: center;">1 1/2</td>\r\n<td style="text-align: center;">27</td>\r\n<td style="text-align: center;">27</td>\r\n<td style="text-align: center;"> </td>\r\n<td style="text-align: center;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">6000002</td>\r\n<td style="text-align: center;">2</td>\r\n<td style="text-align: center;">20</td>\r\n<td style="text-align: center;">22</td>\r\n<td style="text-align: center;"> </td>\r\n<td style="text-align: center;"> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '', 1, 0, 0, 2, '2012-09-26 09:05:20', 42, '', '2012-10-04 13:29:10', 42, 0, '0000-00-00 00:00:00', '2012-09-26 09:05:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 1, '', '', 1, 436, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `o1hap_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(41, 91, 'Стальные радиаторы EUROTHERM', 'стальные-радиаторы-eurotherm', '', '<p style="text-align: justify;"><img src="images/brands/1.png" border="?" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /><strong>Стальные панельные радиаторы EUROTHERM</strong> отличаются высокой надежностью и отличной тепловой производительностью. <br /><strong>Материалы</strong><img src="images/eurol_1.png" border="?" width="197" height="190" style="float: right; margin-left: 7px; margin-right: 7px;" />: Для производства радиаторов EUROTHERM используется листовая холоднокатаная сталь, а это — сталь очень высокого качества (ее толщина равняется 1,10 мм). <br /><strong><br />Безопасность</strong>: Прежде, чем попасть на прилавки магазинов, каждый радиатор подвергается тестированию под давлением 13 атмосфер.<br />Вредные вещества в процессе нагревания этих радиаторов не выделяются, потому что для их окрашивания применяется электростатический метод.<br /><strong><br />Удобство</strong>: Установка радиаторов EUROTHERM на стену может производиться прямо в защитной коробке — это предохранит прибор от неблагоприятного воздействия внешней среды (например, ремонтной пыли) и повреждений в результате механического воздействия.</p>\r\n<table style="width: 739px;" dir="ltr" border="1" cellspacing="0" cellpadding="2"><colgroup><col width="72" /> <col width="217" /> <col width="217" /> <col width="216" /></colgroup>\r\n<tbody>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle"><strong>№</strong></td>\r\n<td style="text-align: center; width: 217px;" valign="middle"><strong>Длинна (мм)</strong></td>\r\n<td style="text-align: center; width: 217px;" valign="middle"><strong>Цена у.е.</strong></td>\r\n<td style="text-align: center; width: 217px;" valign="middle"><strong>Цена грн</strong></td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">1</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x400</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">27.54</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">225.83</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">2</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x500</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">32.13</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">263.47</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">3</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x600</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">38.25</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">313.65</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">4</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x700</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">43.86</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">359.65</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">5</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x800</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">49.47</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">405.65</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">6</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x900</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">54.06</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">443.29</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">7</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1000</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">58.65</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">480.93</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">3</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1100</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">63.75</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">522.75</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">9</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1200</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">68.85</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">564.57</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">10</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1300</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">11</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1400</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">80.58</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">660.76</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">12</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1500</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">13</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1600</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">91.80</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">752.76</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">14</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1700</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">15</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1800</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">102.00</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">836.4</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">16</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x1900</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">17</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x2000</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">120.36</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">986.95</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">18</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x2400</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n</tr>\r\n<tr valign="top">\r\n<td style="text-align: center; width: 217px;" valign="middle">19</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">500x2600</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0</td>\r\n<td style="text-align: center; width: 217px;" valign="middle">0 </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<h3>Почему стальные радиаторы</h3>\r\n<p>Надоело мерзнуть в квартирах зимой? Строите свой дом и стоите перед выбором типа радиаторов? Надоели старые ржавые батареи советских времен?</p>\r\n<p>Тогда следует купить стальные радиаторы отопления, ведь это — решение теплового вопроса в вашем жилище.</p>\r\n<p><strong>Почему именно стальные радиаторы?</strong></p>\r\n<p>Потому что они обладают низкой тепловой инерцией и характеризуются высокой теплоотдачей. А эти качества гарантируют эффективность эксплуатации отопительных приборов данного типа в системах отопления.</p>\r\n<h3>Общие характеристики радиаторов</h3>\r\n<p>По теплопроводности они находятся посередине между алюминиевыми и чугунными.</p>\r\n<p>Изготавливаются из штампованных стальных листов, отличающихся устойчивостью к коррозии. Листы компонуются в вертикальные параллельные каналы, объединенные горизонтальным коллектором.</p>\r\n<p><strong>Радиаторы изготавливаются:</strong></p>\r\n<ul>\r\n<li>однорядными</li>\r\n<li>двухрядными</li>\r\n<li>трехрядными</li>\r\n<li>с оребрением или без.</li>\r\n</ul>\r\n<p>Покрываются они многослойной эмалью, устойчивой к высоким температурам.</p>\r\n<p><strong>Эксплуатационные характеристики</strong></p>\r\n<p>Основные эксплуатационные качества радиаторов отопления зависят от устройства (конструкции) и от толщины стенок. Как правило, они выдерживают давление от 6 до 10 атмосфер, а температура теплоносителя — максимум 120°С, опрессовочное давление — максимум 13 атмосфер.</p>\r\n<p><strong>Где применяются</strong></p>\r\n<p>Рекомендуем купить стальной радиатор для закрытых систем отопления. Дело в том, что, невзирая на явные достоинства, эти отопительные приборы очень чувствительны к кислороду, растворенному в теплоносителе, что и является их слабым местом.</p>\r\n<p>Опустошение системы (слив воды) усугубляет ситуацию: коррозия при попадании воздуха усиливается.</p>\r\n<p>Вывод: стальные радиаторы следует купить для использования в закрытых, автономных системах отопления. При этом теплоноситель должен содержать минимальное количество кислорода.</p>\r\n<h3>Виды стальных радиаторов</h3>\r\n<p>Нынешний рынок тепловых приборов предлагает три вида:</p>\r\n<ul>\r\n<li><strong>секционные</strong><br />секции изготавливаются методом штамповки из листовой стали. Их легко очищать от пыли. Однако максимальное давление для них — 6 атмосфер.</li>\r\n<li><strong>трубчатые</strong><br />внешние панели удобно очищать от пыли; рабочее давление от 10 до 15 атмосфер; обеспечивают комфортное тепло, характеризуются высокой прочностью.</li>\r\n<li><strong>панельные</strong><br />отличаются высокой прочностью и легким уходом; рабочее давление 10-15 атмосфер; уровень теплового комфорта — высокий.</li>\r\n</ul>\r\n<h3>Преимущества</h3>\r\n<p>Если сравнивать стальной радиатор с биметаллическим или чугунным, то обнаружатся явные преимущества:</p>\r\n<ol start="1">\r\n<li>По сравнению с чугунным, стальной — <strong>более производителен</strong>.</li>\r\n</ol>\r\n<p>Считается, что чугунный радиатор дольше держит тепло, поэтому его ставить выгоднее.</p>\r\n<p>Однако это не совсем так. Подсчеты показывают, что для достижения отдачи одинакового количества тепла чугунным и стальным радиаторами необходимо, чтобы по чугунному прошел объем воды больший в 7 раз и при этом температура воды должна быть на 20° С выше.</p>\r\n<p>Достижение высокой теплоотдачи в чугунном материале требует высокого расхода топлива и установки насосов — а это дополнительные расходы.</p>\r\n<ol start="2">\r\n<li>По сравнению со стальным радиатором, биметаллический уступает в технических показателях: например, <strong>объем циркуляционной воды — снижен</strong>.</li>\r\n</ol>\r\n<p><strong>Итак, наша продукция — самое выгодное решение, потому что:</strong></p>\r\n<ul>\r\n<li><strong>поверхность теплообмена — более развита</strong></li>\r\n<li>помещение прогревает — быстро</li>\r\n<li><strong>энергия на нагрев самого себя — не тратится</strong></li>\r\n</ul>\r\n<p>и поэтому наша компания предлагает &gt;</p>\r\n<h3>Купить стальной радиатор</h3>\r\n<p>Если вы хотите сократить расходы на отопление, то стальные радиаторы от EUROTHERM (Турция) порадуют вас своими эксплуатационными свойствами.</p>\r\n<p>Для оформления заказа или уточнения подробностей покупки, звоните:</p>\r\n<ul>\r\n<li><strong>+38 (050) 194 89 99</strong></li>\r\n<li><strong>+38 (067) 423 33 86</strong></li>\r\n</ul>\r\n<p>Продажа осуществляется по всей Украине: Харьков, Киев, Днепропетровск, Симферополь, Одесса, Донецк, Запорожье и другие города.</p>', '', 1, 0, 0, 2, '2012-12-05 13:01:15', 42, '', '2012-12-05 13:10:04', 42, 42, '2014-05-27 14:02:18', '2012-12-05 13:01:15', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 0, '', '', 1, 3265, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `o1hap_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_content_rating`
--

CREATE TABLE IF NOT EXISTS `o1hap_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `o1hap_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_djimageslider`
--

CREATE TABLE IF NOT EXISTS `o1hap_djimageslider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_extensions`
--

CREATE TABLE IF NOT EXISTS `o1hap_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10062 ;

--
-- Dumping data for table `o1hap_extensions`
--

INSERT INTO `o1hap_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"legacy":false,"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MAILTO_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"legacy":false,"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_BANNERS_XML_DESCRIPTION","group":""}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2008 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONTACT_XML_DESCRIPTION","group":""}', '{"show_contact_category":"hide","show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"ru-RU","site":"ru-RU"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MEDIA_XML_DESCRIPTION","group":""}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html","enable_flash":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_word_count":"0","show_headings":"1","show_name":"1","show_articles":"0","show_link":"1","show_description":"1","show_description_image":"1","display_num":"","show_pagination_limit":"1","show_pagination":"1","show_pagination_results":"1","show_cat_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_SEARCH_XML_DESCRIPTION","group":""}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(21, 'com_weblinks', 'component', 'com_weblinks', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_weblinks","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_WEBLINKS_XML_DESCRIPTION","group":""}', '{"show_comp_description":"1","comp_description":"","show_link_hits":"1","show_link_description":"1","show_other_cats":"0","show_headings":"0","show_numbers":"0","show_report":"1","count_clicks":"1","target":"0","link_icons":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONTENT_XML_DESCRIPTION","group":""}', '{"article_layout":"_:default","show_title":"1","link_titles":"0","show_intro":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_readmore":"0","show_readmore_title":"0","readmore_limit":"100","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"0","show_pagination_results":"0","show_feed_link":"1","feed_summary":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10012, 'virtuemart', 'component', 'com_virtuemart', '', 1, 0, 0, 0, '{"legacy":true,"name":"VIRTUEMART","type":"component","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2012 Virtuemart Team. All rights reserved.","authorEmail":"max|at|virtuemart.net","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_USERS_XML_DESCRIPTION","group":""}', '{"allowUserRegistration":"1","new_usertype":"2","useractivation":"1","frontend_userparams":"1","mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"legacy":false,"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_FINDER_XML_DESCRIPTION","group":""}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(100, 'PHPMailer', 'library', 'phpmailer', '', 0, 1, 1, 1, '{"legacy":false,"name":"PHPMailer","type":"library","creationDate":"2001","author":"PHPMailer","copyright":"(c) 2001-2003, Brent R. Matzelle, (c) 2004-2009, Andy Prevost. All Rights Reserved., (c) 2010-2011, Jim Jagielski. All Rights Reserved.","authorEmail":"jimjag@gmail.com","authorUrl":"https:\\/\\/code.google.com\\/a\\/apache-extras.org\\/p\\/phpmailer\\/","version":"5.2","description":"LIB_PHPMAILER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"legacy":false,"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"legacy":false,"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"11.4","description":"LIB_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters.\\n\\t\\tAll rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_RELATED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(217, 'mod_weblinks', 'module', 'mod_weblinks', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_weblinks","type":"module","creationDate":"July 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WEBLINKS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"legacy":false,"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FINDER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_STATUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_TITLE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":""}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_VERSION_XML_DESCRIPTION","group":""}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"legacy":false,"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":""}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 42, '2012-05-03 18:13:33', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"legacy":false,"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"legacy":false,"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LDAP_XML_DESCRIPTION","group":""}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":""}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(405, 'plg_content_geshi', 'plugin', 'geshi', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_geshi","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"","authorUrl":"qbnz.com\\/highlighter","version":"2.5.0","description":"PLG_CONTENT_GESHI_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":""}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":""}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":""}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_VOTE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"1.0","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":""}', '{"linenumbers":"0","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_none","type":"plugin","creationDate":"August 2004","author":"Unknown","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"2.5.0","description":"PLG_NONE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2013","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com\\/","version":"3.5.4.1","description":"PLG_TINY_XML_DESCRIPTION","group":""}', '{"mode":"2","skin":"0","entity_encoding":"raw","lang_mode":"0","lang_code":"en","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","toolbar":"top","toolbar_align":"left","html_height":"550","html_width":"750","resizing":"true","resize_horizontal":"false","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","format_date":"%Y-%m-%d","inserttime":"1","format_time":"%H:%M:%S","colors":"1","table":"1","smilies":"1","media":"1","hr":"1","directionality":"1","fullscreen":"1","style":"1","layer":"1","xhtmlxtras":"1","visualchars":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advimage":"1","advlink":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_READMORE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(421, 'plg_search_weblinks', 'plugin', 'weblinks', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_weblinks","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"legacy":false,"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_P3P_XML_DESCRIPTION","group":""}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"legacy":false,"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CACHE_XML_DESCRIPTION","group":""}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":""}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LOG_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0);
INSERT INTO `o1hap_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEF_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 1, '{"legacy":false,"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":""}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"legacy":false,"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2009 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":""}', '{"autoregister":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 1, '{"legacy":false,"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":""}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"legacy":false,"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"legacy":false,"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"legacy":false,"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"legacy":false,"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 1, 1, 0, '{"legacy":false,"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":""}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"legacy":false,"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"legacy":false,"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"legacy":false,"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"legacy":false,"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(446, 'plg_finder_weblinks', 'plugin', 'weblinks', 'finder', 0, 1, 1, 0, '{"legacy":false,"name":"plg_finder_weblinks","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_FINDER_WEBLINKS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(500, 'atomic', 'template', 'atomic', '', 0, 1, 1, 0, '{"legacy":false,"name":"atomic","type":"template","creationDate":"10\\/10\\/09","author":"Ron Severdia","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"contact@kontentdesign.com","authorUrl":"http:\\/\\/www.kontentdesign.com","version":"2.5.0","description":"TPL_ATOMIC_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(502, 'bluestork', 'template', 'bluestork', '', 1, 1, 1, 0, '{"legacy":false,"name":"bluestork","type":"template","creationDate":"07\\/02\\/09","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"TPL_BLUESTORK_XML_DESCRIPTION","group":""}', '{"useRoundedCorners":"1","showSiteName":"0","textBig":"0","highContrast":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez_20', 'template', 'beez_20', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez_20","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ2_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"legacy":false,"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"2.5.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":""}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(505, 'beez5', 'template', 'beez5', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez5","type":"template","creationDate":"21 May 2010","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ5_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","html5":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (United Kingdom)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"legacy":false,"name":"English (United Kingdom)","type":"language","creationDate":"2008-03-15","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.19","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (United Kingdom)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"legacy":false,"name":"English (United Kingdom)","type":"language","creationDate":"2008-03-15","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.19","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"files_joomla","type":"file","creationDate":"April 2014","author":"Joomla! Project","copyright":"(C) 2005 - 2014 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.20","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(800, 'PKG_JOOMLA', 'package', 'pkg_joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"PKG_JOOMLA","type":"package","creationDate":"2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"2.5.0","description":"PKG_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 'Russian', 'language', 'ru-RU', '', 0, 1, 0, 0, '{"legacy":true,"name":"Russian","type":"language","creationDate":"2012-04-02","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"2.5.4.1","description":"Russian language pack (site) for Joomla! 2.5","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'Russian', 'language', 'ru-RU', '', 1, 1, 0, 0, '{"legacy":true,"name":"Russian","type":"language","creationDate":"2012-04-02","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"2.5.4.1","description":"Russian language pack (administrator) for Joomla! 2.5","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'ru-RU', 'package', 'pkg_ru-RU', '', 0, 1, 1, 0, '{"legacy":false,"name":"Russian Language Pack","type":"package","creationDate":"Unknown","author":"Unknown","copyright":"","authorEmail":"","authorUrl":"","version":"2.5.4.1","description":"Joomla 2.5 Russian Language Package","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'DisplayNews', 'module', 'mod_dn', '', 0, 1, 0, 0, '{"legacy":false,"name":"DisplayNews","type":"module","creationDate":"2014-Apr-30","author":"BK","copyright":"(C) 2006 - 2013 BK. All rights reserved.","authorEmail":"bkomraz1@gmail.com","authorUrl":"http:\\/\\/joomla.rjews.net","version":"2.7","description":"DISPLAY_NEWS_BY_BK_MODULE","group":""}', '{"set_count":"5","minus_leading":"","ordering":"mostrecent","show_tooltips":"0","style":"blog","set_column":"1","moduleclass_sfx":"","set_date_today":"0","set_date_range":"","set_date_month":"","set_date_year":"","set_category_type":"","set_category_id":"","set_article_type":"0","set_article_id":"","set_article_id_extra":"","set_state":"0","set_article_archived_id":"","set_article_archived_id_extra":"","set_author_name":"","set_auto_author":"0","set_related":"0","set_metakeys":"","show_frontpage":"y","hide_current":"0","set_access":"","show_title":"","link_titles":"","text_hover_title":"","title_tag":"span class=\\"title\\"","filter_title":"0","length_limit_title":"20","truncate_ending_title":"1","truncate_ending_title_sign":"...","show_text":"1","link_text":"0","filter_text":"0","preserve_tags":"<img>","limit_text":"0","length_limit_text":"100","truncate_ending":"1","truncate_ending_sign":"...","text_hover_text":"","image":"1","link_image":"0","image_num":"","image_scale":"bestfit","image_bg":"FFFFFF","image_type":"","image_width":"","image_height":"","image_size":"","image_align":"0","image_margin":"","image_class":"0","image_class_name":"","image_default":"0","image_default_file":"","show_category":"","link_category":"","text_hover_category":"","show_more_auto":"0","text_more":"","text_hover_more_category":"","use_modify_date":"0","show_date":"","format_date":"","show_author":"","show_readmore":"2","text_readmore":"","text_hover_readmore":"","show_vote":"","show_hits":"","show_jcomment_counter":"0","scroll_direction":"","scroll_speed":"1","scroll_delay":"30","scroll_mouse_ctrl":"1","scroll_height":"100","lead_space":"50","tail_space":"80","link_type":"0","link_target":"","window_width":"","window_height":"","window_menubar":"0","window_directories":"0","window_location":"0","window_resizable":"0","window_scrollbars":"0","window_status":"0","window_toolbar":"0","article_link":"0","item_id_type":"0","item_id_cat_type":"0","use_rows_template":"1","format":"%t <br\\/>%c<br\\/>%a - %d<br\\/>%b<br\\/>%p%i<br\\/>%m<div class=\\"item-separator\\"> <\\/div>","row_template":"($title_out!='''' ? \\"$title_out\\" : '''').  ($title_out!='''' && $style != ''blog'' && $style != ''featured'' ? \\"<br\\/>\\" : ''''). ($rate_out!='''' ? \\"$rate_out<br\\/>\\" : '''').  ($cat_out!='''' ? \\"$cat_out\\" : '''').  ($cat_out!='''' ? ''<br\\/>'' : '''').  ($author_out!='''' ? \\"$author_out\\" : '''').  ($author_out!='''' && $date_out!='''' ? '' - '' : '''').  ($date_out!='''' ? \\"$date_out\\" : '''').  ($author_out.$date_out!='''' ? ''<br\\/>'' : '''').  ($before_out!='''' ? \\"$before_out<br\\/>\\" : '''').  ($img_out!='''' ? \\"$img_out\\" : '''').  ($text_out!='''' ? \\"$text_out\\" : '''').  ($hits_out!='''' ? \\"($hits_out)\\" : '''').  ($jcomments_out<>'''' ? \\"($jcomments_out)\\" : '''').  ($readmore_out!='''' ? \\"<br\\/>$readmore_out\\" : '''').  (!$last ? ''<div class=\\"item-separator\\"> <\\/div>'' : '''')","show_empty_module":"0","show_title_auto":"0","mod_title_format":"<h3>%c<\\/h3>","use_module_template":"1","module_format":"%t %c %s %r %f %m","module_template":"($mod_title_out != '''' ? \\"$mod_title_out\\" : ''''). $mod_cat_out. $scroll_start. $rows_out. $scroll_finish. ($mod_automore_out != '''' ? $mod_automore_out :'''' )","on_prepare_content_plugins":"1","before_display_content_plugins":"0","force_builtin_rating":"0","rating_txt":"","jcomments":"0","cache":"1","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'PLG_SYSTEM_SOURCERER', 'plugin', 'sourcerer', 'system', 0, 1, 1, 0, '{"legacy":true,"name":"PLG_SYSTEM_SOURCERER","type":"plugin","creationDate":"January 2012","author":"NoNumber! (Peter van Westen)","copyright":"Copyright \\u00a9 2011 NoNumber! All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"2.11.4","description":"PLG_SYSTEM_SOURCERER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 'Button - Sourcerer', 'plugin', 'sourcerer', 'editors-xtd', 0, 1, 1, 0, '{"legacy":true,"name":"Button - Sourcerer","type":"plugin","creationDate":"January 2012","author":"NoNumber! (Peter van Westen)","copyright":"Copyright \\u00a9 2011 NoNumber! All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"2.11.4","description":"PLG_EDITORS-XTD_SOURCERER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 'euro-therm', 'template', 'euro-therm', '', 0, 1, 1, 0, '{"legacy":false,"name":"euro-therm","type":"template","creationDate":"25 November 2012","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ2_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 'Extended Menu', 'module', 'mod_exmenu', '', 0, 1, 0, 0, '{"legacy":false,"name":"Extended Menu","type":"module","creationDate":"November 2011","author":"Daniel Ecer","copyright":"(C) 2005-2011 Daniel Ecer","authorEmail":"","authorUrl":"de.siteof.de","version":"snapshot-20111110 (build 86)","description":"\\n\\t\\n\\tThis Extended Menu module displays a menu in a flexible way.\\n\\tPlease visit <a href=\\"http:\\/\\/de.siteof.de\\/extended-menu.html\\">de.siteof.de<\\/a> for more details.<br \\/>\\n\\tTo use this module don''t forget to publish it (and probably unpublish the default mainmenu module).<br \\/>\\n\\t<br \\/>\\n\\t(If you like this module please consider a <a href=\\"http:\\/\\/de.siteof.de\\/donate.html\\">donation<\\/a>.)\\n\\t\\n\\t","group":""}', '{"class_sfx":"","id_suffix":"","moduleclass_sfx":"","cache":"0","menutype":"","menu_source_type":"menu","menu_source_value":"","menu_source_show_category":"default","menu_source_show_content_item":"default","menu_source_order":"ordering","default_content_itemid":"","menu_style":"list_tree","menu_view_plugin_name":"","menu_images":"0","expand_menu":"0","expand_min":"","max_depth":"10","hide_first":"0","show_parent":"0","parent_item":"","current_level_begin":"0","level_begin":"","split_menu":"","menu_count":"1","query_cache":"0","parse_access_key":"3","title_attribute":"0","level_class":"0","active_menu_class":"0","element_id":"0","menu_template":"1","menu_template_name":"menu.html","spacer":"","end_spacer":"","select_list_submit_text":"Go","select_list_submit_hide":"0","call_getitemid":"1","current_item":"smart","current_item_duplicates":"convert_active","access_keys":"","exact_access_level":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 'Vinaora Cu3er 3D Slideshow', 'module', 'mod_vinaora_cu3er_3d_slideshow', '', 0, 1, 0, 0, '{"legacy":false,"name":"Vinaora Cu3er 3D Slideshow","type":"module","creationDate":"March 2012","author":"VINAORA","copyright":"Copyright (C) 2010-2012 VINAORA. All rights reserved.","authorEmail":"info@vinaora.com","authorUrl":"http:\\/\\/vinaora.com","version":"2.5.3","description":"MOD_VINAORA_CU3ER_3D_SLIDESHOW_DESC","group":""}', '{"config_custom":"-1","slide_panel_width":"","slide_panel_height":"","slide_panel_horizontal_align":"left","slide_panel_vertical_align":"top","ui_visibility_time":"3","border_width":"0","border_color":"#000000","border_style":"solid","border_rounded":"1","border_shadow":"1","slide_dir":"-1","slide_url":"","slide_link":"","slide_link_target":"","slide_description_heading":"","slide_description_paragraph":"","slide_description_link":"","slide_description_link_target":"","transition_type":"auto","transition_num":"","transition_slicing":"","transition_direction":"","transition_duration":"","transition_delay":"","transition_shader":"","transition_light_position":"","transition_cube_color":"#333333","transition_z_multiplier":"","enable_description_box":"1","swffont":"-1","description_round_corners":"0, 0, 0, 0","description_heading_font":"Georgia","description_heading_text_size":"18","description_heading_text_color":"#000000","description_heading_text_align":"left","description_heading_text_margin":"10 , 25 , 0 , 25","description_heading_text_leading":"0","description_heading_text_letterSpacing":"0","description_paragraph_font":"Arial","description_paragraph_text_size":"12","description_paragraph_text_color":"#000000","description_paragraph_text_align":"left","description_paragraph_text_margin":"5, 25, 0, 25","description_paragraph_text_leading":"0","description_paragraph_text_letterSpacing":"0","enable_auto_play":"1","auto_play_symbol":"circular","auto_play_time_defaults":"5","enable_preloader":"1","preloader_symbol":"circular","enable_prev_button":"1","prev_button_round_corners":"0, 0, 0, 0","enable_prev_symbol":"1","prev_symbol_type":"3","enable_next_button":"1","next_button_round_corners":"0, 0, 0, 0","enable_next_symbol":"1","next_symbol_type":"3","enable_debug":"0","debug_x":"5","debug_y":"5","cache":"1","cache_time":"900","cachemode":"static","swfobject_source":"local","swfobject_version":"2.2","flash_wmode":"transparent","description_time":"","description_delay":"","description_x":"","description_y":"","description_width":"","description_height":"","description_rotation":"","description_alpha":"","description_tint":"","description_scaleX":"","description_scaleY":"","auto_play_time":"","auto_play_delay":"","auto_play_x":"","auto_play_y":"","auto_play_width":"","auto_play_height":"","auto_play_rotation":"","auto_play_alpha":"","auto_play_tint":"","auto_play_scaleX":"","auto_play_scaleY":"","preloader_time":"","preloader_delay":"","preloader_x":"","preloader_y":"","preloader_width":"","preloader_height":"","preloader_rotation":"","preloader_alpha":"","preloader_tint":"","preloader_scaleX":"","preloader_scaleY":"","prev_button_time":"","prev_button_delay":"","prev_button_x":"","prev_button_y":"","prev_button_width":"","prev_button_height":"","prev_button_rotation":"","prev_button_alpha":"","prev_button_tint":"","prev_button_scaleX":"","prev_button_scaleY":"","prev_symbol_time":"","prev_symbol_delay":"","prev_symbol_x":"","prev_symbol_y":"","prev_symbol_width":"","prev_symbol_height":"","prev_symbol_rotation":"","prev_symbol_alpha":"","prev_symbol_tint":"","prev_symbol_scaleX":"","prev_symbol_scaleY":"","next_button_time":"","next_button_delay":"","next_button_x":"","next_button_y":"","next_button_width":"","next_button_height":"","next_button_rotation":"","next_button_alpha":"","next_button_tint":"","next_button_scaleX":"","next_button_scaleY":"","next_symbol_time":"","next_symbol_delay":"","next_symbol_x":"","next_symbol_y":"","next_symbol_width":"","next_symbol_height":"","next_symbol_rotation":"","next_symbol_alpha":"","next_symbol_tint":"","next_symbol_scaleX":"","next_symbol_scaleY":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 'Shaper Carousel Module', 'module', 'mod_shaper_carousel', '', 0, 1, 0, 0, '{"legacy":false,"name":"Shaper Carousel Module","type":"module","creationDate":"Apr 2011","author":"JoomShaper.com","copyright":"Copyright (C) 2010 - 2012 JoomShaper.com. All rights reserved.","authorEmail":"support@joomshaper.com","authorUrl":"www.joomshaper.com","version":"1.9.0","description":"Joomla Carousel Module by JoomShaper.com","group":""}', '{"moduleclass_sfx":"","ctype":"0","path":"","catid":"","count":"5","ordering":"a.title","ordering_direction":"ASC","user_id":"0","show_featured":"","width":"85","height":"60","tamount":"6","amount":"4","cssinfo":"1","cache":"1","cache_time":"900","cachemode":"itemid"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10011, 'mod_news_pro_gk4', 'module', 'mod_news_pro_gk4', '', 0, 1, 0, 0, '{"legacy":false,"name":"mod_news_pro_gk4","type":"module","creationDate":"05\\/04\\/2012","author":"Gavick.com","copyright":"(C) 2009-2012 Gavick.com","authorEmail":"info@gavick.com","authorUrl":"www.gavick.com","version":"GK4 3.1.1","description":"\\n\\t\\t<style type=''text\\/css''>span.readonly { padding: 10px; font-family: Arial; font-size:13px !important; font-weight: normal !important; text-align: justify; color: #4d4d4d; line-height: 24px; } span.readonly h1 { clear:both; font-family: Georgia, sans-serif; font-size:38px; margin:30px 20px 23px; padding:0px 0px 24px 10px; color:#333; border-bottom: 1px solid #eee; font-weight: normal; } span.readonly p { margin: 0 26px 10px } span.readonly p a { color: #ab3f0a } span.readonly p.homepage { margin-top: 30px } span.readonly p.license { border-top: 1px solid #eee; font-size: 11px; margin: 30px 26px 0; padding: 6px 0; }<\\/style><span class=''readonly''><h1>News Show Pro GK4 <small>ver. 3.1.1<\\/small><\\/h1><p>GK4 is new generation of our extensions dedicated to Joomla 1.6+.<\\/p><p>Basing on experiences gathered, we created probably the best tool to present articles for Joomla! Huge amount of options and possibilities of formatting causes News Show Pro GK4 is a complex tool in making www pages content attractive. News Show Pro is appropriate while preparing simple structures with an individual article, the same as, complex arrangements including few articles and a list of links.<\\/p> <p class=''homepage''><a href=''http:\\/\\/tools.gavick.com\\/newshowpro.html'' target=''_blank''>Learn more at the NSP GK4 project website.<\\/a><\\/p><p class=''license''>News Show Pro GK4 is released under the <a target=\\"_blank\\" href=\\"http:\\/\\/www.gnu.org\\/licenses\\/gpl-2.0.html\\">GNU\\/GPL v2 license.<\\/a><\\/p><\\/span>\\n\\t","group":""}', '{"moduleclass_sfx":"","automatic_module_id":"1","module_unique_id":"newspro1","module_mode":"normal","module_width":"100","portal_mode_1_module_height":"320","module_font_size":"100","data_source":"com_categories","com_categories":"","com_articles":"","k2_categories":"","k2_tags":"","k2_articles":"","redshop_categories":"","redshop_products":"","vm_products":"","vm_categories":"","news_sort_value":"created","news_sort_order":"DESC","news_since":"","news_frontpage":"1","unauthorized":"0","only_frontpage":"0","startposition":"0","time_offset":"0","news_portal_mode_1_amount":"10","news_portal_mode_2_amount":"10","news_portal_mode_3_amount":"10","news_portal_mode_4_amount":"10","news_full_pages":"3","news_column":"1","news_rows":"1","top_interface_style":"arrows_with_pagination","news_content_header_pos":"left","news_content_header_float":"none","news_header_link":"1","use_title_alias":"0","title_limit_type":"chars","title_limit":"40","news_content_image_pos":"left","news_content_image_float":"left","news_image_link":"1","news_content_text_pos":"left","news_content_text_float":"left","news_text_link":"0","news_limit_type":"words","news_limit":"30","news_content_info_pos":"left","news_content_info_float":"none","news_content_info2_pos":"left","news_content_info2_float":"left","info_format":"%DATE %HITS %CATEGORY %AUTHOR","info2_format":"","category_link":"1","date_format":"d-m-Y","date_publish":"0","username":"0","user_avatar":"1","avatar_size":"16","news_content_rs_pos":"left","news_content_rs_float":"none","art_padding":"0 20px 20px 0","news_header_order":"1","news_header_enabled":"1","news_image_order":"3","news_image_enabled":"1","news_text_order":"4","news_text_enabled":"1","news_info_order":"2","news_info_enabled":"1","news_info2_order":"5","news_info2_enabled":"1","news_rs_store_order":"6","news_rs_store_enabled":"1","news_content_readmore_pos":"right","news_readmore_enabled":"1","news_short_pages":"3","links_amount":"3","links_columns_amount":"1","bottom_interface_style":"arrows_with_counter","links_margin":"0","links_position":"bottom","links_width":"50","show_list_description":"1","list_title_limit_type":"words","list_title_limit":"20","list_text_limit_type":"words","list_text_limit":"30","memory_limit":"128M","create_thumbs":"0","k2_thumbs":"first","thumb_image_type":"full","img_auto_scale":"1","img_keep_aspect_ratio":"0","img_width":"160","img_height":"120","img_margin":"6px 14px 0 0","img_bg":"#000","img_stretch":"0","img_quality":"95","cache_time":"30","simple_crop_editor":"","simple_crop_top":"10","simple_crop_bottom":"10","simple_crop_left":"10","simple_crop_right":"10","crop_rules":"","autoanim":"0","hover_anim":"0","animation_speed":"400","animation_interval":"5000","news_portal_mode_3_open_first":"1","animation_function":"Fx.Transitions.Expo.easeIn","clean_xhtml":"1","more_text_value":"...","parse_plugins":"0","clean_plugins":"0","rs_out_of_stock":"1","rs_add_to_cart":"0","rs_price":"0","rs_price_text":"0","rs_currency_place":"before","rs_price_with_vat":"1","rs_show_default_cart_button":"0","vm_itemid":"9999","vm_shopper_group":"-1","vm_out_of_stock":"1","vm_show_price_type":"base","vm_show_price_with_tax":"0","vm_add_to_cart":"0","vm_show_discount_amount":"0","vm_show_tax":"0","vm_display_type":"text_price","k2store_support":"0","k2store_show_cart":"0","k2store_add_to_cart":"0","k2store_price":"0","k2store_price_text":"0","k2store_currency_place":"before","useCSS":"1","useScript":"2"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 'virtuemart_allinone', 'component', 'com_virtuemart_allinone', '', 1, 1, 0, 0, '{"legacy":true,"name":"VirtueMart_allinone","type":"component","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2012 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 'VM - Payment, Standard', 'plugin', 'standard', 'vmpayment', 0, 1, 1, 0, '{"legacy":true,"name":"VMPAYMENT_STANDARD","type":"plugin","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"Standard payment plugin","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 'VM - Payment, Payzen', 'plugin', 'payzen', 'vmpayment', 0, 1, 1, 0, '{"legacy":true,"name":"VM - Payment, PayZen","type":"plugin","creationDate":"April 2012","author":"Lyra Network","copyright":"Copyright Lyra Network.","authorEmail":"support@payzen.eu","authorUrl":"http:\\/\\/www.lyra-network.com","version":"2.0.6","description":"\\n    \\t<a href=\\"http:\\/\\/www.lyra-network.com\\" target=\\"_blank\\">PayZen<\\/a> is a multi bank payment provider. \\n    ","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 'VM - Payment, SystemPay', 'plugin', 'systempay', 'vmpayment', 0, 1, 1, 0, '{"legacy":true,"name":"VM - Payment, Systempay","type":"plugin","creationDate":"April 2012","author":"Lyra Network","copyright":"Copyright Lyra Network.","authorEmail":"supportvad@lyra-network.com","authorUrl":"http:\\/\\/www.lyra-network.com","version":"2.0.6","description":"\\n    \\t<a href=\\"http:\\/\\/www.lyra-network.com\\" target=\\"_blank\\">Systempay<\\/a> is a multi bank payment provider. \\n    ","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 'VM - Payment, Authorize.net', 'plugin', 'authorizenet', 'vmpayment', 0, 1, 1, 0, '{"legacy":true,"name":"VM Payment - authorize.net AIM","type":"plugin","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 'VM - Payment, Paypal', 'plugin', 'paypal', 'vmpayment', 0, 1, 1, 0, '{"legacy":true,"name":"VMPAYMENT_PAYPAL","type":"plugin","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"<a href=\\"http:\\/\\/paypal.com\\" target=\\"_blank\\">PayPal<\\/a> is a popular\\n\\tpayment provider and available in many countries. \\n    ","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 'VM - Shipment, By weight, ZIP and countries', 'plugin', 'weight_countries', 'vmshipment', 0, 1, 1, 0, '{"legacy":true,"name":"VMSHIPMENT_WEIGHT_COUNTRIES","type":"plugin","creationDate":"April 2012","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2012 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"VMSHIPMENT_WEIGHT_COUNTRIES_PLUGIN_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 'VM - Custom, customer text input', 'plugin', 'textinput', 'vmcustom', 0, 1, 1, 0, '{"legacy":true,"name":"VMCustom - textinput","type":"plugin","creationDate":"June 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.6","description":"text input plugin for product","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10021, 'VM - Custom, product specification', 'plugin', 'specification', 'vmcustom', 0, 1, 1, 0, '{"legacy":true,"name":"VMCustom - specification","type":"plugin","creationDate":"June 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"text input plugin for product","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10022, 'VM - Custom, stockable variants', 'plugin', 'stockable', 'vmcustom', 0, 1, 1, 0, '{"legacy":true,"name":"VMCUSTOM_STOCKABLE","type":"plugin","creationDate":"June 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"1.9.8","description":"VMCUSTOM_STOCKABLE_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10023, 'VM - Search, Virtuemart Product', 'plugin', 'virtuemart', 'search', 0, 0, 1, 0, '{"legacy":false,"name":"plg_search_virtuemart","type":"plugin","creationDate":"Febuary 2010","author":"P.Kohl - Studio42","copyright":"Copyright (C) 2010 Virtuemart. All rights reserved.","authorEmail":"contact@st42.fr","authorUrl":"www.virtuemart.net","version":"1.5","description":"Allows Searching of VirtueMart Component","group":""}', '{"search_limit":"50"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10024, 'mod_virtuemart_currencies', 'module', 'mod_virtuemart_currencies', '', 0, 1, 1, 0, '{"legacy":true,"name":"mod_virtuemart_currencies","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_CURRENCIES_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(10025, 'mod_virtuemart_product', 'module', 'mod_virtuemart_product', '', 0, 1, 1, 0, '{"legacy":true,"name":"mod_virtuemart_product","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_PRODUCT_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(10026, 'mod_virtuemart_search', 'module', 'mod_virtuemart_search', '', 0, 1, 1, 0, '{"legacy":true,"name":"mod_virtuemart_search","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_SEARCH_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(10027, 'mod_virtuemart_manufacturer', 'module', 'mod_virtuemart_manufacturer', '', 0, 1, 1, 0, '{"legacy":true,"name":"mod_virtuemart_manufacturer","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_MANUFACTURER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(10028, 'mod_virtuemart_cart', 'module', 'mod_virtuemart_cart', '', 0, 1, 1, 0, '{"legacy":true,"name":"VirtueMart Shopping Cart","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_CART_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10029, 'mod_virtuemart_category', 'module', 'mod_virtuemart_category', '', 0, 1, 1, 0, '{"legacy":true,"name":"mod_virtuemart_category","type":"module","creationDate":"February 2011","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2011 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2.0.0RC3","description":"MOD_VIRTUEMART_CATEGORY_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(10030, 'ru-RU VirtueMart language package', 'file', 'ru_ru_virtuemart_language_pack', '', 0, 1, 0, 0, '{"legacy":false,"name":"ru-RU VirtueMart language package","type":"file","creationDate":"2012-04-06 11:50","author":"The VirtueMart Development Team","copyright":"Copyright (C) 2004-2012 Virtuemart Team. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.virtuemart.net","version":"2012-04-06 11:50","description":"ru-RU VirtueMart language package","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10031, 'com_xmap', 'component', 'com_xmap', '', 1, 1, 0, 0, '{"legacy":true,"name":"com_xmap","type":"component","creationDate":"2011-04-10","author":"Guillermo Vargas","copyright":"This component is released under the GNU\\/GPL License","authorEmail":"guille@vargas.co.cr","authorUrl":"http:\\/\\/joomla.vargas.co.cr","version":"2.2.1","description":"Xmap - Sitemap Generator for Joomla!","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10032, 'Xmap - Content Plugin', 'plugin', 'com_content', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - Content Plugin","type":"plugin","creationDate":"01\\/26\\/2011","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0.3","description":"Add support for articles and categories on Joomla 1.6 or newer","group":""}', '{"expand_categories":"1","expand_featured":"1","include_archived":"2","show_unauth":"0","add_pagebreaks":"1","max_art":"0","max_art_age":"0","add_images":"0","cat_priority":"-1","cat_changefreq":"-1","art_priority":"-1","art_changefreq":"-1","keywords":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10033, 'Xmap - Kunena Plugin', 'plugin', 'com_kunena', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - Kunena Plugin","type":"plugin","creationDate":"September 2007","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0.2","description":"Xmap Plugin for Kunena component","group":""}', '{"include_topics":"1","max_topics":"","max_age":"","cat_priority":"-1","cat_changefreq":"-1","topic_priority":"-1","topic_changefreq":"-1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10034, 'Xmap - SobiPro Plugin', 'plugin', 'com_sobipro', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - SobiPro Plugin","type":"plugin","creationDate":"07\\/15\\/2011","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0.1","description":"Xmap Plugin for SobiPro component","group":""}', '{"include_entries":"1","max_entries":"","max_age":"","entries_order":"a.ordering","entries_orderdir":"DESC","cat_priority":"-1","cat_changefreq":"weekly","entry_priority":"-1","entry_changefreq":"weekly"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10035, 'Xmap - Mosets Tree Plugin', 'plugin', 'com_mtree', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - Mosets Tree Plugin","type":"plugin","creationDate":"07\\/20\\/2011","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0.2","description":"XMAP_MTREE_PLUGIN_DESCRIPTION","group":""}', '{"cats_order":"cat_name","cats_orderdir":"ASC","include_links":"1","links_order":"ordering","entries_orderdir":"ASC","max_links":"","max_age":"","cat_priority":"0.5","cat_changefreq":"weekly","link_priority":"0.5","link_changefreq":"weekly"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10036, 'Xmap - Virtuemart Plugin', 'plugin', 'com_virtuemart', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - Virtuemart Plugin","type":"plugin","creationDate":"January 2012","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0.0","description":"XMAP_VM_PLUGIN_DESCRIPTION","group":""}', '{"include_products":"1","cat_priority":"-1","cat_changefreq":"-1","prod_priority":"-1","prod_changefreq":"-1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10037, 'Xmap - WebLinks Plugin', 'plugin', 'com_weblinks', 'xmap', 0, 0, 1, 0, '{"legacy":false,"name":"Xmap - WebLinks Plugin","type":"plugin","creationDate":"Apr 2004","author":"Guillermo Vargas","copyright":"GNU GPL","authorEmail":"guille@vargas.co.cr","authorUrl":"joomla.vargas.co.cr","version":"2.0","description":"XMAP_WL_PLUGIN_DESCRIPTION","group":""}', '{"include_links":"1","max_links":"","cat_priority":"-1","cat_changefreq":"-1","link_priority":"-1","link_changefreq":"-1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10038, 'xmap', 'package', 'pkg_xmap', '', 0, 1, 1, 0, '{"legacy":false,"name":"Xmap Package","type":"package","creationDate":"Unknown","author":"Unknown","copyright":"","authorEmail":"","authorUrl":"","version":"2.0.0","description":"The Site Map generator for Joomla!","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10039, 'chronoforms', 'plugin', 'chronoforms', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"chronoforms","type":"plugin","creationDate":"26 Dec 2011","author":"ChronoEngine.com","copyright":"(C) ChronoEngine.com","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"V4 RC3.0","description":"\\n\\tThis plugin must have ChronoForms component in order for it to work.\\n\\tYou just need to put the name of the form you want to show between : {chronoforms}&{\\/chronoforms}, Example: {chronoforms}form1{\\/chronoforms} where form1 is the form name which will be displayed!!\\n\\t","group":""}', '{"after_text_forms":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10040, 'chronoforms', 'component', 'com_chronoforms', '', 1, 1, 0, 0, '{"legacy":false,"name":"ChronoForms","type":"component","creationDate":"6.April.2012","author":"Chronoman","copyright":"ChronoEngine.com 2011","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"4.0 RC3.3","description":"Create everytype of Forms with whatever features you like!!","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10041, 'ChronoForms', 'module', 'mod_chronoforms', '', 0, 1, 0, 0, '{"legacy":false,"name":"ChronoForms","type":"module","creationDate":"26 Dec 2011","author":"ChronoEngine.com","copyright":"Copyright (C) 2006 - 2011 ChronoEngine.com. All rights reserved.","authorEmail":"webmaster@chronoengine.com","authorUrl":"www.chronoengine.com","version":"V4 RC3.0","description":"Show a Chronoform, works on J1.6 and with ChronoForms V4","group":""}', '{"cache":"0","chronoform":"","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10042, 'com_djimageslider', 'component', 'com_djimageslider', '', 1, 1, 0, 0, '{"legacy":false,"name":"com_djimageslider","type":"component","creationDate":"January 2011","author":"Blue Constant Media LTD","copyright":"Copyright (C) 2010 Blue Constant Media LTD, All rights reserved.","authorEmail":"contact@design-joomla.eu","authorUrl":"http:\\/\\/design-joomla.eu","version":"1.3.0 RC1","description":"Create custom slides for DJ Image Slider module","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10043, 'DJ-Image Slider', 'module', 'mod_djimageslider', '', 0, 1, 0, 0, '{"legacy":false,"name":"DJ-Image Slider","type":"module","creationDate":"January 2011","author":"Blue Constant Media LTD","copyright":"Copyright (C) 2010 Blue Constant Media LTD, All rights reserved.","authorEmail":"contact@design-joomla.eu","authorUrl":"http:\\/\\/design-joomla.eu","version":"1.3.RC6","description":"DJ-Image Slider Module","group":""}', '{"slider_source":"0","slider_type":"0","link_image":"1","image_folder":"images\\/sampledata\\/fruitshop","link":"","show_title":"1","show_desc":"1","show_readmore":"0","link_title":"1","link_desc":"0","limit_desc":"","image_width":"150","image_height":"90","fit_to":"0","visible_images":"3","space_between_images":"10","max_images":"20","sort_by":"1","effect":"Cubic","autoplay":"1","show_buttons":"1","show_arrows":"1","show_custom_nav":"0","desc_width":"","desc_bottom":"0","desc_horizontal":"0","left_arrow":"","right_arrow":"","play_button":"","pause_button":"","arrows_top":"30","arrows_horizontal":"5","effect_type":"0","duration":"","delay":"","preload":"800","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `o1hap_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10044, 'Multithumb', 'plugin', 'multithumb', 'content', 0, 0, 1, 0, '{"legacy":true,"name":"Multithumb","type":"plugin","creationDate":"2011-05-16","author":"Boris Komraz | Until Version 2.0 alpha 3 for Joomla 1.5 Martin Larsen","copyright":"(C) 2007-2008 Martin Larsen, (C) 2009-2011 Boris Komraz","authorEmail":"bkomraz1@gmail.com","authorUrl":"http:\\/\\/joomla.rjews.net\\/bk-multithumb","version":"2.5.0.3","description":"MULTITHUMB_DESCR","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10045, 'PLG_SYSTEM_TABBER', 'plugin', 'tabber', 'system', 0, 1, 1, 0, '{"legacy":true,"name":"PLG_SYSTEM_TABBER","type":"plugin","creationDate":"May 2012","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2012 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"2.1.0FREE","description":"PLG_SYSTEM_TABBER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10046, 'PLG_EDITORS-XTD_TABBER', 'plugin', 'tabber', 'editors-xtd', 0, 1, 1, 0, '{"legacy":true,"name":"PLG_EDITORS-XTD_TABBER","type":"plugin","creationDate":"May 2012","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2012 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"2.1.0FREE","description":"PLG_EDITORS-XTD_TABBER_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10047, 'PLG_SYSTEM_NNFRAMEWORK', 'plugin', 'nnframework', 'system', 0, 1, 1, 0, '{"legacy":true,"name":"PLG_SYSTEM_NNFRAMEWORK","type":"plugin","creationDate":"May 2012","author":"NoNumber (Peter van Westen)","copyright":"Copyright \\u00a9 2012 NoNumber All Rights Reserved","authorEmail":"peter@nonumber.nl","authorUrl":"http:\\/\\/www.nonumber.nl","version":"12.5.1","description":"PLG_SYSTEM_NNFRAMEWORK_DESC","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10048, 'com_zoo', 'component', 'com_zoo', '', 1, 1, 0, 0, '{"legacy":false,"name":"com_zoo","type":"component","creationDate":"September 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.10","description":"ZOO component for Joomla 2.5+ developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10049, 'ZOO Category', 'module', 'mod_zoocategory', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZOO Category","type":"module","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Category module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{"theme":"","application":"","depth":"0","add_count":"0","menu_item":"","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10050, 'ZOO Comment', 'module', 'mod_zoocomment', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZOO Comment","type":"module","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Comment module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{"theme":"","application":"","subcategories":"0","count":"10","show_avatar":"1","avatar_size":"40","show_author":"1","show_meta":"1","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10051, 'ZOO Item', 'module', 'mod_zooitem', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZOO Item","type":"module","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.1","description":"Item module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{"theme":"","layout":"","media_position":"left","application":"","subcategories":"0","count":"4","order":"_itemname","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10052, 'ZOO Quick Icons', 'module', 'mod_zooquickicon', '', 1, 1, 2, 0, '{"legacy":false,"name":"ZOO Quick Icons","type":"module","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Quick Icons module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10053, 'ZOO Tag', 'module', 'mod_zootag', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZOO Tag","type":"module","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Tag module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{"theme":"","application":"","subcategories":"0","count":"10","order":"alpha","menu_item":"","moduleclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10054, 'Content - ZOO Shortcode', 'plugin', 'zooshortcode', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"Content - ZOO Shortcode","type":"plugin","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Shortcode plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com) Usage: {zooitem OR zoocategory: ID OR alias} Optionally: {zooitem: ID text: MYTEXT}","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10055, 'Smart Search - ZOO', 'plugin', 'zoosmartsearch', 'finder', 0, 0, 1, 0, '{"legacy":false,"name":"Smart Search - ZOO","type":"plugin","creationDate":"Febuary 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"2.5.0","description":"Smart Search plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10056, 'Search - ZOO', 'plugin', 'zoosearch', 'search', 0, 0, 1, 0, '{"legacy":false,"name":"Search - ZOO","type":"plugin","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Search plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{"search_fulltext":"0","search_limit":"50"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10057, 'System - ZOO Event', 'plugin', 'zooevent', 'system', 0, 0, 1, 0, '{"legacy":false,"name":"System - ZOO Event","type":"plugin","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.0","description":"Event plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10058, 'zoo', 'package', 'pkg_zoo', '', 0, 1, 1, 0, '{"legacy":false,"name":"ZOO Package","type":"package","creationDate":"October 2012","author":"YOOtheme","copyright":"Copyright (C) YOOtheme GmbH","authorEmail":"info@yootheme.com","authorUrl":"http:\\/\\/www.yootheme.com","version":"3.0.10","description":"ZOO component and extensions for Joomla 2.5+ developed by YOOtheme (http:\\/\\/www.yootheme.com)","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10059, 'JBZoo App', 'file', 'file_jbuniversal', '', 0, 1, 0, 0, '{"legacy":false,"name":"JBZoo App","type":"file","creationDate":"2013 05","author":"JBZoo.com","copyright":"Copyright (C) JBZoo.com","authorEmail":"admin@bjbzoo.com","authorUrl":"http:\\/\\/jbzoo.com","version":"2.0 (Free)","description":"JBZoo is universal CCK, application for YooTheme Zoo Joomla component","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10060, 'System - JBZoo (events)', 'plugin', 'jbzoo', 'system', 0, 0, 1, 0, '{"legacy":false,"name":"System - JBZoo (events)","type":"plugin","creationDate":"2013 05","author":"JBZoo.com","copyright":"JBZoo.com","authorEmail":"admin@jbzoo.com","authorUrl":"http:\\/\\/jbzoo.com","version":"2.0 (Free)","description":"Event plugin for JBZoo by JBZoo.com","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10061, 'jbzoo', 'package', 'pkg_jbzoo', '', 0, 1, 1, 0, '{"legacy":false,"name":"JBZoo App","type":"package","creationDate":"2013 05","author":"JBZoo.com","copyright":"Copyright (C) JBZoo","authorEmail":"admin@jbzoo.com","authorUrl":"http:\\/\\/jbzoo.com","version":"2.0 (Free)","description":"\\n        JBZoo is universal CCK, application for YooTheme Zoo Joomla component. Developed by JBZoo.com\\n    ","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_filters`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_finder_taxonomy`
--

INSERT INTO `o1hap_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_terms`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_finder_terms_common`
--

INSERT INTO `o1hap_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_finder_types`
--

CREATE TABLE IF NOT EXISTS `o1hap_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_languages`
--

CREATE TABLE IF NOT EXISTS `o1hap_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_languages`
--

INSERT INTO `o1hap_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_menu`
--

CREATE TABLE IF NOT EXISTS `o1hap_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'The relative ordering of the menu item in the tree.',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(333)),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `o1hap_menu`
--

INSERT INTO `o1hap_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `ordering`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 135, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 22, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 20, 21, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 23, 28, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 24, 25, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 26, 27, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 43, 44, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 33, 34, 0, '*', 1),
(18, 'menu', 'com_weblinks', 'Weblinks', '', 'Weblinks', 'index.php?option=com_weblinks', 'component', 0, 1, 1, 21, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 35, 40, 0, '*', 1),
(19, 'menu', 'com_weblinks_links', 'Links', '', 'Weblinks/Links', 'index.php?option=com_weblinks', 'component', 0, 18, 2, 21, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 36, 37, 0, '*', 1),
(20, 'menu', 'com_weblinks_categories', 'Categories', '', 'Weblinks/Categories', 'index.php?option=com_categories&extension=com_weblinks', 'component', 0, 18, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks-cat', 0, '', 38, 39, 0, '*', 1),
(21, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 31, 32, 0, '*', 1),
(22, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 0, 1, 1, 28, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 41, 42, 0, '*', 1),
(101, 'mainmenu', 'Главная', 'главная', '', 'главная', 'index.php?option=com_content&view=article&id=7', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"0","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u0421\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0430\\u043d\\u0435\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b EUROTHERM","show_page_heading":0,"page_heading":"\\u0421\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u043f\\u0430\\u043d\\u0435\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b EUROTHERM","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 29, 30, 1, '*', 0),
(102, 'mainmenu', 'Товары', 'tovary', '', 'tovary', 'index.php?option=com_content&view=article&id=2', 'component', 0, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 45, 46, 0, '*', 0),
(103, 'mainmenu', 'О компании', 'o-kompanii', '', 'o-kompanii', 'index.php?option=com_content&view=article&id=3', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f EUROTHERM - \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0430 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 - \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0430","show_page_heading":0,"page_heading":"\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f EUROTHERM - \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0430 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 - \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0430","pageclass_sfx":"","menu-meta_description":"\\u041f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0430 \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445, \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0445 \\u0438 \\u0431\\u0438\\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f - \\u043e\\u0442 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u0415\\u0432\\u0440\\u043e\\u0442\\u0435\\u0440\\u043c. \\u041e\\u043f\\u0442 \\u0438 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0430. ","menu-meta_keywords":"","robots":"","secure":0}', 47, 48, 0, '*', 0),
(104, 'mainmenu', 'Бренды', 'brendy', '', 'brendy', 'index.php?option=com_content&view=article&id=4', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"1","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044c \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 - \\u0431\\u0440\\u0435\\u043d\\u0434\\u044b, \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438","show_page_heading":0,"page_heading":"\\u041f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044c \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 - \\u0431\\u0440\\u0435\\u043d\\u0434\\u044b, \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438","pageclass_sfx":"","menu-meta_description":"\\u041a\\u0442\\u043e \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f, \\u043a\\u0430\\u043a\\u0438\\u0445 \\u0442\\u0438\\u043f\\u043e\\u0432 \\u0438 \\u0432 \\u043a\\u0430\\u043a\\u043e\\u0439 \\u0441\\u0442\\u0440\\u0430\\u043d\\u0435.","menu-meta_keywords":"","robots":"","secure":0}', 49, 50, 0, '*', 0),
(105, 'mainmenu', 'Дистрибьюторам', 'distribyutoram', '', 'distribyutoram', 'index.php?option=com_content&view=article&id=5', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u0421\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u043e \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432","show_page_heading":0,"page_heading":"\\u0421\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u043e \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432","pageclass_sfx":"","menu-meta_description":"\\u041f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u0432\\u0437\\u0430\\u0438\\u043c\\u043e\\u0432\\u044b\\u0433\\u043e\\u0434\\u043d\\u043e\\u0435 \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043f\\u043e \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 \\u043d\\u0430 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435.","menu-meta_keywords":"","robots":"","secure":0}', 51, 52, 0, '*', 0),
(106, 'mainmenu', 'Контакты', 'kontakty', '', 'kontakty', 'index.php?option=com_content&view=article&id=6', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b. \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f EUROTHERM \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0430.","show_page_heading":0,"page_heading":"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b. \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f EUROTHERM \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0430.","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 53, 54, 0, '*', 0),
(107, 'katalog', 'Стальные радиаторы', 'stalnye-radiatory', '', 'stalnye-radiatory', '', 'separator', 1, 1, 1, 10012, 0, 42, '2012-12-05 09:54:04', 0, 1, '', 0, '{"menu_image":"","menu_text":1}', 55, 62, 0, '*', 0),
(108, 'katalog', 'EUROTHERM', 'eurotherm', '', 'stalnye-radiatory/eurotherm', 'index.php?option=com_content&view=article&id=41', 'component', 1, 107, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"0","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f - \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0422\\u0443\\u0440\\u0446\\u0438\\u044f","show_page_heading":1,"page_heading":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f - \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0422\\u0443\\u0440\\u0446\\u0438\\u044f","pageclass_sfx":"","menu-meta_description":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043e\\u043f\\u0442\\u043e\\u043c \\u0438 \\u0432 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0443 \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b eurotherm (\\u0442\\u0443\\u0440\\u0446\\u0438\\u044f) - \\u0432\\u044b\\u0433\\u043e\\u0434\\u043d\\u0430\\u044f \\u0446\\u0435\\u043d\\u0430 \\u043f\\u0440\\u044f\\u043c\\u043e\\u0433\\u043e \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u0449\\u0438\\u043a\\u0430.","menu-meta_keywords":"\\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b, \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c, \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u0442\\u0443\\u0440\\u0446\\u0438\\u044f, \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f, \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0441\\u0442\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440","robots":"","secure":0}', 56, 57, 0, '*', 0),
(138, 'main', 'COM_CHRONOFORMS_EASY_WIZARD', 'com_chronoforms_easy_wizard', '', 'com_chronoforms/com_chronoforms_easy_wizard', 'index.php?option=com_chronoforms&task=form_wizard&wizard_mode=easy', 'component', 0, 135, 2, 10040, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 116, 117, 0, '', 1),
(139, 'main', 'COM_CHRONOFORMS_VALIDATE', 'com_chronoforms_validate', '', 'com_chronoforms/com_chronoforms_validate', 'index.php?option=com_chronoforms&task=validatelicense', 'component', 0, 135, 2, 10040, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 118, 119, 0, '', 1),
(109, 'katalog', 'Стальные радиаторы подпункт 2', 'stalnye-radiatory-podpunkt-2', '', 'stalnye-radiatory/stalnye-radiatory-podpunkt-2', 'index.php?option=com_content&view=article&id=9', 'component', 0, 107, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 58, 59, 0, '*', 0),
(110, 'katalog', 'Стальные радиаторы подпункт 3', 'stalnye-radiatory-podpunkt-3', '', 'stalnye-radiatory/stalnye-radiatory-podpunkt-3', 'index.php?option=com_content&view=article&id=10', 'component', 0, 107, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 60, 61, 0, '*', 0),
(111, 'katalog', 'Алюминиевые радиаторы', 'alyuminievye-radiatory', '', 'alyuminievye-radiatory', '', 'separator', 1, 1, 1, 10012, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1}', 63, 70, 0, '*', 0),
(112, 'katalog', 'ИТАЛИЯ', 'italia', '', 'alyuminievye-radiatory/italia', 'index.php?option=com_content&view=article&id=30', 'component', 1, 111, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f \\u0438\\u0437 \\u0418\\u0442\\u0430\\u043b\\u0438\\u0438","show_page_heading":0,"page_heading":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f \\u0438\\u0437 \\u0418\\u0442\\u0430\\u043b\\u0438\\u0438","pageclass_sfx":"","menu-meta_description":"\\u0418\\u0442\\u0430\\u043b\\u044c\\u044f\\u043d\\u0441\\u043a\\u0438\\u0435 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u0444\\u0438\\u0440\\u043c: Global, FONDITAL, Nova Florida. \\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u043f\\u0442\\u043e\\u043c \\u0438 \\u0432 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0443.","menu-meta_keywords":"\\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b, \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u0438\\u0442\\u0430\\u043b\\u0438\\u044f, \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b","robots":"","secure":0}', 64, 65, 0, '*', 0),
(113, 'katalog', 'КИТАЙ', 'kitay', '', 'alyuminievye-radiatory/kitay', 'index.php?option=com_content&view=article&id=32', 'component', 1, 111, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u0410\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u041a\\u0438\\u0442\\u0430\\u0439 - \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435","show_page_heading":0,"page_heading":"\\u0410\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f \\u0438\\u0437 \\u041a\\u0438\\u0442\\u0430\\u044f - \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435","pageclass_sfx":"","menu-meta_description":"\\u041a\\u0438\\u0442\\u0430\\u0439\\u0441\\u043a\\u0438\\u0435 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439: All Termo, SANNYHETER. \\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043e\\u043f\\u0442\\u043e\\u043c \\u0438 \\u0432 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0443 \\u043f\\u043e \\u0432\\u044b\\u0433\\u043e\\u0434\\u043d\\u043e\\u0439 \\u0446\\u0435\\u043d\\u0435.","menu-meta_keywords":"\\u041a\\u0438\\u0442\\u0430\\u0439\\u0441\\u043a\\u0438\\u0435 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b, \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u043a\\u0438\\u0442\\u0430\\u0439, \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b","robots":"","secure":0}', 66, 67, 0, '*', 0),
(114, 'katalog', 'Алюминиевые радиаторы подпункт 3', 'alyuminievye-radiatory-podpunkt-3', '', 'alyuminievye-radiatory/alyuminievye-radiatory-podpunkt-3', 'index.php?option=com_content&view=article&id=13', 'component', 0, 111, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 68, 69, 0, '*', 0),
(115, 'katalog', 'Биметаллические радиаторы', 'bimetallicheskie-radiatory', '', 'bimetallicheskie-radiatory', '', 'separator', 1, 1, 1, 10012, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1}', 71, 78, 0, '*', 0),
(116, 'katalog', 'КИТАЙ', 'kitay', '', 'bimetallicheskie-radiatory/kitay', 'index.php?option=com_content&view=article&id=31', 'component', 1, 115, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u0411\\u0438\\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u041a\\u0438\\u0442\\u0430\\u0439 - \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435, \\u043e\\u043f\\u0442 \\u0438 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0430","show_page_heading":0,"page_heading":"\\u0411\\u0438\\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b \\u041a\\u0438\\u0442\\u0430\\u0439 - \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435, \\u043e\\u043f\\u0442 \\u0438 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0446\\u0430","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 72, 73, 0, '*', 0),
(117, 'katalog', 'Биметаллические радиаторы подпункт 2', 'bimetallicheskie-radiatory-podpunkt-2', '', 'bimetallicheskie-radiatory/bimetallicheskie-radiatory-podpunkt-2', 'index.php?option=com_content&view=article&id=15', 'component', 0, 115, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 74, 75, 0, '*', 0),
(118, 'katalog', 'Биметаллические радиаторы подпункт 3', 'bimetallicheskie-radiatory-podpunkt-3', '', 'bimetallicheskie-radiatory/bimetallicheskie-radiatory-podpunkt-3', 'index.php?option=com_content&view=article&id=16', 'component', 0, 115, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 76, 77, 0, '*', 0),
(119, 'mainmenu', 'Новости', 'novosti', '', 'novosti', 'index.php?option=com_content&view=category&layout=blog&id=9', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_title":"0","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"","link_titles":"1","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"1","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 79, 80, 0, '*', 0),
(120, 'нижнее-меню', 'Меню', 'меню', '', 'меню', '', 'separator', 1, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1}', 81, 96, 0, '*', 0),
(121, 'нижнее-меню', 'Главная', '2012-04-17-12-35-14', '', 'меню/2012-04-17-12-35-14', 'index.php?Itemid=', 'alias', 1, 120, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"101","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 82, 83, 0, '*', 0),
(122, 'нижнее-меню', 'О компании', '2012-04-17-12-36-53', '', 'меню/2012-04-17-12-36-53', 'index.php?Itemid=', 'alias', 1, 120, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"103","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 84, 85, 0, '*', 0),
(123, 'нижнее-меню', 'Бренды', 'бренды', '', 'меню/бренды', 'index.php?option=com_content&view=article&id=4', 'component', 1, 120, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 86, 87, 0, '*', 0),
(124, 'нижнее-меню', 'Акции', 'акции', '', 'меню/акции', 'index.php?option=com_content&view=article&id=27', 'component', 1, 120, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u0410\\u043a\\u0446\\u0438\\u0438 - \\u043f\\u043e\\u043a\\u0443\\u043f\\u043a\\u0430 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f","show_page_heading":0,"page_heading":"\\u0410\\u043a\\u0446\\u0438\\u0438 - \\u043f\\u043e\\u043a\\u0443\\u043f\\u043a\\u0430 \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u043e\\u0432 \\u043e\\u0442\\u043e\\u043f\\u043b\\u0435\\u043d\\u0438\\u044f","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 88, 89, 0, '*', 0),
(125, 'нижнее-меню', 'Дистрибьюторам', '2012-04-17-12-42-41', '', 'меню/2012-04-17-12-42-41', 'index.php?Itemid=', 'alias', 1, 120, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"105","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 90, 91, 0, '*', 0),
(126, 'нижнее-меню', 'Диллерская сеть', 'диллерская-сеть', '', 'меню/диллерская-сеть', 'index.php?option=com_content&view=article&id=28', 'component', -2, 120, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 92, 93, 0, '*', 0),
(127, 'нижнее-меню', 'Контакты', '2012-04-17-12-44-06', '', 'меню/2012-04-17-12-44-06', 'index.php?Itemid=', 'alias', 1, 120, 2, 8, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"106","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 94, 95, 0, '*', 0),
(128, 'main', 'COM_VIRTUEMART', 'com_virtuemart', '', 'com_virtuemart', 'index.php?option=com_virtuemart', 'component', 0, 1, 1, 10012, 0, 0, '0000-00-00 00:00:00', 0, 1, '../components/com_virtuemart/assets/images/vmgeneral/menu_icon.png', 0, '', 97, 98, 0, '', 1),
(129, 'main', 'VirtueMart AIO', 'virtuemart-aio', '', 'virtuemart-aio', 'index.php?option=com_virtuemart_allinone', 'component', 0, 1, 1, 10013, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 99, 100, 0, '', 1),
(140, 'main', 'COM_DJIMAGESLIDER', 'com_djimageslider', '', 'com_djimageslider', 'index.php?option=com_djimageslider', 'component', 0, 1, 1, 10042, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_djimageslider/assets/icon-16-dj.png', 0, '', 121, 126, 0, '', 1),
(130, 'main', 'COM_XMAP_TITLE', 'com_xmap_title', '', 'com_xmap_title', 'index.php?option=com_xmap', 'component', 0, 1, 1, 10031, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_xmap/images/xmap-favicon.png', 0, '', 101, 102, 0, '', 1),
(131, 'mainmenu', 'Карта сайта', 'карта-сайта', '', 'карта-сайта', 'index.php?option=com_xmap&view=html&id=1', 'component', 1, 1, 1, 10031, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u0430\\u0440\\u0442\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430 - \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b EUROTHERM | euro-therm.com.ua","show_page_heading":0,"page_heading":"\\u041a\\u0430\\u0440\\u0442\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0430 - \\u0440\\u0430\\u0434\\u0438\\u0430\\u0442\\u043e\\u0440\\u044b EUROTHERM | euro-therm.com.ua","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 103, 104, 0, '*', 0),
(132, 'боковая-навигация', 'Диллерская сеть', 'диллерская-сеть', '', 'диллерская-сеть', 'index.php?option=com_content&view=article&id=28', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 105, 106, 0, '*', 0),
(133, 'боковая-навигация', 'Специальные предложения', 'специальные-предложения', '', 'специальные-предложения', 'index.php?option=com_content&view=article&id=29', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 107, 108, 0, '*', 0),
(134, 'боковая-навигация', 'Акции и скидки', 'акции-и-скидки', '', 'акции-и-скидки', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 109, 110, 0, '*', 0),
(137, 'main', 'COM_CHRONOFORMS_WIZARD', 'com_chronoforms_wizard', '', 'com_chronoforms/com_chronoforms_wizard', 'index.php?option=com_chronoforms&task=form_wizard', 'component', 0, 135, 2, 10040, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 114, 115, 0, '', 1),
(135, 'main', 'COM_CHRONOFORMS', 'com_chronoforms', '', 'com_chronoforms', 'index.php?option=com_chronoforms', 'component', 0, 1, 1, 10040, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_chronoforms/CF.png', 0, '', 111, 120, 0, '', 1),
(136, 'main', 'COM_CHRONOFORMS_FORMS_MANAGER', 'com_chronoforms_forms_manager', '', 'com_chronoforms/com_chronoforms_forms_manager', 'index.php?option=com_chronoforms', 'component', 0, 135, 2, 10040, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 112, 113, 0, '', 1),
(141, 'main', 'SLIDES', 'slides', '', 'com_djimageslider/slides', 'index.php?option=com_djimageslider&view=items', 'component', 0, 140, 2, 10042, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 122, 123, 0, '', 1),
(142, 'main', 'CATEGORIES', 'categories', '', 'com_djimageslider/categories', 'index.php?option=com_categories&extension=com_djimageslider', 'component', 0, 140, 2, 10042, 0, 0, '0000-00-00 00:00:00', 0, 1, 'class:category', 0, '', 124, 125, 0, '', 1),
(143, 'боковая-навигация', 'Карта проезда', 'map', '', 'map', 'index.php?option=com_content&view=article&id=36', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 127, 128, 0, '*', 0),
(144, 'katalog', 'Металлопластиковые трубы', 'metalloplastikovye-truby', '', 'metalloplastikovye-truby', 'index.php?option=com_content&view=article&id=39', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u043e\\u043f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043a\\u043e\\u0432\\u044b\\u0435 \\u0442\\u0440\\u0443\\u0431\\u044b","show_page_heading":0,"page_heading":"\\u041a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u043e\\u043f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043a\\u043e\\u0432\\u044b\\u0435 \\u0442\\u0440\\u0443\\u0431\\u044b","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 129, 130, 0, '*', 0),
(145, 'katalog', 'Краны шаровые', 'купить-краны-шаровые', '', 'купить-краны-шаровые', 'index.php?option=com_content&view=article&id=40', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"0","link_titles":"0","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 131, 132, 0, '*', 0),
(147, 'main', 'com_zoo', 'com_zoo', '', 'com_zoo', 'index.php?option=com_zoo', 'component', 0, 1, 1, 10048, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_zoo/assets/images/zoo_16.png', 0, '', 133, 134, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_menu_types`
--

CREATE TABLE IF NOT EXISTS `o1hap_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `o1hap_menu_types`
--

INSERT INTO `o1hap_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Меню', 'The main menu for the site'),
(2, 'katalog', 'Каталог', ''),
(3, 'нижнее-меню', 'Нижнее меню', ''),
(4, 'боковая-навигация', 'Боковая навигация', '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_messages`
--

CREATE TABLE IF NOT EXISTS `o1hap_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `o1hap_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_modules`
--

CREATE TABLE IF NOT EXISTS `o1hap_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=127 ;

--
-- Dumping data for table `o1hap_modules`
--

INSERT INTO `o1hap_modules` (`id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 'Меню', '', '', 1, 'top-nav', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 0, '{"menutype":"mainmenu","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(2, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"Home","showComponent":"1","separator":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(79, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 'DisplayNews', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_dn', 1, 1, '', 0, '*'),
(88, 'Логотип', '', '<p><img src="images/logo.png" border="0" alt="" /></p>', 1, 'logo', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(89, 'Телефоны вверху', '', '<p>+38 (050) <span>194 89 99</span></p>\r\n<p>+38 (067)<span> 423 33 86</span></p>', 1, 'top-phoneNumer', 42, '2012-06-11 10:24:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(90, 'Поиск по сайту', '', '', 1, 'top-search', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 0, '{"label":"","width":"20","text":"\\u041f\\u043e\\u0438\\u0441\\u043a...","button":"1","button_pos":"right","imagebutton":"","button_text":"1","opensearch":"1","opensearch_title":"","set_itemid":"105","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(91, 'Extended Menu', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_exmenu', 1, 1, '', 0, '*'),
(92, 'Промо картинки', '', '', 1, 'promo-images', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_vinaora_cu3er_3d_slideshow', 1, 0, '{"lastedit":"20120423-152729","config_custom":"-1","slide_panel_width":"1000","slide_panel_height":"300","slide_panel_horizontal_align":"left","slide_panel_vertical_align":"top","ui_visibility_time":"3","border_width":"0","border_color":"#000000","border_style":"solid","border_rounded":"1","border_shadow":"1","footer":"-1","slide_dir":"promo-images","slide_url":"","slide_link":"","slide_link_target":"","slide_description_heading":"","slide_description_paragraph":"","slide_description_link":"","slide_description_link_target":"","transition_type":"auto","transition_num":"9","transition_slicing":"vertical","transition_direction":"right","transition_duration":"0.6","transition_delay":"","transition_shader":"","transition_light_position":"","transition_cube_color":"#333333","transition_z_multiplier":"","enable_description_box":"1","swffont":"-1","description_round_corners":"0, 0, 0, 0","description_heading_font":"Georgia","description_heading_text_size":"18","description_heading_text_color":"#000000","description_heading_text_align":"left","description_heading_text_margin":"10 , 25 , 0 , 25","description_heading_text_leading":"0","description_heading_text_letterSpacing":"0","description_paragraph_font":"Arial","description_paragraph_text_size":"12","description_paragraph_text_color":"#000000","description_paragraph_text_align":"left","description_paragraph_text_margin":"5, 25, 0, 25","description_paragraph_text_leading":"0","description_paragraph_text_letterSpacing":"0","enable_auto_play":"1","auto_play_symbol":"circular","auto_play_time_defaults":"5","enable_preloader":"0","preloader_symbol":"circular","enable_prev_button":"0","prev_button_round_corners":"0, 0, 0, 0","enable_prev_symbol":"0","prev_symbol_type":"3","enable_next_button":"0","next_button_round_corners":"0, 0, 0, 0","enable_next_symbol":"0","next_symbol_type":"3","enable_debug":"0","debug_x":"5","debug_y":"5","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","swfobject_source":"local","swfobject_version":"2.2","flash_wmode":"transparent","description_time":"","description_delay":"","description_x":"","description_y":"","description_width":"","description_height":"","description_rotation":"","description_alpha":"","description_tint":"","description_scaleX":"","description_scaleY":"","auto_play_time":"","auto_play_delay":"","auto_play_x":"","auto_play_y":"","auto_play_width":"","auto_play_height":"","auto_play_rotation":"","auto_play_alpha":"","auto_play_tint":"","auto_play_scaleX":"","auto_play_scaleY":"","preloader_time":"","preloader_delay":"","preloader_x":"","preloader_y":"","preloader_width":"","preloader_height":"","preloader_rotation":"","preloader_alpha":"","preloader_tint":"","preloader_scaleX":"","preloader_scaleY":"","prev_button_time":"","prev_button_delay":"","prev_button_x":"","prev_button_y":"","prev_button_width":"","prev_button_height":"","prev_button_rotation":"","prev_button_alpha":"","prev_button_tint":"","prev_button_scaleX":"","prev_button_scaleY":"","prev_symbol_time":"","prev_symbol_delay":"","prev_symbol_x":"","prev_symbol_y":"","prev_symbol_width":"","prev_symbol_height":"","prev_symbol_rotation":"","prev_symbol_alpha":"","prev_symbol_tint":"","prev_symbol_scaleX":"","prev_symbol_scaleY":"","next_button_time":"","next_button_delay":"","next_button_x":"","next_button_y":"","next_button_width":"","next_button_height":"","next_button_rotation":"","next_button_alpha":"","next_button_tint":"","next_button_scaleX":"","next_button_scaleY":"","next_symbol_time":"","next_symbol_delay":"","next_symbol_x":"","next_symbol_y":"","next_symbol_width":"","next_symbol_height":"","next_symbol_rotation":"","next_symbol_alpha":"","next_symbol_tint":"","next_symbol_scaleX":"","next_symbol_scaleY":""}', 0, '*'),
(93, 'Каталог', '', '', 1, 'catalog', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"katalog","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(94, 'Слайдер на главной', '', '<p>{source}</p>\r\n<p>&lt;div id="slider"&gt;</p>\r\n<p>&lt;span class="slider-title"&gt;Стальные радиаторы&lt;/span&gt;</p>\r\n<p>&lt;div class="slider-block"&gt;</p>\r\n<p>&lt;a href="#"&gt;&lt;img src="templates/euro-therm/img/slider_img.png" alt=""&gt;&lt;/a&gt;</p>\r\n<p>&lt;div class="slider-block-text"&gt;</p>\r\n<p>&lt;a href="#" class="slider-block-title"&gt;Стальные радиаторы&lt;/a&gt;</p>\r\n<p>&lt;a href="#" class="slider-block-intro"&gt;Мы продолжаем работать для вас дорогие друзья!И расширяя&lt;/a&gt;</p>\r\n<p>&lt;span class="clear"&gt;&lt;/span&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;div class="slider-block"&gt;</p>\r\n<p>&lt;a href="#"&gt;&lt;img src="templates/euro-therm/img/slider_img.png" alt=""&gt;&lt;/a&gt;</p>\r\n<p>&lt;div class="slider-block-text"&gt;</p>\r\n<p>&lt;a href="#" class="slider-block-title"&gt;Стальные радиаторы&lt;/a&gt;</p>\r\n<p>&lt;a href="#" class="slider-block-intro"&gt;Мы продолжаем работать для вас дорогие друзья!И расширяя&lt;/a&gt;</p>\r\n<p>&lt;span class="clear"&gt;&lt;/span&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>&lt;/div&gt;</p>\r\n<p>{/source}</p>', 1, 'mainPage-slider', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(95, 'Дилерская сеть', '', '<div id="diler-net"><a href="distribyutoram">ДИЛЕРСКАЯ СЕТЬ</a></div>', 1, 'diller-net', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(96, 'Специальные предложения', '', '<div id="special-offers"><a href="специальные-предложения"> <span class="ie6fix">СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ</span> <span>Для постоянных заказчиков - доставка Бесплатно</span> </a></div>', 1, 'special-offers', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(97, 'Акции и скидки', '', '<div id="actions-discounts"><a href="акции-и-скидки"> <span class="ie6fix">АКЦИИ И СКИДКИ</span> <span>Гибкая система скидок,<br />при заказе от 100 единиц</span> </a></div>', 1, 'actions', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(98, 'Футер копирайт', '', '<p><a href="http://www.shoplist.com.ua" target="_blank" title="Каталог украинских интернет магазинов"><img src="http://www.shoplist.com.ua/shoplist_small.gif" border="0" alt="Каталог украинских интернет магазинов" /></a> ©2012 Все права защищены</p>', 1, 'footer-copyrights', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(99, 'Новости', '', '', 1, 'news', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_dn', 1, 0, '{"set_count":"2","minus_leading":"","ordering":"mostrecent","show_tooltips":"0","style":"blog","set_column":"1","moduleclass_sfx":"","set_date_today":"0","set_date_range":"","set_date_month":"","set_date_year":"","set_category_type":"2","set_category_id":["9"],"set_article_type":"0","set_article_id_extra":"","set_state":"1","set_article_archived_id_extra":"","set_auto_author":"0","set_related":"0","set_metakeys":"","show_frontpage":"y","hide_current":"0","show_title":"1","link_titles":"1","text_hover_title":"","title_tag":"span class=\\"news-block-title\\"","filter_title":"0","length_limit_title":"20","truncate_ending_title":"1","truncate_ending_title_sign":"...","show_text":"1","link_text":"0","filter_text":"0","preserve_tags":"<img>","limit_text":"2","length_limit_text":"100","truncate_ending":"1","truncate_ending_sign":"...","text_hover_text":"","image":"2","link_image":"1","image_num":"","image_scale":"bestfit","image_bg":"FFFFFF","image_type":"","image_width":"78px","image_height":"78px","image_size":"","image_align":"2","image_margin":"","image_class":"0","image_class_name":"","image_default":"0","image_default_file":"","show_category":"0","show_category_title":"","show_description":"","show_description_image":"","link_category":"","text_hover_category":"","show_more_auto":"0","text_more":"","text_hover_more_category":"","use_modify_date":"0","show_date":"1","format_date":"","show_author":"","show_readmore":"2","text_readmore":"\\u041f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435","text_hover_readmore":"","show_vote":"","show_hits":"","show_jcomment_counter":"0","scroll_direction":"","scroll_speed":"1","scroll_delay":"30","scroll_mouse_ctrl":"1","scroll_height":"100","lead_space":"50","tail_space":"80","link_type":"0","link_target":"","window_width":"","window_height":"","window_menubar":"0","window_directories":"0","window_location":"0","window_resizable":"0","window_scrollbars":"0","window_status":"0","window_toolbar":"0","article_link":"0","item_id_type":"0","item_id_cat_type":"0","use_rows_template":"1","format":"%t <br\\/>%c<br\\/>%a - %d<br\\/>%b<br\\/>%p%i<br\\/>%m<div class=\\"item-separator\\"> <\\/div>","row_template":" ($img_out!='''' ? \\"$img_out\\" : ''''). \\r\\n($rate_out!='''' ? \\"$rate_out<br\\/>\\" : '''').  \\r\\n($cat_out!='''' ? \\"$cat_out\\" : '''').  \\r\\n($cat_out!='''' ? ''<br\\/>'' : '''').  \\r\\n($before_out!='''' ? \\"$before_out<br\\/>\\" : '''').  \\r\\n ($title_out!='''' ? \\"$title_out\\" : '''').  \\r\\n($title_out!='''' && $style != ''blog'' && $style != ''featured'' ? \\"<br\\/>\\" : ''''). \\r\\n($author_out!='''' ? \\"$author_out\\" : '''').    \\r\\n ($text_out!='''' ? \\"$text_out\\" : ''''). \\r\\n ($hits_out!='''' ? \\"($hits_out)\\" : ''''). \\r\\n ($jcomments_out<>'''' ? \\"($jcomments_out)\\" : '''').\\r\\n($author_out!='''' && $date_out!='''' ? '' - '' : '''').  \\r\\n($date_out!='''' ? \\"$date_out\\" : '''').  \\r\\n($author_out.$date_out!='''' ? ''<br\\/>'' : '''').  \\r\\n ($readmore_out!='''' ? \\"<br\\/>$readmore_out\\" : '''').  \\r\\n (!$last ? ''<div class=\\"item-separator\\"> <\\/div>'' : '''')","show_empty_module":"0","show_title_auto":"0","mod_title_format":"<h3>%c<\\/h3>","use_module_template":"1","module_format":"%t %c %s %r %f %m","module_template":"($mod_title_out != '''' ? \\"$mod_title_out\\" : ''''). $mod_cat_out. $scroll_start. $rows_out. $scroll_finish. ($mod_automore_out != '''' ? $mod_automore_out :'''' )","on_prepare_content_plugins":"1","before_display_content_plugins":"0","force_builtin_rating":"0","rating_txt":"","jcomments":"0","cache":"1","cache_time":"900"}', 0, '*'),
(100, 'Shaper Carousel Module', '', '', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_shaper_carousel', 1, 1, '{"moduleclass_sfx":"","ctype":"1","path":"\\/images\\/brands","catid":["2"],"count":"3","ordering":"a.title","ordering_direction":"ASC","user_id":"0","show_featured":"","width":"80","height":"56","tamount":"6","amount":"1","cssinfo":"1","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(101, 'mod_news_pro_gk4', '', '', 1, 'mainPage-slider', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_news_pro_gk4', 1, 0, '{"moduleclass_sfx":"","automatic_module_id":"1","module_unique_id":"newspro1","module_mode":"normal","module_width":"100","portal_mode_1_module_height":"320","module_font_size":"100","data_source":"vm_categories","com_categories":["18"],"com_articles":"","k2_articles":"","redshop_products":"","vm_products":"","vm_categories":["2"],"news_sort_value":"created","news_sort_order":"DESC","news_since":"","news_frontpage":"1","unauthorized":"0","only_frontpage":"0","startposition":"0","time_offset":"0","news_portal_mode_1_amount":"10","news_portal_mode_2_amount":"10","news_portal_mode_3_amount":"10","news_portal_mode_4_amount":"10","news_full_pages":"3","news_column":"1","news_rows":"1","top_interface_style":"arrows_with_pagination","news_content_header_pos":"left","news_content_header_float":"none","news_header_link":"1","use_title_alias":"0","title_limit_type":"chars","title_limit":"26","news_content_image_pos":"left","news_content_image_float":"left","news_image_link":"1","news_content_text_pos":"left","news_content_text_float":"left","news_text_link":"1","news_limit_type":"chars","news_limit":"400","news_content_info_pos":"left","news_content_info_float":"none","news_content_info2_pos":"left","news_content_info2_float":"left","info_format":"%DATE %HITS %CATEGORY %AUTHOR","info2_format":"","category_link":"0","date_format":"d-m-Y","date_publish":"0","username":"users.name","user_avatar":"0","avatar_size":"16","news_content_rs_pos":"left","news_content_rs_float":"none","art_padding":"0 20px 20px 0","news_header_order":"3","news_header_enabled":"1","news_image_order":"1","news_image_enabled":"1","news_text_order":"4","news_text_enabled":"1","news_info_order":"2","news_info_enabled":"1","news_info2_order":"5","news_info2_enabled":"1","news_rs_store_order":"6","news_rs_store_enabled":"0","news_content_readmore_pos":"right","news_readmore_enabled":"0","news_short_pages":"3","links_amount":"3","links_columns_amount":"1","bottom_interface_style":"arrows_with_pagination","links_margin":"0","links_position":"right","links_width":"50","show_list_description":"1","list_title_limit_type":"words","list_title_limit":"20","list_text_limit_type":"words","list_text_limit":"30","memory_limit":"128M","create_thumbs":"1","k2_thumbs":"first","thumb_image_type":"intro","img_auto_scale":"1","img_keep_aspect_ratio":"1","img_width":"78","img_height":"78","img_margin":"6px 14px 0 0","img_bg":"#000","img_stretch":"0","img_quality":"95","cache_time":"30","simple_crop_top":"10","simple_crop_bottom":"10","simple_crop_left":"2","simple_crop_right":"2","crop_rules":"","autoanim":"0","hover_anim":"0","animation_speed":"400","animation_interval":"5000","news_portal_mode_3_open_first":"1","animation_function":"Fx.Transitions.Expo.easeIn","clean_xhtml":"1","more_text_value":"...","parse_plugins":"0","clean_plugins":"0","rs_out_of_stock":"1","rs_add_to_cart":"0","rs_price":"0","rs_price_text":"0","rs_currency_place":"before","rs_price_with_vat":"1","rs_show_default_cart_button":"0","vm_itemid":"5","vm_shopper_group":"-1","vm_out_of_stock":"1","vm_show_price_type":"base","vm_show_price_with_tax":"0","vm_add_to_cart":"0","vm_show_discount_amount":"0","vm_show_tax":"0","vm_display_type":"text_price","k2store_support":"0","k2store_show_cart":"0","k2store_add_to_cart":"0","k2store_price":"0","k2store_price_text":"0","k2store_currency_place":"before","useCSS":"0","useScript":"2"}', 0, '*'),
(102, 'Бренды(news show pro)', '', '', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_news_pro_gk4', 1, 1, '{"moduleclass_sfx":"","automatic_module_id":"1","module_unique_id":"newspro1","module_mode":"normal","module_width":"100","portal_mode_1_module_height":"320","module_font_size":"100","data_source":"com_categories","com_categories":["10"],"com_articles":"4","k2_articles":"","redshop_products":"","vm_products":"","news_sort_value":"created","news_sort_order":"ASC","news_since":"","news_frontpage":"1","unauthorized":"0","only_frontpage":"0","startposition":"0","time_offset":"0","news_portal_mode_1_amount":"10","news_portal_mode_2_amount":"10","news_portal_mode_3_amount":"10","news_portal_mode_4_amount":"10","news_full_pages":"3","news_column":"5","news_rows":"1","top_interface_style":"arrows_with_pagination","news_content_header_pos":"left","news_content_header_float":"none","news_header_link":"0","use_title_alias":"0","title_limit_type":"chars","title_limit":"40","news_content_image_pos":"left","news_content_image_float":"left","news_image_link":"1","news_content_text_pos":"left","news_content_text_float":"left","news_text_link":"0","news_limit_type":"words","news_limit":"30","news_content_info_pos":"left","news_content_info_float":"none","news_content_info2_pos":"left","news_content_info2_float":"left","info_format":"%DATE %HITS %CATEGORY %AUTHOR","info2_format":"","category_link":"0","date_format":"d-m-Y","date_publish":"0","username":"users.name","user_avatar":"0","avatar_size":"16","news_content_rs_pos":"left","news_content_rs_float":"none","art_padding":"0 20px 20px 0","news_header_order":"1","news_header_enabled":"0","news_image_order":"3","news_image_enabled":"1","news_text_order":"4","news_text_enabled":"0","news_info_order":"2","news_info_enabled":"1","news_info2_order":"5","news_info2_enabled":"1","news_rs_store_order":"6","news_rs_store_enabled":"1","news_content_readmore_pos":"right","news_readmore_enabled":"0","news_short_pages":"3","links_amount":"3","links_columns_amount":"1","bottom_interface_style":"arrows_with_counter","links_margin":"0","links_position":"bottom","links_width":"50","show_list_description":"0","list_title_limit_type":"words","list_title_limit":"20","list_text_limit_type":"words","list_text_limit":"30","memory_limit":"128M","create_thumbs":"0","k2_thumbs":"first","thumb_image_type":"full","img_auto_scale":"1","img_keep_aspect_ratio":"0","img_width":"129","img_height":"56","img_margin":"6px 14px 0 0","img_bg":"#000","img_stretch":"0","img_quality":"95","cache_time":"30","simple_crop_top":"10","simple_crop_bottom":"10","simple_crop_left":"10","simple_crop_right":"10","crop_rules":"","autoanim":"0","hover_anim":"0","animation_speed":"400","animation_interval":"5000","news_portal_mode_3_open_first":"1","animation_function":"Fx.Transitions.Expo.easeIn","clean_xhtml":"1","more_text_value":"...","parse_plugins":"0","clean_plugins":"0","rs_out_of_stock":"1","rs_add_to_cart":"0","rs_price":"0","rs_price_text":"0","rs_currency_place":"before","rs_price_with_vat":"1","rs_show_default_cart_button":"0","vm_itemid":"9999","vm_shopper_group":"-1","vm_out_of_stock":"1","vm_show_price_type":"base","vm_show_price_with_tax":"0","vm_add_to_cart":"0","vm_show_discount_amount":"0","vm_show_tax":"0","vm_display_type":"text_price","k2store_support":"0","k2store_show_cart":"0","k2store_add_to_cart":"0","k2store_price":"0","k2store_price_text":"0","k2store_currency_place":"before","useCSS":"1","useScript":"2"}', 0, '*'),
(103, 'Нижнее меню', '', '', 1, 'footer-nav', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"\\u043d\\u0438\\u0436\\u043d\\u0435\\u0435-\\u043c\\u0435\\u043d\\u044e","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(104, 'VM - Currencies Selector', '', '', 4, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_currencies', 1, 1, '', 0, ''),
(105, 'VM - Featured products', '', '', 3, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_product', 1, 1, '', 0, ''),
(106, 'VM - Best Sales', '', '', 1, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_product', 1, 1, '', 0, ''),
(107, 'VM - Search in Shop', '', '', 2, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_search', 1, 1, '', 0, ''),
(108, 'VM - Manufacturer', '', '', 5, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_manufacturer', 1, 1, '', 0, ''),
(109, 'VM - Shopping cart', '', '', 0, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_cart', 1, 1, '', 0, ''),
(110, 'VM - Category', '', '', 6, 'position-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_virtuemart_category', 1, 1, '', 0, ''),
(111, 'Шапка иконки справа', '', '<p>{source}</p>\r\n<div>\r\n<div>&lt;div id="top-rightIcons"&gt;</div>\r\n<div style="text-align: left;">&lt;a class="sitemap-icon" href="карта-сайта"&gt;&lt;/a&gt;</div>\r\n<div>&lt;a class="contacts-icon" href="kontakty"&gt;&lt;/a&gt;</div>\r\n<div>&lt;a class="home-icon" href="http://euro-therm.com.ua"&gt;&lt;/a&gt;</div>\r\n<div>&lt;/div&gt;</div>\r\n</div>\r\n<div>{/source}</div>', 1, 'top-right-icons', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(112, 'Форма обратной связи', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_chronoforms', 1, 0, '{"cache":"0","chronoform":"Form","moduleclass_sfx":""}', 0, '*'),
(113, 'Навигатор', '', '', 1, 'navigator', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 0, '{"showHere":"0","showHome":"0","homeText":"","showLast":"1","separator":">>","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(114, 'DJ-Image Slider', '', '', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_djimageslider', 1, 1, '{"slider_source":"0","slider_type":"0","link_image":"1","image_folder":"images\\/brands","link":"http:\\/\\/euro-therm.com.ua\\/brendy","show_title":"1","show_desc":"1","show_readmore":"0","link_title":"1","link_desc":"0","limit_desc":"","image_width":"150","image_height":"90","fit_to":"0","visible_images":"4","space_between_images":"10","max_images":"20","sort_by":"1","effect":"Cubic","autoplay":"1","show_buttons":"1","show_arrows":"1","show_custom_nav":"0","desc_width":"","desc_bottom":"0","desc_horizontal":"0","left_arrow":"","right_arrow":"","play_button":"","pause_button":"","arrows_top":"30","arrows_horizontal":"5","effect_type":"0","duration":"","delay":"","preload":"800","moduleclass_sfx":"","cache":"0"}', 0, '*'),
(115, 'Cлайдер новостей', '', '', 1, 'mainPage-slider', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_news_pro_gk4', 1, 0, '{"moduleclass_sfx":"","automatic_module_id":"1","module_unique_id":"newspro1","module_mode":"normal","module_width":"100","portal_mode_1_module_height":"320","module_font_size":"100","data_source":"com_categories","com_categories":["9"],"com_articles":"","k2_articles":"","redshop_products":"","vm_products":"","news_sort_value":"created","news_sort_order":"DESC","news_since":"","news_frontpage":"1","unauthorized":"0","only_frontpage":"0","startposition":"0","time_offset":"0","news_portal_mode_1_amount":"10","news_portal_mode_2_amount":"10","news_portal_mode_3_amount":"10","news_portal_mode_4_amount":"10","news_full_pages":"3","news_column":"2","news_rows":"1","top_interface_style":"arrows_with_pagination","news_content_header_pos":"left","news_content_header_float":"none","news_header_link":"1","use_title_alias":"0","title_limit_type":"chars","title_limit":"40","news_content_image_pos":"left","news_content_image_float":"left","news_image_link":"1","news_content_text_pos":"left","news_content_text_float":"left","news_text_link":"0","news_limit_type":"words","news_limit":"30","news_content_info_pos":"left","news_content_info_float":"none","news_content_info2_pos":"left","news_content_info2_float":"left","info_format":"%DATE %HITS %CATEGORY %AUTHOR","info2_format":"","category_link":"1","date_format":"d-m-Y","date_publish":"0","username":"users.name","user_avatar":"1","avatar_size":"16","news_content_rs_pos":"left","news_content_rs_float":"none","art_padding":"0 20px 20px 0","news_header_order":"1","news_header_enabled":"1","news_image_order":"3","news_image_enabled":"1","news_text_order":"4","news_text_enabled":"1","news_info_order":"2","news_info_enabled":"1","news_info2_order":"5","news_info2_enabled":"1","news_rs_store_order":"6","news_rs_store_enabled":"1","news_content_readmore_pos":"right","news_readmore_enabled":"1","news_short_pages":"3","links_amount":"3","links_columns_amount":"1","bottom_interface_style":"arrows_with_counter","links_margin":"0","links_position":"bottom","links_width":"50","show_list_description":"1","list_title_limit_type":"words","list_title_limit":"20","list_text_limit_type":"words","list_text_limit":"30","memory_limit":"128M","create_thumbs":"0","k2_thumbs":"first","thumb_image_type":"full","img_auto_scale":"1","img_keep_aspect_ratio":"0","img_width":"160","img_height":"120","img_margin":"6px 14px 0 0","img_bg":"#000","img_stretch":"0","img_quality":"95","cache_time":"30","simple_crop_top":"10","simple_crop_bottom":"10","simple_crop_left":"10","simple_crop_right":"10","crop_rules":"","autoanim":"0","hover_anim":"0","animation_speed":"400","animation_interval":"5000","news_portal_mode_3_open_first":"1","animation_function":"Fx.Transitions.Expo.easeIn","clean_xhtml":"1","more_text_value":"...","parse_plugins":"0","clean_plugins":"0","rs_out_of_stock":"1","rs_add_to_cart":"0","rs_price":"0","rs_price_text":"0","rs_currency_place":"before","rs_price_with_vat":"1","rs_show_default_cart_button":"0","vm_itemid":"9999","vm_shopper_group":"-1","vm_out_of_stock":"1","vm_show_price_type":"base","vm_show_price_with_tax":"0","vm_add_to_cart":"0","vm_show_discount_amount":"0","vm_show_tax":"0","vm_display_type":"text_price","k2store_support":"0","k2store_show_cart":"0","k2store_add_to_cart":"0","k2store_price":"0","k2store_price_text":"0","k2store_currency_place":"before","useCSS":"1","useScript":"2"}', 0, '*'),
(116, 'Блок на главной вверху', '', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td><img src="images/e.jpg" border="0" width="145" height="134" /></td>\r\n<td valign="middle"><strong> </strong></td>\r\n<td valign="middle"><strong>Стальные панельные радиаторы EUROTHERM</strong> выполнен из листовой холоднокатаной стали высочайшего качества, толщина которой составляет 1,10 мм. Они имеют высокую тепловую производительность. <br /><a href="stalnye-radiatory/stalnye-radiatory-podpunkt-1">Цены</a>    <a href="stalnye-radiatory/stalnye-radiatory-podpunkt-1">Описание</a></td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, 'mainPage-slider', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(117, 'текст - под стальными радиаторы евротерм', '', '<h3>Почему стальные радиаторы<strong> </strong></h3>\r\n<p>Надоело мерзнуть в квартирах зимой? Строите свой дом и стоите перед выбором типа радиаторов? Надоели старые ржавые батареи советских времен?</p>\r\n<p>Тогда следует купить стальные радиаторы отопления, ведь это — решение теплового вопроса в вашем жилище.</p>\r\n<p><strong>Почему именно стальные радиаторы?</strong></p>\r\n<p>Потому что они обладают низкой тепловой инерцией и характеризуются высокой теплоотдачей. А эти качества гарантируют эффективность эксплуатации отопительных приборов данного типа в системах отопления.</p>\r\n<h3>Общие характеристики радиаторов</h3>\r\n<p>По теплопроводности они находятся посередине между алюминиевыми и чугунными.</p>\r\n<p>Изготавливаются из штампованных стальных листов, отличающихся устойчивостью к коррозии. Листы компонуются в вертикальные параллельные каналы, объединенные горизонтальным коллектором.</p>\r\n<p><strong>Радиаторы изготавливаются:</strong></p>\r\n<ul>\r\n<li>однорядными</li>\r\n<li>двухрядными</li>\r\n<li>трехрядными</li>\r\n<li>с оребрением или без.</li>\r\n</ul>\r\n<p>Покрываются они многослойной эмалью, устойчивой к высоким температурам.</p>\r\n<p><strong>Эксплуатационные характеристики</strong></p>\r\n<p>Основные эксплуатационные качества радиаторов отопления зависят от устройства (конструкции) и от толщины стенок. Как правило, они выдерживают давление от 6 до 10 атмосфер, а температура теплоносителя — максимум 120°С, опрессовочное давление — максимум 13 атмосфер.</p>\r\n<p><strong>Где применяются</strong></p>\r\n<p>Рекомендуем купить стальной радиатор для закрытых систем отопления. Дело в том, что, невзирая на явные достоинства, эти отопительные приборы очень чувствительны к кислороду, растворенному в теплоносителе, что и является их слабым местом.</p>\r\n<p>Опустошение системы (слив воды) усугубляет ситуацию: коррозия при попадании воздуха усиливается.</p>\r\n<p>Вывод: стальные радиаторы следует купить для использования в закрытых, автономных системах отопления. При этом теплоноситель должен содержать минимальное количество кислорода.</p>\r\n<h3>Виды стальных радиаторов</h3>\r\n<p>Нынешний рынок тепловых приборов предлагает три вида:</p>\r\n<ul>\r\n<li><strong>секционные</strong><br />секции изготавливаются методом штамповки из листовой стали. Их легко очищать от пыли. Однако максимальное давление для них — 6 атмосфер.</li>\r\n<li><strong>трубчатые</strong><br />внешние панели удобно очищать от пыли; рабочее давление от 10 до 15 атмосфер; обеспечивают комфортное тепло, характеризуются высокой прочностью.</li>\r\n<li><strong>панельные</strong><br />отличаются высокой прочностью и легким уходом; рабочее давление 10-15 атмосфер; уровень теплового комфорта — высокий.</li>\r\n</ul>\r\n<h3>Преимущества</h3>\r\n<p>Если сравнивать стальной радиатор с биметаллическим или чугунным, то обнаружатся явные преимущества:</p>\r\n<ol start="1">\r\n<li>По сравнению с чугунным, стальной — <strong>более производителен</strong>.</li>\r\n</ol>\r\n<p>Считается, что чугунный радиатор дольше держит тепло, поэтому его ставить выгоднее.</p>\r\n<p>Однако это не совсем так. Подсчеты показывают, что для достижения отдачи одинакового количества тепла чугунным и стальным радиаторами необходимо, чтобы по чугунному прошел объем воды больший в 7 раз и при этом температура воды должна быть на 20° С выше.</p>\r\n<p>Достижение высокой теплоотдачи в чугунном материале требует высокого расхода топлива и установки насосов — а это дополнительные расходы.</p>\r\n<ol start="2">\r\n<li>По сравнению со стальным радиатором, биметаллический уступает в технических показателях: например, <strong>объем циркуляционной воды — снижен</strong>.</li>\r\n</ol>\r\n<p><strong>Итак, наша продукция — самое выгодное решение, потому что:</strong></p>\r\n<ul>\r\n<li><strong>поверхность теплообмена — более развита</strong></li>\r\n<li>помещение прогревает — быстро</li>\r\n<li><strong>энергия на нагрев самого себя — не тратится</strong></li>\r\n</ul>\r\n<p>и поэтому наша компания предлагает →</p>\r\n<h3>Купить стальной радиатор</h3>\r\n<p>Если вы хотите сократить расходы на отопление, то стальные радиаторы от EUROTHERM (Турция) порадуют вас своими эксплуатационными свойствами.</p>\r\n<p>Для оформления заказа или уточнения подробностей покупки, звоните:</p>\r\n<ul>\r\n<li><strong>+38 (050) 194 89 99</strong></li>\r\n<li><strong>+38 (067) 423 33 86</strong></li>\r\n</ul>\r\n<p>Продажа осуществляется по всей Украине: Харьков, Киев, Днепропетровск, Симферополь, Одесса, Донецк, Запорожье и другие города.</p>', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(118, 'Текст под - Алюминиевые радиаторы Италия ', '', '<p>Продажа алюминиевых радиаторов итальянского производства осуществляется по всей Украине: Киев, Харьков, Днепропетровск, Одесса, Симферополь, Донецк, Запорожье и др. Доставка 1-3 дня.</p>', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(119, '		Текст под - Алюминиевые радиаторы Китай', '', '<p>Купить алюминиевые радиаторы производства Китай можно в любом городе Украине: Харьков, Днепропетровск, Киев, Симферополь, Одесса, Донецк, Запорожье а также в прочих городах.</p>', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(120, 'Текст под - Биметаллические радиаторы Китай', '', '<h3>Биметаллические радиаторы отопления — тепло гарантированно<strong> </strong></h3>\r\n<p>Пожалуй, кроме искусства время старит все. Не составляют исключение и отопительные батареи. Поэтому вопрос выбора оптимального вида приборов для отопления стоит не только перед строителями, но и перед владельцами не очень новых строений.</p>\r\n<p><strong>Один из самых популярных видов отопительных батарей — биметаллические радиаторы отопления. </strong></p>\r\n<p>Биметаллические отопительные приборы создаются по уникальной технологии: для их создания используют алюминий и сталь. Именно поэтому отопительные батареи данного вида лучшим образом сочетают в себе как свойства трубчатых стальных батарей, так и секционных алюминиевых отопительных приборов.</p>\r\n<h3>Чем хорош союз стали и алюминия?</h3>\r\n<p>Эти металлы отлично дополняют друг друга:</p>\r\n<p>1. Из стали состоят все детали отопительных батарей, которые контактируют с водой. Невозможность соприкосновения алюминия и теплоносителя улучшает показатели рабочих характеристик.</p>\r\n<p>2. Свойства алюминиевых составляющих биметаллических радиаторов способствуют быстрому теплообмену, за счет чего помещение быстрее прогревается.</p>\r\n<p>Применение стали и алюминия в производстве отопительных батарей позволило повысить срок эксплуатации и сегодня "биметаллическое отопление" служет 20 лет как минимум.</p>\r\n<p>Многие советуют своим знакомым купить биметаллические отопительные приборы, потому что они отличаются повышенной прочностью. Этим батареям не страшно давление свыше 25 атмосфер.</p>\r\n<p>Кроме надежности и прочности, биметаллические радиаторы отличаются хорошей теплоотдачей, но и цена за них выше.</p>\r\n<p>Однако отметим, что каждый человек, купивший биметаллические радиаторы, скажет что цена, уплаченная за эти отопительные приборы, адекватна их качеству и окупается достаточно быстро.</p>\r\n<table border="1" cellspacing="5" cellpadding="5">\r\n<tbody>\r\n<tr>\r\n<td colspan="2" width="631">\r\n<p align="center"> <strong>ДОСТОИНСТВА</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="283">\r\n<p><strong>1. Высокая теплопроводность</strong></p>\r\n</td>\r\n<td valign="top" width="348">\r\n<p>Данные приборы быстро прогреваются, и этим обеспечивают быстрый прогрев помещения.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="283">\r\n<p><strong>2. Устойчивость к коррозии</strong></p>\r\n</td>\r\n<td valign="top" width="348">\r\n<p>Это качество значительно продлевает срок службы отопительных батарей, поскольку ржавчина от контакта с воздухом им не страшна.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="283">\r\n<p><strong>3. Небольшой объем теплоносителя и его химический состав</strong></p>\r\n</td>\r\n<td valign="top" width="348">\r\n<p>Нейтральны к химическому составу теплоносителя, поэтому они универсальны в использовании.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="283">\r\n<p><strong>4. Небольшие размеры</strong></p>\r\n</td>\r\n<td valign="top" width="348">\r\n<p>Это качество позволяет экономить пространство.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="283">\r\n<p><strong>5. Элегантный дизайн</strong></p>\r\n</td>\r\n<td valign="top" width="348">\r\n<p>Плавность и элегантность линий биметаллических радиаторов отопления</p>\r\n<p>дарит комфорт и эстетическое удовольствие.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Несомненно, биметаллические радиаторы обладают существенными достоинствами. А есть ли у них недостатки?</p>\r\n<p>Недостатки — относительно высокая стоимость и малая проходная площадь сечения межколлекторных трубок (у алюминиевых радиаторов она больше). В остальном — только плюсы.</p>\r\n<p><strong>Вывод</strong></p>\r\n<p>Не зависимо от вида отопительной системы (индивидуальное отопление или централизованное) следует купить биметаллические радиаторы. Они прослужат верой и правдой гораздо дольше, чем 10 лет.</p>\r\n<p>Мы предлагаем купить продукцию известных торговых марок: All Termo, Classic+ и EUROTHERM, цена — от 55 грн. (или 7,40 у.е.).</p>\r\n<p>Чтобы купить биметаллические радиаторы — оформляйте заказ, позвонив по номерам:</p>\r\n<ul>\r\n<li><strong>+38 (050) 194 89 99</strong></li>\r\n<li><strong>+38 (067) 423 33 86</strong></li>\r\n</ul>\r\n<p>Продажа осуществляется в каждом уголке Украины, в частности в таких крупных городах как: Харьков, Киев, Днепропетровск, Донецк, Запорожье, Симферополь, Одесса.</p>', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*');
INSERT INTO `o1hap_modules` (`id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(121, 'текст внизу на главной', '', '<h3 align="center">5 причин купить стальные панельные радиаторы</h3>\r\n<h3 align="center">или тепло в доме</h3>\r\n<p>Решили обновить тепловую систему в своем доме? У вас новостройка и пора установить отопление? Думаете о том, где бы купить надежные радиаторы с высокой теплоотдачей и при этом не переплачивать?</p>\r\n<p><strong>Компания EUROTHERM предлагает купить стальные панельные радиаторы различных размеров.</strong></p>\r\n<p>Основа конструкции — две стальные пластины, соединенные сваркой. Толщина пластин — от 1,1 до 1,5 мм. Эти пластины штампуются с углублениями, образующими соединительные каналы и коллекторы.</p>\r\n<p>Высота быть от 20 см до 90 см, а длина — от 40 см до 300 см.</p>\r\n<p>Почему стальные радиаторы — хороший выбор?</p>\r\n<p><strong>Давайте рассмотрим, чем панельный радиатор заслужил симпатии потребителей:</strong></p>\r\n<table style="width: 643px;" border="1" cellspacing="5" cellpadding="5">\r\n<tbody>\r\n<tr>\r\n<td colspan="2" width="643">\r\n<p align="center">ХАРАКТЕРИСТИКИ</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="271"><strong>1. Хорошая теплоотдача</strong></td>\r\n<td valign="top" width="372">\r\n<p>Большая площадь поверхности и оребрение гарантируют высокую теплоотдачу.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="271"><strong>2. Красивый внешний вид</strong></td>\r\n<td valign="top" width="372">\r\n<p>Каждый человек стремиться к тому, чтобы в его доме было не только тепло, но и уютно.</p>\r\n<p>Поэтому отопительные батареи должны не только дарить нам тепло, но также быть красивыми внешне.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="271"><strong>3. Стоимость</strong></td>\r\n<td valign="top" width="372">\r\n<p>Отличаясь высокой теплоотдачей, панельные радиаторы славятся доступными ценами.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="271"><strong>4. Широкий выбор типоразмеров</strong></td>\r\n<td valign="top" width="372">\r\n<p>Вы без труда подберете подходящий размер.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top" width="271"><strong>5. Качество</strong></td>\r\n<td valign="top" width="372">\r\n<p>Все панельные радиаторы, предлагаемые компанией EUROTHERM, отличаются повышенной надежностью.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Бесспорно, стальные панельные радиаторы зарекомендовали себя с лучшей стороны, но даже у них есть свои особенности, которые необходимо знать:</p>\r\n<ol><ol>\r\n<li><strong>Не подходит для центральной системы отопления.</strong><br />Низкое рабочее давление может стать причиной вывода отопительных батарей этого вида из строя. Например, давление свыше 13 атмосфер может привести к раздутию или прорыву прибора.<br />Слив воды со стояков центральной тепловой системы на летние месяцы также может привести к подобным повреждениям этих отопительных батарей.</li>\r\n<li><strong>Коррозия</strong><br />Хоть радиаторы и прочные, но бояться коррозии. Если их оставить более двух недель без воды, то в них может попасть воздух, что провоцирует начало коррозии.</li>\r\n<li><strong>Качество теплоносителя</strong><br />Некачественный теплоноситель приводит к быстрому выходу из строя.</li>\r\n</ol></ol>\r\n<p>Как видим, для центральной системы отопления эти отопительные батареи не подходят. Но зато — это лучший вариант для частных отопительных систем как в коттеджных домах, так и многоквартирных застройках с индивидуальным отоплением.</p>\r\n<p><strong>Вывод</strong></p>\r\n<p>Для индивидуальных систем отопления стальные панельные радиаторы — оптимальный вариант, поскольку они подходят для частного отопления своими характеристиками и стоимостью.</p>\r\n<p>Мы предлагаем панельные радиаторы торговой марки EUROTHERM, зарекомендовавших себя с самой лучшей стороны.</p>\r\n<p>Звоните по телефонным номерам</p>\r\n<ol><ol>\r\n<ul>\r\n<li><strong>+38 (050) 194 89 99</strong></li>\r\n<li><strong>+38 (067) 423 33 86</strong></li>\r\n</ul>\r\n</ol></ol>\r\n<p>и оформляйте покупку.</p>', 1, 'brands', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(122, 'ZOO Category', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zoocategory', 1, 1, '', 0, '*'),
(123, 'ZOO Comment', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zoocomment', 1, 1, '', 0, '*'),
(124, 'ZOO Item', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zooitem', 1, 1, '', 0, '*'),
(125, 'ZOO Quick Icons', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zooquickicon', 1, 1, '', 1, '*'),
(126, 'ZOO Tag', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zootag', 1, 1, '', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_modules_menu`
--

CREATE TABLE IF NOT EXISTS `o1hap_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_modules_menu`
--

INSERT INTO `o1hap_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(88, 0),
(89, 0),
(90, 0),
(92, 101),
(93, 0),
(94, 101),
(95, 101),
(95, 102),
(95, 103),
(95, 104),
(95, 106),
(95, 119),
(95, 131),
(96, 0),
(97, 0),
(98, 0),
(99, 101),
(99, 102),
(99, 103),
(99, 104),
(99, 105),
(99, 106),
(99, 107),
(99, 108),
(99, 109),
(99, 110),
(99, 111),
(99, 112),
(99, 113),
(99, 114),
(99, 115),
(99, 116),
(99, 117),
(99, 118),
(99, 119),
(99, 120),
(99, 121),
(99, 122),
(99, 123),
(99, 124),
(99, 125),
(99, 127),
(99, 131),
(99, 132),
(99, 133),
(99, 134),
(100, 0),
(101, 101),
(101, 113),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0),
(108, 0),
(109, 0),
(110, 0),
(111, 0),
(112, 106),
(113, 108),
(113, 112),
(113, 113),
(113, 116),
(113, 119),
(114, -106),
(115, 101),
(116, 0),
(117, 108),
(118, 112),
(119, 113),
(120, 116),
(121, 101);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `o1hap_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_overrider`
--

CREATE TABLE IF NOT EXISTS `o1hap_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_redirect_links`
--

CREATE TABLE IF NOT EXISTS `o1hap_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=414 ;

--
-- Dumping data for table `o1hap_redirect_links`
--

INSERT INTO `o1hap_redirect_links` (`id`, `old_url`, `new_url`, `referer`, `comment`, `hits`, `published`, `created_date`, `modified_date`) VALUES
(1, 'http://promodl.com/euro-therm/index.php/bimetallicheskie-radiatory/font.swf', '', '', '', 0, 0, '2012-04-16 19:09:29', '0000-00-00 00:00:00'),
(2, 'http://promodl.com/euro-therm/index.php/font.swf', '', '', '', 0, 0, '2012-04-16 20:10:34', '0000-00-00 00:00:00'),
(3, 'http://promodl.com/euro-therm/index.php/novosti/font.swf', '', '', '', 0, 0, '2012-04-16 20:48:06', '0000-00-00 00:00:00'),
(4, 'http://promodl.com/euro-therm/главная.html', '', 'http://promodl.com/euro-therm/index.php/component/search/?searchword=Lorem%20ipsum&searchphrase=all&Itemid=101', '', 0, 0, '2012-04-17 12:08:55', '0000-00-00 00:00:00'),
(5, 'http://promodl.com/euro-therm/kontakty.html', '', 'http://promodl.com/euro-therm/index.php', '', 0, 0, '2012-04-19 08:26:47', '0000-00-00 00:00:00'),
(6, 'http://euro-therm.com.ua/главная.html', '', 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1', '', 0, 0, '2012-04-23 15:22:34', '0000-00-00 00:00:00'),
(7, 'http://euro-therm.com.ua/index.php/font.swf', '', 'http://euro-therm.com.ua/media/mod_vinaora_cu3er_3d_slideshow/flash/cu3er.swf', '', 0, 0, '2012-04-23 11:33:35', '0000-00-00 00:00:00'),
(8, 'http://euro-therm.com.ua/<script type=''text/javascript''> <!-- var prefix = ''ma'' + ''il'' + ''to''; var path = ''hr'' + ''ef'' + ''=''; var addy73881 = ''balalaykin'' + ''@''; addy73881 = addy73881 + ''list'' + ''.'' + ''ru''; document.write(''<a '' + path + ''\\'''' + prefix + '':''', '', 'http://euro-therm.com.ua/kontakty', '', 0, 0, '2012-04-23 15:56:49', '0000-00-00 00:00:00'),
(9, 'http://euro-therm.com.ua/indew', '', '', '', 0, 0, '2012-04-23 16:08:29', '0000-00-00 00:00:00'),
(10, 'http://euro-therm.com.ua/indx.php', '', '', '', 0, 0, '2012-04-23 16:08:36', '0000-00-00 00:00:00'),
(11, 'http://euro-therm.com.ua/ index.php', '', 'http://euro-therm.com.ua/tovary', '', 0, 0, '2012-04-23 16:35:33', '0000-00-00 00:00:00'),
(12, 'http://euro-therm.com.ua/фвьштшыекфещк', '', '', '', 0, 0, '2012-04-28 00:56:15', '0000-00-00 00:00:00'),
(13, 'http://euro-therm.com.ua/штвучюзрз', '', '', '', 0, 0, '2012-04-28 01:25:25', '0000-00-00 00:00:00'),
(14, 'http://euro-therm.com.ua/фвьштыекфещк', '', '', '', 0, 0, '2012-04-30 12:22:37', '0000-00-00 00:00:00'),
(15, 'http://euro-therm.com.ua/admonistrator', '', '', '', 0, 0, '2012-04-30 12:22:47', '0000-00-00 00:00:00'),
(16, 'http://euro-therm.com.ua/index/php', '', '', '', 0, 0, '2012-04-30 18:56:00', '0000-00-00 00:00:00'),
(17, 'http://euro-therm.com.ua/therm', '', '', '', 0, 0, '2012-05-03 13:27:51', '0000-00-00 00:00:00'),
(18, 'http://euro-therm.com.ua/indrx.php', '', '', '', 0, 0, '2012-05-03 13:37:48', '0000-00-00 00:00:00'),
(19, 'http://euro-therm.com.ua/admnistraotr', '', '', '', 0, 0, '2012-05-07 11:06:12', '0000-00-00 00:00:00'),
(20, 'http://euro-therm.com.ua/administraotr', '', '', '', 0, 0, '2012-05-07 11:06:27', '0000-00-00 00:00:00'),
(21, 'http://euro-therm.com.ua/administraotor', '', '', '', 0, 0, '2012-05-07 11:06:38', '0000-00-00 00:00:00'),
(22, 'http://euro-therm.com.ua/style>', '', '', '', 0, 0, '2012-05-17 17:23:53', '0000-00-00 00:00:00'),
(23, 'http://euro-therm.com.ua/afministrator', '', '', '', 0, 0, '2012-06-11 12:16:13', '0000-00-00 00:00:00'),
(24, 'http://euro-therm.com.ua/ак', '', '', '', 0, 0, '2012-06-23 08:57:33', '0000-00-00 00:00:00'),
(25, 'http://euro-therm.com.ua/ка', '', '', '', 0, 0, '2012-06-23 08:57:34', '0000-00-00 00:00:00'),
(26, 'http://euro-therm.com.ua/', '', '', '', 0, 0, '2012-06-23 08:57:34', '0000-00-00 00:00:00'),
(27, 'http://euro-therm.com.ua/мен', '', '', '', 0, 0, '2012-06-23 08:57:52', '0000-00-00 00:00:00'),
(28, 'http://euro-therm.com.ua/kontakty/skype:euro_therm', '', '', '', 0, 0, '2012-06-23 08:57:57', '0000-00-00 00:00:00'),
(29, 'http://euro-therm.com.ua/map/skype:euro_therm', '', '', '', 0, 0, '2012-06-23 08:57:59', '0000-00-00 00:00:00'),
(30, 'http://euro-therm.com.ua/index.php/blank.gif', '', 'http://euro-therm.com.ua/index.php/', '', 0, 0, '2012-07-10 14:52:23', '0000-00-00 00:00:00'),
(31, 'http://euro-therm.com.ua/ad', '', '', '', 0, 0, '2012-07-10 14:57:58', '0000-00-00 00:00:00'),
(32, 'http://euro-therm.com.ua/спе', '', '', '', 0, 0, '2012-07-18 14:49:10', '0000-00-00 00:00:00'),
(33, 'http://www.euro-therm.com.ua/Ð°ÐºÑ†Ð¸Ð¸-Ð¸-ÑÐºÐ¸Ð´ÐºÐ¸', '', '', '', 0, 0, '2012-08-19 10:02:50', '0000-00-00 00:00:00'),
(34, 'http://euro-therm.com.ua/undefined', '', 'http://euro-therm.com.ua/%D0%BA%D0%B0%D1%80%D1%82%D0%B0-%D1%81%D0%B0%D0%B9%D1%82%D0%B0', '', 0, 0, '2012-08-26 09:17:38', '0000-00-00 00:00:00'),
(35, 'http://euro-therm.com.ua/меню/undefined', '', 'http://euro-therm.com.ua/%D0%BC%D0%B5%D0%BD%D1%8E/%D0%B0%D0%BA%D1%86%D0%B8%D0%B8', '', 0, 0, '2012-08-26 09:29:39', '0000-00-00 00:00:00'),
(36, 'http://euro-therm.com.ua/metalloplastikovye-truby', '', 'http://euro-therm.com.ua/', '', 0, 0, '2012-08-28 13:29:11', '0000-00-00 00:00:00'),
(37, 'http://euro-therm.com.ua/кекнре', '', '', '', 0, 0, '2012-08-28 18:28:45', '0000-00-00 00:00:00'),
(38, 'http://euro-therm.com.ua/rerer', '', '', '', 0, 0, '2012-08-28 18:29:01', '0000-00-00 00:00:00'),
(39, 'http://euro-therm.com.ua/Search Web', '', '', '', 0, 0, '2012-09-03 17:16:02', '0000-00-00 00:00:00'),
(40, 'http://euro-therm.com.ua/Ð¼ÐµÐ½ÑŽ/Ð±Ñ€ÐµÐ½Ð´Ñ‹', '', '', '', 0, 0, '2012-09-11 13:51:44', '0000-00-00 00:00:00'),
(41, 'http://euro-therm.com.ua/ÑÐ¿ÐµÑ†Ð¸Ð°Ð»ÑŒÐ½Ñ‹Ðµ-Ð¿Ñ€ÐµÐ´Ð»Ð¾Ð¶ÐµÐ½Ð¸Ñ', '', '', '', 0, 0, '2012-09-13 04:26:06', '0000-00-00 00:00:00'),
(42, 'http://euro-therm.com.ua/ÐºÐ°Ñ€Ñ‚Ð°-ÑÐ°Ð¹Ñ‚Ð°', '', '', '', 0, 0, '2012-09-15 03:16:09', '0000-00-00 00:00:00'),
(43, 'http://euro-therm.com.ua/Ð¼ÐµÐ½ÑŽ/Ð°ÐºÑ†Ð¸Ð¸', '', '', '', 0, 0, '2012-09-19 13:35:49', '0000-00-00 00:00:00'),
(44, 'http://euro-therm.com.ua/++++++++++++++++++++++++++++++++++++++++++++++++++++++++Result:+', '', 'http://euro-therm.com.ua/++++++++++++++++++++++++++++++++++++++++++++++++++++++++Result:+%ED%E5+%ED%E0%F8%EB%EE%F1%FC+%F4%EE%F0%EC%FB+%E4%EB%FF+%EE%F2', '', 0, 0, '2012-09-30 02:51:29', '0000-00-00 00:00:00'),
(45, 'http://euro-therm.com.ua/wp-login.php', '', '', '', 0, 0, '2012-10-01 20:18:08', '0000-00-00 00:00:00'),
(46, 'http://euro-therm.com.ua/к', '', '', '', 0, 0, '2012-10-04 04:58:32', '0000-00-00 00:00:00'),
(47, 'http://euro-therm.com.ua/ÐºÑƒÐ¿Ð¸Ñ‚ÑŒ-ÐºÑ€Ð°Ð½Ñ‹-ÑˆÐ°Ñ€Ð¾Ð²Ñ‹Ðµ', '', '', '', 0, 0, '2012-10-23 12:37:30', '0000-00-00 00:00:00'),
(48, 'http://euro-therm.com.ua/nrptxlrk.html', '', '', '', 0, 0, '2012-10-26 01:50:18', '0000-00-00 00:00:00'),
(49, 'http://euro-therm.com.ua/биметаллические-радиаторы', '', '', '', 0, 0, '2012-10-30 12:01:07', '0000-00-00 00:00:00'),
(50, 'http://euro-therm.com.ua/index.php?option=com_formmaker&view=formmaker&id=''', '', '', '', 0, 0, '2012-10-31 12:18:34', '0000-00-00 00:00:00'),
(51, 'http://euro-therm.com.ua/Ð°ÐºÑ†Ð¸Ð¸-Ð¸-ÑÐºÐ¸Ð´ÐºÐ¸', '', '', '', 0, 0, '2012-11-02 06:11:49', '0000-00-00 00:00:00'),
(52, 'http://euro-therm.com.ua/ÃÂºÃÂ°Ã‘â‚¬Ã‘â€šÃÂ°-Ã‘ÂÃÂ°ÃÂ¹Ã‘â€šÃÂ°', '', '', '', 0, 0, '2012-11-21 10:33:23', '0000-00-00 00:00:00'),
(53, 'http://euro-therm.com.ua/component/virtuemart/биметаллические-радиаторы', '', '', '', 0, 0, '2012-12-05 13:13:57', '0000-00-00 00:00:00'),
(54, 'http://euro-therm.com.ua/alyuminievye-radiatory/ie6.html', '', 'http://yandex.ru/yandsearch?text=%C3%B0%C3%A0%C3%A4%C3%A8%C3%A0%C3%B2%C3%AE%C3%B0%C3%BB+%C3%A0%C3%AB%C3%BE%C3%AC%C3%A8%C3%AD%C3%A8%C3%A5%C3%A2%C3%BB%C', '', 0, 0, '2012-12-05 17:11:54', '0000-00-00 00:00:00'),
(55, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ã‘â€¦500-detail', '', '', '', 0, 0, '2012-12-06 02:49:43', '0000-00-00 00:00:00'),
(56, 'http://euro-therm.com.ua/component/virtuemart/алюминиевые-радиаторы', '', '', '', 0, 0, '2012-12-06 03:38:02', '0000-00-00 00:00:00'),
(57, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х800-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2012-12-06 06:06:29', '0000-00-00 00:00:00'),
(58, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,product_sku', '', '', '', 0, 0, '2012-12-06 06:08:36', '0000-00-00 00:00:00'),
(59, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х800-detail', '', '', '', 0, 0, '2012-12-06 10:24:58', '0000-00-00 00:00:00'),
(60, 'http://euro-therm.com.ua/component/virtuemart/стальные-радиаторы2012-04-17-13-20-07_', '', '', '', 0, 0, '2012-12-06 10:47:39', '0000-00-00 00:00:00'),
(61, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х400-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2012-12-06 11:17:59', '0000-00-00 00:00:00'),
(62, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦600-detail', '', '', '', 0, 0, '2012-12-06 12:29:57', '0000-00-00 00:00:00'),
(63, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х600-detail', '', '', '', 0, 0, '2012-12-06 14:51:52', '0000-00-00 00:00:00'),
(64, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х700-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2012-12-06 17:33:07', '0000-00-00 00:00:00'),
(65, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,mf_name', '', '', '', 0, 0, '2012-12-06 19:59:02', '0000-00-00 00:00:00'),
(66, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х700-detail', '', '', '', 0, 0, '2012-12-06 22:10:13', '0000-00-00 00:00:00'),
(67, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1', '', '', '', 0, 0, '2012-12-06 22:53:44', '0000-00-00 00:00:00'),
(68, 'http://euro-therm.com.ua/alyuminievye-radiatory/alyuminievye-radiatory-kitay', '', '', '', 0, 0, '2012-12-06 23:28:38', '0000-00-00 00:00:00'),
(69, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х500-detail', '', '', '', 0, 0, '2012-12-07 00:00:59', '0000-00-00 00:00:00'),
(70, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500', '', '', '', 0, 0, '2012-12-07 05:30:56', '0000-00-00 00:00:00'),
(71, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc', '', '', '', 0, 0, '2012-12-07 16:15:57', '0000-00-00 00:00:00'),
(72, 'http://euro-therm.com.ua/bimetallicheskie-radiatory/ie6.html', '', 'http://yandex.ru/yandsearch?text=%C3%A1%C3%A8%C3%AC%C3%A5%C3%B2%C3%A0%C3%AB%C3%AB%C3%A8%C3%B7%C3%A5%C3%B1%C3%AA%C3%A8%C3%A5+%C3%B0%C3%A0%C3%A4%C3%A8%C', '', 0, 0, '2012-12-08 07:48:52', '0000-00-00 00:00:00'),
(73, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¹Ã…â€œÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦700-detail', '', '', '', 0, 0, '2012-12-08 21:45:43', '0000-00-00 00:00:00'),
(74, 'http://euro-therm.com.ua/index.php?option=com_virtuemart', '', '', '', 0, 0, '2012-12-09 03:58:52', '0000-00-00 00:00:00'),
(75, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name', '', '', '', 0, 0, '2012-12-09 07:14:46', '0000-00-00 00:00:00'),
(76, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,category_name', '', '', '', 0, 0, '2012-12-09 12:47:27', '0000-00-00 00:00:00'),
(77, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,4-9', '', '', '', 0, 0, '2012-12-11 17:18:39', '0000-00-00 00:00:00'),
(78, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,7-12', '', '', '', 0, 0, '2012-12-11 17:34:50', '0000-00-00 00:00:00'),
(79, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,7-36', '', '', '', 0, 0, '2012-12-11 17:41:52', '0000-00-00 00:00:00'),
(80, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,7-18', '', '', '', 0, 0, '2012-12-11 18:28:29', '0000-00-00 00:00:00'),
(81, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-60', '', '', '', 0, 0, '2012-12-11 18:32:08', '0000-00-00 00:00:00'),
(82, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ãƒâ€˜Ã¢â‚¬Â¦800-detail', '', '', '', 0, 0, '2012-12-11 18:40:46', '0000-00-00 00:00:00'),
(83, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-30', '', '', '', 0, 0, '2012-12-11 20:09:30', '0000-00-00 00:00:00'),
(84, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-12', '', '', '', 0, 0, '2012-12-11 21:02:20', '0000-00-00 00:00:00'),
(85, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,4-33', '', '', '', 0, 0, '2012-12-11 22:35:35', '0000-00-00 00:00:00'),
(86, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,7-15', '', '', '', 0, 0, '2012-12-11 23:17:33', '0000-00-00 00:00:00'),
(87, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,4-15', '', '', '', 0, 0, '2012-12-11 23:46:16', '0000-00-00 00:00:00'),
(88, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‹Å“ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦600-detail', '', '', '', 0, 0, '2012-12-12 01:08:39', '0000-00-00 00:00:00'),
(89, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,4-12', '', '', '', 0, 0, '2012-12-12 01:40:52', '0000-00-00 00:00:00'),
(90, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,4-63', '', '', '', 0, 0, '2012-12-12 01:51:23', '0000-00-00 00:00:00'),
(91, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,19-24', '', '', '', 0, 0, '2012-12-12 02:04:43', '0000-00-00 00:00:00'),
(92, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,16-45', '', '', '', 0, 0, '2012-12-12 02:54:13', '0000-00-00 00:00:00'),
(93, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ã‘â€¦400-detail', '', '', '', 0, 0, '2012-12-13 00:44:45', '0000-00-00 00:00:00'),
(94, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х400-detail', '', '', '', 0, 0, '2012-12-13 15:20:18', '0000-00-00 00:00:00'),
(95, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku', '', '', '', 0, 0, '2012-12-14 22:01:29', '0000-00-00 00:00:00'),
(96, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦500-detail', '', '', '', 0, 0, '2012-12-14 23:12:16', '0000-00-00 00:00:00'),
(97, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,product_name', '', '', '', 0, 0, '2012-12-14 23:47:36', '0000-00-00 00:00:00'),
(98, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,category_name?error=404', '', '', '', 0, 0, '2012-12-15 08:19:40', '0000-00-00 00:00:00'),
(99, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku?error=404', '', '', '', 0, 0, '2012-12-15 09:07:22', '0000-00-00 00:00:00'),
(100, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ñ…600-detail', '', '', '', 0, 0, '2012-12-15 16:15:32', '0000-00-00 00:00:00'),
(101, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х500-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2012-12-16 07:37:13', '0000-00-00 00:00:00'),
(102, 'http://euro-therm.com.ua/alyuminievye-radiatory/alyumnievye-radiatory-italia', '', '', '', 0, 0, '2012-12-16 23:53:51', '0000-00-00 00:00:00'),
(103, 'http://euro-therm.com.ua/bimetallicheskie-radiatory/bimetallicheskie-radiatory-kitay', '', '', '', 0, 0, '2012-12-17 02:05:30', '0000-00-00 00:00:00'),
(104, 'http://euro-therm.com.ua/stalnye-radiatory/ie6.html', '', 'http://yandex.ru/yandsearch?text=%C3%B0%C3%A0%C3%A4%C3%A8%C3%A0%C3%B2%C3%AE%C3%B0+%C3%B1%C3%B2%C3%A0%C3%AB%C3%BC%C3%AD%C3%AE%C3%A9&lr=213', '', 0, 0, '2012-12-17 14:06:16', '0000-00-00 00:00:00'),
(105, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦700-detail', '', '', '', 0, 0, '2012-12-17 22:45:13', '0000-00-00 00:00:00'),
(106, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/results,1-3', '', '', '', 0, 0, '2012-12-18 14:17:14', '0000-00-00 00:00:00'),
(107, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500х600-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2012-12-19 17:26:51', '0000-00-00 00:00:00'),
(108, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku/', '', '', '', 0, 0, '2012-12-19 22:34:22', '0000-00-00 00:00:00'),
(109, 'http://euro-therm.com.ua/stalnye-radiatory/undefined', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2012-12-20 03:53:08', '0000-00-00 00:00:00'),
(110, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name/', '', '', '', 0, 0, '2012-12-20 16:09:21', '0000-00-00 00:00:00'),
(111, 'http://euro-therm.com.ua/wp-admin', '', '', '', 0, 0, '2012-12-21 22:07:13', '0000-00-00 00:00:00'),
(112, 'http://euro-therm.com.ua/user', '', '', '', 0, 0, '2012-12-21 22:07:19', '0000-00-00 00:00:00'),
(113, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ãƒâ€˜Ã¢â‚¬Â¦500-detail', '', '', '', 0, 0, '2012-12-22 05:07:51', '0000-00-00 00:00:00'),
(114, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‹Å“ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦700-detail', '', '', '', 0, 0, '2012-12-24 11:58:54', '0000-00-00 00:00:00'),
(115, 'http://euro-therm.com.ua/tovary', '', '', '', 0, 0, '2013-02-27 19:26:53', '0000-00-00 00:00:00'),
(116, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_name', '', '', '', 0, 0, '2013-02-27 21:02:14', '0000-00-00 00:00:00'),
(117, 'http://euro-therm.com.ua/диллерская-сеть', '', '', '', 0, 0, '2013-02-27 23:17:07', '0000-00-00 00:00:00'),
(118, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_sku', '', '', '', 0, 0, '2013-02-27 23:31:24', '0000-00-00 00:00:00'),
(119, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/by,product_name', '', '', '', 0, 0, '2013-02-27 23:32:15', '0000-00-00 00:00:00'),
(120, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/by,category_name', '', '', '', 0, 0, '2013-02-27 23:50:34', '0000-00-00 00:00:00'),
(121, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х400-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2013-02-27 23:52:19', '0000-00-00 00:00:00'),
(122, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х500-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2013-02-28 01:24:47', '0000-00-00 00:00:00'),
(123, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1?error=404', '', '', '', 0, 0, '2013-02-28 02:38:27', '0000-00-00 00:00:00'),
(124, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х400-detail', '', '', '', 0, 0, '2013-02-28 02:44:29', '0000-00-00 00:00:00'),
(125, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х500-detail', '', '', '', 0, 0, '2013-02-28 02:44:30', '0000-00-00 00:00:00'),
(126, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х600-detail', '', '', '', 0, 0, '2013-02-28 02:44:32', '0000-00-00 00:00:00'),
(127, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х700-detail', '', '', '', 0, 0, '2013-02-28 02:44:35', '0000-00-00 00:00:00'),
(128, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х800-detail', '', '', '', 0, 0, '2013-02-28 02:44:37', '0000-00-00 00:00:00'),
(129, 'http://euro-therm.com.ua/меню/диллерская-сеть', '', '', '', 0, 0, '2013-02-28 02:44:52', '0000-00-00 00:00:00'),
(130, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х600-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2013-02-28 03:37:10', '0000-00-00 00:00:00'),
(131, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х700-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2013-02-28 04:55:24', '0000-00-00 00:00:00'),
(132, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc', '', '', '', 0, 0, '2013-02-28 06:15:19', '0000-00-00 00:00:00'),
(133, 'http://euro-therm.com.ua/stalnye-radiatory', '', '', '', 0, 0, '2013-02-28 22:38:32', '0000-00-00 00:00:00'),
(134, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,mf_name?error=404', '', '', '', 0, 0, '2013-03-01 00:33:48', '0000-00-00 00:00:00'),
(135, 'http://euro-therm.com.ua/novosti/17-professionalnyj-partner-grundfos-1', '', '', '', 0, 0, '2013-03-01 00:43:53', '0000-00-00 00:00:00'),
(136, 'http://euro-therm.com.ua/component/virtuemart/Ð°Ð»ÑŽÐ¼Ð¸Ð½Ð¸ÐµÐ²Ñ‹Ðµ-Ñ€Ð°Ð´Ð¸Ð°Ñ‚Ð¾Ñ€Ñ‹', '', '', '', 0, 0, '2013-03-01 00:43:58', '0000-00-00 00:00:00'),
(137, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,category_name?error=404', '', '', '', 0, 0, '2013-03-01 01:00:57', '0000-00-00 00:00:00'),
(138, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,category_name', '', '', '', 0, 0, '2013-03-01 01:01:25', '0000-00-00 00:00:00'),
(139, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,mf_name', '', '', '', 0, 0, '2013-03-01 01:21:04', '0000-00-00 00:00:00'),
(140, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_name?error=404', '', '', '', 0, 0, '2013-03-01 01:21:07', '0000-00-00 00:00:00'),
(141, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/by,product_name?error=404', '', '', '', 0, 0, '2013-03-01 01:21:07', '0000-00-00 00:00:00'),
(142, 'http://euro-therm.com.ua/component/virtuemart/Ð±Ð¸Ð¼ÐµÑ‚Ð°Ð»Ð»Ð¸Ñ‡ÐµÑÐºÐ¸Ðµ-Ñ€Ð°Ð´Ð¸Ð°Ñ‚Ð¾Ñ€Ñ‹', '', '', '', 0, 0, '2013-03-01 01:21:08', '0000-00-00 00:00:00'),
(143, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦800-detail', '', '', '', 0, 0, '2013-03-02 16:09:24', '0000-00-00 00:00:00'),
(144, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ãƒâ€˜Ã¢â‚¬Â¦400-detail', '', '', '', 0, 0, '2013-03-02 16:09:55', '0000-00-00 00:00:00'),
(145, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ñ…400-detail', '', '', '', 0, 0, '2013-03-02 16:10:01', '0000-00-00 00:00:00'),
(146, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ñ…700-detail', '', '', '', 0, 0, '2013-03-02 16:10:04', '0000-00-00 00:00:00'),
(147, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¹Ãƒâ€¦Ã¢â‚¬Å“ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â', '', '', '', 0, 0, '2013-03-02 16:11:45', '0000-00-00 00:00:00'),
(148, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ãƒâ€˜Ã¢â‚¬Â¦700-detail', '', '', '', 0, 0, '2013-03-02 16:12:01', '0000-00-00 00:00:00'),
(149, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ñ…500-detail', '', '', '', 0, 0, '2013-03-02 16:14:31', '0000-00-00 00:00:00'),
(150, 'http://euro-therm.com.ua/Ð´Ð¸Ð»Ð»ÐµÑ€ÑÐºÐ°Ñ-ÑÐµÑ‚ÑŒ', '', '', '', 0, 0, '2013-03-04 21:52:58', '0000-00-00 00:00:00'),
(151, 'http://euro-therm.com.ua/index.php?option=com_fabrik&c=import&view=import&filetype=csv&table=1', '', '', '', 0, 0, '2013-03-05 12:10:39', '0000-00-00 00:00:00'),
(152, 'http://euro-therm.com.ua//images/stories/susu.php?indonesia', '', '', '', 0, 0, '2013-03-06 04:01:25', '0000-00-00 00:00:00'),
(153, 'http://euro-therm.com.ua/泻褍锌懈褌褜-泻褉邪薪褘-褕邪褉芯胁褘械', '', 'http://euro-therm.com.ua/%E6%B3%BB%E8%A4%8D%E9%94%8C%E6%87%88%E8%A4%8C%E8%A4%9C-%E6%B3%BB%E8%A4%89%E9%82%AA%E8%96%AA%E8%A4%98-%E8%A4%95%E9%82%AA%E8%A4', '', 0, 0, '2013-03-08 03:27:19', '0000-00-00 00:00:00'),
(154, 'http://euro-therm.com.ua/upload.php', '', '', '', 0, 0, '2013-03-10 07:07:48', '0000-00-00 00:00:00'),
(155, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,mf_name?error=404', '', '', '', 0, 0, '2013-03-10 08:12:18', '0000-00-00 00:00:00'),
(156, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name?error=404', '', '', '', 0, 0, '2013-03-10 09:04:20', '0000-00-00 00:00:00'),
(157, 'http://euro-therm.com.ua/components/com_user/views/reset/mod.php', '', '', '', 0, 0, '2013-03-11 04:29:05', '0000-00-00 00:00:00'),
(158, 'http://euro-therm.com.ua/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&cid=20', '', '', '', 0, 0, '2013-03-12 12:10:09', '0000-00-00 00:00:00'),
(159, 'http://euro-therm.com.ua/index.php?option=com_fabrik&c=import&view=import&filetype=csv&tableid=1', '', 'http://euro-therm.com.ua/index.php?option=com_fabrik&c=import&view=import&filetype=csv&tableid=1', '', 0, 0, '2013-03-13 04:33:07', '0000-00-00 00:00:00'),
(160, 'http://euro-therm.com.ua/ÐºÐ°ÑÑÐ°-ÑÐ°Ð¹ÑÐ°', '', '', '', 0, 0, '2013-03-13 08:53:45', '0000-00-00 00:00:00'),
(161, 'http://euro-therm.com.ua/ÐºÑÐ¿Ð¸ÑÑ-ÐºÑÐ°Ð½Ñ-ÑÐ°ÑÐ¾Ð²ÑÐµ', '', '', '', 0, 0, '2013-03-13 08:53:56', '0000-00-00 00:00:00'),
(162, 'http://euro-therm.com.ua/ÑÐ¿ÐµÑÐ¸Ð°Ð»ÑÐ½ÑÐµ-Ð¿ÑÐµÐ´Ð»Ð¾Ð¶ÐµÐ½Ð¸Ñ', '', '', '', 0, 0, '2013-03-13 08:53:57', '0000-00-00 00:00:00'),
(163, 'http://euro-therm.com.ua/Ð°ÐºÑÐ¸Ð¸-Ð¸-ÑÐºÐ¸Ð´ÐºÐ¸', '', '', '', 0, 0, '2013-03-13 08:53:58', '0000-00-00 00:00:00'),
(164, 'http://euro-therm.com.ua/Ð¼ÐµÐ½Ñ/Ð±ÑÐµÐ½Ð´Ñ', '', '', '', 0, 0, '2013-03-13 08:54:00', '0000-00-00 00:00:00'),
(165, 'http://euro-therm.com.ua/Ð¼ÐµÐ½Ñ/Ð°ÐºÑÐ¸Ð¸', '', '', '', 0, 0, '2013-03-13 08:54:02', '0000-00-00 00:00:00'),
(166, 'http://euro-therm.com.ua/skype:euro_therm', '', '', '', 0, 0, '2013-03-13 08:54:07', '0000-00-00 00:00:00'),
(167, 'http://euro-therm.com.ua/tmp/php.class.php?token=up', '', '', '', 0, 0, '2013-03-13 13:36:20', '0000-00-00 00:00:00'),
(168, 'http://euro-therm.com.ua/about', '', '', '', 0, 0, '2013-03-13 14:39:56', '0000-00-00 00:00:00'),
(169, 'http://euro-therm.com.ua/LICESNE.php', '', '', '', 0, 0, '2013-03-13 17:22:54', '0000-00-00 00:00:00'),
(170, 'http://euro-therm.com.ua/language/en-GB/en_GB.php?en=phpinfo();', '', '', '', 0, 0, '2013-03-14 00:05:05', '0000-00-00 00:00:00'),
(171, 'http://euro-therm.com.ua/components/com_banners/setting.php?en=phpinfo();', '', '', '', 0, 0, '2013-03-14 00:05:05', '0000-00-00 00:00:00'),
(172, 'http://euro-therm.com.ua/libraries/phputf8/phputf8.php?en=phpinfo();', '', '', '', 0, 0, '2013-03-14 00:05:05', '0000-00-00 00:00:00'),
(173, 'http://euro-therm.com.ua/components/com_wrapper/wraper.php?en=phpinfo();', '', '', '', 0, 0, '2013-03-14 00:05:05', '0000-00-00 00:00:00'),
(174, 'http://euro-therm.com.ua/modules/mod_archive/tmpl/index.php?en=phpinfo();', '', '', '', 0, 0, '2013-03-14 00:05:13', '0000-00-00 00:00:00'),
(175, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,mf_name/results,1-6', '', '', '', 0, 0, '2013-03-14 02:04:58', '0000-00-00 00:00:00'),
(176, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/results,13-24', '', '', '', 0, 0, '2013-03-14 02:07:06', '0000-00-00 00:00:00'),
(177, 'http://euro-therm.com.ua/language/en-GB/language.php', '', '', '', 0, 0, '2013-03-14 08:40:51', '0000-00-00 00:00:00'),
(178, 'http://euro-therm.com.ua/images/stories/jquery.php', '', '', '', 0, 0, '2013-03-14 08:40:52', '0000-00-00 00:00:00'),
(179, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-9', '', '', '', 0, 0, '2013-03-14 11:41:07', '0000-00-00 00:00:00'),
(180, 'http://euro-therm.com.ua/images/stories/x.php?indonesia', '', '', '', 0, 0, '2013-03-14 11:44:17', '0000-00-00 00:00:00'),
(181, 'http://euro-therm.com.ua/images/stories/susu.php?indonesia', '', '', '', 0, 0, '2013-03-14 11:44:18', '0000-00-00 00:00:00'),
(182, 'http://euro-therm.com.ua/images/stories/joomla.class.php?pass=FgYuD@37', '', '', '', 0, 0, '2013-03-14 15:05:38', '0000-00-00 00:00:00'),
(183, 'http://euro-therm.com.ua/images/joomla.class.php?pass=FgYuD@37', '', '', '', 0, 0, '2013-03-14 15:05:39', '0000-00-00 00:00:00'),
(184, 'http://euro-therm.com.ua/tmp/joomla.class.php?pass=FgYuD@37', '', '', '', 0, 0, '2013-03-14 15:05:40', '0000-00-00 00:00:00'),
(185, 'http://euro-therm.com.ua/admin.php', '', '', '', 0, 0, '2013-03-14 17:18:40', '0000-00-00 00:00:00'),
(186, 'http://euro-therm.com.ua/bitrix/admin/index.php?lang=en', '', '', '', 0, 0, '2013-03-14 17:18:40', '0000-00-00 00:00:00'),
(187, 'http://euro-therm.com.ua/admin/login.php', '', '', '', 0, 0, '2013-03-14 17:18:42', '0000-00-00 00:00:00'),
(188, 'http://euro-therm.com.ua/admin/', '', '', '', 0, 0, '2013-03-14 17:18:43', '0000-00-00 00:00:00'),
(189, 'http://euro-therm.com.ua/user/', '', '', '', 0, 0, '2013-03-14 17:18:48', '0000-00-00 00:00:00'),
(190, 'http://euro-therm.com.ua/images/stories/story.php?p1=phpinfo();', '', '', '', 0, 0, '2013-03-15 00:23:36', '0000-00-00 00:00:00'),
(191, 'http://euro-therm.com.ua/images/stories/0day.php', '', '', '', 0, 0, '2013-03-15 02:37:39', '0000-00-00 00:00:00'),
(192, 'http://euro-therm.com.ua/images/stories/gif.php?p1=phpinfo();', '', '', '', 0, 0, '2013-03-15 03:51:45', '0000-00-00 00:00:00'),
(193, 'http://euro-therm.com.ua/images/brands/brand_img', '', '', '', 0, 0, '2013-03-15 16:56:00', '0000-00-00 00:00:00'),
(194, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc?error=404', '', '', '', 0, 0, '2013-03-15 17:11:49', '0000-00-00 00:00:00'),
(195, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,mf_name?error=404', '', '', '', 0, 0, '2013-03-15 18:04:04', '0000-00-00 00:00:00'),
(196, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,category_name', '', '', '', 0, 0, '2013-03-15 18:32:27', '0000-00-00 00:00:00'),
(197, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,mf_name/results,1-60', '', '', '', 0, 0, '2013-03-16 08:29:08', '0000-00-00 00:00:00'),
(198, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/results,1-12', '', '', '', 0, 0, '2013-03-16 09:07:40', '0000-00-00 00:00:00'),
(199, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,mf_name/results,1-3', '', '', '', 0, 0, '2013-03-16 10:14:44', '0000-00-00 00:00:00'),
(200, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-6', '', '', '', 0, 0, '2013-03-19 00:33:21', '0000-00-00 00:00:00'),
(201, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku/results,1-30', '', '', '', 0, 0, '2013-03-19 14:52:59', '0000-00-00 00:00:00'),
(202, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku/results,1-9', '', '', '', 0, 0, '2013-03-19 15:43:53', '0000-00-00 00:00:00'),
(203, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/results,1-9', '', '', '', 0, 0, '2013-03-20 11:07:43', '0000-00-00 00:00:00'),
(204, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku/results,1-12', '', '', '', 0, 0, '2013-03-22 08:14:27', '0000-00-00 00:00:00'),
(205, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_sku/results,1-60', '', '', '', 0, 0, '2013-03-22 08:22:23', '0000-00-00 00:00:00'),
(206, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,mf_name', '', '', '', 0, 0, '2013-03-25 20:11:43', '0000-00-00 00:00:00'),
(207, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,category_name/results,1-9', '', '', '', 0, 0, '2013-03-27 04:05:56', '0000-00-00 00:00:00'),
(208, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name/results,1-12', '', '', '', 0, 0, '2013-03-27 04:21:04', '0000-00-00 00:00:00'),
(209, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name/results,1-9', '', '', '', 0, 0, '2013-03-27 04:29:08', '0000-00-00 00:00:00'),
(210, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/by,product_name/results,1-60', '', '', '', 0, 0, '2013-03-29 16:56:43', '0000-00-00 00:00:00'),
(211, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/results,1-3', '', '', '', 0, 0, '2013-03-30 16:31:18', '0000-00-00 00:00:00'),
(212, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500Ñ…500-detail', '', '', '', 0, 0, '2013-04-05 06:59:38', '0000-00-00 00:00:00'),
(213, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500х800-detail?task=recommend&tmpl=component', '', '', '', 0, 0, '2013-04-06 12:17:02', '0000-00-00 00:00:00'),
(214, 'http://euro-therm.com.ua/administrator/core.php', '', '', '', 0, 0, '2013-04-06 19:07:06', '0000-00-00 00:00:00'),
(215, 'http://euro-therm.com.ua/cgi-bin/web.php', '', '', '', 0, 0, '2013-04-06 19:07:06', '0000-00-00 00:00:00'),
(216, 'http://euro-therm.com.ua/components/config.php', '', '', '', 0, 0, '2013-04-06 19:07:06', '0000-00-00 00:00:00'),
(217, 'http://euro-therm.com.ua/forum/include.php', '', '', '', 0, 0, '2013-04-06 19:07:07', '0000-00-00 00:00:00'),
(218, 'http://euro-therm.com.ua/images/images.php', '', '', '', 0, 0, '2013-04-06 19:07:07', '0000-00-00 00:00:00'),
(219, 'http://euro-therm.com.ua/includes/images.php', '', '', '', 0, 0, '2013-04-06 19:07:07', '0000-00-00 00:00:00'),
(220, 'http://euro-therm.com.ua/language/stats.php', '', '', '', 0, 0, '2013-04-06 19:07:08', '0000-00-00 00:00:00'),
(221, 'http://euro-therm.com.ua/libraries/web.php', '', '', '', 0, 0, '2013-04-06 19:07:08', '0000-00-00 00:00:00'),
(222, 'http://euro-therm.com.ua/logs/include.php', '', '', '', 0, 0, '2013-04-06 19:07:09', '0000-00-00 00:00:00'),
(223, 'http://euro-therm.com.ua/media/include.php', '', '', '', 0, 0, '2013-04-06 19:07:09', '0000-00-00 00:00:00'),
(224, 'http://euro-therm.com.ua/modules/application.php', '', '', '', 0, 0, '2013-04-06 19:07:10', '0000-00-00 00:00:00'),
(225, 'http://euro-therm.com.ua/plugins/config.php', '', '', '', 0, 0, '2013-04-06 19:07:10', '0000-00-00 00:00:00'),
(226, 'http://euro-therm.com.ua/remos_downloads/config.php', '', '', '', 0, 0, '2013-04-06 19:07:10', '0000-00-00 00:00:00'),
(227, 'http://euro-therm.com.ua/templates/version.php', '', '', '', 0, 0, '2013-04-06 19:07:11', '0000-00-00 00:00:00'),
(228, 'http://euro-therm.com.ua/tmp/info.php', '', '', '', 0, 0, '2013-04-06 19:07:11', '0000-00-00 00:00:00'),
(229, 'http://euro-therm.com.ua/xmlrpc/version.php', '', '', '', 0, 0, '2013-04-06 19:07:11', '0000-00-00 00:00:00'),
(230, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/ie6.html', '', '', '', 0, 0, '2013-04-15 16:34:20', '0000-00-00 00:00:00'),
(231, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500?500-detail', '', '', '', 0, 0, '2013-04-15 22:45:27', '0000-00-00 00:00:00'),
(232, 'http://euro-therm.com.ua/images/xxx.php?cmd=ls', '', '', '', 0, 0, '2013-04-21 11:15:37', '0000-00-00 00:00:00'),
(233, 'http://euro-therm.com.ua/wp-admin/', '', '', '', 0, 0, '2013-04-23 15:30:46', '0000-00-00 00:00:00'),
(234, 'http://euro-therm.com.ua/engine/print.php?newsid=0', '', '', '', 0, 0, '2013-04-23 15:30:48', '0000-00-00 00:00:00'),
(235, 'http://euro-therm.com.ua/modx/manager/', '', '', '', 0, 0, '2013-04-23 15:30:56', '0000-00-00 00:00:00'),
(236, 'http://euro-therm.com.ua/media/bb.php', '', '', '', 0, 0, '2013-05-06 21:57:20', '0000-00-00 00:00:00'),
(237, 'http://euro-therm.com.ua/images/stories/images/x.php', '', '', '', 0, 0, '2013-05-07 07:34:26', '0000-00-00 00:00:00'),
(238, 'http://euro-therm.com.ua/components/p.php', '', '', '', 0, 0, '2013-05-07 20:06:07', '0000-00-00 00:00:00'),
(239, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/orderDesc/by,product_name?error=404', '', '', '', 0, 0, '2013-05-13 07:26:07', '0000-00-00 00:00:00'),
(240, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/results,1-6', '', '', '', 0, 0, '2013-05-16 17:14:54', '0000-00-00 00:00:00'),
(241, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/by,product_sku', '', '', '', 0, 0, '2013-05-26 15:26:40', '0000-00-00 00:00:00'),
(242, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,mf_name/results,1-6', '', '', '', 0, 0, '2013-05-26 16:19:56', '0000-00-00 00:00:00'),
(243, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,mf_name/results,1-9', '', '', '', 0, 0, '2013-05-26 19:52:37', '0000-00-00 00:00:00'),
(244, 'http://euro-therm.com.ua/index.php?option=com_user&view=register&lang=ru', '', '', '', 0, 0, '2013-06-06 07:21:03', '0000-00-00 00:00:00'),
(245, 'http://euro-therm.com.ua/admin', '', '', '', 0, 0, '2013-06-26 13:40:53', '0000-00-00 00:00:00'),
(246, 'http://euro-therm.com.ua/edit', '', '', '', 0, 0, '2013-06-26 13:41:20', '0000-00-00 00:00:00'),
(247, 'http://euro-therm.com.ua/media/system/media.php', '', '', '', 0, 0, '2013-06-26 18:23:34', '0000-00-00 00:00:00'),
(248, 'http://euro-therm.com.ua/ргшролр', '', '', '', 0, 0, '2013-06-26 19:56:05', '0000-00-00 00:00:00'),
(249, 'http://euro-therm.com.ua/images/stories/ll.php', '', '', '', 0, 0, '2013-06-27 12:57:38', '0000-00-00 00:00:00'),
(250, 'http://euro-therm.com.ua//administrator/components/com_contact/pp1.php', '', '', '', 0, 0, '2013-06-27 19:43:52', '0000-00-00 00:00:00'),
(251, 'http://euro-therm.com.ua/index.htm', '', '', '', 0, 0, '2013-06-29 05:14:47', '0000-00-00 00:00:00'),
(252, 'http://euro-therm.com.ua/default.htm', '', '', '', 0, 0, '2013-06-29 05:15:51', '0000-00-00 00:00:00'),
(253, 'http://euro-therm.com.ua/default.html', '', '', '', 0, 0, '2013-06-29 05:15:53', '0000-00-00 00:00:00'),
(254, 'http://euro-therm.com.ua/default.php', '', '', '', 0, 0, '2013-06-29 05:15:57', '0000-00-00 00:00:00'),
(255, 'http://euro-therm.com.ua/main.htm', '', '', '', 0, 0, '2013-06-29 05:15:59', '0000-00-00 00:00:00'),
(256, 'http://euro-therm.com.ua/main.html', '', '', '', 0, 0, '2013-06-29 05:16:03', '0000-00-00 00:00:00'),
(257, 'http://euro-therm.com.ua/main.php', '', '', '', 0, 0, '2013-06-29 05:16:06', '0000-00-00 00:00:00'),
(258, 'http://euro-therm.com.ua/.svn/entries', '', '', '', 0, 0, '2013-06-29 15:44:04', '0000-00-00 00:00:00'),
(259, 'http://euro-therm.com.ua/wp-conf.php', '', '', '', 0, 0, '2013-06-29 20:49:57', '0000-00-00 00:00:00'),
(260, 'http://euro-therm.com.ua/includes/Archive/w.php', '', '', '', 0, 0, '2013-07-04 17:59:12', '0000-00-00 00:00:00'),
(261, 'http://euro-therm.com.ua/components/com_contact/views/w.php', '', '', '', 0, 0, '2013-07-05 03:10:53', '0000-00-00 00:00:00'),
(262, 'http://euro-therm.com.ua/components/com_contact/p.php', '', '', '', 0, 0, '2013-07-05 14:14:59', '0000-00-00 00:00:00'),
(263, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_sku/results,1-9', '', '', '', 0, 0, '2013-07-18 04:56:39', '0000-00-00 00:00:00'),
(264, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_sku/results,1-6', '', '', '', 0, 0, '2013-07-18 05:42:18', '0000-00-00 00:00:00'),
(265, 'http://euro-therm.com.ua/bimetallicheskie-radiatory/', '', '', '', 0, 0, '2013-07-20 10:45:57', '0000-00-00 00:00:00'),
(266, 'http://euro-therm.com.ua/kontakty/registration', '', '', '', 0, 0, '2013-07-27 00:51:32', '0000-00-00 00:00:00'),
(267, 'http://euro-therm.com.ua/registration', '', '', '', 0, 0, '2013-07-27 00:51:34', '0000-00-00 00:00:00'),
(268, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_name/results,1-9', '', '', '', 0, 0, '2013-07-28 04:46:39', '0000-00-00 00:00:00'),
(269, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,product_name/results,1-6', '', '', '', 0, 0, '2013-07-28 05:00:12', '0000-00-00 00:00:00'),
(270, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,category_name/results,1-9', '', '', '', 0, 0, '2013-07-28 05:08:31', '0000-00-00 00:00:00'),
(271, 'http://euro-therm.com.ua/component/', '', 'http://euro-therm.com.ua/component/', '', 0, 0, '2013-08-28 08:54:08', '0000-00-00 00:00:00'),
(272, 'http://euro-therm.com.ua/bimetallicheskie-radiatory/undefined', '', 'http://euro-therm.com.ua/bimetallicheskie-radiatory/kitay', '', 0, 0, '2013-09-08 21:20:52', '0000-00-00 00:00:00'),
(273, 'http://euro-therm.com.ua/stalnye-radiatory/function(){return v;}', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-10 15:13:05', '0000-00-00 00:00:00'),
(274, 'http://euro-therm.com.ua/stalnye-radiatory/function Array() { [native code] }', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-10 15:13:05', '0000-00-00 00:00:00'),
(275, 'http://euro-therm.com.ua/stalnye-radiatory/function(){var d=Math.round(this[2]/100*255);if(this[1]==0){return[d,d,d];}else{var b=this[0]60;var g=b`;var h=Math.round((this[2]*(100-this[1]))/10000*255);var e=Math.round((this[2]*(6000-this[1]*g))/600000*255)', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-10 15:13:06', '0000-00-00 00:00:00'),
(276, 'http://euro-therm.com.ua/stalnye-radiatory/function(){var c=this[0],d=this[1],k=this[2],h=0;var j=Math.max(c,d,k),f=Math.min(c,d,k);var l=j-f;var i=j/255,g=(j!=0)?l/j:0;if(g!=0){var e=(j-c)/l;var b=(j-d)/l;var m=(j-k)/l;if(c==j){h=m-b;}else{if(d==j){h=2+e', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-10 15:13:06', '0000-00-00 00:00:00'),
(277, 'http://euro-therm.com.ua/stalnye-radiatory/function (){return v;}', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-13 15:12:33', '0000-00-00 00:00:00'),
(278, 'http://euro-therm.com.ua/stalnye-radiatory/function Array() {    [native code]}', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-13 15:12:33', '0000-00-00 00:00:00'),
(279, 'http://euro-therm.com.ua/stalnye-radiatory/function (){var c=this[0],d=this[1],k=this[2],h=0;var j=Math.max(c,d,k),f=Math.min(c,d,k);var l=j-f;var i=j/255,g=(j!=0)?l/j:0;if(g!=0){var e=(j-c)/l;var b=(j-d)/l;var m=(j-k)/l;if(c==j){h=m-b;}else{if(d==j){h=2+', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-13 15:12:34', '0000-00-00 00:00:00'),
(280, 'http://euro-therm.com.ua/stalnye-radiatory/function (){var d=Math.round(this[2]/100*255);if(this[1]==0){return[d,d,d];}else{var b=this[0]60;var g=b`;var h=Math.round((this[2]*(100-this[1]))/10000*255);var e=Math.round((this[2]*(6000-this[1]*g))/600000*255', '', 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm', '', 0, 0, '2013-09-13 15:12:34', '0000-00-00 00:00:00'),
(281, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/by,category_name/results,1-6', '', '', '', 0, 0, '2013-09-17 21:49:43', '0000-00-00 00:00:00'),
(282, 'http://euro-therm.com.ua/admin/index.php', '', '', '', 0, 0, '2013-09-25 07:02:02', '0000-00-00 00:00:00'),
(283, 'http://euro-therm.com.ua/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid=20&6bc427c8a7981f4fe1f5ac65c1246b5f=9d09f693c63c1988a9f8a564e0da7743', '', '', '', 0, 0, '2013-10-08 13:02:39', '0000-00-00 00:00:00'),
(284, 'http://euro-therm.com.ua/media/system/js/)?n.html:n.body;}})();Element.alias({position:', '', '', '', 0, 0, '2013-10-14 13:17:12', '0000-00-00 00:00:00'),
(285, 'http://euro-therm.com.ua/media/system/js/in e)?e.htmlFor=l:e.setAttribute(', '', '', '', 0, 0, '2013-10-14 13:17:12', '0000-00-00 00:00:00'),
(286, 'http://euro-therm.com.ua/media/system/js/in this)?this.htmlFor:this.getAttribute(', '', '', '', 0, 0, '2013-10-14 13:17:16', '0000-00-00 00:00:00'),
(287, 'http://euro-therm.com.ua/components/com_chronoforms/js/formcheck/"+g+"', '', '', '', 0, 0, '2013-10-14 13:17:17', '0000-00-00 00:00:00'),
(288, 'http://euro-therm.com.ua/index.php?option=com_tag&controller=tag&task=add&article_id=-260479/**//*!union*//**//*!select*//**/group_concat(0x3C6B65793E,username,0x3a,password,0x3a,usertype,0x3C62723E,0x3C6B6579733E)/**/from/**/jos_users/**/where/**/usertyp', '', '', '', 0, 0, '2013-10-18 21:49:51', '0000-00-00 00:00:00'),
(289, 'http://euro-therm.com.ua/user/login/index.php', '', '', '', 0, 0, '2013-10-20 16:47:48', '0000-00-00 00:00:00'),
(290, 'http://euro-therm.com.ua/novosti/ie6.html', '', 'http://euro-therm.com.ua/', '', 0, 0, '2013-10-21 13:36:10', '0000-00-00 00:00:00'),
(291, 'http://euro-therm.com.ua/index.php?option=com_user&view=register', '', '', '', 0, 0, '2013-11-05 11:39:05', '0000-00-00 00:00:00'),
(292, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ñ…800-detail', '', '', '', 0, 0, '2013-11-06 20:12:59', '0000-00-00 00:00:00'),
(293, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/500', '', '', '', 0, 0, '2013-11-08 14:40:46', '0000-00-00 00:00:00'),
(294, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ã‘â€¦800-detail', '', '', '', 0, 0, '2013-11-11 15:30:44', '0000-00-00 00:00:00'),
(295, 'http://euro-therm.com.ua/ÃÂºÃ‘Æ’ÃÂ¿ÃÂ¸Ã‘â€šÃ‘Å’-ÃÂºÃ‘â‚¬ÃÂ°ÃÂ½Ã‘â€¹-Ã‘Ë†ÃÂ°Ã‘â‚¬ÃÂ¾ÃÂ²Ã‘â€¹ÃÂµ', '', '', '', 0, 0, '2013-11-11 22:45:12', '0000-00-00 00:00:00'),
(296, 'http://euro-therm.com.ua/stalnye-radiatory/stalnye-radiatory-podpunkt-1/orderDesc/by,mf_name', '', '', '', 0, 0, '2013-11-12 09:48:42', '0000-00-00 00:00:00'),
(297, 'http://euro-therm.com.ua/ÃÂ¼ÃÂµÃÂ½Ã‘Å½/ÃÂ±Ã‘â‚¬ÃÂµÃÂ½ÃÂ´Ã‘â€¹', '', '', '', 0, 0, '2013-11-14 18:07:49', '0000-00-00 00:00:00'),
(298, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ã‘â€¦600-detail', '', '', '', 0, 0, '2013-11-15 19:51:01', '0000-00-00 00:00:00'),
(299, 'http://euro-therm.com.ua/component/virtuemart/ÑÑ‚Ð°Ð»ÑŒÐ½Ñ‹Ðµ-Ñ€Ð°Ð´Ð¸Ð°Ñ‚Ð¾Ñ€Ñ‹2012-04-17-13-20-07_', '', '', '', 0, 0, '2013-11-16 04:12:53', '0000-00-00 00:00:00'),
(300, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ã‘â€¦700-detail', '', '', '', 0, 0, '2013-11-16 11:52:41', '0000-00-00 00:00:00'),
(301, 'http://euro-therm.com.ua/index.php?option=com_ignitegallery&task=view&gallery=-1+union+select+1,2,group_concat(0x3C6B65793E,username,0x3a,password,0x3a,usertype,0x3a,0x3C62723E,0x3C6B6579733E)KHG,4,5,6,7,8,9,10+from+jos_users+where+usertype=''Super Adminis', '', '', '', 0, 0, '2013-11-18 03:34:20', '0000-00-00 00:00:00'),
(302, 'http://euro-therm.com.ua/muieblackcat', '', '', '', 0, 0, '2013-11-25 21:22:12', '0000-00-00 00:00:00'),
(303, 'http://euro-therm.com.ua//admin/phpmyadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:17', '0000-00-00 00:00:00'),
(304, 'http://euro-therm.com.ua//admin/pma/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:26', '0000-00-00 00:00:00'),
(305, 'http://euro-therm.com.ua//admin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:32', '0000-00-00 00:00:00'),
(306, 'http://euro-therm.com.ua//db/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:35', '0000-00-00 00:00:00'),
(307, 'http://euro-therm.com.ua//dbadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:36', '0000-00-00 00:00:00'),
(308, 'http://euro-therm.com.ua//myadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:37', '0000-00-00 00:00:00'),
(309, 'http://euro-therm.com.ua//mysql/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:38', '0000-00-00 00:00:00'),
(310, 'http://euro-therm.com.ua//mysqladmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:45', '0000-00-00 00:00:00'),
(311, 'http://euro-therm.com.ua//php-my-admin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:46', '0000-00-00 00:00:00'),
(312, 'http://euro-therm.com.ua//phpMyAdmin-2.5.5-pl1/index.php', '', '', '', 0, 0, '2013-11-25 21:22:47', '0000-00-00 00:00:00'),
(313, 'http://euro-therm.com.ua//phpMyAdmin-2.5.5/index.php', '', '', '', 0, 0, '2013-11-25 21:22:53', '0000-00-00 00:00:00'),
(314, 'http://euro-therm.com.ua//phpadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:22:58', '0000-00-00 00:00:00'),
(315, 'http://euro-therm.com.ua//phpmyadmin1/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:00', '0000-00-00 00:00:00'),
(316, 'http://euro-therm.com.ua//phpmyadmin2/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:01', '0000-00-00 00:00:00'),
(317, 'http://euro-therm.com.ua//pma/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:02', '0000-00-00 00:00:00'),
(318, 'http://euro-therm.com.ua//scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:03', '0000-00-00 00:00:00'),
(319, 'http://euro-therm.com.ua//typo3/phpmyadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:04', '0000-00-00 00:00:00'),
(320, 'http://euro-therm.com.ua//web/phpMyAdmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:04', '0000-00-00 00:00:00'),
(321, 'http://euro-therm.com.ua//web/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:05', '0000-00-00 00:00:00'),
(322, 'http://euro-therm.com.ua//websql/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:06', '0000-00-00 00:00:00'),
(323, 'http://euro-therm.com.ua//xampp/phpmyadmin/scripts/setup.php', '', '', '', 0, 0, '2013-11-25 21:23:07', '0000-00-00 00:00:00'),
(324, 'http://euro-therm.com.ua/textpattern/', '', '', '', 0, 0, '2013-12-05 09:04:40', '0000-00-00 00:00:00'),
(325, 'http://euro-therm.com.ua/bitrix/admin/', '', '', '', 0, 0, '2013-12-05 09:04:46', '0000-00-00 00:00:00'),
(326, 'http://euro-therm.com.ua/netcat/', '', '', '', 0, 0, '2013-12-05 09:04:48', '0000-00-00 00:00:00'),
(327, 'http://euro-therm.com.ua/typo3/index.php', '', '', '', 0, 0, '2013-12-05 09:04:50', '0000-00-00 00:00:00');
INSERT INTO `o1hap_redirect_links` (`id`, `old_url`, `new_url`, `referer`, `comment`, `hits`, `published`, `created_date`, `modified_date`) VALUES
(328, 'http://euro-therm.com.ua/login_form', '', '', '', 0, 0, '2013-12-05 09:04:52', '0000-00-00 00:00:00'),
(329, 'http://euro-therm.com.ua/sitetree/', '', '', '', 0, 0, '2013-12-05 09:05:02', '0000-00-00 00:00:00'),
(330, 'http://euro-therm.com.ua/cms/kernel/admin.php', '', '', '', 0, 0, '2013-12-05 09:05:10', '0000-00-00 00:00:00'),
(331, 'http://euro-therm.com.ua/login.php', '', '', '', 0, 0, '2013-12-05 09:05:12', '0000-00-00 00:00:00'),
(332, 'http://euro-therm.com.ua/system/cms/', '', '', '', 0, 0, '2013-12-05 09:05:26', '0000-00-00 00:00:00'),
(333, 'http://euro-therm.com.ua/member/login/', '', '', '', 0, 0, '2013-12-05 09:05:30', '0000-00-00 00:00:00'),
(334, 'http://euro-therm.com.ua/admin/login/', '', '', '', 0, 0, '2013-12-05 09:06:30', '0000-00-00 00:00:00'),
(335, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm<br>', '', '', '', 0, 0, '2013-12-05 20:46:41', '0000-00-00 00:00:00'),
(336, 'http://euro-therm.com.ua/templates/euro-therm/js/''+c+''', '', '', '', 0, 0, '2013-12-09 18:31:13', '0000-00-00 00:00:00'),
(337, 'http://euro-therm.com.ua/images/j.php', '', '', '', 0, 0, '2013-12-09 18:50:22', '0000-00-00 00:00:00'),
(338, 'http://euro-therm.com.ua/ÃÂ°ÃÂºÃ‘â€ ÃÂ¸ÃÂ¸-ÃÂ¸-Ã‘ÂÃÂºÃÂ¸ÃÂ´ÃÂºÃÂ¸', '', '', '', 0, 0, '2013-12-11 06:13:48', '0000-00-00 00:00:00'),
(339, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500Ãƒâ€˜Ã¢â‚¬Â¦600-detail', '', '', '', 0, 0, '2013-12-18 06:23:53', '0000-00-00 00:00:00'),
(340, 'http://euro-therm.com.ua/index.php?option=com_kunena&func=userlist&search=%''+and+1=2)+union+select+1,group_concat(0x3C6B65793E,username,0x3a,password,0x3a,usertype,0x3a,0x3C62723E,0x3C6B6579733E),3,4,5,6,7,8,9,10,11,12,13,14,15,16+from+jos_users+where+use', '', '', '', 0, 0, '2013-12-25 02:43:51', '0000-00-00 00:00:00'),
(341, 'http://euro-therm.com.ua/''+c+''', '', '', '', 0, 0, '2013-12-25 16:52:35', '0000-00-00 00:00:00'),
(342, 'http://euro-therm.com.ua/index.php?view=videos&type=member&user_id=-62+union+select+1,2,password,4,5,6,7,8,9,10,11,12,group_concat(0x3C6B65793E,username,0x3a,password,0x3a,usertype,0x3a,block,0x3a,activation,0x3a,0x3C62723E,0x3C6B6579733E),14,15,16,17,18,', '', '', '', 0, 0, '2013-12-27 21:52:14', '0000-00-00 00:00:00'),
(343, 'http://euro-therm.com.ua/stalnye-radiatory/eurotherm/500ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‹Å“ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¦500-detail', '', '', '', 0, 0, '2014-01-06 13:17:45', '0000-00-00 00:00:00'),
(344, 'http://euro-therm.com.ua/administrator/index.php', '', '', '', 0, 0, '2014-01-10 15:47:38', '0000-00-00 00:00:00'),
(345, 'http://euro-therm.com.ua/index.php?do=form&id=1+and+1=0+union+select+1,2,3,4,5,6,7,8,9,group_concat(0x3a,0x3c6b65793e,0x3a,name,0x3a,password,0x3c6b6579733e),11+from+dle_users+where+user_group=1+--++--+', '', '', '', 0, 0, '2014-01-11 15:54:22', '0000-00-00 00:00:00'),
(346, 'http://euro-therm.com.ua/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager', '', '', '', 0, 0, '2014-01-12 21:38:51', '0000-00-00 00:00:00'),
(347, 'http://euro-therm.com.ua/administartor', '', '', '', 0, 0, '2014-01-13 13:17:07', '0000-00-00 00:00:00'),
(348, 'http://euro-therm.com.ua/administartor/', '', '', '', 0, 0, '2014-01-13 13:17:16', '0000-00-00 00:00:00'),
(349, 'http://euro-therm.com.ua/administartor/index.php', '', '', '', 0, 0, '2014-01-13 13:17:29', '0000-00-00 00:00:00'),
(350, 'http://euro-therm.com.ua/tmp/j.php', '', '', '', 0, 0, '2014-01-16 08:03:19', '0000-00-00 00:00:00'),
(351, 'http://euro-therm.com.ua/tmp/k.php', '', '', '', 0, 0, '2014-01-16 22:11:06', '0000-00-00 00:00:00'),
(352, 'http://euro-therm.com.ua/images/i.php', '', '', '', 0, 0, '2014-01-18 19:11:07', '0000-00-00 00:00:00'),
(353, 'http://euro-therm.com.ua/images/stories/gif.php', '', '', '', 0, 0, '2014-01-27 12:46:19', '0000-00-00 00:00:00'),
(354, 'http://euro-therm.com.ua/images/banners/banner.php', '', '', '', 0, 0, '2014-01-27 18:05:11', '0000-00-00 00:00:00'),
(355, 'http://euro-therm.com.ua/''', '', '', '', 0, 0, '2014-02-02 23:25:51', '0000-00-00 00:00:00'),
(356, 'http://euro-therm.com.ua/components/com_jmultimedia/assets/thumbs/phpthumb/fonts/index.html', '', '', '', 0, 0, '2014-02-06 02:27:15', '0000-00-00 00:00:00'),
(357, 'http://euro-therm.com.ua/components/com_flexicontent/librairies/phpthumb/fonts/index.html', '', '', '', 0, 0, '2014-02-06 02:27:16', '0000-00-00 00:00:00'),
(358, 'http://euro-therm.com.ua/components/com_hotornot2/phpthumb/fonts/index.html', '', '', '', 0, 0, '2014-02-06 02:27:16', '0000-00-00 00:00:00'),
(359, 'http://euro-therm.com.ua/components/com_portfolio/includes/phpthumb/fonts/index.html', '', '', '', 0, 0, '2014-02-06 02:27:17', '0000-00-00 00:00:00'),
(360, 'http://euro-therm.com.ua/components/com_alphauserpoints/assets/phpthumb/fonts/index.html', '', '', '', 0, 0, '2014-02-06 02:27:18', '0000-00-00 00:00:00'),
(361, 'http://euro-therm.com.ua/components/com_community/index.html', '', '', '', 0, 0, '2014-02-06 14:59:39', '0000-00-00 00:00:00'),
(362, 'http://euro-therm.com.ua/   - Russia', '', '', '', 0, 0, '2014-02-07 08:56:50', '0000-00-00 00:00:00'),
(363, 'http://euro-therm.com.ua/images/post.php', '', '', '', 0, 0, '2014-02-08 09:53:25', '0000-00-00 00:00:00'),
(364, 'http://euro-therm.com.ua/tmp/xmlrpc.php', '', '', '', 0, 0, '2014-02-08 23:46:56', '0000-00-00 00:00:00'),
(365, 'http://euro-therm.com.ua/index.php/using-joomla/extensions/components/users-component/registration-form', '', 'http://eurookna24.ru/index.php/using-joomla/extensions/components/users-component/registration-form', '', 0, 0, '2014-02-12 13:48:11', '0000-00-00 00:00:00'),
(366, 'http://euro-therm.com.ua/Fckeditor/_whatsnew.html', '', '', '', 0, 0, '2014-02-22 13:02:53', '0000-00-00 00:00:00'),
(367, 'http://euro-therm.com.ua//kreotonsystems.php', '', '', '', 0, 0, '2014-02-27 12:36:51', '0000-00-00 00:00:00'),
(368, 'http://euro-therm.com.ua//readme.html', '', '', '', 0, 0, '2014-02-27 12:36:51', '0000-00-00 00:00:00'),
(369, 'http://euro-therm.com.ua//wp-login.php', '', '', '', 0, 0, '2014-02-27 12:36:52', '0000-00-00 00:00:00'),
(370, 'http://euro-therm.com.ua//wp-admin/ms-users.php', '', '', '', 0, 0, '2014-02-27 12:36:52', '0000-00-00 00:00:00'),
(371, 'http://euro-therm.com.ua/index.php?option=com_myblog', '', '', '', 0, 0, '2014-03-02 12:42:22', '0000-00-00 00:00:00'),
(372, 'http://euro-therm.com.ua/modules/mod_articless/mod_articless.php', '', '', '', 0, 0, '2014-03-03 13:34:13', '0000-00-00 00:00:00'),
(373, 'http://euro-therm.com.ua/bitrix/admin/index.php', '', '', '', 0, 0, '2014-03-03 17:52:15', '0000-00-00 00:00:00'),
(374, 'http://euro-therm.com.ua/shell.php', '', '', '', 0, 0, '2014-03-03 18:47:25', '0000-00-00 00:00:00'),
(375, 'http://euro-therm.com.ua/wso.php', '', '', '', 0, 0, '2014-03-03 19:48:33', '0000-00-00 00:00:00'),
(376, 'http://euro-therm.com.ua/netcat/admin/index.php', '', '', '', 0, 0, '2014-03-03 20:43:09', '0000-00-00 00:00:00'),
(377, 'http://euro-therm.com.ua/remview.php', '', '', '', 0, 0, '2014-03-04 07:37:46', '0000-00-00 00:00:00'),
(378, 'http://euro-therm.com.ua/install/upgrade.php', '', '', '', 0, 0, '2014-03-04 08:32:48', '0000-00-00 00:00:00'),
(379, 'http://euro-therm.com.ua/vb/install/upgrade.php', '', '', '', 0, 0, '2014-03-04 09:56:00', '0000-00-00 00:00:00'),
(380, 'http://euro-therm.com.ua/modules/modules/mod_ariimageslider/mod_ariimageslider.php', '', '', '', 0, 0, '2014-03-12 15:38:12', '0000-00-00 00:00:00'),
(381, 'http://euro-therm.com.ua/modules/img/mode.php', '', '', '', 0, 0, '2014-03-26 15:53:25', '0000-00-00 00:00:00'),
(382, 'http://euro-therm.com.ua/includes/img/mode.php', '', '', '', 0, 0, '2014-03-27 03:37:27', '0000-00-00 00:00:00'),
(383, 'http://euro-therm.com.ua/tmp/old.php', '', '', '', 0, 0, '2014-03-27 14:46:25', '0000-00-00 00:00:00'),
(384, 'http://euro-therm.com.ua/index.php?option=com_user&task=register', '', '', '', 0, 0, '2014-03-27 15:25:35', '0000-00-00 00:00:00'),
(385, 'http://euro-therm.com.ua/index.php?option=com_tag&controller=tag&task=add&article_id=-260479/**//*!union*//**//*!select*//**/concat(username,0x3a,password,0x3a,usertype)/**//*!from*//**/jos_users/**/&tmpl=component', '', '', '', 0, 0, '2014-03-30 18:26:52', '0000-00-00 00:00:00'),
(386, 'http://euro-therm.com.ua/forum', '', '', '', 0, 0, '2014-03-31 18:14:47', '0000-00-00 00:00:00'),
(387, 'http://euro-therm.com.ua/blog/wp-login.php', '', '', '', 0, 0, '2014-04-02 02:41:35', '0000-00-00 00:00:00'),
(388, 'http://euro-therm.com.ua//index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&cid=20', '', '', '', 0, 0, '2014-04-02 23:52:45', '0000-00-00 00:00:00'),
(389, 'http://euro-therm.com.ua//mambots/editors/fckeditor/editor/filemanager/browser/default/browser.html', '', '', '', 0, 0, '2014-04-07 23:19:45', '0000-00-00 00:00:00'),
(390, 'http://euro-therm.com.ua/modules/mod_articless/patch.php', '', '', '', 0, 0, '2014-04-09 04:58:44', '0000-00-00 00:00:00'),
(391, 'http://euro-therm.com.ua/modules/mod_help/mod_help.php', '', '', '', 0, 0, '2014-04-10 09:07:37', '0000-00-00 00:00:00'),
(392, 'http://euro-therm.com.ua/modules/mod_system/mod_system.php', '', '', '', 0, 0, '2014-04-11 07:26:17', '0000-00-00 00:00:00'),
(393, 'http://euro-therm.com.ua/maps/myplaces', '', '', '', 0, 0, '2014-04-11 13:38:29', '0000-00-00 00:00:00'),
(394, 'http://euro-therm.com.ua/maps/tt/optin', '', '', '', 0, 0, '2014-04-11 14:43:52', '0000-00-00 00:00:00'),
(395, 'http://euro-therm.com.ua/kh', '', '', '', 0, 0, '2014-04-12 00:07:42', '0000-00-00 00:00:00'),
(396, 'http://euro-therm.com.ua/maps/c', '', '', '', 0, 0, '2014-04-12 12:38:55', '0000-00-00 00:00:00'),
(397, 'http://euro-therm.com.ua/intl/ru_ua/dmca.html', '', '', '', 0, 0, '2014-04-12 12:45:14', '0000-00-00 00:00:00'),
(398, 'http://euro-therm.com.ua/maps/suggest', '', '', '', 0, 0, '2014-04-13 00:22:00', '0000-00-00 00:00:00'),
(399, 'http://euro-therm.com.ua/kh/', '', '', '', 0, 0, '2014-04-13 00:26:52', '0000-00-00 00:00:00'),
(400, 'http://euro-therm.com.ua/modules/mod_add/mod_add.php', '', '', '', 0, 0, '2014-04-13 07:36:14', '0000-00-00 00:00:00'),
(401, 'http://euro-therm.com.ua/intl/ru_ua/help/terms_maps.html', '', '', '', 0, 0, '2014-04-13 15:54:12', '0000-00-00 00:00:00'),
(402, 'http://euro-therm.com.ua/modules/404.php', '', '', '', 0, 0, '2014-04-14 08:19:47', '0000-00-00 00:00:00'),
(403, 'http://euro-therm.com.ua/administrator/xbczmsd.php', '', '', '', 0, 0, '2014-04-15 06:37:53', '0000-00-00 00:00:00'),
(404, 'http://euro-therm.com.ua/templates/atomic/error.php', '', '', '', 0, 0, '2014-04-16 08:32:54', '0000-00-00 00:00:00'),
(405, 'http://euro-therm.com.ua//images/stories/food.php', '', '', '', 0, 0, '2014-04-23 23:39:05', '0000-00-00 00:00:00'),
(406, 'http://euro-therm.com.ua//images/stories/petx.php', '', '', '', 0, 0, '2014-04-25 12:08:15', '0000-00-00 00:00:00'),
(407, 'http://euro-therm.com.ua//x_.php', '', '', '', 0, 0, '2014-04-27 02:17:34', '0000-00-00 00:00:00'),
(408, 'http://euro-therm.com.ua/admin//error', '', '', '', 0, 0, '2014-05-11 08:29:49', '0000-00-00 00:00:00'),
(409, 'http://euro-therm.com.ua//modules/mod_search/tmpl/index.php?search=hello', '', '', '', 0, 0, '2014-05-19 16:41:26', '0000-00-00 00:00:00'),
(410, 'http://euro-therm.com.ua//plugins/system/index.php?search=hello', '', '', '', 0, 0, '2014-05-20 10:23:10', '0000-00-00 00:00:00'),
(411, 'http://euro-therm.com.ua/modules/mod_wrapper/mod.php', '', '', '', 0, 0, '2014-05-20 23:31:04', '0000-00-00 00:00:00'),
(412, 'http://euro-therm.com.ua/components/com_media/movie.php', '', '', '', 0, 0, '2014-05-21 13:39:54', '0000-00-00 00:00:00'),
(413, 'http://euro-therm.com.ua/index.php?option=com_user&view=reset&layout=confirm', '', '', '', 0, 0, '2014-05-24 07:08:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_schemas`
--

CREATE TABLE IF NOT EXISTS `o1hap_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_schemas`
--

INSERT INTO `o1hap_schemas` (`extension_id`, `version_id`) VALUES
(700, '2.5.20');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_session`
--

CREATE TABLE IF NOT EXISTS `o1hap_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  `usertype` varchar(50) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_session`
--

INSERT INTO `o1hap_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`, `usertype`) VALUES
('87161af138b0b707a5fd33ba1b87419e', 1, 1, '1401376177', '__default|a:8:{s:15:"session.counter";i:18;s:19:"session.timer.start";i:1401346590;s:18:"session.timer.last";i:1401374432;s:17:"session.timer.now";i:1401376173;s:22:"session.client.browser";s:76:"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:28.0) Gecko/20100101 Firefox/28.0";s:8:"registry";O:9:"JRegistry":1:{s:7:"\0*\0data";O:8:"stdClass":0:{}}s:4:"user";O:5:"JUser":25:{s:9:"\0*\0isRoot";N;s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:6:"groups";a:0:{}s:5:"guest";i:1;s:13:"lastResetTime";N;s:10:"resetCount";N;s:10:"\0*\0_params";O:9:"JRegistry":1:{s:7:"\0*\0data";O:8:"stdClass":0:{}}s:14:"\0*\0_authGroups";N;s:14:"\0*\0_authLevels";a:2:{i:0;i:1;i:1;i:1;}s:15:"\0*\0_authActions";N;s:12:"\0*\0_errorMsg";N;s:10:"\0*\0_errors";a:0:{}s:3:"aid";i:0;}s:13:"session.token";s:32:"a481d3cda0b12fc92436cf9db8fd5baf";}', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_template_styles`
--

CREATE TABLE IF NOT EXISTS `o1hap_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `o1hap_template_styles`
--

INSERT INTO `o1hap_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(2, 'bluestork', 1, '1', 'Bluestork - Default', '{"useRoundedCorners":"1","showSiteName":"0"}'),
(3, 'atomic', 0, '0', 'Atomic - Default', '{}'),
(4, 'beez_20', 0, '0', 'Beez2 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(6, 'beez5', 0, '0', 'Beez5 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/sampledata\\/fruitshop\\/fruits.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","html5":"0"}'),
(7, 'euro-therm', 0, '1', 'euro-therm - По умолчанию', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_updates`
--

CREATE TABLE IF NOT EXISTS `o1hap_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `categoryid` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(10) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  PRIMARY KEY (`update_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=137 ;

--
-- Dumping data for table `o1hap_updates`
--

INSERT INTO `o1hap_updates` (`update_id`, `update_site_id`, `extension_id`, `categoryid`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`) VALUES
(77, 6, 0, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/km-KH_details.xml', ''),
(2, 5, 10030, 0, 'ru-RU VirtueMart language package', 'ru-RU VirtueMart language package', 'ru_ru_virtuemart_language_pack', 'file', '', 0, '2012-04-17', '', 'http://gruz.org.ua/images/stories/files/virtuemart_languages/update_ru_ru.xml', 'http://virtuemart.net'),
(3, 5, 10030, 0, 'ru-RU VirtueMart language package', 'ru-RU VirtueMart language package', 'ru_ru_virtuemart_language_pack', 'file', '', 0, '2012-04-17', '', 'http://gruz.org.ua/images/stories/files/virtuemart_languages/update_ru_ru.xml', 'http://virtuemart.net'),
(4, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(5, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(6, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(7, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(8, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(9, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(10, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(11, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(12, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(13, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(14, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(15, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(16, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(17, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(18, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(19, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(20, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.4.3', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(74, 6, 0, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '2.5.8.1', '', 'http://update.joomla.org/language/details/hy-AM_details.xml', ''),
(22, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(23, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(24, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(25, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(26, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(27, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(28, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(29, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(30, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(31, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(32, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(33, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(34, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(35, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(36, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(37, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(38, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(39, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(40, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(41, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(42, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(43, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(44, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(45, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(46, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(47, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(48, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(49, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(50, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(51, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(52, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(53, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(54, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(55, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(56, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(57, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(58, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.7', '', 'http://update.joomla.org/core/extension.xml', ''),
(59, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(60, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(61, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.17', '', 'http://update.joomla.org/core/extension.xml', ''),
(62, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(63, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(76, 6, 0, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/da-DK_details.xml', ''),
(75, 6, 0, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/id-ID_details.xml', ''),
(66, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.17', '', 'http://update.joomla.org/core/extension.xml', ''),
(67, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(68, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.20', '', 'http://update.joomla.org/core/extension.xml', ''),
(69, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(70, 4, 0, 0, 'DisplayNews', 'DisplayNews Module', 'mod_dn', 'module', '', 0, '2.7', '', 'http://joomla.rjews.net/extensions.xml', 'http://joomla.rjews.net/display-news'),
(71, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.20', '', 'http://update.joomla.org/core/extension.xml', ''),
(72, 3, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.6.1', '', 'http://joomlaportal.ru/downloads/joomla/languages/ru-RU.xml', ''),
(73, 4, 0, 0, 'DisplayNews', 'DisplayNews Module', 'mod_dn', 'module', '', 0, '2.7', '', 'http://joomla.rjews.net/extensions.xml', 'http://joomla.rjews.net/display-news'),
(78, 6, 0, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '2.5.10.1', '', 'http://update.joomla.org/language/details/sv-SE_details.xml', ''),
(79, 6, 0, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '2.5.14.1', '', 'http://update.joomla.org/language/details/hu-HU_details.xml', ''),
(80, 6, 0, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/bg-BG_details.xml', ''),
(81, 6, 0, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/fr-FR_details.xml', ''),
(82, 6, 0, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/it-IT_details.xml', ''),
(83, 6, 0, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/es-ES_details.xml', ''),
(84, 6, 0, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/nl-NL_details.xml', ''),
(85, 6, 0, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/tr-TR_details.xml', ''),
(86, 6, 0, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '2.5.13.11', '', 'http://update.joomla.org/language/details/uk-UA_details.xml', ''),
(87, 6, 0, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/sk-SK_details.xml', ''),
(88, 6, 0, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '2.5.8.1', '', 'http://update.joomla.org/language/details/be-BY_details.xml', ''),
(89, 6, 0, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/lv-LV_details.xml', ''),
(90, 6, 0, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/et-EE_details.xml', ''),
(91, 6, 0, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '2.5.11.1', '', 'http://update.joomla.org/language/details/ro-RO_details.xml', ''),
(92, 6, 0, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/nl-BE_details.xml', ''),
(93, 6, 0, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/mk-MK_details.xml', ''),
(94, 6, 0, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/ja-JP_details.xml', ''),
(95, 6, 0, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/sr-YU_details.xml', ''),
(96, 6, 0, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '2.5.20.2', '', 'http://update.joomla.org/language/details/ar-AA_details.xml', ''),
(97, 6, 0, 0, 'German', '', 'pkg_de-DE', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/de-DE_details.xml', ''),
(98, 6, 0, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '2.5.16.1', '', 'http://update.joomla.org/language/details/nb-NO_details.xml', ''),
(99, 6, 0, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/en-AU_details.xml', ''),
(100, 6, 0, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/en-US_details.xml', ''),
(101, 6, 0, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/sr-RS_details.xml', ''),
(102, 6, 0, 0, 'Lithuanian', '', 'pkg_lt-LT', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/lt-LT_details.xml', ''),
(103, 6, 0, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '2.5.1.5', '', 'http://update.joomla.org/language/details/sq-AL_details.xml', ''),
(104, 6, 0, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/cs-CZ_details.xml', ''),
(105, 6, 0, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/fa-IR_details.xml', ''),
(106, 6, 0, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '2.5.7.4', '', 'http://update.joomla.org/language/details/gl-ES_details.xml', ''),
(107, 6, 0, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/pl-PL_details.xml', ''),
(108, 6, 0, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '2.5.19.1', '', 'http://update.joomla.org/language/details/sy-IQ_details.xml', ''),
(109, 6, 0, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '2.5.8.1', '', 'http://update.joomla.org/language/details/pt-PT_details.xml', ''),
(110, 6, 0, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/ru-RU_details.xml', ''),
(111, 6, 0, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/he-IL_details.xml', ''),
(112, 6, 0, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/ca-ES_details.xml', ''),
(113, 6, 0, 0, 'Laotian', '', 'pkg_lo-LA', 'package', '', 0, '2.5.6.1', '', 'http://update.joomla.org/language/details/lo-LA_details.xml', ''),
(114, 6, 0, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '2.5.16.1', '', 'http://update.joomla.org/language/details/af-ZA_details.xml', ''),
(115, 6, 0, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/zh-CN_details.xml', ''),
(116, 6, 0, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '2.5.6.1', '', 'http://update.joomla.org/language/details/el-GR_details.xml', ''),
(117, 6, 0, 0, 'Esperanto', '', 'pkg_eo-XX', 'package', '', 0, '2.5.19.1', '', 'http://update.joomla.org/language/details/eo-XX_details.xml', ''),
(118, 6, 0, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/fi-FI_details.xml', ''),
(119, 6, 0, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '2.5.9.1', '', 'http://update.joomla.org/language/details/pt-BR_details.xml', ''),
(120, 6, 0, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '2.5.19.1', '', 'http://update.joomla.org/language/details/zh-TW_details.xml', ''),
(121, 6, 0, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '2.5.8.1', '', 'http://update.joomla.org/language/details/vi-VN_details.xml', ''),
(122, 6, 0, 0, 'Kurdish Sorani', '', 'pkg_ckb-IQ', 'package', '', 0, '2.5.9.1', '', 'http://update.joomla.org/language/details/ckb-IQ_details.xml', ''),
(123, 6, 0, 0, 'Bengali', '', 'pkg_bn-BD', 'package', '', 0, '2.5.0.1', '', 'http://update.joomla.org/language/details/bn-BD_details.xml', ''),
(124, 6, 0, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/bs-BA_details.xml', ''),
(125, 6, 0, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '2.5.13.1', '', 'http://update.joomla.org/language/details/hr-HR_details.xml', ''),
(126, 6, 0, 0, 'Azeri', '', 'pkg_az-AZ', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/az-AZ_details.xml', ''),
(127, 6, 0, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '2.5.8.1', '', 'http://update.joomla.org/language/details/nn-NO_details.xml', ''),
(128, 6, 0, 0, 'Tamil India', '', 'pkg_ta-IN', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/ta-IN_details.xml', ''),
(129, 6, 0, 0, 'Scottish Gaelic', '', 'pkg_gd-GB', 'package', '', 0, '2.5.7.1', '', 'http://update.joomla.org/language/details/gd-GB_details.xml', ''),
(130, 6, 0, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/th-TH_details.xml', ''),
(131, 6, 0, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '1.7.0.1', '', 'http://update.joomla.org/language/details/eu-ES_details.xml', ''),
(132, 6, 0, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '2.5.7.2', '', 'http://update.joomla.org/language/details/ug-CN_details.xml', ''),
(133, 6, 0, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '2.5.11.1', '', 'http://update.joomla.org/language/details/ko-KR_details.xml', ''),
(134, 6, 0, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '2.5.6.1', '', 'http://update.joomla.org/language/details/hi-IN_details.xml', ''),
(135, 6, 0, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '2.5.6.1', '', 'http://update.joomla.org/language/details/cy-GB_details.xml', ''),
(136, 6, 0, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '2.5.20.1', '', 'http://update.joomla.org/language/details/sw-KE_details.xml', '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_update_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_update_categories` (
  `categoryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT '',
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `updatesite` int(11) DEFAULT '0',
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Update Categories' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_update_sites`
--

CREATE TABLE IF NOT EXISTS `o1hap_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  PRIMARY KEY (`update_site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `o1hap_update_sites`
--

INSERT INTO `o1hap_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`) VALUES
(1, 'Joomla Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1401194041),
(2, 'Joomla Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1401194041),
(3, 'Joomla! Russia Updates', 'collection', 'http://joomlaportal.ru/translations.xml', 1, 1401194041),
(4, 'Joomla.Rjews.Net', 'extension', 'http://joomla.rjews.net/extensions.xml', 1, 1401194041),
(5, 'ru-RU VirtueMart language package', 'extension', 'http://gruz.org.ua/images/stories/files/virtuemart_languages/update_ru_ru.xml', 0, 1334935813),
(6, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist.xml', 1, 1401195132);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `o1hap_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dumping data for table `o1hap_update_sites_extensions`
--

INSERT INTO `o1hap_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 10002),
(4, 10003),
(5, 10030),
(6, 600);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_usergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `o1hap_usergroups`
--

INSERT INTO `o1hap_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 20, 'Public'),
(2, 1, 6, 17, 'Registered'),
(3, 2, 7, 14, 'Author'),
(4, 3, 8, 11, 'Editor'),
(5, 4, 9, 10, 'Publisher'),
(6, 1, 2, 5, 'Manager'),
(7, 6, 3, 4, 'Administrator'),
(8, 1, 18, 19, 'Super Users');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_users`
--

CREATE TABLE IF NOT EXISTS `o1hap_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `o1hap_users`
--

INSERT INTO `o1hap_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`) VALUES
(42, 'Super User', 'admin', 'euro_therm@mail.ru', 'e0fc33ef9953a672282635204e0ce424:LqsQXopMD1H5T7DunK8nZ4uRXLHDpmnt', 'deprecated', 0, 1, '2012-04-16 15:38:20', '2014-05-27 08:43:59', '0', '{"admin_style":"","admin_language":"","language":"","editor":"tinymce","helpsite":"","timezone":""}', '0000-00-00 00:00:00', 0),
(43, 'Klerik', 'chernihoff', 'chernihoff.valera@yandex.ru', '41d6b72d8ad9c34d68eb8fbacf036d0c:k7gyQ0QejImgGczhgJYQS4WMgyUV497T', '', 1, 0, '2014-01-30 06:13:40', '0000-00-00 00:00:00', '25e980b9009e23ab68f37bc648c4b7b6', '{}', '0000-00-00 00:00:00', 0),
(44, 'serega', 'serega1sport1', 'dimka7123@outlook.com', '63ec2e369edb5e4cf6880265356ab707:BmhjzWTTKWvD1XdXEQSjG8iTzyvATZrt', '', 1, 0, '2014-02-06 00:02:48', '0000-00-00 00:00:00', '4a1c952e32dcb9918923a1abfddff63d', '{}', '0000-00-00 00:00:00', 0),
(45, 'seoden4k', 'seoden4k', 'seoden4ik@gmail.com', '5bb63741bb5849b596264ccb6c8f8f7d:krALRv7zP9ATgJLiRxQj4N47JDCoohcj', '', 0, 0, '2014-03-02 21:14:32', '0000-00-00 00:00:00', '', '{}', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_user_notes`
--

CREATE TABLE IF NOT EXISTS `o1hap_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_user_profiles`
--

CREATE TABLE IF NOT EXISTS `o1hap_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `o1hap_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_user_usergroup_map`
--

INSERT INTO `o1hap_user_usergroup_map` (`user_id`, `group_id`) VALUES
(42, 8),
(43, 2),
(44, 2),
(45, 2);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_viewlevels`
--

CREATE TABLE IF NOT EXISTS `o1hap_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `o1hap_viewlevels`
--

INSERT INTO `o1hap_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 1, '[6,2,8]'),
(3, 'Special', 2, '[6,3,8]');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_adminmenuentries`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_adminmenuentries` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` tinyint(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The ID of the VM Module, this Item is assigned to',
  `parent_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` char(64) NOT NULL DEFAULT '0',
  `link` char(64) NOT NULL DEFAULT '0',
  `depends` char(64) NOT NULL DEFAULT '' COMMENT 'Names of the Parameters, this Item depends on',
  `icon_class` char(96) DEFAULT NULL,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `tooltip` char(128) DEFAULT NULL,
  `view` char(32) DEFAULT NULL,
  `task` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Administration Menu Items' AUTO_INCREMENT=29 ;

--
-- Dumping data for table `o1hap_virtuemart_adminmenuentries`
--

INSERT INTO `o1hap_virtuemart_adminmenuentries` (`id`, `module_id`, `parent_id`, `name`, `link`, `depends`, `icon_class`, `ordering`, `published`, `tooltip`, `view`, `task`) VALUES
(1, 1, 0, 'COM_VIRTUEMART_CATEGORY_S', '', '', 'vmicon vmicon-16-folder_camera', 1, 1, '', 'category', ''),
(2, 1, 0, 'COM_VIRTUEMART_PRODUCT_S', '', '', 'vmicon vmicon-16-camera', 2, 1, '', 'product', ''),
(3, 1, 0, 'COM_VIRTUEMART_PRODUCT_CUSTOM_FIELD_S', '', '', 'vmicon vmicon-16-document_move', 5, 1, '', 'custom', ''),
(4, 1, 0, 'COM_VIRTUEMART_PRODUCT_INVENTORY', '', '', 'vmicon vmicon-16-price_watch', 7, 1, '', 'inventory', ''),
(5, 1, 0, 'COM_VIRTUEMART_CALC_S', '', '', 'vmicon vmicon-16-calculator', 8, 1, '', 'calc', ''),
(6, 1, 0, 'COM_VIRTUEMART_REVIEW_RATE_S', '', '', 'vmicon vmicon-16-comments', 9, 1, '', 'ratings', ''),
(7, 2, 0, 'COM_VIRTUEMART_ORDER_S', '', '', 'vmicon vmicon-16-page_white_stack', 1, 1, '', 'orders', ''),
(8, 2, 0, 'COM_VIRTUEMART_COUPON_S', '', '', 'vmicon vmicon-16-shopping', 10, 1, '', 'coupon', ''),
(9, 2, 0, 'COM_VIRTUEMART_REPORT', '', '', 'vmicon vmicon-16-to_do_list_cheked_1', 3, 1, '', 'report', ''),
(10, 2, 0, 'COM_VIRTUEMART_USER_S', '', '', 'vmicon vmicon-16-user', 4, 1, '', 'user', ''),
(11, 2, 0, 'COM_VIRTUEMART_SHOPPERGROUP_S', '', '', 'vmicon vmicon-16-user-group', 5, 1, '', 'shoppergroup', ''),
(12, 3, 0, 'COM_VIRTUEMART_MANUFACTURER_S', '', '', 'vmicon vmicon-16-wrench_orange', 1, 1, '', 'manufacturer', ''),
(13, 3, 0, 'COM_VIRTUEMART_MANUFACTURER_CATEGORY_S', '', '', 'vmicon vmicon-16-folder_wrench', 2, 1, '', 'manufacturercategories', ''),
(14, 4, 0, 'COM_VIRTUEMART_STORE', '', '', 'vmicon vmicon-16-reseller_account_template', 1, 1, '', 'user', 'editshop'),
(15, 4, 0, 'COM_VIRTUEMART_MEDIA_S', '', '', 'vmicon vmicon-16-pictures', 2, 1, '', 'media', ''),
(16, 4, 0, 'COM_VIRTUEMART_SHIPMENTMETHOD_S', '', '', 'vmicon vmicon-16-lorry', 3, 1, '', 'shipmentmethod', ''),
(17, 4, 0, 'COM_VIRTUEMART_PAYMENTMETHOD_S', '', '', 'vmicon vmicon-16-creditcards', 4, 1, '', 'paymentmethod', ''),
(18, 5, 0, 'COM_VIRTUEMART_CONFIGURATION', '', '', 'vmicon vmicon-16-config', 1, 1, '', 'config', ''),
(19, 5, 0, 'COM_VIRTUEMART_USERFIELD_S', '', '', 'vmicon vmicon-16-participation_rate', 2, 1, '', 'userfields', ''),
(20, 5, 0, 'COM_VIRTUEMART_ORDERSTATUS_S', '', '', 'vmicon vmicon-16-orderstatus', 3, 1, '', 'orderstatus', ''),
(21, 5, 0, 'COM_VIRTUEMART_CURRENCY_S', '', '', 'vmicon vmicon-16-coins', 5, 1, '', 'currency', ''),
(22, 5, 0, 'COM_VIRTUEMART_COUNTRY_S', '', '', 'vmicon vmicon-16-globe', 6, 1, '', 'country', ''),
(23, 11, 0, 'COM_VIRTUEMART_MIGRATION_UPDATE', '', '', 'vmicon vmicon-16-installer_box', 1, 1, '', 'updatesmigration', ''),
(24, 11, 0, 'COM_VIRTUEMART_ABOUT', '', '', 'vmicon vmicon-16-info', 2, 1, '', 'about', ''),
(25, 11, 0, 'COM_VIRTUEMART_HELP_TOPICS', 'http://virtuemart.net/', '', 'vmicon vmicon-16-help', 4, 1, '', '', ''),
(26, 11, 0, 'COM_VIRTUEMART_COMMUNITY_FORUM', 'http://forum.virtuemart.net/', '', 'vmicon vmicon-16-reseller_programm', 6, 1, '', '', ''),
(27, 11, 0, 'COM_VIRTUEMART_STATISTIC_SUMMARY', '', '', 'vmicon vmicon-16-info', 1, 1, '', 'virtuemart', ''),
(28, 77, 0, 'COM_VIRTUEMART_USER_GROUP_S', '', '', 'vmicon vmicon-16-user', 2, 1, '', 'usergroups', '');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_calcs`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_calcs` (
  `virtuemart_calc_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Belongs to vendor',
  `calc_name` char(64) NOT NULL DEFAULT '' COMMENT 'Name of the rule',
  `calc_descr` char(128) NOT NULL DEFAULT '' COMMENT 'Description',
  `calc_kind` char(16) NOT NULL DEFAULT '' COMMENT 'Discount/Tax/Margin/Commission',
  `calc_value_mathop` char(8) NOT NULL DEFAULT '' COMMENT 'the mathematical operation like (+,-,+%,-%)',
  `calc_value` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'The Amount',
  `calc_currency` smallint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Currency of the Rule',
  `calc_shopper_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Visible for Shoppers',
  `calc_vendor_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Visible for Vendors',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Startdate if nothing is set = permanent',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Enddate if nothing is set = permanent',
  `for_override` tinyint(1) NOT NULL DEFAULT '0',
  `calc_params` text,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_calc_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_calc_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_calc_categories` (
  `id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_calc_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_category_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_calc_id` (`virtuemart_calc_id`,`virtuemart_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_calc_countries`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_calc_countries` (
  `id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_calc_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_country_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_calc_id` (`virtuemart_calc_id`,`virtuemart_country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_calc_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_calc_shoppergroups` (
  `id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_calc_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_shoppergroup_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_calc_id` (`virtuemart_calc_id`,`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_calc_states`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_calc_states` (
  `id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_calc_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_state_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_calc_id` (`virtuemart_calc_id`,`virtuemart_state_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_categories` (
  `virtuemart_category_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `category_template` char(24) DEFAULT NULL,
  `category_layout` char(16) DEFAULT NULL,
  `category_product_layout` char(16) DEFAULT NULL,
  `products_per_row` tinyint(2) DEFAULT NULL,
  `limit_list_start` smallint(1) unsigned DEFAULT NULL,
  `limit_list_step` smallint(1) unsigned DEFAULT NULL,
  `limit_list_max` smallint(1) unsigned DEFAULT NULL,
  `limit_list_initial` smallint(1) unsigned DEFAULT NULL,
  `hits` int(1) unsigned NOT NULL DEFAULT '0',
  `metarobot` char(40) NOT NULL DEFAULT '',
  `metaauthor` char(64) NOT NULL DEFAULT '',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_category_id`),
  KEY `idx_category_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Product Categories are stored here' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `o1hap_virtuemart_categories`
--

INSERT INTO `o1hap_virtuemart_categories` (`virtuemart_category_id`, `virtuemart_vendor_id`, `category_template`, `category_layout`, `category_product_layout`, `products_per_row`, `limit_list_start`, `limit_list_step`, `limit_list_max`, `limit_list_initial`, `hits`, `metarobot`, `metaauthor`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(3, 1, '0', '0', '0', 0, 0, 10, 0, 10, 0, '', '', 0, 0, 1, '2012-04-17 13:21:10', 42, '2012-04-17 13:21:10', 42, '0000-00-00 00:00:00', 0),
(2, 1, '0', '0', '0', 0, 0, 10, 0, 10, 0, '', '', 0, 0, 1, '2012-04-17 13:20:07', 42, '2012-04-17 13:20:07', 42, '0000-00-00 00:00:00', 0),
(4, 1, '0', '0', '0', 0, 0, 10, 0, 10, 0, '', '', 0, 0, 1, '2012-04-17 13:21:29', 42, '2012-04-17 13:21:29', 42, '0000-00-00 00:00:00', 0),
(5, 1, '0', '0', '0', 0, 0, 10, 0, 10, 0, '', '', 0, 0, 1, '2012-06-22 13:35:22', 42, '2012-06-22 13:35:22', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_categories_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_categories_ru_ru` (
  `virtuemart_category_id` int(1) unsigned NOT NULL,
  `category_name` char(180) NOT NULL DEFAULT '',
  `category_description` varchar(20000) NOT NULL DEFAULT '',
  `metadesc` char(192) NOT NULL DEFAULT '',
  `metakey` char(192) NOT NULL DEFAULT '',
  `customtitle` char(255) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_category_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_virtuemart_categories_ru_ru`
--

INSERT INTO `o1hap_virtuemart_categories_ru_ru` (`virtuemart_category_id`, `category_name`, `category_description`, `metadesc`, `metakey`, `customtitle`, `slug`) VALUES
(3, 'Алюминиевые радиаторы', '', '', '', '', 'алюминиевые-радиаторы'),
(2, 'Стальные радиаторы', '', '', '', '', 'стальные-радиаторы2012-04-17-13-20-07_'),
(4, 'Биметаллические радиаторы', '', '', '', '', 'биметаллические-радиаторы'),
(5, 'EUROTHERM', '<p style="text-align: justify;"><img src="images/brands/1.png" border="�" width="130" height="56" style="float: left; margin-left: 5px; margin-right: 5px;" /><strong>Стальные панельные радиаторы EUROTHERM</strong> отличаются высокой надежностью и отличной тепловой производительностью. <br /><strong>Материалы</strong><img src="images/eurol_1.png" border="�" width="197" height="190" style="float: right; margin-left: 7px; margin-right: 7px;" />: Для производства радиаторов EUROTHERM используется листовая холоднокатаная сталь, а это — сталь очень высокого качества (ее толщина равняется 1,10 мм). <br /><strong><br />Безопасность</strong>: Прежде, чем попасть на прилавки магазинов, каждый радиатор подвергается тестированию под давлением 13 атмосфер.<br />Вредные вещества в процессе нагревания этих радиаторов не выделяются, потому что для их окрашивания применяется электростатический метод.<br /><strong><br />Удобство</strong>: Установка радиаторов EUROTHERM на стену может производиться прямо в защитной коробке — это предохранит прибор от неблагоприятного воздействия внешней среды (например, ремонтной пыли) и повреждений в результате механического воздействия.</p>', 'Купить оптом и в розницу стальные радиаторы eurotherm (турция) - выгодная цена прямого поставщика.', 'стальные радиаторы, стальные радиаторы купить, стальные радиаторы турция, стальные радиаторы отопления, купить стальной радиатор', 'Купить стальные радиаторы отопления - производства Турция', 'eurotherm');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_category_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_category_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_parent_id` int(1) unsigned NOT NULL DEFAULT '0',
  `category_child_id` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_category_parent_id` (`category_parent_id`,`category_child_id`),
  KEY `category_child_id` (`category_child_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Category child-parent relation list' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `o1hap_virtuemart_category_categories`
--

INSERT INTO `o1hap_virtuemart_category_categories` (`id`, `category_parent_id`, `category_child_id`, `ordering`) VALUES
(1, 0, 1, 0),
(2, 0, 2, 0),
(3, 0, 3, 0),
(4, 0, 4, 0),
(5, 0, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_category_medias`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_category_medias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_category_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_media_id` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_category_id` (`virtuemart_category_id`,`virtuemart_media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_configs`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_configs` (
  `virtuemart_config_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `config` text,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds configuration settings' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_virtuemart_configs`
--

INSERT INTO `o1hap_virtuemart_configs` (`virtuemart_config_id`, `config`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 'shop_is_offline=s:1:"0";|offline_message=czo3MzoiT3VyIFNob3AgaXMgY3VycmVudGx5IGRvd24gZm9yIG1haW50ZW5hbmNlLiBQbGVhc2UgY2hlY2sgYmFjayBhZ2FpbiBzb29uLiI7|use_as_catalog=s:1:"0";|currency_converter_module=s:14:"convertECB.php";|order_mail_html=s:1:"1";|dateformat=s:8:"%m/%d/%y";|useSSL=s:1:"0";|dangeroustools=s:1:"0";|debug_enable=s:4:"none";|google_jquery=s:1:"1";|multix=s:4:"none";|pdf_button_enable=s:1:"1";|show_emailfriend=s:1:"1";|show_printicon=s:1:"0";|show_out_of_stock_products=s:1:"1";|coupons_enable=s:1:"0";|coupons_default_expire=s:3:"1,D";|weight_unit_default=s:2:"KG";|lwh_unit_default=s:1:"M";|list_limit=s:4:"1000";|showReviewFor=s:3:"all";|reviewMode=s:10:"registered";|showRatingFor=s:3:"all";|ratingMode=s:10:"registered";|reviews_autopublish=s:1:"1";|reviews_minimum_comment_length=s:3:"100";|reviews_maximum_comment_length=s:4:"2000";|vmtemplate=s:7:"default";|categorytemplate=s:7:"default";|showCategory=s:1:"1";|categorylayout=s:7:"default";|categories_per_row=s:1:"3";|productlayout=s:7:"default";|products_per_row=s:1:"3";|vmlayout=s:7:"default";|show_featured=s:1:"1";|featured_products_per_row=s:1:"3";|show_topTen=s:1:"1";|topten_products_per_row=s:1:"3";|show_recent=s:1:"1";|show_latest=s:1:"1";|assets_general_path=s:33:"components/com_virtuemart/assets/";|media_category_path=s:35:"images/stories/virtuemart/category/";|media_product_path=s:34:"images/stories/virtuemart/product/";|media_manufacturer_path=s:39:"images/stories/virtuemart/manufacturer/";|media_vendor_path=s:33:"images/stories/virtuemart/vendor/";|forSale_path_thumb=s:42:"images/stories/virtuemart/forSale/resized/";|img_resize_enable=s:1:"1";|img_width=s:2:"90";|img_height=s:2:"90";|no_image_set=s:11:"noimage.gif";|no_image_found=s:24:"update_quantity_cart.png";|browse_orderby_field=s:23:"p.virtuemart_product_id";|browse_orderby_fields=a:4:{i:0;s:12:"product_name";i:1;s:11:"product_sku";i:2;s:13:"category_name";i:3;s:7:"mf_name";}|browse_search_fields=a:6:{i:0;s:12:"product_name";i:1;s:11:"product_sku";i:2;s:14:"product_s_desc";i:3;s:13:"category_name";i:4;s:20:"category_description";i:5;s:7:"mf_name";}|show_prices=s:1:"1";|price_access_level_published=s:1:"0";|price_show_packaging_pricelabel=s:1:"0";|show_tax=s:1:"1";|basePrice=s:1:"1";|basePriceText=s:1:"1";|basePriceRounding=s:1:"2";|variantModification=s:1:"1";|variantModificationText=s:1:"1";|variantModificationRounding=s:1:"2";|basePriceVariant=s:1:"1";|basePriceVariantText=s:1:"1";|basePriceVariantRounding=s:1:"2";|basePriceWithTax=s:1:"1";|basePriceWithTaxText=s:1:"1";|basePriceWithTaxRounding=s:1:"2";|discountedPriceWithoutTax=s:1:"1";|discountedPriceWithoutTaxText=s:1:"1";|discountedPriceWithoutTaxRounding=s:1:"2";|salesPriceWithDiscount=s:1:"1";|salesPriceWithDiscountText=s:1:"1";|salesPriceWithDiscountRounding=s:1:"2";|salesPrice=s:1:"1";|salesPriceText=s:1:"1";|salesPriceRounding=s:1:"2";|priceWithoutTax=s:1:"1";|priceWithoutTaxText=s:1:"1";|priceWithoutTaxRounding=s:1:"2";|discountAmount=s:1:"1";|discountAmountText=s:1:"1";|discountAmountRounding=s:1:"2";|taxAmount=s:1:"1";|taxAmountText=s:1:"1";|taxAmountRounding=s:1:"2";|check_stock=s:1:"0";|automatic_payment=s:1:"1";|automatic_shipment=s:1:"1";|agree_to_tos_onorder=s:1:"0";|oncheckout_show_legal_info=s:1:"1";|oncheckout_show_register=s:1:"1";|oncheckout_show_steps=s:1:"0";|oncheckout_show_register_text=s:47:"COM_VIRTUEMART_ONCHECKOUT_DEFAULT_TEXT_REGISTER";|seo_disabled=s:1:"0";|seo_translate=s:1:"0";|seo_use_id=s:1:"0";|soap_ws_cat_on=s:1:"0";|soap_ws_cat_cache_on=s:1:"1";|soap_auth_getcat=s:1:"1";|soap_auth_addcat=s:1:"1";|soap_auth_upcat=s:1:"1";|soap_auth_delcat=s:1:"1";|soap_auth_cat_otherget=s:1:"1";|soap_auth_cat_otheradd=s:1:"1";|soap_auth_cat_otherupdate=s:1:"1";|soap_auth_cat_otherdelete=s:1:"1";|soap_ws_user_on=s:1:"0";|soap_ws_user_cache_on=s:1:"1";|soap_auth_getuser=s:1:"1";|soap_auth_adduser=s:1:"1";|soap_auth_upuser=s:1:"1";|soap_auth_deluser=s:1:"1";|soap_auth_user_otherget=s:1:"1";|soap_auth_user_otheradd=s:1:"1";|soap_auth_user_otherupdate=s:1:"1";|soap_auth_user_otherdelete=s:1:"1";|soap_ws_prod_on=s:1:"0";|soap_ws_prod_cache_on=s:1:"1";|soap_auth_getprod=s:1:"1";|soap_auth_addprod=s:1:"1";|soap_auth_upprod=s:1:"1";|soap_auth_delprod=s:1:"1";|soap_auth_prod_otherget=s:1:"1";|soap_auth_prod_otheradd=s:1:"1";|soap_auth_prod_otherupdate=s:1:"1";|soap_auth_prod_otherdelete=s:1:"1";|soap_ws_order_on=s:1:"0";|soap_ws_order_cache_on=s:1:"1";|soap_auth_getorder=s:1:"1";|soap_auth_addorder=s:1:"1";|soap_auth_uporder=s:1:"1";|soap_auth_delorder=s:1:"1";|soap_auth_order_otherget=s:1:"1";|soap_auth_order_otheradd=s:1:"1";|soap_auth_order_otherupdate=s:1:"1";|soap_auth_order_otherdelete=s:1:"1";|soap_ws_sql_on=s:1:"0";|soap_ws_sql_cache_on=s:1:"1";|soap_auth_execsql=s:1:"1";|soap_auth_execsql_select=s:1:"1";|soap_auth_execsql_insert=s:1:"1";|soap_auth_execsql_update=s:1:"1";|soap_ws_custom_on=s:1:"0";|soap_ws_custom_cache_on=s:1:"1";|soap_EP_custom=s:24:"VM_CustomizedService.php";|soap_wsdl_custom=s:18:"VM_Customized.wsdl";|sctime=d:1335191656.988501071929931640625;|vmlang=s:5:"ru_ru";|virtuemart_config_id=i:1;|enableEnglish=s:1:"1";|enable_content_plugin=s:1:"0";|pdf_icon=s:1:"0";|ask_question=s:1:"0";|asks_minimum_comment_length=s:2:"50";|asks_maximum_comment_length=s:4:"2000";|product_navigation=s:1:"0";|recommend_unauth=s:1:"0";|stockhandle=s:4:"none";|rised_availability=s:0:"";|image=s:0:"";|display_stock=s:1:"0";|show_manufacturers=s:1:"1";|manufacturer_per_row=s:0:"";|pagination_sequence=s:0:"";|forSale_path=s:0:"";|css=s:1:"1";|jquery=s:1:"1";|jprice=s:1:"1";|jsite=s:1:"1";|askprice=s:1:"0";|addtocart_popup=s:1:"0";|vmlang_js=s:1:"0";|oncheckout_only_registered=s:1:"0";|oncheckout_show_images=s:1:"0";|pdf_invoice=s:1:"1";|browse_cat_orderby_field=s:13:"category_name";|seo_sufix=s:7:"-detail";|task=s:4:"save";|option=s:14:"com_virtuemart";|view=s:6:"config";|e835d1c1b207609e8bbf5ae45563a519=s:1:"1";', '2012-04-23 14:34:17', 42, '2012-04-23 14:34:17', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_countries`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_countries` (
  `virtuemart_country_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_worldzone_id` tinyint(11) NOT NULL DEFAULT '1',
  `country_name` char(64) DEFAULT NULL,
  `country_3_code` char(3) DEFAULT NULL,
  `country_2_code` char(2) DEFAULT NULL,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_country_id`),
  KEY `idx_country_3_code` (`country_3_code`),
  KEY `idx_country_2_code` (`country_2_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Country records' AUTO_INCREMENT=249 ;

--
-- Dumping data for table `o1hap_virtuemart_countries`
--

INSERT INTO `o1hap_virtuemart_countries` (`virtuemart_country_id`, `virtuemart_worldzone_id`, `country_name`, `country_3_code`, `country_2_code`, `ordering`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 'Afghanistan', 'AFG', 'AF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 'Albania', 'ALB', 'AL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 'Algeria', 'DZA', 'DZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'American Samoa', 'ASM', 'AS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 'Andorra', 'AND', 'AD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, 1, 'Angola', 'AGO', 'AO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 1, 'Anguilla', 'AIA', 'AI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, 1, 'Antarctica', 'ATA', 'AQ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 1, 'Antigua and Barbuda', 'ATG', 'AG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 1, 'Argentina', 'ARG', 'AR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 1, 'Armenia', 'ARM', 'AM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 1, 'Aruba', 'ABW', 'AW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(13, 1, 'Australia', 'AUS', 'AU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(14, 1, 'Austria', 'AUT', 'AT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 1, 'Azerbaijan', 'AZE', 'AZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 1, 'Bahamas', 'BHS', 'BS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 1, 'Bahrain', 'BHR', 'BH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 1, 'Bangladesh', 'BGD', 'BD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 1, 'Barbados', 'BRB', 'BB', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(20, 1, 'Belarus', 'BLR', 'BY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(21, 1, 'Belgium', 'BEL', 'BE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(22, 1, 'Belize', 'BLZ', 'BZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(23, 1, 'Benin', 'BEN', 'BJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(24, 1, 'Bermuda', 'BMU', 'BM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(25, 1, 'Bhutan', 'BTN', 'BT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(26, 1, 'Bolivia', 'BOL', 'BO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 1, 'Bosnia and Herzegowina', 'BIH', 'BA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 1, 'Botswana', 'BWA', 'BW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 1, 'Bouvet Island', 'BVT', 'BV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(30, 1, 'Brazil', 'BRA', 'BR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(31, 1, 'British Indian Ocean Territory', 'IOT', 'IO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(32, 1, 'Brunei Darussalam', 'BRN', 'BN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(33, 1, 'Bulgaria', 'BGR', 'BG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(34, 1, 'Burkina Faso', 'BFA', 'BF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(35, 1, 'Burundi', 'BDI', 'BI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(36, 1, 'Cambodia', 'KHM', 'KH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(37, 1, 'Cameroon', 'CMR', 'CM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(38, 1, 'Canada', 'CAN', 'CA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(39, 1, 'Cape Verde', 'CPV', 'CV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(40, 1, 'Cayman Islands', 'CYM', 'KY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(41, 1, 'Central African Republic', 'CAF', 'CF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(42, 1, 'Chad', 'TCD', 'TD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(43, 1, 'Chile', 'CHL', 'CL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(44, 1, 'China', 'CHN', 'CN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(45, 1, 'Christmas Island', 'CXR', 'CX', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(46, 1, 'Cocos (Keeling) Islands', 'CCK', 'CC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(47, 1, 'Colombia', 'COL', 'CO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(48, 1, 'Comoros', 'COM', 'KM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(49, 1, 'Congo', 'COG', 'CG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(50, 1, 'Cook Islands', 'COK', 'CK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(51, 1, 'Costa Rica', 'CRI', 'CR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(52, 1, 'Cote D''Ivoire', 'CIV', 'CI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(53, 1, 'Croatia', 'HRV', 'HR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(54, 1, 'Cuba', 'CUB', 'CU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(55, 1, 'Cyprus', 'CYP', 'CY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(56, 1, 'Czech Republic', 'CZE', 'CZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(57, 1, 'Denmark', 'DNK', 'DK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(58, 1, 'Djibouti', 'DJI', 'DJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(59, 1, 'Dominica', 'DMA', 'DM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(60, 1, 'Dominican Republic', 'DOM', 'DO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(61, 1, 'East Timor', 'TMP', 'TP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(62, 1, 'Ecuador', 'ECU', 'EC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(63, 1, 'Egypt', 'EGY', 'EG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(64, 1, 'El Salvador', 'SLV', 'SV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(65, 1, 'Equatorial Guinea', 'GNQ', 'GQ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(66, 1, 'Eritrea', 'ERI', 'ER', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(67, 1, 'Estonia', 'EST', 'EE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(68, 1, 'Ethiopia', 'ETH', 'ET', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(69, 1, 'Falkland Islands (Malvinas)', 'FLK', 'FK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(70, 1, 'Faroe Islands', 'FRO', 'FO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(71, 1, 'Fiji', 'FJI', 'FJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(72, 1, 'Finland', 'FIN', 'FI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(73, 1, 'France', 'FRA', 'FR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(75, 1, 'French Guiana', 'GUF', 'GF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(76, 1, 'French Polynesia', 'PYF', 'PF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(77, 1, 'French Southern Territories', 'ATF', 'TF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(78, 1, 'Gabon', 'GAB', 'GA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(79, 1, 'Gambia', 'GMB', 'GM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(80, 1, 'Georgia', 'GEO', 'GE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(81, 1, 'Germany', 'DEU', 'DE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(82, 1, 'Ghana', 'GHA', 'GH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(83, 1, 'Gibraltar', 'GIB', 'GI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(84, 1, 'Greece', 'GRC', 'GR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(85, 1, 'Greenland', 'GRL', 'GL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(86, 1, 'Grenada', 'GRD', 'GD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(87, 1, 'Guadeloupe', 'GLP', 'GP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(88, 1, 'Guam', 'GUM', 'GU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(89, 1, 'Guatemala', 'GTM', 'GT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(90, 1, 'Guinea', 'GIN', 'GN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(91, 1, 'Guinea-bissau', 'GNB', 'GW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(92, 1, 'Guyana', 'GUY', 'GY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(93, 1, 'Haiti', 'HTI', 'HT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(94, 1, 'Heard and Mc Donald Islands', 'HMD', 'HM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(95, 1, 'Honduras', 'HND', 'HN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(96, 1, 'Hong Kong', 'HKG', 'HK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(97, 1, 'Hungary', 'HUN', 'HU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(98, 1, 'Iceland', 'ISL', 'IS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(99, 1, 'India', 'IND', 'IN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(100, 1, 'Indonesia', 'IDN', 'ID', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(101, 1, 'Iran (Islamic Republic of)', 'IRN', 'IR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(102, 1, 'Iraq', 'IRQ', 'IQ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(103, 1, 'Ireland', 'IRL', 'IE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(104, 1, 'Israel', 'ISR', 'IL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(105, 1, 'Italy', 'ITA', 'IT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(106, 1, 'Jamaica', 'JAM', 'JM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(107, 1, 'Japan', 'JPN', 'JP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(108, 1, 'Jordan', 'JOR', 'JO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(109, 1, 'Kazakhstan', 'KAZ', 'KZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(110, 1, 'Kenya', 'KEN', 'KE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(111, 1, 'Kiribati', 'KIR', 'KI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(112, 1, 'Korea, Democratic People''s Republic of', 'PRK', 'KP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(113, 1, 'Korea, Republic of', 'KOR', 'KR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(114, 1, 'Kuwait', 'KWT', 'KW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(115, 1, 'Kyrgyzstan', 'KGZ', 'KG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(116, 1, 'Lao People''s Democratic Republic', 'LAO', 'LA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(117, 1, 'Latvia', 'LVA', 'LV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(118, 1, 'Lebanon', 'LBN', 'LB', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(119, 1, 'Lesotho', 'LSO', 'LS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(120, 1, 'Liberia', 'LBR', 'LR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(121, 1, 'Libyan Arab Jamahiriya', 'LBY', 'LY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(122, 1, 'Liechtenstein', 'LIE', 'LI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(123, 1, 'Lithuania', 'LTU', 'LT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(124, 1, 'Luxembourg', 'LUX', 'LU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(125, 1, 'Macau', 'MAC', 'MO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(126, 1, 'Macedonia, The Former Yugoslav Republic of', 'MKD', 'MK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(127, 1, 'Madagascar', 'MDG', 'MG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(128, 1, 'Malawi', 'MWI', 'MW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(129, 1, 'Malaysia', 'MYS', 'MY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(130, 1, 'Maldives', 'MDV', 'MV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(131, 1, 'Mali', 'MLI', 'ML', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(132, 1, 'Malta', 'MLT', 'MT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(133, 1, 'Marshall Islands', 'MHL', 'MH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(134, 1, 'Martinique', 'MTQ', 'MQ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(135, 1, 'Mauritania', 'MRT', 'MR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(136, 1, 'Mauritius', 'MUS', 'MU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(137, 1, 'Mayotte', 'MYT', 'YT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(138, 1, 'Mexico', 'MEX', 'MX', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(139, 1, 'Micronesia, Federated States of', 'FSM', 'FM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(140, 1, 'Moldova, Republic of', 'MDA', 'MD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(141, 1, 'Monaco', 'MCO', 'MC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(142, 1, 'Mongolia', 'MNG', 'MN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(143, 1, 'Montserrat', 'MSR', 'MS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(144, 1, 'Morocco', 'MAR', 'MA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(145, 1, 'Mozambique', 'MOZ', 'MZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(146, 1, 'Myanmar', 'MMR', 'MM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(147, 1, 'Namibia', 'NAM', 'NA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(148, 1, 'Nauru', 'NRU', 'NR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(149, 1, 'Nepal', 'NPL', 'NP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(150, 1, 'Netherlands', 'NLD', 'NL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(151, 1, 'Netherlands Antilles', 'ANT', 'AN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(152, 1, 'New Caledonia', 'NCL', 'NC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(153, 1, 'New Zealand', 'NZL', 'NZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(154, 1, 'Nicaragua', 'NIC', 'NI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(155, 1, 'Niger', 'NER', 'NE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(156, 1, 'Nigeria', 'NGA', 'NG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(157, 1, 'Niue', 'NIU', 'NU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(158, 1, 'Norfolk Island', 'NFK', 'NF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(159, 1, 'Northern Mariana Islands', 'MNP', 'MP', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(160, 1, 'Norway', 'NOR', 'NO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(161, 1, 'Oman', 'OMN', 'OM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(162, 1, 'Pakistan', 'PAK', 'PK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(163, 1, 'Palau', 'PLW', 'PW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(164, 1, 'Panama', 'PAN', 'PA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(165, 1, 'Papua New Guinea', 'PNG', 'PG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(166, 1, 'Paraguay', 'PRY', 'PY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(167, 1, 'Peru', 'PER', 'PE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(168, 1, 'Philippines', 'PHL', 'PH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(169, 1, 'Pitcairn', 'PCN', 'PN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(170, 1, 'Poland', 'POL', 'PL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(171, 1, 'Portugal', 'PRT', 'PT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(172, 1, 'Puerto Rico', 'PRI', 'PR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(173, 1, 'Qatar', 'QAT', 'QA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(174, 1, 'Reunion', 'REU', 'RE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(175, 1, 'Romania', 'ROM', 'RO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(176, 1, 'Russian Federation', 'RUS', 'RU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(177, 1, 'Rwanda', 'RWA', 'RW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(178, 1, 'Saint Kitts and Nevis', 'KNA', 'KN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(179, 1, 'Saint Lucia', 'LCA', 'LC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(180, 1, 'Saint Vincent and the Grenadines', 'VCT', 'VC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(181, 1, 'Samoa', 'WSM', 'WS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(182, 1, 'San Marino', 'SMR', 'SM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(183, 1, 'Sao Tome and Principe', 'STP', 'ST', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(184, 1, 'Saudi Arabia', 'SAU', 'SA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(185, 1, 'Senegal', 'SEN', 'SN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(186, 1, 'Seychelles', 'SYC', 'SC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(187, 1, 'Sierra Leone', 'SLE', 'SL', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(188, 1, 'Singapore', 'SGP', 'SG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(189, 1, 'Slovakia', 'SVK', 'SK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(190, 1, 'Slovenia', 'SVN', 'SI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(191, 1, 'Solomon Islands', 'SLB', 'SB', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(192, 1, 'Somalia', 'SOM', 'SO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(193, 1, 'South Africa', 'ZAF', 'ZA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(194, 1, 'South Georgia and the South Sandwich Islands', 'SGS', 'GS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(195, 1, 'Spain', 'ESP', 'ES', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(196, 1, 'Sri Lanka', 'LKA', 'LK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(197, 1, 'St. Helena', 'SHN', 'SH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(198, 1, 'St. Pierre and Miquelon', 'SPM', 'PM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(199, 1, 'Sudan', 'SDN', 'SD', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(200, 1, 'Suriname', 'SUR', 'SR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(201, 1, 'Svalbard and Jan Mayen Islands', 'SJM', 'SJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(202, 1, 'Swaziland', 'SWZ', 'SZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(203, 1, 'Sweden', 'SWE', 'SE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(204, 1, 'Switzerland', 'CHE', 'CH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(205, 1, 'Syrian Arab Republic', 'SYR', 'SY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(206, 1, 'Taiwan', 'TWN', 'TW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(207, 1, 'Tajikistan', 'TJK', 'TJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(208, 1, 'Tanzania, United Republic of', 'TZA', 'TZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(209, 1, 'Thailand', 'THA', 'TH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(210, 1, 'Togo', 'TGO', 'TG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(211, 1, 'Tokelau', 'TKL', 'TK', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(212, 1, 'Tonga', 'TON', 'TO', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(213, 1, 'Trinidad and Tobago', 'TTO', 'TT', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(214, 1, 'Tunisia', 'TUN', 'TN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(215, 1, 'Turkey', 'TUR', 'TR', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(216, 1, 'Turkmenistan', 'TKM', 'TM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(217, 1, 'Turks and Caicos Islands', 'TCA', 'TC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(218, 1, 'Tuvalu', 'TUV', 'TV', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(219, 1, 'Uganda', 'UGA', 'UG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(220, 1, 'Ukraine', 'UKR', 'UA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(221, 1, 'United Arab Emirates', 'ARE', 'AE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(222, 1, 'United Kingdom', 'GBR', 'GB', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(223, 1, 'United States', 'USA', 'US', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(224, 1, 'United States Minor Outlying Islands', 'UMI', 'UM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(225, 1, 'Uruguay', 'URY', 'UY', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(226, 1, 'Uzbekistan', 'UZB', 'UZ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(227, 1, 'Vanuatu', 'VUT', 'VU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(228, 1, 'Vatican City State (Holy See)', 'VAT', 'VA', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(229, 1, 'Venezuela', 'VEN', 'VE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(230, 1, 'Viet Nam', 'VNM', 'VN', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(231, 1, 'Virgin Islands (British)', 'VGB', 'VG', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(232, 1, 'Virgin Islands (U.S.)', 'VIR', 'VI', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(233, 1, 'Wallis and Futuna Islands', 'WLF', 'WF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(234, 1, 'Western Sahara', 'ESH', 'EH', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(235, 1, 'Yemen', 'YEM', 'YE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(237, 1, 'The Democratic Republic of Congo', 'DRC', 'DC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(238, 1, 'Zambia', 'ZMB', 'ZM', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(239, 1, 'Zimbabwe', 'ZWE', 'ZW', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(240, 1, 'East Timor', 'XET', 'XE', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(241, 1, 'Jersey', 'XJE', 'XJ', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(242, 1, 'St. Barthelemy', 'XSB', 'XB', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(243, 1, 'St. Eustatius', 'XSE', 'XU', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(244, 1, 'Canary Islands', 'XCA', 'XC', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(245, 1, 'Serbia', 'SRB', 'RS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(246, 1, 'Sint Maarten (French Antilles)', 'MAF', 'MF', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(247, 1, 'Sint Maarten (Netherlands Antilles)', 'SXM', 'SX', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(248, 1, 'Palestinian Territory, occupied', 'PSE', 'PS', 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_coupons`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_coupons` (
  `virtuemart_coupon_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_code` char(32) NOT NULL DEFAULT '',
  `percent_or_total` enum('percent','total') NOT NULL DEFAULT 'percent',
  `coupon_type` enum('gift','permanent') NOT NULL DEFAULT 'gift',
  `coupon_value` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `coupon_start_date` datetime DEFAULT NULL,
  `coupon_expiry_date` datetime DEFAULT NULL,
  `coupon_value_valid` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_coupon_id`),
  KEY `idx_coupon_code` (`coupon_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Used to store coupon codes' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_currencies`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_currencies` (
  `virtuemart_currency_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `currency_name` char(64) DEFAULT NULL,
  `currency_code_2` char(2) DEFAULT NULL,
  `currency_code_3` char(3) DEFAULT NULL,
  `currency_numeric_code` int(4) DEFAULT NULL,
  `currency_exchange_rate` decimal(10,5) DEFAULT NULL,
  `currency_symbol` char(4) DEFAULT NULL,
  `currency_decimal_place` char(4) DEFAULT NULL,
  `currency_decimal_symbol` char(4) DEFAULT NULL,
  `currency_thousands` char(4) DEFAULT NULL,
  `currency_positive_style` char(64) DEFAULT NULL,
  `currency_negative_style` char(64) DEFAULT NULL,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '1',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_currency_id`),
  KEY `virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `idx_currency_code_3` (`currency_code_3`),
  KEY `idx_currency_numeric_code` (`currency_numeric_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Used to store currencies' AUTO_INCREMENT=202 ;

--
-- Dumping data for table `o1hap_virtuemart_currencies`
--

INSERT INTO `o1hap_virtuemart_currencies` (`virtuemart_currency_id`, `virtuemart_vendor_id`, `currency_name`, `currency_code_2`, `currency_code_3`, `currency_numeric_code`, `currency_exchange_rate`, `currency_symbol`, `currency_decimal_place`, `currency_decimal_symbol`, `currency_thousands`, `currency_positive_style`, `currency_negative_style`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(2, 1, 'United Arab Emirates dirham', '', 'AED', 784, 0.00000, 'د.إ', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'Albanian lek', '', 'ALL', 8, 0.00000, 'Lek', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 'Netherlands Antillean gulden', '', 'ANG', 532, 0.00000, 'ƒ', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 1, 'Argentine peso', '', 'ARS', 32, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 1, 'Australian dollar', '', 'AUD', 36, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 1, 'Aruban florin', '', 'AWG', 533, 0.00000, 'ƒ', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 1, 'Barbadian dollar', '', 'BBD', 52, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 1, 'Bangladeshi taka', '', 'BDT', 50, 0.00000, '৳', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 1, 'Bahraini dinar', '', 'BHD', 48, 0.00000, 'ب.د', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 1, 'Burundian franc', '', 'BIF', 108, 0.00000, 'Fr', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 1, 'Bermudian dollar', '', 'BMD', 60, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 1, 'Brunei dollar', '', 'BND', 96, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 1, 'Bolivian boliviano', '', 'BOB', 68, 0.00000, '$b', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(20, 1, 'Brazilian real', '', 'BRL', 986, 0.00000, 'R$', '2', '.', ',', '{symbol} {number}', '{symbol} {sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(21, 1, 'Bahamian dollar', '', 'BSD', 44, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(22, 1, 'Bhutanese ngultrum', '', 'BTN', 64, 0.00000, 'BTN', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(24, 1, 'Botswana pula', '', 'BWP', 72, 0.00000, 'P', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(25, 1, 'Belize dollar', '', 'BZD', 84, 0.00000, 'BZ$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(26, 1, 'Canadian dollar', '', 'CAD', 124, 0.00000, '$', '2', '.', ',', '{symbol}{number}', '{symbol}{sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 1, 'Swiss franc', '', 'CHF', 756, 0.00000, 'CHF', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 1, 'Unidad de Fomento', '', 'CLF', 990, 0.00000, 'CLF', '0', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 1, 'Chilean peso', '', 'CLP', 152, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(30, 1, 'Chinese renminbi yuan', '', 'CNY', 156, 0.00000, '元', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(31, 1, 'Colombian peso', '', 'COP', 170, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(32, 1, 'Costa Rican colón', '', 'CRC', 188, 0.00000, '₡', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(33, 1, 'Czech koruna', '', 'CZK', 203, 0.00000, 'Kč', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(34, 1, 'Cuban peso', '', 'CUP', 192, 0.00000, '₱', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(35, 1, 'Cape Verdean escudo', '', 'CVE', 132, 0.00000, '$', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(40, 1, 'Danish krone', '', 'DKK', 208, 0.00000, 'kr', '2', '.', ',', '{symbol}{number}', '{symbol}{sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(41, 1, 'Dominican peso', '', 'DOP', 214, 0.00000, 'RD$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(42, 1, 'Algerian dinar', '', 'DZD', 12, 0.00000, 'د.ج', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(44, 1, 'Egyptian pound', '', 'EGP', 818, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(46, 1, 'Ethiopian birr', '', 'ETB', 230, 0.00000, 'ETB', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(47, 1, 'Euro', '', 'EUR', 978, 0.00000, '€', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(49, 1, 'Fijian dollar', '', 'FJD', 242, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(50, 1, 'Falkland pound', '', 'FKP', 238, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(52, 1, 'British pound', '', 'GBP', 826, 0.00000, '£', '2', '.', ',', '{symbol}{number}', '{symbol}{sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(54, 1, 'Gibraltar pound', '', 'GIP', 292, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(55, 1, 'Gambian dalasi', '', 'GMD', 270, 0.00000, 'D', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(56, 1, 'Guinean franc', '', 'GNF', 324, 0.00000, 'Fr', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(58, 1, 'Guatemalan quetzal', '', 'GTQ', 320, 0.00000, 'Q', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(60, 1, 'Guyanese dollar', '', 'GYD', 328, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(61, 1, 'Hong Kong dollar', '', 'HKD', 344, 0.00000, '元', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(62, 1, 'Honduran lempira', '', 'HNL', 340, 0.00000, 'L', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(63, 1, 'Haitian gourde', '', 'HTG', 332, 0.00000, 'G', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(64, 1, 'Hungarian forint', '', 'HUF', 348, 0.00000, 'Ft', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(65, 1, 'Indonesian rupiah', '', 'IDR', 360, 0.00000, 'Rp', '0', '', '', '{symbol}{number}', '{symbol}{sign}{number}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(67, 1, 'Israeli new sheqel', '', 'ILS', 376, 0.00000, '₪', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(68, 1, 'Indian rupee', '', 'INR', 356, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(69, 1, 'Iraqi dinar', '', 'IQD', 368, 0.00000, 'ع.د', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(70, 1, 'Iranian rial', '', 'IRR', 364, 0.00000, '﷼', '2', ',', '', '{number} {symbol}', '{sign}{number}{symb0l}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(73, 1, 'Jamaican dollar', '', 'JMD', 388, 0.00000, 'J$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(74, 1, 'Jordanian dinar', '', 'JOD', 400, 0.00000, 'د.ا', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(75, 1, 'Japanese yen', '', 'JPY', 392, 0.00000, '¥', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(76, 1, 'Kenyan shilling', '', 'KES', 404, 0.00000, 'Sh', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(77, 1, 'Cambodian riel', '', 'KHR', 116, 0.00000, '៛', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(78, 1, 'Comorian franc', '', 'KMF', 174, 0.00000, 'Fr', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(79, 1, 'North Korean won', '', 'KPW', 408, 0.00000, '₩', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(80, 1, 'South Korean won', '', 'KRW', 410, 0.00000, '₩', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(81, 1, 'Kuwaiti dinar', '', 'KWD', 414, 0.00000, 'د.ك', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(82, 1, 'Cayman Islands dollar', '', 'KYD', 136, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(83, 1, 'Lao kip', '', 'LAK', 418, 0.00000, '₭', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(84, 1, 'Lebanese pound', '', 'LBP', 422, 0.00000, '£', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(85, 1, 'Sri Lankan rupee', '', 'LKR', 144, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(86, 1, 'Liberian dollar', '', 'LRD', 430, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(87, 1, 'Lesotho loti', '', 'LSL', 426, 0.00000, 'L', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(89, 1, 'Libyan dinar', '', 'LYD', 434, 0.00000, 'ل.د', '3', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(90, 1, 'Moroccan dirham', '', 'MAD', 504, 0.00000, 'د.م.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(92, 1, 'Mongolian tögrög', '', 'MNT', 496, 0.00000, '₮', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(93, 1, 'Macanese pataca', '', 'MOP', 446, 0.00000, 'P', '1', ',', '', '{symbol}{number}', '{symbol}{sign}{number}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(94, 1, 'Mauritanian ouguiya', '', 'MRO', 478, 0.00000, 'UM', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(96, 1, 'Mauritian rupee', '', 'MUR', 480, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(97, 1, 'Maldivian rufiyaa', '', 'MVR', 462, 0.00000, 'ރ.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(98, 1, 'Malawian kwacha', '', 'MWK', 454, 0.00000, 'MK', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(100, 1, 'Malaysian ringgit', '', 'MYR', 458, 0.00000, 'RM', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(102, 1, 'Nigerian naira', '', 'NGN', 566, 0.00000, '₦', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(105, 1, 'Norwegian krone', '', 'NOK', 578, 0.00000, 'kr', '2', ',', '', '{symbol}{number}', '{symbol}{sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(106, 1, 'Nepalese rupee', '', 'NPR', 524, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(107, 1, 'New Zealand dollar', '', 'NZD', 554, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(108, 1, 'Omani rial', '', 'OMR', 512, 0.00000, '﷼', '3', '.', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(109, 1, 'Panamanian balboa', '', 'PAB', 590, 0.00000, 'B/.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(110, 1, 'Peruvian nuevo sol', '', 'PEN', 604, 0.00000, 'S/.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(111, 1, 'Papua New Guinean kina', '', 'PGK', 598, 0.00000, 'K', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(112, 1, 'Philippine peso', '', 'PHP', 608, 0.00000, '₱', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(113, 1, 'Pakistani rupee', '', 'PKR', 586, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(114, 1, 'Polish Złoty', '', 'PLN', 985, 0.00000, 'zł', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(116, 1, 'Paraguayan guaraní', '', 'PYG', 600, 0.00000, '₲', '0', '', '.', '{symbol} {number}', '{symbol} {sign}{number}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(117, 1, 'Qatari riyal', '', 'QAR', 634, 0.00000, '﷼', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(118, 1, 'Romanian leu', '', 'RON', 946, 0.00000, 'lei', '2', ',', '.', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(119, 1, 'Rwandan franc', '', 'RWF', 646, 0.00000, 'Fr', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(120, 1, 'Saudi riyal', '', 'SAR', 682, 0.00000, '﷼', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(121, 1, 'Solomon Islands dollar', '', 'SBD', 90, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(122, 1, 'Seychellois rupee', '', 'SCR', 690, 0.00000, '₨', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(124, 1, 'Swedish krona', '', 'SEK', 752, 0.00000, 'kr', '2', ',', '.', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(125, 1, 'Singapore dollar', '', 'SGD', 702, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(126, 1, 'Saint Helenian pound', '', 'SHP', 654, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(127, 1, 'Sierra Leonean leone', '', 'SLL', 694, 0.00000, 'Le', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(128, 1, 'Somali shilling', '', 'SOS', 706, 0.00000, 'S', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(130, 1, 'São Tomé and Príncipe dobra', '', 'STD', 678, 0.00000, 'Db', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(131, 1, 'Russian ruble', '', 'RUB', 643, 0.00000, 'руб', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(132, 1, 'Salvadoran colón', '', 'SVC', 222, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(133, 1, 'Syrian pound', '', 'SYP', 760, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(134, 1, 'Swazi lilangeni', '', 'SZL', 748, 0.00000, 'L', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(135, 1, 'Thai baht', '', 'THB', 764, 0.00000, '฿', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(136, 1, 'Tunisian dinar', '', 'TND', 788, 0.00000, 'د.ت', '3', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(137, 1, 'Tongan paʻanga', '', 'TOP', 776, 0.00000, 'T$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(139, 1, 'Turkish new lira', '', 'TRY', 949, 0.00000, 'YTL', '2', ',', '.', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(140, 1, 'Trinidad and Tobago dollar', '', 'TTD', 780, 0.00000, 'TT$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(141, 1, 'New Taiwan dollar', '', 'TWD', 901, 0.00000, 'NT$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(142, 1, 'Tanzanian shilling', '', 'TZS', 834, 0.00000, 'Sh', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(144, 1, 'United States dollar', '', 'USD', 840, 0.00000, '$', '2', '.', ',', '{symbol}{number}', '{symbol}{sign}{number}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(147, 1, 'Vietnamese Dong', '', 'VND', 704, 0.00000, '₫', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(148, 1, 'Vanuatu vatu', '', 'VUV', 548, 0.00000, 'Vt', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(149, 1, 'Samoan tala', '', 'WST', 882, 0.00000, 'T', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(151, 1, 'Yemeni rial', '', 'YER', 886, 0.00000, '﷼', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(152, 1, 'Serbian dinar', '', 'RSD', 941, 0.00000, 'Дин.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(153, 1, 'South African rand', '', 'ZAR', 710, 0.00000, 'R', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(154, 1, 'Zambian kwacha', '', 'ZMK', 894, 0.00000, 'ZK', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(156, 1, 'Zimbabwean dollar', '', 'ZWD', 932, 0.00000, 'Z$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(158, 1, 'Armenian dram', '', 'AMD', 51, 0.00000, 'դր.', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(159, 1, 'Myanmar kyat', '', 'MMK', 104, 0.00000, 'K', '2', ',', '', '{number} {symbol}', '{symbol} {sign}{number}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(160, 1, 'Croatian kuna', '', 'HRK', 191, 0.00000, 'kn', '2', ',', '.', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(161, 1, 'Eritrean nakfa', '', 'ERN', 232, 0.00000, 'Nfk', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(162, 1, 'Djiboutian franc', '', 'DJF', 262, 0.00000, 'Fr', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(163, 1, 'Icelandic króna', '', 'ISK', 352, 0.00000, 'kr', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(164, 1, 'Kazakhstani tenge', '', 'KZT', 398, 0.00000, 'лв', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(165, 1, 'Kyrgyzstani som', '', 'KGS', 417, 0.00000, 'лв', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(166, 1, 'Latvian lats', '', 'LVL', 428, 0.00000, 'Ls', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(167, 1, 'Lithuanian litas', '', 'LTL', 440, 0.00000, 'Lt', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(168, 1, 'Mexican peso', '', 'MXN', 484, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(169, 1, 'Moldovan leu', '', 'MDL', 498, 0.00000, 'L', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(170, 1, 'Namibian dollar', '', 'NAD', 516, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(171, 1, 'Nicaraguan córdoba', '', 'NIO', 558, 0.00000, 'C$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(172, 1, 'Ugandan shilling', '', 'UGX', 800, 0.00000, 'Sh', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(173, 1, 'Macedonian denar', '', 'MKD', 807, 0.00000, 'ден', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(174, 1, 'Uruguayan peso', '', 'UYU', 858, 0.00000, '$', '0', '', '', '{symbol}number}', '{symbol}{sign}{number}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(175, 1, 'Uzbekistani som', '', 'UZS', 860, 0.00000, 'лв', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(176, 1, 'Azerbaijani manat', '', 'AZN', 934, 0.00000, 'ман', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(177, 1, 'Ghanaian cedi', '', 'GHS', 936, 0.00000, '₵', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(178, 1, 'Venezuelan bolívar', '', 'VEF', 937, 0.00000, 'Bs', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(179, 1, 'Sudanese pound', '', 'SDG', 938, 0.00000, '£', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(180, 1, 'Uruguay Peso', '', 'UYI', 940, 0.00000, 'UYI', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(181, 1, 'Mozambican metical', '', 'MZN', 943, 0.00000, 'MT', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(182, 1, 'WIR Euro', '', 'CHE', 947, 0.00000, '€', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(183, 1, 'WIR Franc', '', 'CHW', 948, 0.00000, 'CHW', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(184, 1, 'Central African CFA franc', '', 'XAF', 950, 0.00000, 'Fr', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(185, 1, 'East Caribbean dollar', '', 'XCD', 951, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(186, 1, 'West African CFA franc', '', 'XOF', 952, 0.00000, 'Fr', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(187, 1, 'CFP franc', '', 'XPF', 953, 0.00000, 'Fr', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(188, 1, 'Surinamese dollar', '', 'SRD', 968, 0.00000, '$', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(189, 1, 'Malagasy ariary', '', 'MGA', 969, 0.00000, 'MGA', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(190, 1, 'Unidad de Valor Real', '', 'COU', 970, 0.00000, 'COU', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(191, 1, 'Afghan afghani', '', 'AFN', 971, 0.00000, '؋', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(192, 1, 'Tajikistani somoni', '', 'TJS', 972, 0.00000, 'ЅМ', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(193, 1, 'Angolan kwanza', '', 'AOA', 973, 0.00000, 'Kz', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(194, 1, 'Belarusian ruble', '', 'BYR', 974, 0.00000, 'p.', '0', '', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(195, 1, 'Bulgarian lev', '', 'BGN', 975, 0.00000, 'лв', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(196, 1, 'Congolese franc', '', 'CDF', 976, 0.00000, 'Fr', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(197, 1, 'Bosnia and Herzegovina convert', '', 'BAM', 977, 0.00000, 'KM', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(198, 1, 'Mexican Unid', '', 'MXV', 979, 0.00000, 'MXV', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(199, 1, 'Ukrainian hryvnia', '', 'UAH', 980, 8.20000, '₴', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 0, 1, '2012-08-31 09:37:06', 42, '2012-08-31 09:37:06', 42, '0000-00-00 00:00:00', 0),
(200, 1, 'Georgian lari', '', 'GEL', 981, 0.00000, 'ლ', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(201, 1, 'Mvdol', '', 'BOV', 984, 0.00000, 'BOV', '2', ',', '', '{number} {symbol}', '{sign}{number} {symbol}', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_customs`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_customs` (
  `virtuemart_custom_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `custom_parent_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `custom_jplugin_id` int(11) NOT NULL DEFAULT '0',
  `custom_element` char(50) NOT NULL DEFAULT '',
  `admin_only` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Display in admin only',
  `custom_title` char(255) NOT NULL DEFAULT '' COMMENT 'field title',
  `custom_tip` char(255) NOT NULL DEFAULT '' COMMENT 'tip',
  `custom_value` char(255) DEFAULT NULL COMMENT 'defaut value',
  `custom_field_desc` char(255) DEFAULT NULL COMMENT 'description or unit',
  `field_type` char(1) NOT NULL DEFAULT '0' COMMENT 'S:string,I:int,P:parent, B:bool,D:date,T:time,H:hidden',
  `is_list` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'list of values',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:hidden',
  `is_cart_attribute` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Add attributes to cart',
  `layout_pos` char(24) DEFAULT NULL COMMENT 'Layout Position',
  `custom_params` text,
  `shared` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'valide for all vendors?',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_custom_id`),
  KEY `idx_custom_plugin_virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `idx_custom_plugin_element` (`custom_element`),
  KEY `idx_custom_plugin_ordering` (`ordering`),
  KEY `idx_custom_parent_id` (`custom_parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='custom fields definition' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `o1hap_virtuemart_customs`
--

INSERT INTO `o1hap_virtuemart_customs` (`virtuemart_custom_id`, `custom_parent_id`, `virtuemart_vendor_id`, `custom_jplugin_id`, `custom_element`, `admin_only`, `custom_title`, `custom_tip`, `custom_value`, `custom_field_desc`, `field_type`, `is_list`, `is_hidden`, `is_cart_attribute`, `layout_pos`, `custom_params`, `shared`, `published`, `created_on`, `created_by`, `ordering`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 0, 1, 0, '', 0, 'COM_VIRTUEMART_RELATED_PRODUCTS', 'COM_VIRTUEMART_RELATED_PRODUCTS_TIP', '', 'COM_VIRTUEMART_RELATED_PRODUCTS_DESC', 'R', 0, 0, 0, NULL, NULL, 0, 1, '2011-05-25 21:52:43', 62, 0, '2011-05-25 21:52:43', 62, '0000-00-00 00:00:00', 0),
(2, 0, 1, 0, '', 0, 'COM_VIRTUEMART_RELATED_CATEGORIES', 'COM_VIRTUEMART_RELATED_CATEGORIES_TIP', NULL, 'COM_VIRTUEMART_RELATED_CATEGORIES_DESC', 'Z', 0, 0, 0, NULL, NULL, 0, 1, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_invoices`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_invoices` (
  `virtuemart_invoice_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `virtuemart_order_id` int(1) unsigned DEFAULT NULL,
  `invoice_number` char(64) DEFAULT NULL,
  `order_status` char(2) DEFAULT NULL,
  `xhtml` text,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_invoice_id`),
  UNIQUE KEY `idx_invoice_number` (`invoice_number`,`virtuemart_vendor_id`),
  KEY `idx_virtuemart_order_id` (`virtuemart_order_id`),
  KEY `idx_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='custom fields definition' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_manufacturercategories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_manufacturercategories` (
  `virtuemart_manufacturercategories_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_manufacturercategories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Manufacturers are assigned to these categories' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_manufacturercategories_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_manufacturercategories_ru_ru` (
  `virtuemart_manufacturercategories_id` int(1) unsigned NOT NULL,
  `mf_category_name` char(180) NOT NULL DEFAULT '',
  `mf_category_desc` varchar(20000) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_manufacturercategories_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_manufacturers`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_manufacturers` (
  `virtuemart_manufacturer_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_manufacturercategories_id` int(11) DEFAULT NULL,
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_manufacturer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Manufacturers are those who deliver products' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_manufacturers_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_manufacturers_ru_ru` (
  `virtuemart_manufacturer_id` int(1) unsigned NOT NULL,
  `mf_name` char(180) NOT NULL DEFAULT '',
  `mf_email` char(255) NOT NULL DEFAULT '',
  `mf_desc` varchar(20000) NOT NULL DEFAULT '',
  `mf_url` char(255) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_manufacturer_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_manufacturer_medias`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_manufacturer_medias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_manufacturer_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_media_id` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_category_id` (`virtuemart_manufacturer_id`,`virtuemart_media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_medias`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_medias` (
  `virtuemart_media_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `file_title` char(126) NOT NULL DEFAULT '',
  `file_description` char(254) NOT NULL DEFAULT '',
  `file_meta` char(254) NOT NULL DEFAULT '',
  `file_mimetype` char(64) NOT NULL DEFAULT '',
  `file_type` char(32) NOT NULL DEFAULT '',
  `file_url` text,
  `file_url_thumb` char(254) NOT NULL DEFAULT '',
  `file_is_product_image` tinyint(1) NOT NULL DEFAULT '0',
  `file_is_downloadable` tinyint(1) NOT NULL DEFAULT '0',
  `file_is_forSale` tinyint(1) NOT NULL DEFAULT '0',
  `file_params` text,
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_media_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Additional Images and Files which are assigned to products' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_virtuemart_medias`
--

INSERT INTO `o1hap_virtuemart_medias` (`virtuemart_media_id`, `virtuemart_vendor_id`, `file_title`, `file_description`, `file_meta`, `file_mimetype`, `file_type`, `file_url`, `file_url_thumb`, `file_is_product_image`, `file_is_downloadable`, `file_is_forSale`, `file_params`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 'catalog_second_lvl_img.png', '', '', 'image/png', 'product', 'images/stories/virtuemart/product/catalog_second_lvl_img.png', 'images/stories/virtuemart/product/resized/catalog_second_lvl_img_90x90.png', 0, 0, 0, '', 0, 1, '2012-04-17 13:27:21', 42, '2012-04-17 13:27:21', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_migration_oldtonew_ids`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_migration_oldtonew_ids` (
  `id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `cats` longblob,
  `catsxref` blob,
  `manus` longblob,
  `mfcats` blob,
  `shoppergroups` longblob,
  `products` longblob,
  `products_start` int(1) DEFAULT NULL,
  `orderstates` blob,
  `orders` longblob,
  `orders_start` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='xref table for vm1 ids to vm2 ids' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_modules`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_modules` (
  `module_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` char(255) DEFAULT NULL,
  `module_description` text,
  `module_perms` char(255) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `is_admin` enum('0','1') NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`module_id`),
  KEY `idx_module_name` (`module_name`),
  KEY `idx_module_ordering` (`ordering`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='VirtueMart Core Modules, not: Joomla modules' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `o1hap_virtuemart_modules`
--

INSERT INTO `o1hap_virtuemart_modules` (`module_id`, `module_name`, `module_description`, `module_perms`, `published`, `is_admin`, `ordering`) VALUES
(1, 'product', 'Here you can administer your online catalog of products.  Categories , Products (view=product), Attributes  ,Product Types      Product Files (view=media), Inventory  , Calculation Rules ,Customer Reviews  ', 'storeadmin,admin', 1, '1', 1),
(2, 'order', 'View Order and Update Order Status:    Orders , Coupons , Revenue Report ,Shopper , Shopper Groups ', 'admin,storeadmin', 1, '1', 2),
(3, 'manufacturer', 'Manage the manufacturers of products in your store.', 'storeadmin,admin', 1, '1', 3),
(4, 'store', 'Store Configuration: Store Information, Payment Methods , Shipment, Shipment Rates', 'storeadmin,admin', 1, '1', 4),
(5, 'configuration', 'Configuration: shop configuration , currencies (view=currency), Credit Card List, Countries, userfields, order status  ', 'admin,storeadmin', 1, '1', 5),
(6, 'msgs', 'This module is unprotected an used for displaying system messages to users.  We need to have an area that does not require authorization when things go wrong.', 'none', 0, '0', 99),
(7, 'shop', 'This is the Washupito store module.  This is the demo store included with the VirtueMart distribution.', 'none', 1, '0', 99),
(8, 'store', 'Store Configuration: Store Information, Payment Methods , Shipment, Shipment Rates', 'storeadmin,admin', 1, '1', 4),
(9, 'account', 'This module allows shoppers to update their account information and view previously placed orders.', 'shopper,storeadmin,admin,demo', 1, '0', 99),
(10, 'checkout', '', 'none', 0, '0', 99),
(11, 'tools', 'Tools', 'admin', 1, '1', 8),
(13, 'zone', 'This is the zone-shipment module. Here you can manage your shipment costs according to Zones.', 'admin,storeadmin', 0, '1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_orders`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_orders` (
  `virtuemart_order_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_user_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `order_number` char(64) DEFAULT NULL,
  `order_pass` char(8) DEFAULT NULL,
  `order_total` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_salesPrice` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_billTaxAmount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_billDiscountAmount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_discountAmount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_subtotal` decimal(15,5) DEFAULT NULL,
  `order_tax` decimal(10,5) DEFAULT NULL,
  `order_shipment` decimal(10,2) DEFAULT NULL,
  `order_shipment_tax` decimal(10,5) DEFAULT NULL,
  `order_payment` decimal(10,2) DEFAULT NULL,
  `order_payment_tax` decimal(10,5) DEFAULT NULL,
  `coupon_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `coupon_code` char(32) DEFAULT NULL,
  `order_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `order_currency` smallint(1) DEFAULT NULL,
  `order_status` char(1) DEFAULT NULL,
  `user_currency_id` char(4) DEFAULT NULL,
  `user_currency_rate` decimal(10,5) NOT NULL DEFAULT '1.00000',
  `virtuemart_paymentmethod_id` mediumint(1) unsigned DEFAULT NULL,
  `virtuemart_shipmentmethod_id` mediumint(1) unsigned DEFAULT NULL,
  `customer_note` text,
  `ip_address` char(15) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_order_id`),
  KEY `idx_orders_virtuemart_user_id` (`virtuemart_user_id`),
  KEY `idx_orders_virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `idx_orders_order_number` (`order_number`),
  KEY `idx_orders_virtuemart_paymentmethod_id` (`virtuemart_paymentmethod_id`),
  KEY `idx_orders_virtuemart_shipmentmethod_id` (`virtuemart_shipmentmethod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Used to store all orders' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_orderstates`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_orderstates` (
  `virtuemart_orderstate_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `order_status_code` char(1) NOT NULL DEFAULT '',
  `order_status_name` char(64) DEFAULT NULL,
  `order_status_description` text,
  `order_stock_handle` char(1) NOT NULL DEFAULT 'A',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_orderstate_id`),
  KEY `idx_order_status_ordering` (`ordering`),
  KEY `idx_order_status_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='All available order statuses' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `o1hap_virtuemart_orderstates`
--

INSERT INTO `o1hap_virtuemart_orderstates` (`virtuemart_orderstate_id`, `virtuemart_vendor_id`, `order_status_code`, `order_status_name`, `order_status_description`, `order_stock_handle`, `ordering`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 'P', 'Pending', '', 'R', 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 'C', 'Confirmed', '', 'R', 2, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 'X', 'Cancelled', '', 'A', 3, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'R', 'Refunded', '', 'A', 4, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 'S', 'Shipped', '', 'O', 5, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_order_calc_rules`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_order_calc_rules` (
  `virtuemart_order_calc_rule_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_order_id` int(11) DEFAULT NULL,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `calc_rule_name` char(64) NOT NULL DEFAULT '' COMMENT 'Name of the rule',
  `calc_kind` char(16) NOT NULL DEFAULT '' COMMENT 'Discount/Tax/Margin/Commission',
  `calc_amount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_order_calc_rule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores all calculation rules which are part of an order' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_order_histories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_order_histories` (
  `virtuemart_order_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_order_id` int(1) unsigned NOT NULL DEFAULT '0',
  `order_status_code` char(1) NOT NULL DEFAULT '0',
  `customer_notified` tinyint(1) NOT NULL DEFAULT '0',
  `comments` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_order_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores all actions and changes that occur to an order' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_order_items`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_order_items` (
  `virtuemart_order_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_order_id` int(11) DEFAULT NULL,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `virtuemart_product_id` int(11) DEFAULT NULL,
  `order_item_sku` char(64) NOT NULL DEFAULT '',
  `order_item_name` char(255) NOT NULL DEFAULT '',
  `product_quantity` int(11) DEFAULT NULL,
  `product_item_price` decimal(15,5) DEFAULT NULL,
  `product_tax` decimal(15,5) DEFAULT NULL,
  `product_basePriceWithTax` decimal(15,5) DEFAULT NULL,
  `product_final_price` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `product_subtotal_discount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `product_subtotal_with_tax` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_item_currency` int(11) DEFAULT NULL,
  `order_status` char(1) DEFAULT NULL,
  `product_attribute` text,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_order_item_id`),
  KEY `virtuemart_product_id` (`virtuemart_product_id`),
  KEY `idx_order_item_virtuemart_order_id` (`virtuemart_order_id`),
  KEY `idx_order_item_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores all items (products) which are part of an order' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_order_userinfos`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_order_userinfos` (
  `virtuemart_order_userinfo_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_order_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_user_id` int(1) unsigned NOT NULL DEFAULT '0',
  `address_type` char(2) DEFAULT NULL,
  `address_type_name` char(32) DEFAULT NULL,
  `company` char(64) DEFAULT NULL,
  `title` char(32) DEFAULT NULL,
  `last_name` char(32) DEFAULT NULL,
  `first_name` char(32) DEFAULT NULL,
  `middle_name` char(32) DEFAULT NULL,
  `phone_1` char(24) DEFAULT NULL,
  `phone_2` char(24) DEFAULT NULL,
  `fax` char(24) DEFAULT NULL,
  `address_1` char(64) NOT NULL DEFAULT '',
  `address_2` char(64) DEFAULT NULL,
  `city` char(32) NOT NULL DEFAULT '',
  `virtuemart_state_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_country_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `zip` char(16) NOT NULL DEFAULT '',
  `email` char(255) DEFAULT NULL,
  `agreed` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_order_userinfo_id`),
  KEY `i_virtuemart_order_id` (`virtuemart_order_id`),
  KEY `i_virtuemart_user_id` (`virtuemart_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores the BillTo and ShipTo Information at order time' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_paymentmethods`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_paymentmethods` (
  `virtuemart_paymentmethod_id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `payment_jplugin_id` int(11) NOT NULL DEFAULT '0',
  `slug` char(255) NOT NULL DEFAULT '',
  `payment_element` char(50) NOT NULL DEFAULT '',
  `payment_params` text,
  `shared` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'valide for all vendors?',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_paymentmethod_id`),
  KEY `idx_payment_jplugin_id` (`payment_jplugin_id`),
  KEY `idx_payment_element` (`payment_element`,`virtuemart_vendor_id`),
  KEY `idx_payment_method_ordering` (`ordering`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='The payment methods of your store' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_paymentmethods_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_paymentmethods_ru_ru` (
  `virtuemart_paymentmethod_id` int(1) unsigned NOT NULL,
  `payment_name` char(180) NOT NULL DEFAULT '',
  `payment_desc` varchar(20000) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_paymentmethod_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_paymentmethod_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_paymentmethod_shoppergroups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_paymentmethod_id` mediumint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_shoppergroup_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_paymentmethod_id` (`virtuemart_paymentmethod_id`,`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='xref table for paymentmethods to shoppergroup' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_permgroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_permgroups` (
  `virtuemart_permgroup_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `group_name` char(128) DEFAULT NULL,
  `group_level` int(11) DEFAULT NULL,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_permgroup_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds all the user groups' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `o1hap_virtuemart_permgroups`
--

INSERT INTO `o1hap_virtuemart_permgroups` (`virtuemart_permgroup_id`, `virtuemart_vendor_id`, `group_name`, `group_level`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 'admin', 0, 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 'storeadmin', 250, 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 'shopper', 500, 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'demo', 750, 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_products`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_products` (
  `virtuemart_product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `product_parent_id` int(1) unsigned NOT NULL DEFAULT '0',
  `product_sku` char(64) DEFAULT NULL,
  `product_weight` decimal(10,4) DEFAULT NULL,
  `product_weight_uom` char(7) DEFAULT NULL,
  `product_length` decimal(10,4) DEFAULT NULL,
  `product_width` decimal(10,4) DEFAULT NULL,
  `product_height` decimal(10,4) DEFAULT NULL,
  `product_lwh_uom` char(7) DEFAULT NULL,
  `product_url` char(255) DEFAULT NULL,
  `product_in_stock` int(1) DEFAULT NULL,
  `product_ordered` int(1) DEFAULT NULL,
  `low_stock_notification` int(1) unsigned DEFAULT NULL,
  `product_available_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_availability` char(32) DEFAULT NULL,
  `product_special` tinyint(1) DEFAULT NULL,
  `product_sales` int(1) unsigned DEFAULT NULL,
  `product_unit` char(4) DEFAULT NULL,
  `product_packaging` int(11) DEFAULT NULL,
  `product_params` text,
  `hits` int(11) unsigned DEFAULT NULL,
  `intnotes` text,
  `metarobot` text,
  `metaauthor` text,
  `layout` char(16) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_product_id`),
  KEY `idx_product_virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `idx_product_product_parent_id` (`product_parent_id`),
  KEY `idx_product_sku` (`product_sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='All products are stored here.' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `o1hap_virtuemart_products`
--

INSERT INTO `o1hap_virtuemart_products` (`virtuemart_product_id`, `virtuemart_vendor_id`, `product_parent_id`, `product_sku`, `product_weight`, `product_weight_uom`, `product_length`, `product_width`, `product_height`, `product_lwh_uom`, `product_url`, `product_in_stock`, `product_ordered`, `low_stock_notification`, `product_available_date`, `product_availability`, `product_special`, `product_sales`, `product_unit`, `product_packaging`, `product_params`, `hits`, `intnotes`, `metarobot`, `metaauthor`, `layout`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(2, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 15:55:01', 42, '2012-06-05 14:56:49', 42, '0000-00-00 00:00:00', 0),
(3, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 15:57:52', 42, '2012-06-05 14:58:56', 42, '0000-00-00 00:00:00', 0),
(4, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:02:37', 42, '2012-06-05 14:59:56', 42, '0000-00-00 00:00:00', 0),
(5, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:04:23', 42, '2012-06-05 15:00:42', 42, '0000-00-00 00:00:00', 0),
(6, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:04:47', 42, '2012-06-05 15:02:04', 42, '0000-00-00 00:00:00', 0),
(7, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:05:16', 42, '2012-06-05 15:02:36', 42, '0000-00-00 00:00:00', 0),
(8, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:05:40', 42, '2012-06-05 15:03:12', 42, '0000-00-00 00:00:00', 0),
(9, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:06:07', 42, '2012-06-05 15:04:17', 42, '0000-00-00 00:00:00', 0),
(10, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:06:34', 42, '2012-06-05 15:05:06', 42, '0000-00-00 00:00:00', 0),
(11, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:06:57', 42, '2012-04-30 18:06:35', 42, '0000-00-00 00:00:00', 0),
(12, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:07:21', 42, '2012-06-05 15:05:58', 42, '0000-00-00 00:00:00', 0),
(13, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:07:43', 42, '2012-04-30 16:28:12', 42, '0000-00-00 00:00:00', 0),
(14, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:08:25', 42, '2012-06-05 15:06:43', 42, '0000-00-00 00:00:00', 0),
(15, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:08:50', 42, '2012-04-30 16:28:28', 42, '0000-00-00 00:00:00', 0),
(16, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:09:12', 42, '2012-06-05 15:07:24', 42, '0000-00-00 00:00:00', 0),
(17, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:09:34', 42, '2012-04-30 18:26:12', 42, '0000-00-00 00:00:00', 0),
(18, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:09:34', 42, '2012-06-05 15:08:11', 42, '0000-00-00 00:00:00', 0),
(19, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:10:40', 42, '2012-04-30 16:29:02', 42, '0000-00-00 00:00:00', 0),
(20, 1, 0, '', 0.0000, 'KG', 0.0000, 0.0000, 0.0000, 'M', '', 10, 0, 0, '2012-04-20 00:00:00', '', 0, 0, '', 0, 'min_order_level="0"|max_order_level="0"|', NULL, '', '', '', '0', 1, '2012-04-20 16:11:36', 42, '2012-04-30 16:29:17', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_products_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_products_ru_ru` (
  `virtuemart_product_id` int(1) unsigned NOT NULL,
  `product_s_desc` varchar(2000) NOT NULL DEFAULT '',
  `product_desc` varchar(18500) NOT NULL DEFAULT '',
  `product_name` char(180) NOT NULL DEFAULT '',
  `metadesc` char(192) NOT NULL DEFAULT '',
  `metakey` char(192) NOT NULL DEFAULT '',
  `customtitle` char(255) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_product_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_virtuemart_products_ru_ru`
--

INSERT INTO `o1hap_virtuemart_products_ru_ru` (`virtuemart_product_id`, `product_s_desc`, `product_desc`, `product_name`, `metadesc`, `metakey`, `customtitle`, `slug`) VALUES
(2, '', '', '500х400', '', '', '', '500х400'),
(3, '', '', '500х500', '', '', '', '500х500'),
(4, '', '', '500х600', '', '', '', '500х600'),
(5, '', '', '500х700', '', '', '', '500х700'),
(6, '', '', '500х800', '', '', '', '500х800'),
(7, '', '', '500х900', '', '', '', '500х900'),
(8, '', '', '500х1000', '', '', '', '500х1000'),
(9, '', '', '500х1100', '', '', '', '500х1100'),
(10, '', '', '500х1200', '', '', '', '500х1200'),
(11, '', '', '500х1300', '', '', '', '500х1300'),
(12, '', '', '500х1400', '', '', '', '500х1400'),
(13, '', '', '500х1500', '', '', '', '500х14002012-04-20-16-07-43_'),
(14, '', '', '500х1600', '', '', '', '500х1600'),
(15, '', '', '500х1700', '', '', '', '500х1700'),
(16, '', '', '500х1800', '', '', '', '500х1800'),
(17, '', '', '500х1900', '', '', '', '500х1900'),
(18, '', '', '500х2000', '', '', '', '500х2000'),
(19, '', '', '500х2400', '', '', '', '500х2400'),
(20, '', '', '500х2600', '', '', '', '500х2600');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_categories`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_category_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`virtuemart_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Maps Products to Categories' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `o1hap_virtuemart_product_categories`
--

INSERT INTO `o1hap_virtuemart_product_categories` (`id`, `virtuemart_product_id`, `virtuemart_category_id`, `ordering`) VALUES
(2, 3, 5, 1),
(3, 2, 5, 1),
(4, 4, 5, 1),
(5, 5, 5, 1),
(6, 6, 5, 1),
(7, 7, 5, 1),
(8, 8, 5, 1),
(9, 9, 5, 1),
(10, 10, 5, 1),
(11, 11, 5, 10),
(12, 12, 5, 1),
(13, 13, 5, 12),
(14, 14, 5, 1),
(15, 15, 5, 14),
(16, 16, 5, 1),
(17, 17, 5, 1),
(18, 18, 5, 1),
(19, 19, 5, 18),
(20, 20, 5, 19);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_customfields`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_customfields` (
  `virtuemart_customfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'field id',
  `virtuemart_product_id` int(11) NOT NULL DEFAULT '0',
  `virtuemart_custom_id` int(11) NOT NULL DEFAULT '1' COMMENT 'custom group id',
  `custom_value` varchar(20000) DEFAULT NULL COMMENT 'field value',
  `custom_price` char(255) DEFAULT NULL COMMENT 'price',
  `custom_param` text COMMENT 'Param for Plugins',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(1) unsigned NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(1) unsigned NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_customfield_id`),
  KEY `idx_virtuemart_product_id` (`virtuemart_product_id`),
  KEY `idx_virtuemart_custom_id` (`virtuemart_custom_id`),
  KEY `idx_custom_value` (`custom_value`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='custom fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_manufacturers`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_manufacturers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(11) DEFAULT NULL,
  `virtuemart_manufacturer_id` smallint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`virtuemart_manufacturer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Maps a product to a manufacturer' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_medias`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_medias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_media_id` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_category_id` (`virtuemart_product_id`,`virtuemart_media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_prices`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_prices` (
  `virtuemart_product_price_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_shoppergroup_id` int(11) DEFAULT NULL,
  `product_price` decimal(15,5) DEFAULT NULL,
  `override` tinyint(1) DEFAULT NULL,
  `product_override_price` decimal(15,5) DEFAULT NULL,
  `product_tax_id` int(11) DEFAULT NULL,
  `product_discount_id` int(11) DEFAULT NULL,
  `product_currency` mediumint(3) DEFAULT NULL,
  `product_price_vdate` datetime DEFAULT NULL,
  `product_price_edate` datetime DEFAULT NULL,
  `price_quantity_start` int(11) unsigned DEFAULT NULL,
  `price_quantity_end` int(11) unsigned DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_product_price_id`),
  KEY `idx_product_price_product_id` (`virtuemart_product_id`),
  KEY `idx_product_price_virtuemart_shoppergroup_id` (`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds price records for a product' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `o1hap_virtuemart_product_prices`
--

INSERT INTO `o1hap_virtuemart_product_prices` (`virtuemart_product_price_id`, `virtuemart_product_id`, `virtuemart_shoppergroup_id`, `product_price`, `override`, `product_override_price`, `product_tax_id`, `product_discount_id`, `product_currency`, `product_price_vdate`, `product_price_edate`, `price_quantity_start`, `price_quantity_end`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(2, 2, NULL, 27.54000, NULL, 28.08000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 14:56:49', 42, '2012-06-05 14:56:49', 42, '0000-00-00 00:00:00', 0),
(3, 3, NULL, 32.13000, NULL, 33.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 14:58:56', 42, '2012-06-05 14:58:56', 42, '0000-00-00 00:00:00', 0),
(4, 4, NULL, 38.25000, NULL, 39.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 14:59:56', 42, '2012-06-05 14:59:56', 42, '0000-00-00 00:00:00', 0),
(5, 5, NULL, 43.86000, NULL, 44.70000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:00:42', 42, '2012-06-05 15:00:42', 42, '0000-00-00 00:00:00', 0),
(6, 6, NULL, 49.47000, NULL, 49.82000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:02:04', 42, '2012-06-05 15:02:04', 42, '0000-00-00 00:00:00', 0),
(7, 7, NULL, 54.06000, NULL, 55.20000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:02:36', 42, '2012-06-05 15:02:36', 42, '0000-00-00 00:00:00', 0),
(8, 8, NULL, 58.65000, NULL, 58.91000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:03:12', 42, '2012-06-05 15:03:12', 42, '0000-00-00 00:00:00', 0),
(9, 9, NULL, 63.75000, NULL, 65.40000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:04:17', 42, '2012-06-05 15:04:17', 42, '0000-00-00 00:00:00', 0),
(10, 10, NULL, 68.85000, NULL, 69.92000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:05:06', 42, '2012-06-05 15:05:06', 42, '0000-00-00 00:00:00', 0),
(11, 11, NULL, 0.00000, 1, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 18:06:35', 42, '2012-04-30 18:06:35', 42, '0000-00-00 00:00:00', 0),
(12, 12, NULL, 80.58000, NULL, 79.73000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:05:58', 42, '2012-06-05 15:05:58', 42, '0000-00-00 00:00:00', 0),
(13, 13, NULL, 0.00000, NULL, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 16:28:12', 42, '2012-04-30 16:28:12', 42, '0000-00-00 00:00:00', 0),
(14, 14, NULL, 91.80000, NULL, 96.30000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:06:43', 42, '2012-06-05 15:06:43', 42, '0000-00-00 00:00:00', 0),
(15, 15, NULL, 0.00000, NULL, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 16:28:28', 42, '2012-04-30 16:28:28', 42, '0000-00-00 00:00:00', 0),
(16, 16, NULL, 102.00000, NULL, 103.98000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:07:24', 42, '2012-06-05 15:07:24', 42, '0000-00-00 00:00:00', 0),
(17, 17, NULL, 0.00000, NULL, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 18:26:12', 42, '2012-04-30 18:26:12', 42, '0000-00-00 00:00:00', 0),
(18, 18, NULL, 120.36000, NULL, 120.19000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-06-05 15:08:11', 42, '2012-06-05 15:08:11', 42, '0000-00-00 00:00:00', 0),
(19, 19, NULL, 0.00000, NULL, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 16:29:02', 42, '2012-04-30 16:29:02', 42, '0000-00-00 00:00:00', 0),
(20, 20, NULL, 0.00000, NULL, 0.00000, 0, 0, 144, NULL, NULL, NULL, NULL, '2012-04-30 16:29:17', 42, '2012-04-30 16:29:17', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_relations`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `related_products` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`related_products`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_product_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_product_shoppergroups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_shoppergroup_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Maps Products to Categories' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_ratings`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_ratings` (
  `virtuemart_rating_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `rates` int(11) NOT NULL DEFAULT '0',
  `ratingcount` int(1) unsigned NOT NULL DEFAULT '0',
  `rating` decimal(10,1) NOT NULL DEFAULT '0.0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_rating_id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`virtuemart_rating_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores all ratings for a product' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_rating_reviews`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_rating_reviews` (
  `virtuemart_rating_review_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `review_ok` tinyint(1) NOT NULL DEFAULT '0',
  `review_rates` int(1) unsigned NOT NULL DEFAULT '0',
  `review_ratingcount` int(1) unsigned NOT NULL DEFAULT '0',
  `review_rating` decimal(10,2) NOT NULL DEFAULT '0.00',
  `review_editable` tinyint(1) NOT NULL DEFAULT '1',
  `lastip` char(50) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_rating_review_id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`created_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_rating_votes`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_rating_votes` (
  `virtuemart_rating_vote_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `vote` int(11) NOT NULL DEFAULT '0',
  `lastip` char(50) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_rating_vote_id`),
  UNIQUE KEY `i_virtuemart_product_id` (`virtuemart_product_id`,`created_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores all ratings for a product' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_shipmentmethods`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_shipmentmethods` (
  `virtuemart_shipmentmethod_id` mediumint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `shipment_jplugin_id` int(11) NOT NULL DEFAULT '0',
  `slug` char(255) NOT NULL DEFAULT '',
  `shipment_element` char(50) NOT NULL DEFAULT '',
  `shipment_params` text,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_shipmentmethod_id`),
  KEY `idx_shipment_jplugin_id` (`shipment_jplugin_id`),
  KEY `idx_shipment_element` (`shipment_element`,`virtuemart_vendor_id`),
  KEY `idx_shipment_method_ordering` (`ordering`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Shipment created from the shipment plugins' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_shipmentmethods_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_shipmentmethods_ru_ru` (
  `virtuemart_shipmentmethod_id` int(1) unsigned NOT NULL,
  `shipment_name` char(180) NOT NULL DEFAULT '',
  `shipment_desc` varchar(20000) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_shipmentmethod_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_shipmentmethod_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_shipmentmethod_shoppergroups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_shipmentmethod_id` mediumint(1) unsigned DEFAULT NULL,
  `virtuemart_shoppergroup_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_shipmentmethod_id` (`virtuemart_shipmentmethod_id`,`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='xref table for shipment to shoppergroup' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_shoppergroups` (
  `virtuemart_shoppergroup_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(11) NOT NULL DEFAULT '1',
  `shopper_group_name` char(32) DEFAULT NULL,
  `shopper_group_desc` char(128) DEFAULT NULL,
  `custom_price_display` tinyint(1) NOT NULL DEFAULT '0',
  `price_display` blob,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_shoppergroup_id`),
  KEY `idx_shopper_group_virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `idx_shopper_group_name` (`shopper_group_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Shopper Groups that users can be assigned to' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `o1hap_virtuemart_shoppergroups`
--

INSERT INTO `o1hap_virtuemart_shoppergroups` (`virtuemart_shoppergroup_id`, `virtuemart_vendor_id`, `shopper_group_name`, `shopper_group_desc`, `custom_price_display`, `price_display`, `default`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(2, 1, '-default-', 'This is the default shopper group.', 0, NULL, 1, 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(1, 1, '-anonymous-', 'Shopper group for anonymous shoppers', 0, NULL, 2, 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_states`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_states` (
  `virtuemart_state_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `virtuemart_country_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `virtuemart_worldzone_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `state_name` char(64) DEFAULT NULL,
  `state_3_code` char(3) DEFAULT NULL,
  `state_2_code` char(2) DEFAULT NULL,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_state_id`),
  UNIQUE KEY `idx_state_3_code` (`virtuemart_country_id`,`state_3_code`),
  UNIQUE KEY `idx_state_2_code` (`virtuemart_country_id`,`state_2_code`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`),
  KEY `i_virtuemart_country_id` (`virtuemart_country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='States that are assigned to a country' AUTO_INCREMENT=570 ;

--
-- Dumping data for table `o1hap_virtuemart_states`
--

INSERT INTO `o1hap_virtuemart_states` (`virtuemart_state_id`, `virtuemart_vendor_id`, `virtuemart_country_id`, `virtuemart_worldzone_id`, `state_name`, `state_3_code`, `state_2_code`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 223, 0, 'Alabama', 'ALA', 'AL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 223, 0, 'Alaska', 'ALK', 'AK', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 223, 0, 'Arizona', 'ARZ', 'AZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 223, 0, 'Arkansas', 'ARK', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 223, 0, 'California', 'CAL', 'CA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, 1, 223, 0, 'Colorado', 'COL', 'CO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 1, 223, 0, 'Connecticut', 'CCT', 'CT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, 1, 223, 0, 'Delaware', 'DEL', 'DE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 1, 223, 0, 'District Of Columbia', 'DOC', 'DC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 1, 223, 0, 'Florida', 'FLO', 'FL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 1, 223, 0, 'Georgia', 'GEA', 'GA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 1, 223, 0, 'Hawaii', 'HWI', 'HI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(13, 1, 223, 0, 'Idaho', 'IDA', 'ID', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(14, 1, 223, 0, 'Illinois', 'ILL', 'IL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 1, 223, 0, 'Indiana', 'IND', 'IN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 1, 223, 0, 'Iowa', 'IOA', 'IA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 1, 223, 0, 'Kansas', 'KAS', 'KS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 1, 223, 0, 'Kentucky', 'KTY', 'KY', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 1, 223, 0, 'Louisiana', 'LOA', 'LA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(20, 1, 223, 0, 'Maine', 'MAI', 'ME', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(21, 1, 223, 0, 'Maryland', 'MLD', 'MD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(22, 1, 223, 0, 'Massachusetts', 'MSA', 'MA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(23, 1, 223, 0, 'Michigan', 'MIC', 'MI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(24, 1, 223, 0, 'Minnesota', 'MIN', 'MN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(25, 1, 223, 0, 'Mississippi', 'MIS', 'MS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(26, 1, 223, 0, 'Missouri', 'MIO', 'MO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 1, 223, 0, 'Montana', 'MOT', 'MT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 1, 223, 0, 'Nebraska', 'NEB', 'NE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 1, 223, 0, 'Nevada', 'NEV', 'NV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(30, 1, 223, 0, 'New Hampshire', 'NEH', 'NH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(31, 1, 223, 0, 'New Jersey', 'NEJ', 'NJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(32, 1, 223, 0, 'New Mexico', 'NEM', 'NM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(33, 1, 223, 0, 'New York', 'NEY', 'NY', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(34, 1, 223, 0, 'North Carolina', 'NOC', 'NC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(35, 1, 223, 0, 'North Dakota', 'NOD', 'ND', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(36, 1, 223, 0, 'Ohio', 'OHI', 'OH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(37, 1, 223, 0, 'Oklahoma', 'OKL', 'OK', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(38, 1, 223, 0, 'Oregon', 'ORN', 'OR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(39, 1, 223, 0, 'Pennsylvania', 'PEA', 'PA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(40, 1, 223, 0, 'Rhode Island', 'RHI', 'RI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(41, 1, 223, 0, 'South Carolina', 'SOC', 'SC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(42, 1, 223, 0, 'South Dakota', 'SOD', 'SD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(43, 1, 223, 0, 'Tennessee', 'TEN', 'TN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(44, 1, 223, 0, 'Texas', 'TXS', 'TX', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(45, 1, 223, 0, 'Utah', 'UTA', 'UT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(46, 1, 223, 0, 'Vermont', 'VMT', 'VT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(47, 1, 223, 0, 'Virginia', 'VIA', 'VA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(48, 1, 223, 0, 'Washington', 'WAS', 'WA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(49, 1, 223, 0, 'West Virginia', 'WEV', 'WV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(50, 1, 223, 0, 'Wisconsin', 'WIS', 'WI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(51, 1, 223, 0, 'Wyoming', 'WYO', 'WY', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(52, 1, 38, 0, 'Alberta', 'ALB', 'AB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(53, 1, 38, 0, 'British Columbia', 'BRC', 'BC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(54, 1, 38, 0, 'Manitoba', 'MAB', 'MB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(55, 1, 38, 0, 'New Brunswick', 'NEB', 'NB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(56, 1, 38, 0, 'Newfoundland and Labrador', 'NFL', 'NL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(57, 1, 38, 0, 'Northwest Territories', 'NWT', 'NT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(58, 1, 38, 0, 'Nova Scotia', 'NOS', 'NS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(59, 1, 38, 0, 'Nunavut', 'NUT', 'NU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(60, 1, 38, 0, 'Ontario', 'ONT', 'ON', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(61, 1, 38, 0, 'Prince Edward Island', 'PEI', 'PE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(62, 1, 38, 0, 'Quebec', 'QEC', 'QC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(63, 1, 38, 0, 'Saskatchewan', 'SAK', 'SK', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(64, 1, 38, 0, 'Yukon', 'YUT', 'YT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(65, 1, 222, 0, 'England', 'ENG', 'EN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(66, 1, 222, 0, 'Northern Ireland', 'NOI', 'NI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(67, 1, 222, 0, 'Scotland', 'SCO', 'SD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(68, 1, 222, 0, 'Wales', 'WLS', 'WS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(69, 1, 13, 0, 'Australian Capital Territory', 'ACT', 'AC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(70, 1, 13, 0, 'New South Wales', 'NSW', 'NS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(71, 1, 13, 0, 'Northern Territory', 'NOT', 'NT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(72, 1, 13, 0, 'Queensland', 'QLD', 'QL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(73, 1, 13, 0, 'South Australia', 'SOA', 'SA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(74, 1, 13, 0, 'Tasmania', 'TAS', 'TS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(75, 1, 13, 0, 'Victoria', 'VIC', 'VI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(76, 1, 13, 0, 'Western Australia', 'WEA', 'WA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(77, 1, 138, 0, 'Aguascalientes', 'AGS', 'AG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(78, 1, 138, 0, 'Baja California Norte', 'BCN', 'BN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(79, 1, 138, 0, 'Baja California Sur', 'BCS', 'BS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(80, 1, 138, 0, 'Campeche', 'CAM', 'CA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(81, 1, 138, 0, 'Chiapas', 'CHI', 'CS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(82, 1, 138, 0, 'Chihuahua', 'CHA', 'CH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(83, 1, 138, 0, 'Coahuila', 'COA', 'CO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(84, 1, 138, 0, 'Colima', 'COL', 'CM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(85, 1, 138, 0, 'Distrito Federal', 'DFM', 'DF', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(86, 1, 138, 0, 'Durango', 'DGO', 'DO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(87, 1, 138, 0, 'Guanajuato', 'GTO', 'GO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(88, 1, 138, 0, 'Guerrero', 'GRO', 'GU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(89, 1, 138, 0, 'Hidalgo', 'HGO', 'HI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(90, 1, 138, 0, 'Jalisco', 'JAL', 'JA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(91, 1, 138, 0, 'M', 'EDM', 'EM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(92, 1, 138, 0, 'Michoac', 'MCN', 'MI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(93, 1, 138, 0, 'Morelos', 'MOR', 'MO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(94, 1, 138, 0, 'Nayarit', 'NAY', 'NY', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(95, 1, 138, 0, 'Nuevo Le', 'NUL', 'NL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(96, 1, 138, 0, 'Oaxaca', 'OAX', 'OA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(97, 1, 138, 0, 'Puebla', 'PUE', 'PU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(98, 1, 138, 0, 'Quer', 'QRO', 'QU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(99, 1, 138, 0, 'Quintana Roo', 'QUR', 'QR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(100, 1, 138, 0, 'San Luis Potos', 'SLP', 'SP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(101, 1, 138, 0, 'Sinaloa', 'SIN', 'SI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(102, 1, 138, 0, 'Sonora', 'SON', 'SO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(103, 1, 138, 0, 'Tabasco', 'TAB', 'TA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(104, 1, 138, 0, 'Tamaulipas', 'TAM', 'TM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(105, 1, 138, 0, 'Tlaxcala', 'TLX', 'TX', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(106, 1, 138, 0, 'Veracruz', 'VER', 'VZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(107, 1, 138, 0, 'Yucat', 'YUC', 'YU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(108, 1, 138, 0, 'Zacatecas', 'ZAC', 'ZA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(109, 1, 30, 0, 'Acre', 'ACR', 'AC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(110, 1, 30, 0, 'Alagoas', 'ALG', 'AL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(111, 1, 30, 0, 'Amapá', 'AMP', 'AP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(112, 1, 30, 0, 'Amazonas', 'AMZ', 'AM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(113, 1, 30, 0, 'Bahía', 'BAH', 'BA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(114, 1, 30, 0, 'Ceará', 'CEA', 'CE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(115, 1, 30, 0, 'Distrito Federal', 'DFB', 'DF', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(116, 1, 30, 0, 'Espírito Santo', 'ESS', 'ES', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(117, 1, 30, 0, 'Goiás', 'GOI', 'GO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(118, 1, 30, 0, 'Maranhão', 'MAR', 'MA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(119, 1, 30, 0, 'Mato Grosso', 'MAT', 'MT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(120, 1, 30, 0, 'Mato Grosso do Sul', 'MGS', 'MS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(121, 1, 30, 0, 'Minas Gerais', 'MIG', 'MG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(122, 1, 30, 0, 'Paraná', 'PAR', 'PR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(123, 1, 30, 0, 'Paraíba', 'PRB', 'PB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(124, 1, 30, 0, 'Pará', 'PAB', 'PA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(125, 1, 30, 0, 'Pernambuco', 'PER', 'PE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(126, 1, 30, 0, 'Piauí', 'PIA', 'PI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(127, 1, 30, 0, 'Rio Grande do Norte', 'RGN', 'RN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(128, 1, 30, 0, 'Rio Grande do Sul', 'RGS', 'RS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(129, 1, 30, 0, 'Rio de Janeiro', 'RDJ', 'RJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(130, 1, 30, 0, 'Rondônia', 'RON', 'RO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(131, 1, 30, 0, 'Roraima', 'ROR', 'RR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(132, 1, 30, 0, 'Santa Catarina', 'SAC', 'SC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(133, 1, 30, 0, 'Sergipe', 'SER', 'SE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(134, 1, 30, 0, 'São Paulo', 'SAP', 'SP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(135, 1, 30, 0, 'Tocantins', 'TOC', 'TO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(136, 1, 44, 0, 'Anhui', 'ANH', '34', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(137, 1, 44, 0, 'Beijing', 'BEI', '11', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(138, 1, 44, 0, 'Chongqing', 'CHO', '50', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(139, 1, 44, 0, 'Fujian', 'FUJ', '35', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(140, 1, 44, 0, 'Gansu', 'GAN', '62', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(141, 1, 44, 0, 'Guangdong', 'GUA', '44', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(142, 1, 44, 0, 'Guangxi Zhuang', 'GUZ', '45', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(143, 1, 44, 0, 'Guizhou', 'GUI', '52', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(144, 1, 44, 0, 'Hainan', 'HAI', '46', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(145, 1, 44, 0, 'Hebei', 'HEB', '13', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(146, 1, 44, 0, 'Heilongjiang', 'HEI', '23', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(147, 1, 44, 0, 'Henan', 'HEN', '41', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(148, 1, 44, 0, 'Hubei', 'HUB', '42', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(149, 1, 44, 0, 'Hunan', 'HUN', '43', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(150, 1, 44, 0, 'Jiangsu', 'JIA', '32', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(151, 1, 44, 0, 'Jiangxi', 'JIX', '36', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(152, 1, 44, 0, 'Jilin', 'JIL', '22', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(153, 1, 44, 0, 'Liaoning', 'LIA', '21', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(154, 1, 44, 0, 'Nei Mongol', 'NML', '15', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(155, 1, 44, 0, 'Ningxia Hui', 'NIH', '64', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(156, 1, 44, 0, 'Qinghai', 'QIN', '63', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(157, 1, 44, 0, 'Shandong', 'SNG', '37', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(158, 1, 44, 0, 'Shanghai', 'SHH', '31', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(159, 1, 44, 0, 'Shaanxi', 'SHX', '61', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(160, 1, 44, 0, 'Sichuan', 'SIC', '51', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(161, 1, 44, 0, 'Tianjin', 'TIA', '12', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(162, 1, 44, 0, 'Xinjiang Uygur', 'XIU', '65', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(163, 1, 44, 0, 'Xizang', 'XIZ', '54', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(164, 1, 44, 0, 'Yunnan', 'YUN', '53', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(165, 1, 44, 0, 'Zhejiang', 'ZHE', '33', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(166, 1, 104, 0, 'Israel', 'ISL', 'IL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(167, 1, 104, 0, 'Gaza Strip', 'GZS', 'GZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(168, 1, 104, 0, 'West Bank', 'WBK', 'WB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(169, 1, 151, 0, 'St. Maarten', 'STM', 'SM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(170, 1, 151, 0, 'Bonaire', 'BNR', 'BN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(171, 1, 151, 0, 'Curacao', 'CUR', 'CR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(172, 1, 175, 0, 'Alba', 'ABA', 'AB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(173, 1, 175, 0, 'Arad', 'ARD', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(174, 1, 175, 0, 'Arges', 'ARG', 'AG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(175, 1, 175, 0, 'Bacau', 'BAC', 'BC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(176, 1, 175, 0, 'Bihor', 'BIH', 'BH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(177, 1, 175, 0, 'Bistrita-Nasaud', 'BIS', 'BN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(178, 1, 175, 0, 'Botosani', 'BOT', 'BT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(179, 1, 175, 0, 'Braila', 'BRL', 'BR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(180, 1, 175, 0, 'Brasov', 'BRA', 'BV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(181, 1, 175, 0, 'Bucuresti', 'BUC', 'B', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(182, 1, 175, 0, 'Buzau', 'BUZ', 'BZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(183, 1, 175, 0, 'Calarasi', 'CAL', 'CL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(184, 1, 175, 0, 'Caras Severin', 'CRS', 'CS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(185, 1, 175, 0, 'Cluj', 'CLJ', 'CJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(186, 1, 175, 0, 'Constanta', 'CST', 'CT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(187, 1, 175, 0, 'Covasna', 'COV', 'CV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(188, 1, 175, 0, 'Dambovita', 'DAM', 'DB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(189, 1, 175, 0, 'Dolj', 'DLJ', 'DJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(190, 1, 175, 0, 'Galati', 'GAL', 'GL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(191, 1, 175, 0, 'Giurgiu', 'GIU', 'GR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(192, 1, 175, 0, 'Gorj', 'GOR', 'GJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(193, 1, 175, 0, 'Hargita', 'HRG', 'HR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(194, 1, 175, 0, 'Hunedoara', 'HUN', 'HD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(195, 1, 175, 0, 'Ialomita', 'IAL', 'IL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(196, 1, 175, 0, 'Iasi', 'IAS', 'IS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(197, 1, 175, 0, 'Ilfov', 'ILF', 'IF', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(198, 1, 175, 0, 'Maramures', 'MAR', 'MM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(199, 1, 175, 0, 'Mehedinti', 'MEH', 'MH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(200, 1, 175, 0, 'Mures', 'MUR', 'MS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(201, 1, 175, 0, 'Neamt', 'NEM', 'NT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(202, 1, 175, 0, 'Olt', 'OLT', 'OT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(203, 1, 175, 0, 'Prahova', 'PRA', 'PH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(204, 1, 175, 0, 'Salaj', 'SAL', 'SJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(205, 1, 175, 0, 'Satu Mare', 'SAT', 'SM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(206, 1, 175, 0, 'Sibiu', 'SIB', 'SB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(207, 1, 175, 0, 'Suceava', 'SUC', 'SV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(208, 1, 175, 0, 'Teleorman', 'TEL', 'TR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(209, 1, 175, 0, 'Timis', 'TIM', 'TM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(210, 1, 175, 0, 'Tulcea', 'TUL', 'TL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(211, 1, 175, 0, 'Valcea', 'VAL', 'VL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(212, 1, 175, 0, 'Vaslui', 'VAS', 'VS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(213, 1, 175, 0, 'Vrancea', 'VRA', 'VN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(214, 1, 105, 0, 'Agrigento', 'AGR', 'AG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(215, 1, 105, 0, 'Alessandria', 'ALE', 'AL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(216, 1, 105, 0, 'Ancona', 'ANC', 'AN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(217, 1, 105, 0, 'Aosta', 'AOS', 'AO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(218, 1, 105, 0, 'Arezzo', 'ARE', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(219, 1, 105, 0, 'Ascoli Piceno', 'API', 'AP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(220, 1, 105, 0, 'Asti', 'AST', 'AT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(221, 1, 105, 0, 'Avellino', 'AVE', 'AV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(222, 1, 105, 0, 'Bari', 'BAR', 'BA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(223, 1, 105, 0, 'Belluno', 'BEL', 'BL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(224, 1, 105, 0, 'Benevento', 'BEN', 'BN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(225, 1, 105, 0, 'Bergamo', 'BEG', 'BG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(226, 1, 105, 0, 'Biella', 'BIE', 'BI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(227, 1, 105, 0, 'Bologna', 'BOL', 'BO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(228, 1, 105, 0, 'Bolzano', 'BOZ', 'BZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(229, 1, 105, 0, 'Brescia', 'BRE', 'BS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(230, 1, 105, 0, 'Brindisi', 'BRI', 'BR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(231, 1, 105, 0, 'Cagliari', 'CAG', 'CA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(232, 1, 105, 0, 'Caltanissetta', 'CAL', 'CL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(233, 1, 105, 0, 'Campobasso', 'CBO', 'CB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(234, 1, 105, 0, 'Carbonia-Iglesias', 'CAR', 'CI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(235, 1, 105, 0, 'Caserta', 'CAS', 'CE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(236, 1, 105, 0, 'Catania', 'CAT', 'CT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(237, 1, 105, 0, 'Catanzaro', 'CTZ', 'CZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(238, 1, 105, 0, 'Chieti', 'CHI', 'CH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(239, 1, 105, 0, 'Como', 'COM', 'CO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(240, 1, 105, 0, 'Cosenza', 'COS', 'CS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(241, 1, 105, 0, 'Cremona', 'CRE', 'CR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(242, 1, 105, 0, 'Crotone', 'CRO', 'KR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(243, 1, 105, 0, 'Cuneo', 'CUN', 'CN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(244, 1, 105, 0, 'Enna', 'ENN', 'EN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(245, 1, 105, 0, 'Ferrara', 'FER', 'FE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(246, 1, 105, 0, 'Firenze', 'FIR', 'FI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(247, 1, 105, 0, 'Foggia', 'FOG', 'FG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(248, 1, 105, 0, 'Forli-Cesena', 'FOC', 'FC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(249, 1, 105, 0, 'Frosinone', 'FRO', 'FR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(250, 1, 105, 0, 'Genova', 'GEN', 'GE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(251, 1, 105, 0, 'Gorizia', 'GOR', 'GO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(252, 1, 105, 0, 'Grosseto', 'GRO', 'GR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(253, 1, 105, 0, 'Imperia', 'IMP', 'IM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(254, 1, 105, 0, 'Isernia', 'ISE', 'IS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(255, 1, 105, 0, 'L''Aquila', 'AQU', 'AQ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(256, 1, 105, 0, 'La Spezia', 'LAS', 'SP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(257, 1, 105, 0, 'Latina', 'LAT', 'LT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(258, 1, 105, 0, 'Lecce', 'LEC', 'LE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(259, 1, 105, 0, 'Lecco', 'LCC', 'LC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(260, 1, 105, 0, 'Livorno', 'LIV', 'LI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(261, 1, 105, 0, 'Lodi', 'LOD', 'LO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(262, 1, 105, 0, 'Lucca', 'LUC', 'LU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(263, 1, 105, 0, 'Macerata', 'MAC', 'MC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(264, 1, 105, 0, 'Mantova', 'MAN', 'MN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(265, 1, 105, 0, 'Massa-Carrara', 'MAS', 'MS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(266, 1, 105, 0, 'Matera', 'MAA', 'MT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(267, 1, 105, 0, 'Medio Campidano', 'MED', 'VS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(268, 1, 105, 0, 'Messina', 'MES', 'ME', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(269, 1, 105, 0, 'Milano', 'MIL', 'MI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(270, 1, 105, 0, 'Modena', 'MOD', 'MO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(271, 1, 105, 0, 'Napoli', 'NAP', 'NA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(272, 1, 105, 0, 'Novara', 'NOV', 'NO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(273, 1, 105, 0, 'Nuoro', 'NUR', 'NU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(274, 1, 105, 0, 'Ogliastra', 'OGL', 'OG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(275, 1, 105, 0, 'Olbia-Tempio', 'OLB', 'OT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(276, 1, 105, 0, 'Oristano', 'ORI', 'OR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(277, 1, 105, 0, 'Padova', 'PDA', 'PD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(278, 1, 105, 0, 'Palermo', 'PAL', 'PA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(279, 1, 105, 0, 'Parma', 'PAA', 'PR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(280, 1, 105, 0, 'Pavia', 'PAV', 'PV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(281, 1, 105, 0, 'Perugia', 'PER', 'PG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(282, 1, 105, 0, 'Pesaro e Urbino', 'PES', 'PU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(283, 1, 105, 0, 'Pescara', 'PSC', 'PE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(284, 1, 105, 0, 'Piacenza', 'PIA', 'PC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(285, 1, 105, 0, 'Pisa', 'PIS', 'PI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(286, 1, 105, 0, 'Pistoia', 'PIT', 'PT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(287, 1, 105, 0, 'Pordenone', 'POR', 'PN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(288, 1, 105, 0, 'Potenza', 'PTZ', 'PZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(289, 1, 105, 0, 'Prato', 'PRA', 'PO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(290, 1, 105, 0, 'Ragusa', 'RAG', 'RG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(291, 1, 105, 0, 'Ravenna', 'RAV', 'RA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(292, 1, 105, 0, 'Reggio Calabria', 'REG', 'RC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(293, 1, 105, 0, 'Reggio Emilia', 'REE', 'RE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(294, 1, 105, 0, 'Rieti', 'RIE', 'RI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(295, 1, 105, 0, 'Rimini', 'RIM', 'RN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(296, 1, 105, 0, 'Roma', 'ROM', 'RM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(297, 1, 105, 0, 'Rovigo', 'ROV', 'RO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(298, 1, 105, 0, 'Salerno', 'SAL', 'SA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(299, 1, 105, 0, 'Sassari', 'SAS', 'SS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(300, 1, 105, 0, 'Savona', 'SAV', 'SV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(301, 1, 105, 0, 'Siena', 'SIE', 'SI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(302, 1, 105, 0, 'Siracusa', 'SIR', 'SR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(303, 1, 105, 0, 'Sondrio', 'SOO', 'SO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(304, 1, 105, 0, 'Taranto', 'TAR', 'TA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(305, 1, 105, 0, 'Teramo', 'TER', 'TE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(306, 1, 105, 0, 'Terni', 'TRN', 'TR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(307, 1, 105, 0, 'Torino', 'TOR', 'TO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(308, 1, 105, 0, 'Trapani', 'TRA', 'TP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(309, 1, 105, 0, 'Trento', 'TRE', 'TN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(310, 1, 105, 0, 'Treviso', 'TRV', 'TV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(311, 1, 105, 0, 'Trieste', 'TRI', 'TS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(312, 1, 105, 0, 'Udine', 'UDI', 'UD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(313, 1, 105, 0, 'Varese', 'VAR', 'VA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(314, 1, 105, 0, 'Venezia', 'VEN', 'VE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(315, 1, 105, 0, 'Verbano Cusio Ossola', 'VCO', 'VB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(316, 1, 105, 0, 'Vercelli', 'VER', 'VC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(317, 1, 105, 0, 'Verona', 'VRN', 'VR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(318, 1, 105, 0, 'Vibo Valenzia', 'VIV', 'VV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(319, 1, 105, 0, 'Vicenza', 'VII', 'VI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(320, 1, 105, 0, 'Viterbo', 'VIT', 'VT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(321, 1, 195, 0, 'A Coru', 'ACO', '15', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(322, 1, 195, 0, 'Alava', 'ALA', '01', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(323, 1, 195, 0, 'Albacete', 'ALB', '02', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(324, 1, 195, 0, 'Alicante', 'ALI', '03', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(325, 1, 195, 0, 'Almeria', 'ALM', '04', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(326, 1, 195, 0, 'Asturias', 'AST', '33', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(327, 1, 195, 0, 'Avila', 'AVI', '05', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(328, 1, 195, 0, 'Badajoz', 'BAD', '06', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(329, 1, 195, 0, 'Baleares', 'BAL', '07', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(330, 1, 195, 0, 'Barcelona', 'BAR', '08', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(331, 1, 195, 0, 'Burgos', 'BUR', '09', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(332, 1, 195, 0, 'Caceres', 'CAC', '10', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(333, 1, 195, 0, 'Cadiz', 'CAD', '11', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(334, 1, 195, 0, 'Cantabria', 'CAN', '39', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(335, 1, 195, 0, 'Castellon', 'CAS', '12', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(336, 1, 195, 0, 'Ceuta', 'CEU', '51', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(337, 1, 195, 0, 'Ciudad Real', 'CIU', '13', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(338, 1, 195, 0, 'Cordoba', 'COR', '14', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(339, 1, 195, 0, 'Cuenca', 'CUE', '16', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(340, 1, 195, 0, 'Girona', 'GIR', '17', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(341, 1, 195, 0, 'Granada', 'GRA', '18', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(342, 1, 195, 0, 'Guadalajara', 'GUA', '19', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(343, 1, 195, 0, 'Guipuzcoa', 'GUI', '20', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(344, 1, 195, 0, 'Huelva', 'HUL', '21', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(345, 1, 195, 0, 'Huesca', 'HUS', '22', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(346, 1, 195, 0, 'Jaen', 'JAE', '23', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(347, 1, 195, 0, 'La Rioja', 'LRI', '26', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(348, 1, 195, 0, 'Las Palmas', 'LPA', '35', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(349, 1, 195, 0, 'Leon', 'LEO', '24', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(350, 1, 195, 0, 'Lleida', 'LLE', '25', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(351, 1, 195, 0, 'Lugo', 'LUG', '27', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(352, 1, 195, 0, 'Madrid', 'MAD', '28', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(353, 1, 195, 0, 'Malaga', 'MAL', '29', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(354, 1, 195, 0, 'Melilla', 'MEL', '52', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(355, 1, 195, 0, 'Murcia', 'MUR', '30', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(356, 1, 195, 0, 'Navarra', 'NAV', '31', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(357, 1, 195, 0, 'Ourense', 'OUR', '32', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(358, 1, 195, 0, 'Palencia', 'PAL', '34', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(359, 1, 195, 0, 'Pontevedra', 'PON', '36', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(360, 1, 195, 0, 'Salamanca', 'SAL', '37', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(361, 1, 195, 0, 'Santa Cruz de Tenerife', 'SCT', '38', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(362, 1, 195, 0, 'Segovia', 'SEG', '40', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(363, 1, 195, 0, 'Sevilla', 'SEV', '41', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(364, 1, 195, 0, 'Soria', 'SOR', '42', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(365, 1, 195, 0, 'Tarragona', 'TAR', '43', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(366, 1, 195, 0, 'Teruel', 'TER', '44', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(367, 1, 195, 0, 'Toledo', 'TOL', '45', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(368, 1, 195, 0, 'Valencia', 'VAL', '46', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(369, 1, 195, 0, 'Valladolid', 'VLL', '47', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(370, 1, 195, 0, 'Vizcaya', 'VIZ', '48', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(371, 1, 195, 0, 'Zamora', 'ZAM', '49', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(372, 1, 195, 0, 'Zaragoza', 'ZAR', '50', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(373, 1, 10, 0, 'Buenos Aires', 'BAS', 'BA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(374, 1, 10, 0, 'Ciudad Autonoma De Buenos Aires', 'CBA', 'CB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(375, 1, 10, 0, 'Catamarca', 'CAT', 'CA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(376, 1, 10, 0, 'Chaco', 'CHO', 'CH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(377, 1, 10, 0, 'Chubut', 'CTT', 'CT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(378, 1, 10, 0, 'Cordoba', 'COD', 'CO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(379, 1, 10, 0, 'Corrientes', 'CRI', 'CR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(380, 1, 10, 0, 'Entre Rios', 'ERS', 'ER', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(381, 1, 10, 0, 'Formosa', 'FRM', 'FR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(382, 1, 10, 0, 'Jujuy', 'JUJ', 'JU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(383, 1, 10, 0, 'La Pampa', 'LPM', 'LP', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(384, 1, 10, 0, 'La Rioja', 'LRI', 'LR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(385, 1, 10, 0, 'Mendoza', 'MED', 'ME', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(386, 1, 10, 0, 'Misiones', 'MIS', 'MI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(387, 1, 10, 0, 'Neuquen', 'NQU', 'NQ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(388, 1, 10, 0, 'Rio Negro', 'RNG', 'RN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(389, 1, 10, 0, 'Salta', 'SAL', 'SA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);
INSERT INTO `o1hap_virtuemart_states` (`virtuemart_state_id`, `virtuemart_vendor_id`, `virtuemart_country_id`, `virtuemart_worldzone_id`, `state_name`, `state_3_code`, `state_2_code`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(390, 1, 10, 0, 'San Juan', 'SJN', 'SJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(391, 1, 10, 0, 'San Luis', 'SLU', 'SL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(392, 1, 10, 0, 'Santa Cruz', 'SCZ', 'SC', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(393, 1, 10, 0, 'Santa Fe', 'SFE', 'SF', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(394, 1, 10, 0, 'Santiago Del Estero', 'SEN', 'SE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(395, 1, 10, 0, 'Tierra Del Fuego', 'TFE', 'TF', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(396, 1, 10, 0, 'Tucuman', 'TUC', 'TU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(397, 1, 11, 0, 'Aragatsotn', 'ARG', 'AG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(398, 1, 11, 0, 'Ararat', 'ARR', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(399, 1, 11, 0, 'Armavir', 'ARM', 'AV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(400, 1, 11, 0, 'Gegharkunik', 'GEG', 'GR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(401, 1, 11, 0, 'Kotayk', 'KOT', 'KT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(402, 1, 11, 0, 'Lori', 'LOR', 'LO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(403, 1, 11, 0, 'Shirak', 'SHI', 'SH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(404, 1, 11, 0, 'Syunik', 'SYU', 'SU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(405, 1, 11, 0, 'Tavush', 'TAV', 'TV', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(406, 1, 11, 0, 'Vayots-Dzor', 'VAD', 'VD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(407, 1, 11, 0, 'Yerevan', 'YER', 'ER', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(408, 1, 99, 0, 'Andaman & Nicobar Islands', 'ANI', 'AI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(409, 1, 99, 0, 'Andhra Pradesh', 'AND', 'AN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(410, 1, 99, 0, 'Arunachal Pradesh', 'ARU', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(411, 1, 99, 0, 'Assam', 'ASS', 'AS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(412, 1, 99, 0, 'Bihar', 'BIH', 'BI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(413, 1, 99, 0, 'Chandigarh', 'CHA', 'CA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(414, 1, 99, 0, 'Chhatisgarh', 'CHH', 'CH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(415, 1, 99, 0, 'Dadra & Nagar Haveli', 'DAD', 'DD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(416, 1, 99, 0, 'Daman & Diu', 'DAM', 'DA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(417, 1, 99, 0, 'Delhi', 'DEL', 'DE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(418, 1, 99, 0, 'Goa', 'GOA', 'GO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(419, 1, 99, 0, 'Gujarat', 'GUJ', 'GU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(420, 1, 99, 0, 'Haryana', 'HAR', 'HA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(421, 1, 99, 0, 'Himachal Pradesh', 'HIM', 'HI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(422, 1, 99, 0, 'Jammu & Kashmir', 'JAM', 'JA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(423, 1, 99, 0, 'Jharkhand', 'JHA', 'JH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(424, 1, 99, 0, 'Karnataka', 'KAR', 'KA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(425, 1, 99, 0, 'Kerala', 'KER', 'KE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(426, 1, 99, 0, 'Lakshadweep', 'LAK', 'LA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(427, 1, 99, 0, 'Madhya Pradesh', 'MAD', 'MD', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(428, 1, 99, 0, 'Maharashtra', 'MAH', 'MH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(429, 1, 99, 0, 'Manipur', 'MAN', 'MN', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(430, 1, 99, 0, 'Meghalaya', 'MEG', 'ME', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(431, 1, 99, 0, 'Mizoram', 'MIZ', 'MI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(432, 1, 99, 0, 'Nagaland', 'NAG', 'NA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(433, 1, 99, 0, 'Orissa', 'ORI', 'OR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(434, 1, 99, 0, 'Pondicherry', 'PON', 'PO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(435, 1, 99, 0, 'Punjab', 'PUN', 'PU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(436, 1, 99, 0, 'Rajasthan', 'RAJ', 'RA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(437, 1, 99, 0, 'Sikkim', 'SIK', 'SI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(438, 1, 99, 0, 'Tamil Nadu', 'TAM', 'TA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(439, 1, 99, 0, 'Tripura', 'TRI', 'TR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(440, 1, 99, 0, 'Uttaranchal', 'UAR', 'UA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(441, 1, 99, 0, 'Uttar Pradesh', 'UTT', 'UT', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(442, 1, 99, 0, 'West Bengal', 'WES', 'WE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(443, 1, 101, 0, 'Ahmadi va Kohkiluyeh', 'BOK', 'BO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(444, 1, 101, 0, 'Ardabil', 'ARD', 'AR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(445, 1, 101, 0, 'Azarbayjan-e Gharbi', 'AZG', 'AG', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(446, 1, 101, 0, 'Azarbayjan-e Sharqi', 'AZS', 'AS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(447, 1, 101, 0, 'Bushehr', 'BUS', 'BU', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(448, 1, 101, 0, 'Chaharmahal va Bakhtiari', 'CMB', 'CM', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(449, 1, 101, 0, 'Esfahan', 'ESF', 'ES', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(450, 1, 101, 0, 'Fars', 'FAR', 'FA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(451, 1, 101, 0, 'Gilan', 'GIL', 'GI', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(452, 1, 101, 0, 'Gorgan', 'GOR', 'GO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(453, 1, 101, 0, 'Hamadan', 'HAM', 'HA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(454, 1, 101, 0, 'Hormozgan', 'HOR', 'HO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(455, 1, 101, 0, 'Ilam', 'ILA', 'IL', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(456, 1, 101, 0, 'Kerman', 'KER', 'KE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(457, 1, 101, 0, 'Kermanshah', 'BAK', 'BA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(458, 1, 101, 0, 'Khorasan-e Junoubi', 'KHJ', 'KJ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(459, 1, 101, 0, 'Khorasan-e Razavi', 'KHR', 'KR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(460, 1, 101, 0, 'Khorasan-e Shomali', 'KHS', 'KS', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(461, 1, 101, 0, 'Khuzestan', 'KHU', 'KH', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(462, 1, 101, 0, 'Kordestan', 'KOR', 'KO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(463, 1, 101, 0, 'Lorestan', 'LOR', 'LO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(464, 1, 101, 0, 'Markazi', 'MAR', 'MR', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(465, 1, 101, 0, 'Mazandaran', 'MAZ', 'MZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(466, 1, 101, 0, 'Qazvin', 'QAS', 'QA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(467, 1, 101, 0, 'Qom', 'QOM', 'QO', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(468, 1, 101, 0, 'Semnan', 'SEM', 'SE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(469, 1, 101, 0, 'Sistan va Baluchestan', 'SBA', 'SB', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(470, 1, 101, 0, 'Tehran', 'TEH', 'TE', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(471, 1, 101, 0, 'Yazd', 'YAZ', 'YA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(472, 1, 101, 0, 'Zanjan', 'ZAN', 'ZA', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(535, 1, 84, 0, 'ΛΕΥΚΑΔΑΣ', 'ΛΕΥ', 'ΛΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(532, 1, 84, 0, 'ΛΑΡΙΣΑΣ', 'ΛΑΡ', 'ΛΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(504, 1, 84, 0, 'ΑΡΚΑΔΙΑΣ', 'ΑΡΚ', 'ΑΚ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(503, 1, 84, 0, 'ΑΡΓΟΛΙΔΑΣ', 'ΑΡΓ', 'ΑΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(533, 1, 84, 0, 'ΛΑΣΙΘΙΟΥ', 'ΛΑΣ', 'ΛΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(534, 1, 84, 0, 'ΛΕΣΒΟΥ', 'ΛΕΣ', 'ΛΣ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(530, 1, 84, 0, 'ΚΥΚΛΑΔΩΝ', 'ΚΥΚ', 'ΚΥ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(553, 1, 84, 0, 'ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ', 'ΑΙΤ', 'ΑΙ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(529, 1, 84, 0, 'ΚΟΡΙΝΘΙΑΣ', 'ΚΟΡ', 'ΚΟ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(531, 1, 84, 0, 'ΛΑΚΩΝΙΑΣ', 'ΛΑΚ', 'ΛK', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(517, 1, 84, 0, 'ΗΜΑΘΙΑΣ', 'ΗΜΑ', 'ΗΜ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(518, 1, 84, 0, 'ΗΡΑΚΛΕΙΟΥ', 'ΗΡΑ', 'ΗΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(519, 1, 84, 0, 'ΘΕΣΠΡΩΤΙΑΣ', 'ΘΕΠ', 'ΘΠ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(520, 1, 84, 0, 'ΘΕΣΣΑΛΟΝΙΚΗΣ', 'ΘΕΣ', 'ΘΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(521, 1, 84, 0, 'ΙΩΑΝΝΙΝΩΝ', 'ΙΩΑ', 'ΙΩ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(522, 1, 84, 0, 'ΚΑΒΑΛΑΣ', 'ΚΑΒ', 'ΚΒ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(523, 1, 84, 0, 'ΚΑΡΔΙΤΣΑΣ', 'ΚΑΡ', 'ΚΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(524, 1, 84, 0, 'ΚΑΣΤΟΡΙΑΣ', 'ΚΑΣ', 'ΚΣ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(525, 1, 84, 0, 'ΚΕΡΚΥΡΑΣ', 'ΚΕΡ', 'ΚΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(526, 1, 84, 0, 'ΚΕΦΑΛΛΗΝΙΑΣ', 'ΚΕΦ', 'ΚΦ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(527, 1, 84, 0, 'ΚΙΛΚΙΣ', 'ΚΙΛ', 'ΚΙ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(528, 1, 84, 0, 'ΚΟΖΑΝΗΣ', 'ΚΟΖ', 'ΚZ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(507, 1, 84, 0, 'ΑΧΑΪΑΣ', 'ΑΧΑ', 'ΑΧ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(508, 1, 84, 0, 'ΒΟΙΩΤΙΑΣ', 'ΒΟΙ', 'ΒΟ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(509, 1, 84, 0, 'ΓΡΕΒΕΝΩΝ', 'ΓΡΕ', 'ΓΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(510, 1, 84, 0, 'ΔΡΑΜΑΣ', 'ΔΡΑ', 'ΔΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(511, 1, 84, 0, 'ΔΩΔΕΚΑΝΗΣΟΥ', 'ΔΩΔ', 'ΔΩ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(512, 1, 84, 0, 'ΕΒΡΟΥ', 'ΕΒΡ', 'ΕΒ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(513, 1, 84, 0, 'ΕΥΒΟΙΑΣ', 'ΕΥΒ', 'ΕΥ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(514, 1, 84, 0, 'ΕΥΡΥΤΑΝΙΑΣ', 'ΕΥΡ', 'ΕΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(515, 1, 84, 0, 'ΖΑΚΥΝΘΟΥ', 'ΖΑΚ', 'ΖΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(516, 1, 84, 0, 'ΗΛΕΙΑΣ', 'ΗΛΕ', 'ΗΛ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(505, 1, 84, 0, 'ΑΡΤΑΣ', 'ΑΡΤ', 'ΑΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(506, 1, 84, 0, 'ΑΤΤΙΚΗΣ', 'ΑΤΤ', 'ΑΤ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(536, 1, 84, 0, 'ΜΑΓΝΗΣΙΑΣ', 'ΜΑΓ', 'ΜΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(537, 1, 84, 0, 'ΜΕΣΣΗΝΙΑΣ', 'ΜΕΣ', 'ΜΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(538, 1, 84, 0, 'ΞΑΝΘΗΣ', 'ΞΑΝ', 'ΞΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(539, 1, 84, 0, 'ΠΕΛΛΗΣ', 'ΠΕΛ', 'ΠΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(540, 1, 84, 0, 'ΠΙΕΡΙΑΣ', 'ΠΙΕ', 'ΠΙ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(541, 1, 84, 0, 'ΠΡΕΒΕΖΑΣ', 'ΠΡΕ', 'ΠΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(542, 1, 84, 0, 'ΡΕΘΥΜΝΗΣ', 'ΡΕΘ', 'ΡΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(543, 1, 84, 0, 'ΡΟΔΟΠΗΣ', 'ΡΟΔ', 'ΡΟ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(544, 1, 84, 0, 'ΣΑΜΟΥ', 'ΣΑΜ', 'ΣΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(545, 1, 84, 0, 'ΣΕΡΡΩΝ', 'ΣΕΡ', 'ΣΕ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(546, 1, 84, 0, 'ΤΡΙΚΑΛΩΝ', 'ΤΡΙ', 'ΤΡ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(547, 1, 84, 0, 'ΦΘΙΩΤΙΔΑΣ', 'ΦΘΙ', 'ΦΘ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(548, 1, 84, 0, 'ΦΛΩΡΙΝΑΣ', 'ΦΛΩ', 'ΦΛ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(549, 1, 84, 0, 'ΦΩΚΙΔΑΣ', 'ΦΩΚ', 'ΦΩ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(550, 1, 84, 0, 'ΧΑΛΚΙΔΙΚΗΣ', 'ΧΑΛ', 'ΧΑ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(551, 1, 84, 0, 'ΧΑΝΙΩΝ', 'ΧΑΝ', 'ΧΝ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(552, 1, 84, 0, 'ΧΙΟΥ', 'ΧΙΟ', 'ΧΙ', 0, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(569, 1, 81, 0, 'Schleswig-Holstein', 'SHO', 'SH', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(554, 1, 81, 0, 'Freie und Hansestadt Hamburg', 'HAM', 'HH', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(555, 1, 81, 0, 'Niedersachsen', 'NIS', 'NI', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(556, 1, 81, 0, 'Freie Hansestadt Bremen', 'HBR', 'HB', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(557, 1, 81, 0, 'Nordrhein-Westfalen', 'NRW', 'NW', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(558, 1, 81, 0, 'Hessen', 'HES', 'HE', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(559, 1, 81, 0, 'Rheinland-Pfalz', 'RLP', 'RP', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(560, 1, 81, 0, 'Baden-Württemberg', 'BWÜ', 'BW', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(561, 1, 81, 0, 'Freistaat Bayern', 'BAV', 'BY', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(562, 1, 81, 0, 'Saarland', 'SLA', 'SL', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(563, 1, 81, 0, 'Berlin', 'BER', 'BE', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(564, 1, 81, 0, 'Brandenburg', 'BRB', 'BB', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(565, 1, 81, 0, 'Mecklenburg-Vorpommern', 'MVO', 'MV', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(566, 1, 81, 0, 'Freistaat Sachsen', 'SAC', 'SN', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(567, 1, 81, 0, 'Sachsen-Anhalt', 'SAA', 'ST', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(568, 1, 81, 0, 'Freistaat Thüringen', 'THÜ', 'TH', 0, 1, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_userfields`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_userfields` (
  `virtuemart_userfield_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '1',
  `name` char(50) NOT NULL DEFAULT '',
  `title` char(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `type` char(50) NOT NULL DEFAULT '',
  `maxlength` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `cols` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  `value` char(50) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `registration` tinyint(1) NOT NULL DEFAULT '0',
  `shipment` tinyint(1) NOT NULL DEFAULT '0',
  `account` tinyint(1) NOT NULL DEFAULT '1',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `calculated` tinyint(1) NOT NULL DEFAULT '0',
  `sys` tinyint(4) NOT NULL DEFAULT '0',
  `params` mediumtext,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_userfield_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds the fields for the user information' AUTO_INCREMENT=30 ;

--
-- Dumping data for table `o1hap_virtuemart_userfields`
--

INSERT INTO `o1hap_virtuemart_userfields` (`virtuemart_userfield_id`, `virtuemart_vendor_id`, `name`, `title`, `description`, `type`, `maxlength`, `size`, `required`, `cols`, `rows`, `value`, `default`, `registration`, `shipment`, `account`, `readonly`, `calculated`, `sys`, `params`, `ordering`, `shared`, `published`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 1, 'email', 'COM_VIRTUEMART_REGISTER_EMAIL', '', 'emailaddress', 100, 30, 1, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 1, NULL, 8, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 'password', 'COM_VIRTUEMART_SHOPPER_FORM_PASSWORD_1', '', 'password', 25, 30, 1, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 1, NULL, 4, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 'password2', 'COM_VIRTUEMART_SHOPPER_FORM_PASSWORD_2', '', 'password', 25, 30, 1, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 1, NULL, 5, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'agreed', 'COM_VIRTUEMART_I_AGREE_TO_TOS', '', 'checkbox', NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 1, NULL, 29, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 'name', 'COM_VIRTUEMART_USER_DISPLAYED_NAME', '', 'text', 25, 30, 1, 0, 0, '', NULL, 1, 0, 1, 0, 0, 1, '', 3, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, 1, 'username', 'COM_VIRTUEMART_USERNAME', '', 'text', 25, 30, 1, 0, 0, '', NULL, 1, 0, 1, 0, 0, 1, '', 3, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 1, 'address_type_name', 'COM_VIRTUEMART_USER_FORM_ADDRESS_LABEL', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 1, NULL, 6, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, 1, 'delimiter_billto', 'COM_VIRTUEMART_USER_FORM_BILLTO_LBL', '', 'delimiter', 25, 30, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 6, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 1, 'company', 'COM_VIRTUEMART_SHOPPER_FORM_COMPANY_NAME', '', 'text', 64, 30, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 7, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 1, 'title', 'COM_VIRTUEMART_SHOPPER_FORM_TITLE', '', 'select', 0, 0, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 1, NULL, 8, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 1, 'first_name', 'COM_VIRTUEMART_SHOPPER_FORM_FIRST_NAME', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 9, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 1, 'middle_name', 'COM_VIRTUEMART_SHOPPER_FORM_MIDDLE_NAME', '', 'text', 32, 30, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 10, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(13, 1, 'last_name', 'COM_VIRTUEMART_SHOPPER_FORM_LAST_NAME', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 11, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(14, 1, 'address_1', 'COM_VIRTUEMART_SHOPPER_FORM_ADDRESS_1', '', 'text', 64, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 12, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 1, 'address_2', 'COM_VIRTUEMART_SHOPPER_FORM_ADDRESS_2', '', 'text', 64, 30, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 13, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 1, 'zip', 'COM_VIRTUEMART_SHOPPER_FORM_ZIP', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 14, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 1, 'city', 'COM_VIRTUEMART_SHOPPER_FORM_CITY', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 15, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 1, 'virtuemart_country_id', 'COM_VIRTUEMART_SHOPPER_FORM_COUNTRY', '', 'select', 0, 0, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 16, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 1, 'virtuemart_state_id', 'COM_VIRTUEMART_SHOPPER_FORM_STATE', '', 'select', 0, 0, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 17, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(20, 1, 'phone_1', 'COM_VIRTUEMART_SHOPPER_FORM_PHONE', '', 'text', 32, 30, 1, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 18, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(21, 1, 'phone_2', 'COM_VIRTUEMART_SHOPPER_FORM_PHONE2', '', 'text', 32, 30, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 19, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(22, 1, 'fax', 'COM_VIRTUEMART_SHOPPER_FORM_FAX', '', 'text', 32, 30, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, 20, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(23, 1, 'delimiter_sendregistration', 'COM_VIRTUEMART_BUTTON_SEND_REG', '', 'delimiter', 25, 30, 0, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, 28, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(24, 1, 'delimiter_userinfo', 'COM_VIRTUEMART_ORDER_PRINT_CUST_INFO_LBL', '', 'delimiter', NULL, NULL, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 1, 0, 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(25, 1, 'extra_field_1', 'COM_VIRTUEMART_SHOPPER_FORM_EXTRA_FIELD_1', '', 'text', 255, 30, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 31, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(26, 1, 'extra_field_2', 'COM_VIRTUEMART_SHOPPER_FORM_EXTRA_FIELD_2', '', 'text', 255, 30, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 32, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 1, 'extra_field_3', 'COM_VIRTUEMART_SHOPPER_FORM_EXTRA_FIELD_3', '', 'text', 255, 30, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 33, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 1, 'extra_field_4', 'COM_VIRTUEMART_SHOPPER_FORM_EXTRA_FIELD_4', '', 'select', 1, 1, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 34, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 1, 'extra_field_5', 'COM_VIRTUEMART_SHOPPER_FORM_EXTRA_FIELD_5', '', 'select', 1, 1, 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, NULL, 35, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_userfield_values`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_userfield_values` (
  `virtuemart_userfield_value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_userfield_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `fieldtitle` char(255) NOT NULL DEFAULT '',
  `fieldvalue` char(255) NOT NULL DEFAULT '',
  `sys` tinyint(4) NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_userfield_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Holds the different values for dropdown and radio lists' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_userinfos`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_userinfos` (
  `virtuemart_userinfo_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_user_id` int(1) unsigned NOT NULL DEFAULT '0',
  `address_type` char(2) NOT NULL DEFAULT '',
  `address_type_name` char(32) NOT NULL DEFAULT '',
  `name` char(64) DEFAULT NULL,
  `company` char(64) DEFAULT NULL,
  `title` char(32) DEFAULT NULL,
  `last_name` char(32) DEFAULT NULL,
  `first_name` char(32) DEFAULT NULL,
  `middle_name` char(32) DEFAULT NULL,
  `phone_1` char(24) DEFAULT NULL,
  `phone_2` char(24) DEFAULT NULL,
  `fax` char(24) DEFAULT NULL,
  `address_1` char(64) NOT NULL DEFAULT '',
  `address_2` char(64) DEFAULT NULL,
  `city` char(32) NOT NULL DEFAULT '',
  `virtuemart_state_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_country_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `zip` char(32) NOT NULL DEFAULT '',
  `agreed` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_userinfo_id`),
  KEY `idx_userinfo_virtuemart_user_id` (`virtuemart_userinfo_id`,`virtuemart_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Customer Information, BT = BillTo and ST = ShipTo' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_virtuemart_userinfos`
--

INSERT INTO `o1hap_virtuemart_userinfos` (`virtuemart_userinfo_id`, `virtuemart_user_id`, `address_type`, `address_type_name`, `name`, `company`, `title`, `last_name`, `first_name`, `middle_name`, `phone_1`, `phone_2`, `fax`, `address_1`, `address_2`, `city`, `virtuemart_state_id`, `virtuemart_country_id`, `zip`, `agreed`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 42, 'BT', '', 'Super User', '', '', '1', '1', '', '1', '', '', '1', '', '1', 0, 0, '1', 0, '2012-04-24 08:09:26', 42, '2012-04-24 08:09:26', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_vendors`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_vendors` (
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_name` char(64) DEFAULT NULL,
  `vendor_currency` int(11) DEFAULT NULL,
  `vendor_accepted_currencies` varchar(1024) NOT NULL DEFAULT '',
  `vendor_params` text,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_vendor_id`),
  KEY `idx_vendor_name` (`vendor_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Vendors manage their products in your store' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_virtuemart_vendors`
--

INSERT INTO `o1hap_virtuemart_vendors` (`virtuemart_vendor_id`, `vendor_name`, `vendor_currency`, `vendor_accepted_currencies`, `vendor_params`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(1, 'Euro-therm', 144, '199,144', 'vendor_min_pov="0"|vendor_min_poq=1|vendor_freeshipment=0|vendor_address_format=""|vendor_date_format=""|', '2012-04-24 08:09:26', 42, '2012-04-24 08:09:26', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_vendors_ru_ru`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_vendors_ru_ru` (
  `virtuemart_vendor_id` int(1) unsigned NOT NULL,
  `vendor_store_desc` text NOT NULL,
  `vendor_terms_of_service` text NOT NULL,
  `vendor_legal_info` text NOT NULL,
  `vendor_store_name` char(180) NOT NULL DEFAULT '',
  `vendor_phone` char(26) NOT NULL DEFAULT '',
  `vendor_url` char(255) NOT NULL DEFAULT '',
  `slug` char(192) NOT NULL DEFAULT '',
  PRIMARY KEY (`virtuemart_vendor_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_virtuemart_vendors_ru_ru`
--

INSERT INTO `o1hap_virtuemart_vendors_ru_ru` (`virtuemart_vendor_id`, `vendor_store_desc`, `vendor_terms_of_service`, `vendor_legal_info`, `vendor_store_name`, `vendor_phone`, `vendor_url`, `slug`) VALUES
(1, '', '', '', 'Euro-therm', '', '', 'euro-therm');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_vendor_medias`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_vendor_medias` (
  `id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_media_id` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`,`virtuemart_media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_vmusers`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_vmusers` (
  `virtuemart_user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  `user_is_vendor` tinyint(1) NOT NULL DEFAULT '0',
  `customer_number` char(32) DEFAULT NULL,
  `perms` char(40) NOT NULL DEFAULT 'shopper',
  `virtuemart_paymentmethod_id` mediumint(1) unsigned DEFAULT NULL,
  `virtuemart_shipmentmethod_id` mediumint(1) unsigned DEFAULT NULL,
  `agreed` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_user_id`),
  UNIQUE KEY `i_virtuemart_user_id` (`virtuemart_user_id`,`virtuemart_vendor_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Holds the unique user data' AUTO_INCREMENT=43 ;

--
-- Dumping data for table `o1hap_virtuemart_vmusers`
--

INSERT INTO `o1hap_virtuemart_vmusers` (`virtuemart_user_id`, `virtuemart_vendor_id`, `user_is_vendor`, `customer_number`, `perms`, `virtuemart_paymentmethod_id`, `virtuemart_shipmentmethod_id`, `agreed`, `created_on`, `created_by`, `modified_on`, `modified_by`, `locked_on`, `locked_by`) VALUES
(42, 1, 1, '21232f297a57a5a743894a0e4a801fc3', 'admin', NULL, NULL, 0, '0000-00-00 00:00:00', 0, '2012-04-24 08:09:26', 42, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_vmuser_shoppergroups`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_vmuser_shoppergroups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_user_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_shoppergroup_id` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_virtuemart_user_id` (`virtuemart_user_id`,`virtuemart_shoppergroup_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='xref table for users to shopper group' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_virtuemart_vmuser_shoppergroups`
--

INSERT INTO `o1hap_virtuemart_vmuser_shoppergroups` (`id`, `virtuemart_user_id`, `virtuemart_shoppergroup_id`) VALUES
(1, 42, 2);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_waitingusers`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_waitingusers` (
  `virtuemart_waitinguser_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(1) unsigned NOT NULL DEFAULT '0',
  `virtuemart_user_id` int(1) unsigned NOT NULL DEFAULT '0',
  `notify_email` char(150) NOT NULL DEFAULT '',
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `notify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ordering` int(2) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_waitinguser_id`),
  KEY `virtuemart_product_id` (`virtuemart_product_id`),
  KEY `notify_email` (`notify_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stores notifications, users waiting f. products out of stock' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_virtuemart_worldzones`
--

CREATE TABLE IF NOT EXISTS `o1hap_virtuemart_worldzones` (
  `virtuemart_worldzone_id` smallint(1) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_vendor_id` smallint(1) DEFAULT NULL,
  `zone_name` char(255) DEFAULT NULL,
  `zone_cost` decimal(10,2) DEFAULT NULL,
  `zone_limit` decimal(10,2) DEFAULT NULL,
  `zone_description` text,
  `zone_tax_rate` int(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(2) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`virtuemart_worldzone_id`),
  KEY `i_virtuemart_vendor_id` (`virtuemart_vendor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='The Zones managed by the Zone Shipment Module' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_weblinks`
--

CREATE TABLE IF NOT EXISTS `o1hap_weblinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_xmap_items`
--

CREATE TABLE IF NOT EXISTS `o1hap_xmap_items` (
  `uid` varchar(100) NOT NULL,
  `itemid` int(11) NOT NULL,
  `view` varchar(10) NOT NULL,
  `sitemap_id` int(11) NOT NULL,
  `properties` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`uid`,`itemid`,`view`,`sitemap_id`),
  KEY `uid` (`uid`,`itemid`),
  KEY `view` (`view`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_xmap_sitemap`
--

CREATE TABLE IF NOT EXISTS `o1hap_xmap_sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `introtext` text,
  `metadesc` text,
  `metakey` text,
  `attribs` text,
  `selections` text,
  `excluded_items` text,
  `is_default` int(1) DEFAULT '0',
  `state` int(2) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count_xml` int(11) DEFAULT NULL,
  `count_html` int(11) DEFAULT NULL,
  `views_xml` int(11) DEFAULT NULL,
  `views_html` int(11) DEFAULT NULL,
  `lastvisit_xml` int(11) DEFAULT NULL,
  `lastvisit_html` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `o1hap_xmap_sitemap`
--

INSERT INTO `o1hap_xmap_sitemap` (`id`, `title`, `alias`, `introtext`, `metadesc`, `metakey`, `attribs`, `selections`, `excluded_items`, `is_default`, `state`, `access`, `created`, `count_xml`, `count_html`, `views_xml`, `views_html`, `lastvisit_xml`, `lastvisit_html`) VALUES
(1, 'Карта сайта', 'karta-sajta', '', NULL, NULL, '{"showintro":"1","show_menutitle":"1","classname":"","columns":"","exlinks":"img_blue.gif","compress_xml":"1","beautify_xml":"1","news_publication_name":""}', '{"katalog":{"priority":"0.5","changefreq":"weekly","ordering":0},"mainmenu":{"priority":"0.5","changefreq":"weekly","ordering":1}}', NULL, 1, 1, 1, '2012-04-19 05:31:37', 13, 16, 1, 785, 1335796535, 1401033859);

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_application`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `application_group` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `o1hap_zoo_application`
--

INSERT INTO `o1hap_zoo_application` (`id`, `name`, `alias`, `application_group`, `description`, `params`) VALUES
(4, 'Каталог', 'каталог', 'product', '', ' {\n	"group": "product",\n	"template": "default",\n	"global.config.items_per_page": "15",\n	"global.config.item_order":  {\n		"0": "_itemname",\n		"1": "",\n		"2": "",\n		"3": "",\n		"4": "",\n		"5": "",\n		"6": "",\n		"7": "",\n		"8": ""\n	},\n	"global.config.alpha_index": "3",\n	"global.config.show_feed_link": "0",\n	"global.config.feed_title": "",\n	"global.config.alternate_feed_link": "",\n	"global.config.show_empty_categories": "0",\n	"global.template.show_alpha_index": "1",\n	"global.template.show_title": "1",\n	"global.template.show_description": "1",\n	"global.template.show_image": "1",\n	"global.template.alignment": "left",\n	"global.template.show_categories": "1",\n	"global.template.categories_cols": "4",\n	"global.template.show_categories_titles": "1",\n	"global.template.show_categories_item_count": "1",\n	"global.template.show_categories_descriptions": "1",\n	"global.template.show_categories_images": "1",\n	"global.template.show_sub_categories": "1",\n	"global.template.show_sub_categories_item_count": "1",\n	"global.template.items_cols": "2",\n	"global.template.items_media_alignment": "left",\n	"global.template.item_media_alignment": "right",\n	"global.comments.enable_comments": "1",\n	"global.comments.require_name_and_mail": "1",\n	"global.comments.registered_users_only": "0",\n	"global.comments.approved": "0",\n	"global.comments.time_between_user_posts": "120",\n	"global.comments.captcha": "",\n	"global.comments.email_notification": "",\n	"global.comments.email_reply_notification": "0",\n	"global.comments.avatar": "1",\n	"global.comments.order": "ASC",\n	"global.comments.max_depth": "5",\n	"global.comments.facebook_enable": "0",\n	"global.comments.facebook_app_id": "",\n	"global.comments.facebook_app_secret": "",\n	"global.comments.twitter_enable": "0",\n	"global.comments.twitter_consumer_key": "",\n	"global.comments.twitter_consumer_secret": "",\n	"global.comments.akismet_enable": "0",\n	"global.comments.akismet_api_key": "",\n	"global.comments.mollom_enable": "0",\n	"global.comments.mollom_public_key": "",\n	"global.comments.mollom_private_key": "",\n	"global.comments.blacklist": ""\n}');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_category`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`),
  KEY `PUBLISHED_INDEX` (`published`),
  KEY `APPLICATIONID_ID_INDEX` (`published`,`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `o1hap_zoo_category`
--

INSERT INTO `o1hap_zoo_category` (`id`, `application_id`, `name`, `alias`, `description`, `parent`, `ordering`, `published`, `params`) VALUES
(6, 4, 'Алюминиевые радиаторы', 'aluminium-radiator', '', 0, 1, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(7, 4, 'EUROTEM', 'eurotem', '', 5, 0, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(8, 4, 'Китай', 'china', '', 6, 1, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(9, 4, 'Италия', 'italy', '', 6, 0, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(10, 4, 'Биметаллические радиаторы', 'bimetallik-radiators', '', 0, 2, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(11, 4, 'Китай', 'china-2', '', 10, 0, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(12, 4, 'Металлопластиковые трубы', 'metallic-tubs', '', 0, 3, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(13, 4, 'Краны шаровые', 'ball-valves', '', 0, 4, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}'),
(5, 4, 'Стальные радиаторы', 'steel-radiators', '', 0, 0, 1, ' {\n	"content.teaser_description": "",\n	"content.teaser_image": "",\n	"content.teaser_image_width": "",\n	"content.teaser_image_height": "",\n	"content.image": "",\n	"content.image_width": "",\n	"content.image_height": "",\n	"content.categories_title": "",\n	"content.items_title": "",\n	"metadata.title": "",\n	"metadata.description": "",\n	"metadata.keywords": "",\n	"metadata.robots": "",\n	"metadata.author": ""\n}');

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_category_item`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_category_item` (
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`item_id`),
  KEY `ITEMID_INDEX` (`item_id`),
  KEY `CATEGORYID_INDEX` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_comment`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `content` text NOT NULL,
  `state` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `STATE_INDEX` (`state`),
  KEY `CREATED_INDEX` (`created`),
  KEY `ITEMID_INDEX` (`item_id`),
  KEY `AUTHOR_INDEX` (`author`),
  KEY `ITEMID_STATE_INDEX` (`item_id`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_item`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `priority` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `state` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `searchable` int(11) NOT NULL,
  `elements` longtext NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`),
  KEY `PUBLISH_INDEX` (`publish_up`,`publish_down`),
  KEY `STATE_INDEX` (`state`),
  KEY `ACCESS_INDEX` (`access`),
  KEY `CREATED_BY_INDEX` (`created_by`),
  KEY `NAME_INDEX` (`name`),
  KEY `APPLICATIONID_INDEX` (`application_id`),
  KEY `TYPE_INDEX` (`type`),
  KEY `MULTI_INDEX` (`application_id`,`access`,`state`,`publish_up`,`publish_down`),
  KEY `MULTI_INDEX2` (`id`,`access`,`state`,`publish_up`,`publish_down`),
  KEY `ID_APPLICATION_INDEX` (`id`,`application_id`),
  FULLTEXT KEY `SEARCH_FULLTEXT` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_rating`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `element_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `value` tinyint(4) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_search_index`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_search_index` (
  `item_id` int(11) NOT NULL,
  `element_id` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`item_id`,`element_id`),
  FULLTEXT KEY `SEARCH_FULLTEXT` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_submission`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_tag`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_tag` (
  `item_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`name`),
  UNIQUE KEY `NAME_ITEMID_INDEX` (`name`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `o1hap_zoo_version`
--

CREATE TABLE IF NOT EXISTS `o1hap_zoo_version` (
  `version` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `o1hap_zoo_version`
--

INSERT INTO `o1hap_zoo_version` (`version`) VALUES
('2.5.21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
