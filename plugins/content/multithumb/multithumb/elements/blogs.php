<?php
class JElementBlogs extends JElement
{
   var $_name = 'Blogs';
   var $options = array();

		// $parent is the parent of the children we want to see
		// $level is increased when we go deeper into the tree,
		//        used to display a nice indented tree
		function display_children($parent, $level ) {
				// retrieve all children of $parent
				$db =& JFactory::getDBO();

				$query = 'SELECT #__menu.id, #__menu.name as title, #__menu.parent, #__menu.link
					FROM #__menu, #__menu_types
								WHERE parent='.$parent.'
										  AND #__menu.menutype = #__menu_types.menutype
										  AND #__menu.published=1 
												ORDER BY #__menu_types.id, ordering, parent;';

				$result = $db->setQuery( $query );

				// $db->setQuery($query);
				// $options = $db->loadObjectList();
				$rows = $db->loadObjectList();

		   // display each child
		   foreach ($rows as $row) {
						$row->title = str_repeat('-',$level).$row->title;
						if ( strpos( $row->link, "layout=blog") !== false or
							 strpos( $row->link, "view=frontpage") !== false ) {
								$this->options[] = $row;
						}

						// call this function again to display this
						// child's children
						$this->display_children($row->id, $level+1);
		   }
		}

   function fetchElement($name, $value, &$node, $control_name)
   {
	  $db =& JFactory::getDBO();
		  $this->display_children(0, 0);

	  $size = ( $node->attributes('size') ? $node->attributes('size') : 5 );
		  // echo print_r( $this->options );

	  return JHTML::_('select.genericlist',  $this->options, ''.$control_name.'['.$name.'][]',  ' multiple="multiple" size="' . $size . '" class="inputbox"', 'id', 'title', $value, $control_name.$name);
   }
}
?>