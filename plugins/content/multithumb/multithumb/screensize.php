<html>
<head>
<script type="text/javascript">
function resolution() {
   var myWidth = 0, myHeight = 0;
   if( typeof( window.innerWidth ) == 'number' ) {
	 //Non-IE
	 myWidth = window.innerWidth;
	 myHeight = window.innerHeight;
   } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
	 //IE 6+ in 'standards compliant mode'
	 myWidth = document.documentElement.clientWidth;
	 myHeight = document.documentElement.clientHeight;
   } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
	 //IE 4 compatible
	 myWidth = document.body.clientWidth;
	 myHeight = document.body.clientHeight;
   }
   var url = window.location.href;
   if (url.indexOf("?") == "-1"){
	  window.location.href=window.location.href+"?height="+myHeight+"&width="+myWidth;
   }
   else {
	  window.location.href=window.location.href+"&height="+myHeight+"&width="+myWidth;
   }
}
</script>
</head>
<?php
if(!isset($_GET['width']) and !isset($_GET['height'])){
  echo '<body onload="resolution();">';
}else{
  echo '<body>';
  $screen_width = $_GET['width'];
  $screen_height = $_GET['height'];
}
?>
</body>
</html>